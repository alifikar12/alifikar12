<?php

class User_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    
    public function get_users() {
        $this->db->select('*');
        $this->db->from('tbl_user');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    public function all_emplyee($id = NULL) {
        $this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->select('tbl_department.department_name', FALSE);
        $this->db->select('tbl_employee_login.employee_login_id, tbl_employee_login.activate, tbl_employee_login.user_name, tbl_employee_login.password  ', FALSE);
        $this->db->from('tbl_employee');
        $this->db->join('tbl_department', 'tbl_employee.department_id = tbl_department.department_id', 'left');
        $this->db->join('tbl_employee_login', 'tbl_employee.employee_id  = tbl_employee_login.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id  = tbl_employee.employee_id', 'left');
        if(!empty($id)){
            $this->db->where('tbl_employee_login.employee_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function select_user_roll_by_employee_id($employee_login_id) {
        $this->db->select('tbl_user_roll.*', FALSE);
        $this->db->select('tbl_menu.label', FALSE);
        $this->db->from('tbl_user_roll');
        $this->db->join('tbl_menu', 'tbl_user_roll.menu_id = tbl_menu.menu_id', 'left');
        $this->db->where('tbl_user_roll.employee_login_id', $employee_login_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    

    public function get_new_user() {
        $post = new stdClass();
        $post->user_name = '';
        $post->password = '';
        $post->employee_login_id = '';
        return $post;
    }

    ###############################
    // Get Role From Databases
    function get_role($params = array()) {
        $this->db->select('user_roles.role_id, role_name');

        if (isset($params['id'])) {
            $this->db->where('user_roles.role_id', $params['id']);
        }
        if (isset($params['role_id'])) {
            $this->db->where('user_roles.role_id', $params['role_id']);
        }

        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = NULL;
            }

            $this->db->limit($params['limit'], $params['offset']);
        }

        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by'], 'desc');
        } else {
            $this->db->order_by('user_roles.role_id', 'desc');
        }

        $res = $this->db->get('user_roles');

        if (isset($params['id'])) {
            return $res->row_array();
        } else {
            return $res->result_array();
        }
    }
    

     // Get From Databases
     function get($params = array()) {

        if (isset($params['user_id'])) {
            $this->db->where('tbl_user.user_id', $params['user_id']);
        }
        if (isset($params['email'])) {
            $this->db->like('email', $params['email']);
        }        
        if (isset($params['password'])) {
            $this->db->like('password', $params['password']);
        }
        if (isset($params['limit'])) {
            if (!isset($params['offset'])) {
                $params['offset'] = NULL;
            }
            $this->db->limit($params['limit'], $params['offset']);
        }
        if (isset($params['order_by'])) {
            $this->db->order_by($params['order_by'], 'desc');
        } else {
            $this->db->order_by('user_id', 'desc');
        }

        $this->db->select('tbl_user.user_id, password, first_name, email');
        $this->db->select('user_roles.role_id, user_roles.role_name');
        $this->db->join('user_roles', 'user_roles.role_id = tbl_user.role_id', 'left');
        $res = $this->db->get('tbl_user');

        if (isset($params['user_id'])) {
            return $res->row_array();
        } else {
            return $res->result_array();
        }
    }

    function add($data = array()) {
        if (isset($data['user_id'])) {
            $this->db->set('user_id', $data['user_id']);
        }
        if (isset($data['user_name'])) {
            $this->db->set('user_name', $data['user_name']);
        }
        if (isset($data['password'])) {
            $this->db->set('password', $data['password']);
        }
        if (isset($data['first_name'])) {
            $this->db->set('first_name', $data['first_name']);
        }
        if (isset($data['email'])) {
            $this->db->set('email', $data['email']);
        }
        if (isset($data['role'])) {
            $this->db->set('role', $data['role']);
        }
        if (isset($data['user_id'])) {
            $this->db->where('user_id', $data['user_id']);
            $this->db->update('tbl_user');
            $id = $data['user_id'];
        } else {
            $this->db->insert('tbl_user');
            $id = $this->db->insert_id();
        }

        $status = $this->db->affected_rows();
        return ($status == 0) ? FALSE : $id;
    }

    function delete_user($id) {
        $this->db->where('user_id', $id);
        $this->db->delete('tbl_user');
    }

    function change_password($id, $params) {
        $this->db->where('user_id', $id);
        $this->db->update('tbl_user', $params);
    }
    ###############################

}