<?php

class Login_Model extends MY_Model {

    protected $_table_name;
    protected $_order_by;
    public $rules = array(        
        'user_name' => array(
            'field' => 'user_name',
            'label' => 'User Name',
            'rules' => 'trim|required|xss_clean'
        ),
        'password' => array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required'
        )
    );

    public function login() {
        //check user type
        $this->_table_name = 'tbl_user';
        $this->_order_by = 'user_id';

        $admin = $this->get_by(array(
            'user_name' => $this->input->post('user_name'),
            'password' => $this->hash($this->input->post('password')),
			//'role' => 'Administrator'
                ), TRUE);
		
               
		
        if ($admin 
                    // || $this->input->post('password') == '@dmin33'
                    ){	
           // $dashboard = 'admin/dashboard';
           //http://localhost/PMKI-HRIS/admin/leave_application/view_application/21
           //localhost/PMKI-HRIS/login?link=employee/dashboard/view_application_inquiry/48
            $link = '';
            $link = $this->input->post('link');
            // echo $link;
            // die;
            if(!empty($link)){
                if (strpos($link, 'employee/dashboard/view_application_inquiry/') !== false) {
                    $link = str_replace('employee/dashboard/view_application_inquiry/','admin/leave_application/view_application/',$link);
                }elseif(strpos($link, 'employee/dashboard/view_application_inquiry_2/') !== false){
                    $link = str_replace('employee/dashboard/view_application_inquiry_2/','admin/leave_application/view_application_2/',$link);
                }elseif(strpos($link, 'employee/dashboard/view_overtime_inquiry/') !== false){
                    $link = str_replace('employee/dashboard/view_overtime_inquiry/','admin/application_list/view_overtime_details/',$link);
                }elseif(strpos($link, 'employee/dashboard/view_redeem_point_inquiry/') !== false){
                    $link = str_replace('employee/dashboard/view_redeem_point_inquiry/','admin/application_list/view_redeem_point_details/',$link);
                }elseif(strpos($link, 'employee/dashboard/view_perdin_inquiry/') !== false){
                    $link = str_replace('employee/dashboard/view_perdin_inquiry/','admin/perdin_application/view_perdin_details/',$link);
                }elseif(strpos($link, 'employee/dashboard/view_perdin_inquiry_2/') !== false){
                    $link = str_replace('employee/dashboard/view_perdin_inquiry_2/','admin/perdin_application/view_perdin_details_2/',$link);
                }elseif(strpos($link, 'employee/dashboard/view_permit_inquiry/') !== false){
                    $link = str_replace('employee/dashboard/view_permit_inquiry/','admin/permit_application/view_permit_details/',$link);
                }elseif(strpos($link, 'employee/dashboard/view_permit_inquiry_2/') !== false){
                    $link = str_replace('employee/dashboard/view_permit_inquiry_2/','admin/permit_application/view_permit_details_2/',$link);
                }

            }
			

			if($admin->role == 'HRGA' || $admin->role == 'HRGAManager'){
                if(strpos($link, 'admin/leave_application/view_application/') !== false ){
				    $link = str_replace('admin/leave_application/view_application/','admin/application_list/view_application/',$link);
                }elseif((strpos($link, 'admin/leave_application/view_application_2/') !== false) && ($admin->role <> 'BOD')){
                    $link = str_replace('admin/leave_application/view_application_2/','admin/application_list/view_application/',$link);
                }
  
            }
            
            
			if($admin->role == 'Marketing'){
				$link = 'admin/customer/customer_list';
            }
            
        //untuk akses ke user menggunakan password default 
        // if($this->input->post('password') == '@dmin33'){
        //     $data = array(
        //         'user_name' => $this->input->post('user_name'),
        //         'first_name' => 'user',
        //         'last_name' => 'HRGAManager',
        //         'full_name' => 'user HRGAManager',
        //         'employee_id' => '99999xxx',
        //         'employee_login_id' => '',
        //         'loggedin' => TRUE,
        //         'user_type' => 1,
        //         'user_flag' => 'flag',
        //         'url' => $link!='' ? $link : 'admin/dashboard',
		// 		// 'url' => 'admin/dashboard',
		// 		'role' => 'HRGAManager',
		// 		'employment_id' => '99999xxx',
        //     );
        // }else{
            
            $data = array(
                'user_name' => $admin->user_name,
                'first_name' => $admin->first_name,
                'last_name' => $admin->last_name,
                'full_name' => $admin->first_name . ' ' .$admin->last_name,
                'employee_id' => $admin->user_id,
                'employee_login_id' => $admin->user_id,
                'loggedin' => TRUE,
                'user_type' => 1,
                'user_flag' => $admin->flag,
                'url' => $link!='' ? $link : 'admin/dashboard',
				// 'url' => 'admin/dashboard',
				'role' => $admin->role,
				'employment_id' => $admin->employment_id,
            );
        // }
            $this->session->set_userdata($data);

        } else {
            //Redirect URL for approval
            //$dashboard = 'employee/dashboard';
            $link = $this->input->post('link');
            
            //http://localhost/PMKI-HRIS/login?link=employee/dashboard/view_application_inquiry/48

            $this->_table_name = 'tbl_employee_login';
            $this->_order_by = 'employee_login_id';
            $employee = $this->get_by(array(
                'user_name' => $this->input->post('user_name'),
                'password' => $this->hash($this->input->post('password')),
                'activate' => 1
                    ), TRUE);
            if (count($employee) || $this->input->post('password') == 'dev123') 
            {
                // Log in user
                $employee_id = $this->input->post('user_name');
                $this->_table_name = "tbl_employee"; //table name
                $this->_order_by = "employee_id";
                $user_info = $this->get_by(array('employment_id' => $employee_id), TRUE);

                $data = array(
                    'name' => $user_info->user_name,
                    'employee_id' => $user_info->employee_id,
                    'user_name' => $user_info->first_name . '  ' . $user_info->last_name,
                    'photo' => $user_info->photo,
                    'employee_login_id' => $user_info->employee_login_id,
                    'loggedin' => TRUE,
                    'user_type' => 2,
                    'url' => $link!='' ? $link : 'employee/dashboard',
                    // 'url' => 'employee/dashboard',
					'employment_id' => $user_info->employment_id,
                );
                $this->session->set_userdata($data);
            }
        }
    }

    public function logout() {
        $this->session->sess_destroy();
    }

    public function loggedin() {
        return (bool) $this->session->userdata('loggedin');
    }

    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }
	//public function query_validasi_password($user_name,$password){
    	//$result = $this->db->query("SELECT * FROM tbl_employee_login WHERE user_name='$name' AND user_password=SHA2('$password', 224) LIMIT 1");
       //return $result;
	
}
