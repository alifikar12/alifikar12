<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of emp_model
 *
 * @author nayem
 */
class Emp_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

   
    public function get_data_numbering($doc_type) {
        $this->db->select('tbl_numbering.*', FALSE);
        $this->db->from('tbl_numbering');
        $this->db->where('tbl_numbering.doc_type', $doc_type);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }


    public function get_saldo_point($id) {
        $this->db->select('tbl_employee_point.*', FALSE);
        $this->db->from('tbl_employee_point');
        $this->db->where('tbl_employee_point.employee_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function get_saldo_cuti($id) {
        $this->db->select('tbl_saldo_cuti.*', FALSE);
        $this->db->from('tbl_saldo_cuti');
        $this->db->where('tbl_saldo_cuti.employee_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function all_emplyee_info($id = NULL) {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_employee_bank.*', FALSE);
        $this->db->select('tbl_employee_document.*', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_department.department_name', FALSE);
        $this->db->select('countries.countryName', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->select('tbl_employee_point.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->join('tbl_employee_bank', 'tbl_employee_bank.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_employee_document', 'tbl_employee_document.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_designations.department_id', 'left');
        $this->db->join('countries', 'countries.idCountry  = tbl_employee.country_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id  = tbl_employee.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_employee_point', 'tbl_employee_point.employee_id  = tbl_employee.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_employee.employee_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        if (!empty($id)) {
            $this->db->select('tbl_employee.nationality', FALSE);
            $this->db->select('countries.countryName', FALSE);
            $this->db->from('tbl_employee');
            $this->db->join('countries', 'countries.idCountry  =  tbl_employee.nationality ', 'left');
            $query_nationality = $this->db->get();
            $nationality = $query_nationality->row();
            if (!empty($nationality)) {
                $result->nationality = $nationality->countryName;
            }
        }

        return $result;
    }

    public function get_all_notice($limit = NULL) {
        $this->db->select('tbl_notice.*', FALSE);
        $this->db->from('tbl_notice');
        if (!empty($limit)) {
            $this->db->limit('5');
        }
        $this->db->where('flag', 1);
        $this->db->order_by("notice_id", "desc");
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_all_events() {
        $this->db->select('*');
        $this->db->from('tbl_holiday');
        // $this->db->limit('7');
        $this->db->where('YEAR(start_date) = YEAR(NOW())');
        $this->db->order_by("start_date", "asc");
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_all_awards($id = NULL) {
        $this->db->select('tbl_employee_award.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_department.department_name', FALSE);
        $this->db->from('tbl_employee_award');
        $this->db->join('tbl_employee', 'tbl_employee_award.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_designations.department_id', 'left');
        $this->db->order_by("award_date", "desc");
        if (!empty($id)) {
            $this->db->where('tbl_employee_award.employee_award_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }

        return $result;
    }

//    public function get_employee_award_info($id) {
//        $this->db->select('tbl_employee_award.*', FALSE);
//        $this->db->select('tbl_employee.*', FALSE);
//        $this->db->select('tbl_designations.*', FALSE);
//        $this->db->select('tbl_department.department_name', FALSE);
//        $this->db->from('tbl_employee_award');
//        $this->db->join('tbl_employee', 'tbl_employee_award.employee_id  = tbl_employee.employee_id', 'left');
//        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
//        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_designations.department_id', 'left');
//        $this->db->where('tbl_employee_award.employee_award_id', $id);
//        $query_result = $this->db->get();
//        $result = $query_result->row();
//        return $result;
//    }

    public function get_trans_point_applied_by_user($employee_id) {

        // $result = $this->db->query("SELECT doc_number, SUM(price) AS total_redeem, `status`, submit_date FROM `pmki_hris`.`tbl_transaksi_point` GROUP BY doc_number WHERE tbl_transaksi_point.employee_id = '$employee_id'");
		// return $result;

        // GROUP BY doc_number ");
        $this->db->select('doc_number, SUM(subtotal) AS total_redeem, `status`, submit_date', FALSE);
        $this->db->from('tbl_transaksi_point');
        $this->db->group_by('doc_number');
        $this->db->where('tbl_transaksi_point.employee_id', $employee_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_leave_applied_by_user($employee_id) {
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id  = tbl_application_list.leave_category_id');
		$this->db->join('tbl_employee', 'tbl_employee.employment_id  = tbl_application_list.employment_id');
        $this->db->where('tbl_application_list.employee_id', $employee_id);
        $this->db->order_by('tbl_application_list.application_list_id', 'DESC');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_permit_applied_by_user($employee_id) {
        $this->db->select('tbl_permit_list.*', FALSE);
        $this->db->select('tbl_permit_category.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_permit_list');
        $this->db->join('tbl_permit_category', 'tbl_permit_category.permit_category_id  = tbl_permit_list.permit_category_id');
		$this->db->join('tbl_employee', 'tbl_employee.employee_id  = tbl_permit_list.employee_id');
        $this->db->where('tbl_permit_list.employee_id', $employee_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_perdin_applied_by_user($employee_id) {
        $this->db->select('tbl_perdin_list.*', FALSE);
        $this->db->select('tbl_perdin_category.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_perdin_list');
        $this->db->join('tbl_perdin_category', 'tbl_perdin_category.perdin_category_id  = tbl_perdin_list.perdin_category_id');
		$this->db->join('tbl_employee', 'tbl_employee.employee_id  = tbl_perdin_list.employee_id');
        $this->db->where('tbl_perdin_list.employee_id', $employee_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_overtime_applied_by_user($employee_id) {
        $this->db->select('tbl_overtime_list.*', FALSE);
        $this->db->select('tbl_perlengkapan_lembur.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_overtime_list');
        $this->db->join('tbl_perlengkapan_lembur', 'tbl_perlengkapan_lembur.perlengkapan_id  = tbl_overtime_list.perlengkapan_id');
		$this->db->join('tbl_employee', 'tbl_employee.employee_id  = tbl_overtime_list.employee_id');
        $this->db->where('tbl_overtime_list.employee_id', $employee_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_all_leave_applied_for_approved($employment_id) {
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id  = tbl_application_list.leave_category_id');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id  = tbl_application_list.employment_id');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employment_id  = tbl_employee.employment_id', 'left');
        $this->db->where('tbl_employee_matrix_approval.direct_leader_id', $employment_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_all_leave_applied_for_approved_2($employment_id) {
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id  = tbl_application_list.leave_category_id');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id  = tbl_application_list.employment_id');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employment_id  = tbl_employee.employment_id', 'left');
        $this->db->where('tbl_employee_matrix_approval.direct_leader_id_2', $employment_id);
        //$this->db->where('tbl_application_list.leave_status', 'partial approved');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_all_permit_applied_for_approved($employment_id) {		
        $this->db->select('tbl_permit_list.*', FALSE);
        $this->db->select('tbl_permit_category.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_permit_list');
        $this->db->join('tbl_permit_category', 'tbl_permit_category.permit_category_id  = tbl_permit_list.permit_category_id');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id  = tbl_permit_list.employment_id');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employment_id  = tbl_employee.employment_id', 'left');
        $this->db->where('tbl_employee_matrix_approval.direct_leader_id', $employment_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_all_permit_applied_for_approved_2($employment_id) {		
        $this->db->select('tbl_permit_list.*', FALSE);
        $this->db->select('tbl_permit_category.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_permit_list');
        $this->db->join('tbl_permit_category', 'tbl_permit_category.permit_category_id  = tbl_permit_list.permit_category_id');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id  = tbl_permit_list.employment_id');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employment_id  = tbl_employee.employment_id', 'left');
        $this->db->where('tbl_employee_matrix_approval.direct_leader_id_2', $employment_id);
        // $this->db->where('tbl_permit_list.permit_status <> "pending"');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_all_perdin_applied_for_approved($employment_id) {		
        $this->db->select('tbl_perdin_list.*', FALSE);
        $this->db->select('tbl_perdin_category.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_perdin_list');
        $this->db->join('tbl_perdin_category', 'tbl_perdin_category.perdin_category_id  = tbl_perdin_list.perdin_category_id');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id  = tbl_perdin_list.employment_id');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employment_id  = tbl_employee.employment_id', 'left');
        $this->db->where('tbl_employee_matrix_approval.direct_leader_id', $employment_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    // public function get_emp_leave_info_2($id = NULL, $employment_id = NULL) {
    //     $this->db->select('tbl_application_list.*', FALSE);
    //     $this->db->select('tbl_leave_category.*', FALSE);
    //     $this->db->select('tbl_employee.*', FALSE);
    //     $this->db->select('tbl_saldo_cuti.*', FALSE);
	// 	$this->db->select('tbl_employee_matrix_approval.*', FALSE);
    //     $this->db->from('tbl_application_list');
    //     $this->db->join('tbl_employee', 'tbl_employee.employment_id = tbl_application_list.employment_id', 'left');
    //     $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id = tbl_application_list.leave_category_id', 'left');
    //     $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_application_list.employee_id', 'left');
	// 	$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_application_list.employee_id', 'left');
    //     if (!empty($id)) {
    //         $this->db->where('tbl_application_list.application_list_id', $id);
    //         //$this->db->where('tbl_application_list.leave_status', 'partial approved');
    //         $query_result = $this->db->get();
    //         $result = $query_result->row();
    //     } else {
    //         $this->db->where('tbl_employee_matrix_approval.direct_leader_id_2', $employment_id);
    //         $query_result = $this->db->get();
    //         $result = $query_result->result();
    //     }
    //     return $result;
    // }

    public function get_all_perdin_applied_for_approved_2($employment_id) {		
        $this->db->select('tbl_perdin_list.*', FALSE);
        $this->db->select('tbl_perdin_category.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_perdin_list');
        $this->db->join('tbl_perdin_category', 'tbl_perdin_category.perdin_category_id  = tbl_perdin_list.perdin_category_id');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id  = tbl_perdin_list.employment_id');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employment_id  = tbl_employee.employment_id', 'left');
        $this->db->where('tbl_employee_matrix_approval.direct_leader_id_2', $employment_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_all_overtime_applied_for_approved($employment_id) {		
        $this->db->select('tbl_overtime_list.*', FALSE);
        $this->db->select('tbl_perlengkapan_lembur.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_overtime_list');
        $this->db->join('tbl_perlengkapan_lembur', 'tbl_perlengkapan_lembur.perlengkapan_id  = tbl_overtime_list.perlengkapan_id');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id  = tbl_overtime_list.employment_id');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employment_id  = tbl_employee.employment_id', 'left');
        $this->db->where('tbl_employee_matrix_approval.direct_leader_id', $employment_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_approved_leave($employee_id) {
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id  = tbl_application_list.leave_category_id', 'left');
        $this->db->where('tbl_application_list.employee_id', $employee_id);
        $this->db->where('tbl_application_list.leave_status', 'fully approved');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    // public function get_total_attendace_by_date($start_date, $end_date, $employee_id) {
    //     $this->db->select('tbl_attendance.*', FALSE);
    //     $this->db->from('tbl_attendance');
    //     $this->db->where('employee_id', $employee_id);
    //     $this->db->where('date >=', $start_date);
    //     $this->db->where('date <=', $end_date);
    //     $query_result = $this->db->get();
    //     $result = $query_result->result();
    //     return $result;
    // }


    public function get_total_attendace_by_date($start_date, $end_date, $employee_id) {
        $this->db->select('DISTINCT(finger_att_logs.Date)', FALSE);        
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->from('finger_att_logs');
        $this->db->join('tbl_employee', 'tbl_employee.finger_id  = finger_att_logs.PIN', 'left');
        $this->db->where('tbl_employee.employee_id', $employee_id);
        $this->db->where('Date >=', $start_date);
        $this->db->where('Date <=', $end_date);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    

    public function get_public_holiday($start_date, $end_date) {
        $this->db->select('tbl_holiday.*', FALSE);
        $this->db->from('tbl_holiday');        
        $this->db->where('start_date >=', $start_date);
        $this->db->where('end_date <=', $end_date);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_emp_redeem_info($id = NULL) {
        $this->db->select('tbl_transaksi_point.*, tbl_transaksi_point.`status` AS status_redeem', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_employee_point.*', FALSE);
		// $this->db->select('tbl_employee_matrix_approval_redeem.*', FALSE);
        $this->db->from('tbl_transaksi_point');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id = tbl_transaksi_point.employment_id', 'left');
        $this->db->join('tbl_employee_point', 'tbl_employee_point.employee_id = tbl_transaksi_point.employee_id', 'left');
		// $this->db->join('tbl_employee_matrix_approval_redeem', 'tbl_employee_matrix_approval_redeem.employee_id = tbl_transaksi_point.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_transaksi_point.doc_number', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function get_emp_redeem_info_rows($id = NULL) {
        $this->db->select('tbl_transaksi_point.*, tbl_transaksi_point.`status` AS status_redeem', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_employee_point.*', FALSE);
		// $this->db->select('tbl_employee_matrix_approval_redeem.*', FALSE);
        $this->db->from('tbl_transaksi_point');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id = tbl_transaksi_point.employment_id', 'left');
        $this->db->join('tbl_employee_point', 'tbl_employee_point.employee_id = tbl_transaksi_point.employee_id', 'left');
		// $this->db->join('tbl_employee_matrix_approval_redeem', 'tbl_employee_matrix_approval_redeem.employee_id = tbl_transaksi_point.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_transaksi_point.doc_number', $id);
            $query_result = $this->db->get();
            $result = $query_result->result();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function get_emp_leave_info($id = NULL) {
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id = tbl_application_list.employment_id', 'left');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id = tbl_application_list.leave_category_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_application_list.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_application_list.application_list_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function get_emp_leave_info_2($id = NULL) {
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id = tbl_application_list.employment_id', 'left');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id = tbl_application_list.leave_category_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_application_list.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_application_list.application_list_id', $id);
            //$this->db->where('tbl_application_list.leave_status', 'partial approved');
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function get_emp_permit_info($id = NULL) {
        $this->db->select('tbl_permit_list.*', FALSE);
        $this->db->select('tbl_permit_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_permit_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_permit_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_permit_category', 'tbl_permit_category.permit_category_id = tbl_permit_list.permit_category_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_permit_list.permit_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function get_emp_perdin_info($id = NULL) {
        $this->db->select('tbl_perdin_list.*', FALSE);
        $this->db->select('tbl_perdin_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_perdin_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_perdin_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_perdin_category', 'tbl_perdin_category.perdin_category_id = tbl_perdin_list.perdin_category_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_perdin_list.perdin_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function get_emp_overtime_info($id = NULL) {
        $this->db->select('tbl_overtime_list.*', FALSE);
        $this->db->select('tbl_perlengkapan_lembur.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_overtime_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_overtime_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_perlengkapan_lembur', 'tbl_perlengkapan_lembur.perlengkapan_id = tbl_overtime_list.perlengkapan_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_overtime_list.overtime_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function get_no_leave(){
        //ambil nomor leave from auto increment
        $this->db->select('AUTO_INCREMENT AS no_leave');
        $this->db->from('information_schema.TABLES');
        $this->db->where('TABLE_SCHEMA', "pmki_hris");
        $this->db->where('TABLE_NAME', "tbl_application_list");
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function get_no_permit(){
        //ambil nomor permit from auto increment
        $this->db->select('AUTO_INCREMENT AS no_permit');
        $this->db->from('information_schema.TABLES');
        $this->db->where('TABLE_SCHEMA', "pmki_hris");
        $this->db->where('TABLE_NAME', "tbl_permit_list");
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    } 

    public function get_no_perdin(){
        $this->db->select('AUTO_INCREMENT AS no_perdin');
        $this->db->from('information_schema.TABLES');
        $this->db->where('TABLE_SCHEMA', "pmki_hris");
        $this->db->where('TABLE_NAME', "tbl_perdin_list");
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    } 

    public function get_no_overtime(){
        //ambil nomor overtime from auto increment
        $this->db->select('AUTO_INCREMENT AS no_overtime');
        $this->db->from('information_schema.TABLES');
        $this->db->where('TABLE_SCHEMA', "pmki_hris");
        $this->db->where('TABLE_NAME', "tbl_overtime_list");
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    } 


}
