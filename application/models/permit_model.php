<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of application_model
 *
 * @author NaYeM
 */
class Permit_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;


    public function all_emplyee_info_for_select_opt() {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_employee_bank.*', FALSE);
        $this->db->select('tbl_employee_document.*', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_department.department_name', FALSE);
        $this->db->select('countries.countryName', FALSE);
		$this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->join('tbl_employee_bank', 'tbl_employee_bank.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_employee_document', 'tbl_employee_document.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_designations.department_id', 'left');
        $this->db->join('countries', 'countries.idCountry  = tbl_employee.country_id', 'left');
		$this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id  = tbl_employee.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id  = tbl_employee.employee_id', 'left');
       
        $query_result = $this->db->get();
        $result = $query_result->result();
    
        return $result;
    }
    


    public function all_emplyee_info($id = NULL) {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_employee_bank.*', FALSE);
        $this->db->select('tbl_employee_document.*', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_department.department_name', FALSE);
        $this->db->select('countries.countryName', FALSE);
		$this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->join('tbl_employee_bank', 'tbl_employee_bank.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_employee_document', 'tbl_employee_document.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_designations.department_id', 'left');
        $this->db->join('countries', 'countries.idCountry  = tbl_employee.country_id', 'left');
		$this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id  = tbl_employee.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id  = tbl_employee.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_employee.employment_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        if (!empty($id)) {
            $this->db->select('tbl_employee.nationality', FALSE);
            $this->db->select('countries.countryName', FALSE);
            $this->db->from('tbl_employee');
            $this->db->join('countries', 'countries.idCountry  =  tbl_employee.nationality ', 'left');
            $query_nationality = $this->db->get();
            $nationality = $query_nationality->row();
            if (!empty($nationality)) {
                $result->nationality = $nationality->countryName;
            }
        }

        return $result;
    }

    public function get_my_leave_info($employment_id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);		
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id  = tbl_application_list.leave_category_id');
		$this->db->join('tbl_employee', 'tbl_employee.employment_id  = tbl_application_list.employment_id');
        $this->db->where('tbl_application_list.employment_id', $employment_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_emp_leave_info($id = NULL, $employment_id = NULL) {
				
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
		$this->db->select('tbl_saldo_cuti.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_application_list.employee_id', 'left');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id = tbl_application_list.leave_category_id', 'left');
        $this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_application_list.employee_id', 'left');
		if (!empty($id)) {
            $this->db->where('tbl_application_list.application_list_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            
            $this->db->where('tbl_employee_matrix_approval.direct_leader_id', $employment_id);
            // $this->db->where('tbl_application_list.approve1 = ""')->or_where('tbl_application_list.approve2 =""');
            $query_result = $this->db->get();
            $result = $query_result->result();
            
        }
		
        return $result;
    }


    public function get_emp_perdin_info_2($id = NULL, $employment_id = NULL) {
        $this->db->select('tbl_perdin_list.*', FALSE);
        $this->db->select('tbl_perdin_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_perdin_list');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id = tbl_perdin_list.employment_id', 'left');
        $this->db->join('tbl_perdin_category', 'tbl_perdin_category.perdin_category_id = tbl_perdin_list.perdin_category_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_perdin_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_perdin_list.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_perdin_list.perdin_list_id', $id);
            //$this->db->where('tbl_application_list.leave_status', 'partial approved');
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $this->db->where('tbl_employee_matrix_approval.direct_leader_id_2', $employment_id);
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function get_emp_permit_info_2($id = NULL, $employment_id = NULL) {
        $this->db->select('tbl_permit_list.*', FALSE);
        $this->db->select('tbl_permit_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_permit_list');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id = tbl_permit_list.employment_id', 'left');
        $this->db->join('tbl_permit_category', 'tbl_permit_category.permit_category_id = tbl_permit_list.permit_category_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_permit_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_permit_list.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_permit_list.permit_id', $id);
            //$this->db->where('tbl_application_list.leave_status', 'partial approved');
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $this->db->where('tbl_employee_matrix_approval.direct_leader_id_2', $employment_id);
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }


    public function get_emp_leave_info_2($id = NULL, $employment_id = NULL) {
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id = tbl_application_list.employment_id', 'left');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id = tbl_application_list.leave_category_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_application_list.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_application_list.application_list_id', $id);
            //$this->db->where('tbl_application_list.leave_status', 'partial approved');
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $this->db->where('tbl_employee_matrix_approval.direct_leader_id_2', $employment_id);
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }
	
	public function cek_saldo_cuti($employee_id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_employee');  
        $this->db->where('employee_id', $employee_id); 	
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
	
	public function get_application_info($application_list_id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
		$this->db->select('tbl_application_list.leave_status', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_application_list');  
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_employee.employee_id', 'left');
		$this->db->where('application_list_id', $application_list_id); 	
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function get_permit_by_status($status){
        $this->db->select('tbl_permit_category.*', FALSE);
        $this->db->from('tbl_permit_category');
        $this->db->where('status', $status); 	
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;

    }

}
