<?php

class Navigation_Model extends MY_Model
{
    public $_table_name;
    public $_order_by;
    public $_primary_key;
    
    public function get_fontawesome(){
        $this->db->select('fontawesome.*', TRUE);
        $this->db->from('fontawesome');   
        $this->db->where('code <> ""');  
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    } 

    public function get_last_id_menu(){
        //ambil id terakhir tbl_menu from auto increment
        $this->db->select('AUTO_INCREMENT AS menu_id');
        $this->db->from('information_schema.TABLES');
        $this->db->where('TABLE_SCHEMA', "pmki_hris");
        $this->db->where('TABLE_NAME', "tbl_menu");
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    } 

    public function get_new_menuInfo() {

        $post = new stdClass();
        $post->label            = '';
        $post->link             = '';
        $post->icon             = '';        
        $post->parent_id        = '';
        $post->parent           = '';
        $post->sort             = '';
        $post->menu_id          = '';
        $post->English          = '';
        $post->Administrator    = '';
        $post->BOD              = '';
        $post->IT               = '';
        $post->HRGA             = '';
        $post->HRGAManager      = '';
        $post->Payroll          = '';
        $post->Marketing        = '';
        
        return $post;
    }
    
}