<?php

/**
 * Description of employee_model
 *
 * @author NaYeM
 */
class Employee_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;


    public function get_add_department_by_id($department_id) {
        $this->db->select('tbl_department.*', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->from('tbl_department');
        $this->db->join('tbl_designations', 'tbl_department.department_id = tbl_designations.department_id', 'left');
        $this->db->where('tbl_department.department_id', $department_id);

        $query_result = $this->db->get();
        $result = $query_result->result();

        return $result;
    }

    public function all_emplyee_info($id = NULL) {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_employee_bank.*', FALSE);
        $this->db->select('tbl_employee_document.*', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_department.department_name', FALSE);
        $this->db->select('countries.countryName', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->select('tbl_employee_ess_setup.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
        $this->db->select('tbl_employee_point.*', FALSE);
        $this->db->select('company.name AS company_name', FALSE);
        $this->db->from('tbl_employee');
        $this->db->join('tbl_employee_bank', 'tbl_employee_bank.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_employee_document', 'tbl_employee_document.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_designations.department_id', 'left');
        $this->db->join('countries', 'countries.idCountry  = tbl_employee.country_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_employee_ess_setup', 'tbl_employee_ess_setup.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_employee_point', 'tbl_employee_point.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('company', 'company.id  = tbl_employee.company_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_employee.employee_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        if (!empty($id)) {
            $this->db->select('tbl_employee.nationality', FALSE);
            $this->db->select('countries.countryName', FALSE);
            $this->db->from('tbl_employee');
            $this->db->join('countries', 'countries.idCountry  =  tbl_employee.nationality ', 'left');
            $query_nationality = $this->db->get();
            $nationality = $query_nationality->row();
            if (!empty($nationality)) {
                $result->nationality = $nationality->countryName;
            }
        }

        return $result;
    }


    public function all_approval_info($id = NULL) {
        //$this->db->select('DISTINCT(tbl_employee_matrix_approval.direct_leader_name)', FALSE);  
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_employee_bank.*', FALSE);
        $this->db->select('tbl_employee_document.*', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_department.department_name', FALSE);
        $this->db->select('countries.countryName', FALSE);
		$this->db->select('tbl_employee_matrix_approval.matrix_approval_id, tbl_employee_matrix_approval.direct_leader_id, tbl_employee_matrix_approval.direct_leader_name,
							tbl_employee_matrix_approval.direct_leader_email, tbl_employee_matrix_approval.direct_leader_id_2, tbl_employee_matrix_approval.direct_leader_name_2, tbl_employee_matrix_approval.direct_leader_email_2,
							tbl_employee_matrix_approval.email_hr_pic, tbl_employee_matrix_approval.nama_pic_hr, tbl_employee_matrix_approval.email_hybris,
							tbl_employee_matrix_approval.hybris_name', FALSE);
		$this->db->select('tbl_employee_ess_setup.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->join('tbl_employee_bank', 'tbl_employee_bank.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_employee_document', 'tbl_employee_document.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_employee.designations_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_designations.department_id', 'left');
        $this->db->join('countries', 'countries.idCountry  = tbl_employee.country_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id  = tbl_employee.employee_id', 'left');
		$this->db->join('tbl_employee_ess_setup', 'tbl_employee_ess_setup.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->where('tbl_employee_matrix_approval.direct_leader_name !=', '0');
        $this->db->where('tbl_employee_matrix_approval.direct_leader_name !=', '');
        $query_result = $this->db->get();
        $result = $query_result->result();
      

        return $result;
    }

    public function get_approval_employee($id = NULL) {
        $this->db->select('employee_id, employment_id, CONCAT(first_name," ", last_name) AS fullname, email,  email_2, designations_id, `status`,  
                            finger_id,  department_id, personal_email,  `level`,  employment_status, gender', FALSE);
        // $this->db->select('tbl_user.*', FALSE);
        $this->db->from('tbl_employee');
        // $this->db->join('tbl_user', 'tbl_user.employment_id = tbl_employee.employment_id', 'left');
        $this->db->where('tbl_employee.level <>', '0');
        $this->db->where('tbl_employee.level <>', 'Non Staf');
        $this->db->where('tbl_employee.level <>', 'Staf');
        $this->db->where('tbl_employee.status', 1);
        // $this->db->where('tbl_user.employment_id <>', '');
        
            $query_result = $this->db->get();
            $result = $query_result->result();
        

        // $query_result = $this->db->get();
        // $result = $query_result->result();
        return $result;
    }

    public function get_approval_from_tbl_user($id_approval1 = NULL) {
        $this->db->select('"" AS employee_id, employment_id, CONCAT(first_name," ", last_name) AS fullname, email, email AS email_2, "" AS designations_id,
         "" AS status, "" AS finger_id,"" AS department_id, "" AS personal_email,"" AS level, "" AS employment_status', FALSE);
        $this->db->from('tbl_user');
        $this->db->where('tbl_user.active', 1);

         if (!empty($id_approval1)) {
            $this->db->where('tbl_user.employment_id', $id_approval1);
            $query_result = $this->db->get();
            $result = $query_result->row();
        }  else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }

        return $result;
    }
    
    public function get_approval1_employee($id_approval1 = NULL) {
        $this->db->select('employee_id, employment_id, CONCAT(first_name," ", last_name) AS fullname, email,  email_2, designations_id, `status`,  
                            finger_id,  department_id, personal_email,  `level`,  employment_status, gender', FALSE);
        // $this->db->select('tbl_user.*', FALSE);
        $this->db->from('tbl_employee');
        // $this->db->join('tbl_user', 'tbl_user.employment_id = tbl_employee.employment_id', 'left');
        $this->db->where('tbl_employee.level <>', '0');
        $this->db->where('tbl_employee.level <>', 'Non Staf');
        $this->db->where('tbl_employee.level <>', 'Staf');
        $this->db->where('tbl_employee.status', 1);
        // $this->db->where('tbl_user.employment_id <>', '');
        if (!empty($id_approval1)) {
            $this->db->where('tbl_employee.employment_id', $id_approval1);
            $query_result = $this->db->get();
            $result = $query_result->row();
        }  else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }

        // $query_result = $this->db->get();
        // $result = $query_result->result();
        return $result;
    }

    public function get_approval2_employee($id_approval2 = NULL) {
        $this->db->select('employee_id, employment_id, CONCAT(first_name," ", last_name) AS fullname, email,  email_2, designations_id, `status`,  
                            finger_id,  department_id, personal_email,  `level`,  employment_status', FALSE);
        // $this->db->select('tbl_user.*', FALSE);
        $this->db->from('tbl_employee');
        // $this->db->join('tbl_user', 'tbl_user.employment_id = tbl_employee.employment_id', 'left');
        $this->db->where('tbl_employee.level <>', '0');
        $this->db->where('tbl_employee.level <>', 'Non Staf');
        $this->db->where('tbl_employee.level <>', 'Staf');
        $this->db->where('tbl_employee.status', 1);
        // $this->db->where('tbl_user.employment_id <>', '');
        if (!empty($id_approval2)) {
            $this->db->where('tbl_employee.employment_id', $id_approval2);
            $query_result = $this->db->get();
            $result = $query_result->row();
        }  else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }

        // $query_result = $this->db->get();
        // $result = $query_result->result();
        return $result;
    }

    public function get_tbl_user($id = NULL) {
        $this->db->select('"" AS employee_id, employment_id, CONCAT(first_name," ", last_name) AS fullname, email, email AS email_2, "" AS designations_id,
         "" AS status, "" AS finger_id,"" AS department_id, "" AS personal_email,"" AS level, "" AS employment_status', FALSE);
        $this->db->from('tbl_user');
        $this->db->where('tbl_user.active', 1);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


    public function get_hrga_list($id = NULL) {
        $this->db->select('employee_id, employment_id, CONCAT(first_name," ", last_name) AS fullname, email,  email_2, designations_id, status,  finger_id,  department_id, personal_email,  level,  employment_status', FALSE);
        $this->db->from('tbl_employee');
        $this->db->where('tbl_employee.status', '1');
        $this->db->where('tbl_employee.level <>', '0');
        $this->db->where('tbl_employee.level <>', 'Non Staf');
        $this->db->where('tbl_employee.level <>', 'Staf');
        $this->db->where('tbl_employee.department_id', '5')->or_where('tbl_employee.first_name','alwin')->or_where('tbl_employee.first_name','bagus')->or_where('tbl_employee.first_name','yuanita');
        // $this->db->where('tbl_employee.first_name','Florencia');
        
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_bod_list($id = NULL) {
        $this->db->select('employee_id, employment_id, CONCAT(first_name," ", last_name) AS fullname, email,  email_2, designations_id, status,  finger_id,  department_id, personal_email,  level,  employment_status', FALSE);
        $this->db->from('tbl_employee');
        $this->db->where('tbl_employee.department_id', '1');
        $this->db->where('tbl_employee.status', '1');
        $this->db->where('tbl_employee.level <>', '0');
        $this->db->where('tbl_employee.level <>', 'Non Staf');
        $this->db->where('tbl_employee.level <>', 'Staf');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


    public function get_employee_award_by_id($id=NULL) {

        $this->db->select('tbl_employee_award.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_employee_award');
        $this->db->join('tbl_employee', 'tbl_employee_award.employee_id = tbl_employee.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_employee_award.employee_award_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function get_level_employee() {
        $this->db->select('DISTINCT(tbl_employee.level)', FALSE);        
        $this->db->from('tbl_employee');
        $this->db->where('level <>', '0');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

}
