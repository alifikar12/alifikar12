<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of attendance_model
 *
 * @author NaYeM
 */
class Attendance_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function cek_saldo_cuti($employee_id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
		$this->db->select('tbl_saldo_cuti.*', FALSE);
        $this->db->from('tbl_saldo_cuti');  
        $this->db->where('employee_id', $employee_id); 	
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function getDepartmentByCompanyId($company_id){
        $this->db->select('tbl_department.*', FALSE);
        $this->db->from('tbl_department');  
        $this->db->where('company_id', $company_id); 	
        $query_result = $this->db->get();
        $result = $query_result->result_array();
        return $result;
    }

    public function get_employee_id_by_dept_id($department_id) {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_department.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id = tbl_employee.designations_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id = tbl_designations.department_id', 'left');
        if($department_id!=0){
            $this->db->where('tbl_department.department_id', $department_id);
        }
        $this->db->where('tbl_employee.status', 1);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_employee($employee_id) {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->where('tbl_employee.employee_id', $employee_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


    public function get_employee_join_finger($employee_id) {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('finger_att_logs.Date,finger_att_logs.Time,finger_att_logs.StatusAbsen', FALSE);
        $this->db->from('tbl_employee');
        $this->db->join('finger_att_logs', 'finger_att_logs.PIN  = tbl_employee.finger_id', 'left');
        $this->db->where('tbl_employee.employee_id', $employee_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


    // $this->db->select('finger_att_logs.Date,finger_att_logs.Time,finger_att_logs.StatusAbsen', FALSE);
    // $this->db->select('tbl_employee.first_name, tbl_employee.last_name ', FALSE);
    // $this->db->from('finger_att_logs');
    // $this->db->join('tbl_employee', 'finger_att_logs.PIN  = tbl_employee.finger_id', 'left');
    // $this->db->where('finger_att_logs.PIN', $finger_id);
    // $this->db->where('finger_att_logs.date', $sdate);


    public function get_periode($start_month, $end_month) {
        $this->db->select('tbl_months.*', FALSE);
        $this->db->from('tbl_months');
        $this->db->where('month_id >=', $start_month); 
        $this->db->where('month_id <=', $end_month); 
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function attendance_report_by_empid($employee_id = null, $sdate = null, $flag = NULL) {

        $this->db->select('tbl_attendance.date,tbl_attendance.attendance_status', FALSE);
        $this->db->select('tbl_employee.first_name, tbl_employee.last_name ', FALSE);
        $this->db->from('tbl_attendance');
        $this->db->join('tbl_employee', 'tbl_attendance.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->where('tbl_attendance.employee_id', $employee_id);
        $this->db->where('tbl_attendance.date', $sdate);
        $query_result = $this->db->get();
        $result = $query_result->result();

        if (empty($result)) {
            $val['attendance_status'] = $flag;
            $val['date'] = $sdate;
            $result[] = (object) $val;
        } else {
            if ($result[0]->attendance_status == 0) {
                if ($flag == 'H') {
                    $result[0]->attendance_status = 'H';
                }
            }
        }


        return $result;
    }
	
	 public function attendance_report_by_empid_with_time($finger_id = null, $sdate = null, $flag = NULL) {
        //DEBUG
        //$this->output->enable_profiler(TRUE);
        $this->db->select('finger_att_logs.Date AS date,finger_att_logs.Time,finger_att_logs.StatusAbsen', FALSE);
        $this->db->select('tbl_employee.first_name, tbl_employee.last_name ', FALSE);
        $this->db->from('finger_att_logs');
        $this->db->join('tbl_employee', 'finger_att_logs.PIN  = tbl_employee.finger_id', 'left');
        $this->db->where('finger_att_logs.PIN', $finger_id);
        $this->db->where('finger_att_logs.date', $sdate);
        $query_result = $this->db->get();
        $result = $query_result->result();

        if (empty($result)) {
            $val['StatusAbsen'] = $flag;
            $val['date'] = $sdate;
            $result[] = (object) $val;
        } 
        // else {
        //     if ($result[0]->StatusAbsen == 0) {               
        //         if ($flag == 'C') {
        //             $result[0]->StatusAbsen = 'C';
        //         }
        //         if ($flag == 'L') {
        //             $result[0]->StatusAbsen = 'L';
        //         }
        //         if ($flag == 'I') {
        //             $result[0]->StatusAbsen = 'I';
        //         }
        //         if ($flag == 'W') {
        //             $result[0]->StatusAbsen = 'W';
        //         }
        //         if ($flag == 'O') {
        //             $result[0]->StatusAbsen = 'O';
        //         }
        //         if ($flag == 'S') {
        //             $result[0]->StatusAbsen = 'S';
        //         }
        //         if ($flag == 'N') {
        //             $result[0]->StatusAbsen = 'N';
        //         }
               
        //    }
        // }

        return $result;
    }

    public function get_noshow_list_by_employee_id($start_date, $end_date, $employee_id) {
        $this->db->select('tbl_noshow_list.*', FALSE);
        $this->db->from('tbl_noshow_list');
        $this->db->where('employee_id', $employee_id); 
        $this->db->where('leave_status', 'fully approved');		
        $this->db->where('leave_start_date >=', $start_date); 
        $this->db->where('leave_start_date <=', $end_date); 
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    //get data overtime
    public function overtime_report_by_empid_with_time($employee_id = null, $sdate = null, $flag = NULL) {
        $this->db->SELECT('tbl_overtime_list.*', FALSE);
        $this->db->SELECT('tbl_employee.*', FALSE);
        $this->db->FROM('tbl_overtime_list');
        $this->db->JOIN('tbl_employee', 'tbl_overtime_list.employee_id  = tbl_employee.employee_id', 'left');
        $this->db->WHERE('tbl_overtime_list.employee_id', $employee_id);
		$this->db->WHERE('overtime_status', 'fully approved');	
        $this->db->WHERE('tbl_overtime_list.overtime_date', $sdate);
        $query_result = $this->db->get();
        $result = $query_result->result();
		/*
		if (empty($result)) {
            $val['StatusAbsen'] = $flag;
            $val['date'] = $sdate;
            $result[] = (object) $val;
        } else {
            //if ($result[0]->StatusAbsen == 0) {
                if ($flag == 'H') {
                    $result[0]->StatusAbsen = 'H';
                }
                if ($flag == 'L') {
                    $result[0]->StatusAbsen = 'L';
                }
                if ($flag == 'P') {
                    $result[0]->StatusAbsen = 'P';
                }
                if ($flag == 'O') {
                    $result[0]->StatusAbsen = 'O';
                }
           //}
        }
		*/
        return $result;
    }    


    public function get_application_list_by_finger_id($start_date, $end_date, $finger_id) {
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->from('tbl_application_list');
        //$this->db->like('leave_start_date', $yymm);  
        $this->db->where('finger_id', $finger_id); 
        $this->db->where('leave_status', 'fully approved');		
        $this->db->where('leave_start_date >=', $start_date); 
        $this->db->where('leave_start_date <=', $end_date); 
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    

    public function get_application_list_by_employee_id($start_date, $end_date, $employee_id) {
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->from('tbl_application_list');
        //$this->db->like('leave_start_date', $yymm);  
        $this->db->where('employee_id', $employee_id); 
        $this->db->where('leave_status', 'fully approved');		
        $this->db->where('leave_start_date >=', $start_date); 
        $this->db->where('leave_start_date <=', $end_date); 
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    //GET permit list by employee id
    public function get_permit_list_by_employee_id($start_date, $end_date, $employee_id) {
        $this->db->select('tbl_permit_list.*', FALSE);
        $this->db->from('tbl_permit_list');
        $this->db->where('employee_id', $employee_id); 
        $this->db->where('permit_status', 'fully approved');		
        $this->db->where('permit_date >=', $start_date); 
        $this->db->where('permit_date <=', $end_date); 
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    //GET overtime list by employee id
    public function get_overtime_list_by_employee_id($start_date, $end_date, $employee_id) {
        $this->db->select('tbl_overtime_list.*', FALSE);
        $this->db->from('tbl_overtime_list');
        $this->db->where('employee_id', $employee_id); 
        $this->db->where('overtime_status', 'fully approved');		
        $this->db->where('overtime_date >=', $start_date); 
        $this->db->where('overtime_date <=', $end_date); 
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    //GET sick list by employee id
    public function get_sick_application_list_by_employee_id($start_date, $end_date, $employee_id) {
        $this->db->select('tbl_sick_application.*', FALSE);
        $this->db->from('tbl_sick_application');
        $this->db->where('employee_id', $employee_id); 
        $this->db->where('status', 'fully approved');		
        $this->db->where('sick_start_date >=', $start_date); 
        $this->db->where('sick_end_date <=', $end_date); 
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    //GET perdin list by employee id
    public function get_perdin_application_list_by_employee_id($start_date, $end_date, $employee_id) {
        $this->db->select('tbl_perdin_list.*', FALSE);
        $this->db->from('tbl_perdin_list');
        $this->db->where('employee_id', $employee_id); 
        $this->db->where('perdin_status', 'fully approved');		
        $this->db->where('perdin_start_date >=', $start_date); 
        $this->db->where('perdin_end_date <=', $end_date); 
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    //date range
    public function dateSl( $start_date, $end_date, $step = '+1 day', $format = 'd' ) {
        $dates = [];
        $current = strtotime( $start_date );
        $last = strtotime( $end_date );
    
        while( $current <= $last ) {
    
            $dates[] = date( $format, $current );
            $current = strtotime( $step, $current );
        }
    
        return $dates;
    }

    //date range
    public function dateRange( $start_date, $end_date, $step = '+1 day', $format = 'Y-m-d' ) {
        $dates = [];
        $current = strtotime( $start_date );
        $last = strtotime( $end_date );
    
        while( $current <= $last ) {
    
            $dates[] = date( $format, $current );
            $current = strtotime( $step, $current );
        }
    
        return $dates;
    }
    

}
