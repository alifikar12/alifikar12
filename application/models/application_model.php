<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of application_model
 *
 * @author DSB
 */
class Application_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;


    public function get_applist($id = NULL) {
		//DEBUG
		// $this->output->enable_profiler(TRUE);		
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_application_list.employee_id', 'left');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id = tbl_application_list.leave_category_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->order_by('tbl_application_list.application_date','DESC');
		  
        $this->db->where('tbl_employee.employee_id', $id);         
        $query_result = $this->db->get();
        $result = $query_result->result();            
    
		
        return $result;
    }


    public function get_my_leave_info($employment_id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);		
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id  = tbl_application_list.leave_category_id');
		$this->db->join('tbl_employee', 'tbl_employee.employment_id  = tbl_application_list.employment_id');
        $this->db->where('tbl_application_list.employment_id', $employment_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_all_emp_leave_info($id = NULL) {
		//DEBUG
		// $this->output->enable_profiler(TRUE);		
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_application_list.employee_id', 'left');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id = tbl_application_list.leave_category_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->order_by('tbl_application_list.application_date','DESC');
		if (!empty($id)) {
            $this->db->where('tbl_application_list.application_list_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {            
            // $this->db->where('tbl_application_list.approve1 <> ""')->or_where('tbl_employee_matrix_approval.direct_leader_id_2', $employment_id)
            // ->or_where('tbl_employee_matrix_approval.direct_leader_id', $employment_id);           
            $query_result = $this->db->get();
            $result = $query_result->result();            
        }
		
        return $result;
    }

    

    public function get_emp_leave_info($id = NULL, $employment_id = NULL) {
		//DEBUG
		// $this->output->enable_profiler(TRUE);		
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_application_list.employee_id', 'left');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id = tbl_application_list.leave_category_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->order_by('tbl_application_list.application_date','DESC');
		if (!empty($id)) {
            $this->db->where('tbl_application_list.application_list_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {            
            $this->db->where('tbl_application_list.approve1 <> ""')->or_where('tbl_employee_matrix_approval.direct_leader_id_2', $employment_id)
            ->or_where('tbl_employee_matrix_approval.direct_leader_id', $employment_id);           
            $query_result = $this->db->get();
            $result = $query_result->result();            
        }
		
        return $result;
    }

    public function get_emp_leave_info_2($id = NULL, $employment_id = NULL) {
		//DEBUG
		// $this->output->enable_profiler(TRUE);		
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_application_list.employee_id', 'left');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id = tbl_application_list.leave_category_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_application_list.employee_id', 'left');
		$this->db->order_by('tbl_application_list.application_date','DESC');
		if (!empty($id)) {
            $this->db->where('tbl_application_list.application_list_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {            
                     
            $query_result = $this->db->get();
            $result = $query_result->result();            
        }
		
        return $result;
    }

    public function get_emp_permit_info($id = NULL) {	
        $this->db->select('tbl_permit_list.*', FALSE);
        $this->db->select('tbl_permit_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_permit_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_permit_list.employee_id', 'left');
        $this->db->join('tbl_permit_category', 'tbl_permit_category.permit_category_id = tbl_permit_list.permit_category_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_permit_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_permit_list.employee_id', 'left');
        $this->db->order_by('tbl_permit_list.permit_date','DESC');
		if (!empty($id)) {
            $this->db->where('tbl_permit_list.permit_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {            
            // $this->db->where('tbl_permit_list.approve <> ""');           
            $query_result = $this->db->get();
            $result = $query_result->result();            
        }
		
        return $result;
    }

    
    public function get_emp_perdin_info($id = NULL) {	
        $this->db->select('tbl_perdin_list.*', FALSE);
        $this->db->select('tbl_perdin_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_perdin_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_perdin_list.employee_id', 'left');
        $this->db->join('tbl_perdin_category', 'tbl_perdin_category.perdin_category_id = tbl_perdin_list.perdin_category_id', 'left');
        $this->db->join('tbl_saldo_cuti', 'tbl_saldo_cuti.employee_id = tbl_perdin_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_perdin_list.employee_id', 'left');
        $this->db->order_by('tbl_perdin_list.submit_date','DESC');
		if (!empty($id)) {
            $this->db->where('tbl_perdin_list.perdin_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {            
            // $this->db->where('tbl_permit_list.approve <> ""');           
            $query_result = $this->db->get();
            $result = $query_result->result();            
        }
		
        return $result;
    }

    
    public function get_emp_overtime_info($id = NULL) {	
        $this->db->select('tbl_overtime_list.*', FALSE);
        $this->db->select('tbl_perlengkapan_lembur.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);	
        $this->db->from('tbl_overtime_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_overtime_list.employee_id', 'left');
		$this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_employee.employee_id', 'left');
        $this->db->join('tbl_perlengkapan_lembur', 'tbl_perlengkapan_lembur.perlengkapan_id = tbl_overtime_list.perlengkapan_id', 'left');       

		if (!empty($id)) {
            $this->db->where('tbl_overtime_list.overtime_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {            
            $this->db->where('tbl_overtime_list.approve <> ""');           
            $query_result = $this->db->get();
            $result = $query_result->result();            
        }
		
        return $result;
    }

    // $this->db->select('doc_number, SUM(price) AS total_redeem, `status`, submit_date', FALSE);
    // $this->db->from('tbl_transaksi_point');
    // $this->db->group_by('doc_number');
    // $this->db->where('tbl_transaksi_point.employee_id', $employee_id);
    // $query_result = $this->db->get();
    // $result = $query_result->result();
    // return $result;

    public function get_emp_redeem_info($id = NULL) {	
        //DEBUG
		// $this->output->enable_profiler(TRUE);
        $this->db->select('doc_number, SUM(price) AS total_redeem, tbl_transaksi_point.`status` AS status_redeem, submit_date, approve1, unapprove1, date_approve1, date_unapprove1', FALSE);
        $this->db->select('tbl_employee.*', FALSE);	
        $this->db->from('tbl_transaksi_point');
        
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_transaksi_point.employee_id', 'left');  
        $this->db->group_by('tbl_transaksi_point.doc_number');

        $this->db->order_by('tbl_transaksi_point.doc_number', 'DESC');

		if (!empty($id)) {
           
            $this->db->where('tbl_transaksi_point.doc_number', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {            
            // $this->db->where('tbl_transaksi_point.approve1 <> ""');           
            $query_result = $this->db->get();
            $result = $query_result->result();            
        }
		
        return $result;
    }


    public function get_saldo_point($id) {
        // $this->output->enable_profiler(TRUE);
        $this->db->select('tbl_employee_point.*', FALSE);
        $this->db->from('tbl_employee_point');
        $this->db->where('tbl_employee_point.employee_id', $id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    

    public function get_emp_redeem_info_rows($id = NULL) {
        $this->db->select('tbl_transaksi_point.*, tbl_transaksi_point.`status` AS status_redeem', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_employee_point.*', FALSE);
		// $this->db->select('tbl_employee_matrix_approval_redeem.*', FALSE);
        $this->db->from('tbl_transaksi_point');
        $this->db->join('tbl_employee', 'tbl_employee.employment_id = tbl_transaksi_point.employment_id', 'left');
        $this->db->join('tbl_employee_point', 'tbl_employee_point.employee_id = tbl_transaksi_point.employee_id', 'left');
		// $this->db->join('tbl_employee_matrix_approval_redeem', 'tbl_employee_matrix_approval_redeem.employee_id = tbl_transaksi_point.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_transaksi_point.doc_number', $id);
            $query_result = $this->db->get();
            $result = $query_result->result();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        return $result;
    }

    public function get_emp_leave_info_for_dl($id = NULL, $employment_id = NULL) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);		
        $this->db->select('tbl_application_list.*', FALSE);
        $this->db->select('tbl_leave_category.*', FALSE);
        $this->db->select('tbl_employee.*', FALSE);
		$this->db->select('tbl_employee_matrix_approval.*', FALSE);
        $this->db->from('tbl_application_list');
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_application_list.employee_id', 'left');
        $this->db->join('tbl_leave_category', 'tbl_leave_category.leave_category_id = tbl_application_list.leave_category_id', 'left');
        $this->db->join('tbl_employee_matrix_approval', 'tbl_employee_matrix_approval.employee_id = tbl_employee.employee_id', 'left');
		
		if (!empty($id)) {
            $this->db->where('tbl_application_list.application_list_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            
            $this->db->where('tbl_employee.direct_leader_id', $employment_id)->or_where('tbl_employee.direct_leader_id_2', $employment_id);
            $this->db->where('tbl_application_list.approve1 = ""');
            $query_result = $this->db->get();
            $result = $query_result->result();
            
        }

        // elseif(){
        //     $this->db->where('tbl_employee.direct_leader_id = "$this->session->userdata("employment_id")"');
        //     $query_result = $this->db->get();
        //     $result = $query_result->result();
        // }
		
        return $result;
    }
	
	// public function cek_saldo_cuti($employee_id) {
	// 	//DEBUG
	// 	//$this->output->enable_profiler(TRUE);
	// 	$this->db->select('tbl_employee.*', FALSE);
    //     $this->db->from('tbl_employee');  
    //     $this->db->where('employee_id', $employee_id); 	
    //     $query_result = $this->db->get();
    //     $result = $query_result->result();
    //     return $result;
    // }

    public function cek_saldo_cuti($employee_id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
		$this->db->select('tbl_saldo_cuti.*', FALSE);
        $this->db->from('tbl_saldo_cuti');  
        $this->db->where('employee_id', $employee_id); 	
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
	
	public function cek_hak_cuti_from_leave_category($leave_category_id) {
		$this->db->select('tbl_leave_category.*', FALSE);
        $this->db->from('tbl_leave_category');  
        $this->db->where('leave_category_id', $leave_category_id); 	
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
	
	public function get_application_info($application_list_id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
		$this->db->select('tbl_application_list.leave_status', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_application_list');  
        $this->db->join('tbl_employee', 'tbl_employee.employment_id = tbl_application_list.employment_id', 'left');
		$this->db->where('application_list_id', $application_list_id); 	
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function get_permit_info($permit_id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
		$this->db->select('tbl_permit_list.permit_status', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_permit_list');  
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_permit_list.employee_id', 'left');
		$this->db->where('permit_id', $permit_id); 	
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function get_overtime_info($overtime_id) {
		$this->db->select('tbl_overtime_list.overtime_status', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_overtime_list');  
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_overtime_list.employee_id', 'left');
		$this->db->where('overtime_id', $overtime_id); 	
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function get_perdin_info($perdin_id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
		$this->db->select('tbl_perdin_list.perdin_status', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_perdin_list');  
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_perdin_list.employee_id', 'left');
		$this->db->where('perdin_id', $perdin_id); 	
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }
       

    public function get_redeem_info($document_number) {
        //DEBUG
		//$this->output->enable_profiler(TRUE);
		$this->db->select('tbl_transaksi_point.status AS status_redeem', FALSE);
		$this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_transaksi_point');  
        $this->db->join('tbl_employee', 'tbl_employee.employee_id = tbl_transaksi_point.employee_id', 'left');
		$this->db->where('doc_number', $document_number); 	
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

}
