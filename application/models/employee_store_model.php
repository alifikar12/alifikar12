<?php

/**
 * Description of employee_model
 *
 * @author NaYeM
 */
class Employee_Store_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;



public function all_emplyee_store_info($id = NULL) {
        $this->db->select('tbl_store_employee.*', FALSE);
        $this->db->select('tbl_store_employee_bank.*', FALSE);
        $this->db->select('tbl_store_employee_document.*', FALSE);
        $this->db->select('tbl_designations.*', FALSE);
        $this->db->select('tbl_department.department_name', FALSE);
        $this->db->select('countries.countryName', FALSE);
		$this->db->select('tbl_store_employee_matrix_approval.matrix_approval_id, tbl_store_employee_matrix_approval.direct_leader_id, tbl_store_employee_matrix_approval.direct_leader_name,
							tbl_store_employee_matrix_approval.direct_leader_email, tbl_store_employee_matrix_approval.direct_leader_id_2, tbl_store_employee_matrix_approval.direct_leader_name_2, tbl_store_employee_matrix_approval.direct_leader_email_2,
							tbl_store_employee_matrix_approval.email_hr_pic, tbl_store_employee_matrix_approval.nama_pic_hr, tbl_store_employee_matrix_approval.email_hybris,
							tbl_store_employee_matrix_approval.hybris_name', FALSE);
        $this->db->select('tbl_store_employee_ess_setup.*', FALSE);
        // $this->db->select('tbl_store_employee_saldo_cuti.*', FALSE);
        // $this->db->select('tbl_store_employee_saldo_cuti.`id` AS saldo_cuti_id, tbl_store_employee_saldo_cuti.`employee_id`, tbl_store_employee_saldo_cuti.`employment_id`, tbl_store_employee_saldo_cuti.`expired`, tbl_store_employee_saldo_cuti.`date_reload`,
        //                     tbl_store_employee_saldo_cuti.`sisa_cuti_thn_lalu`, tbl_store_employee_saldo_cuti.`saldo_cuti`', FALSE);
        $this->db->from('tbl_store_employee');
        $this->db->join('tbl_store_employee_bank', 'tbl_store_employee_bank.employee_id = tbl_store_employee.employee_id', 'left');
        $this->db->join('tbl_store_employee_document', 'tbl_store_employee_document.employee_id  = tbl_store_employee.employee_id', 'left');
        $this->db->join('tbl_designations', 'tbl_designations.designations_id  = tbl_store_employee.designations_id', 'left');
        $this->db->join('tbl_department', 'tbl_department.department_id  = tbl_designations.department_id', 'left');
        $this->db->join('countries', 'countries.idCountry  = tbl_store_employee.country_id', 'left');
		$this->db->join('tbl_store_employee_matrix_approval', 'tbl_store_employee_matrix_approval.employee_id  = tbl_store_employee.employee_id', 'left');
        $this->db->join('tbl_store_employee_ess_setup', 'tbl_store_employee_ess_setup.employee_id  = tbl_store_employee.employee_id', 'left');
        // $this->db->join('tbl_store_employee_saldo_cuti', 'tbl_store_employee_saldo_cuti.employee_id  = tbl_store_employee.employee_id', 'left');
        if (!empty($id)) {
            $this->db->where('tbl_store_employee.employee_id', $id);
            $query_result = $this->db->get();
            $result = $query_result->row();
        } else {
            $query_result = $this->db->get();
            $result = $query_result->result();
        }
        if (!empty($id)) {
            $this->db->select('tbl_store_employee.nationality', FALSE);
            $this->db->select('countries.countryName', FALSE);
            $this->db->from('tbl_store_employee');
            $this->db->join('countries', 'countries.idCountry  =  tbl_store_employee.nationality ', 'left');
            $query_nationality = $this->db->get();
            $nationality = $query_nationality->row();
            if (!empty($nationality)) {
                $result->nationality = $nationality->countryName;
            }
        }

        return $result;
    }

}