<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of email_model
 *
 * @author DSB
 */
require './PHPMailer-master/PHPMailerAutoload.php';
class Email_Model extends MY_Model {

	public function __construct() {
        parent::__construct();
        $this->load->model('application_model');
    }


    public $_table_name;
    public $_order_by;  
	public $_primary_key;

	public function smtp_server($email_to, $subject, $body){
		$mail = new PHPMailer;
		
		##optional security
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);

		//$mail->SMTPDebug = 4;                               	    	// Enable verbose debug output

		$mail->isSMTP();                                      	    	// Set mailer to use SMTP
		
		$mail->Host         = 'hrms.oneunivrs.id';  						// Specify main and backup SMTP servers
		$mail->SMTPAuth     = true;                               		// Enable SMTP authentication
		$mail->Username     = 'noreply@hrms.oneunivrs.id';            	    	// SMTP username
		$mail->Password     = '~w.IdafX,EwW';                          	// SMTP password
		$mail->SMTPSecure   = 'ssl';                            		// Enable TLS encryption, `ssl` also accepted
		$mail->Port         = 465;                                   	// TCP port to connect to

		$mail->setFrom('noreply@hrms.oneunivrs.id', 'Noreply');
		$mail->addAddress($email_to);     								// Add a recipient
		$mail->addReplyTo('noreply@hrms.oneunivrs.id', 'Noreply');
		//$mail->addCC('ebes.doni@gmail.com');
		
		$mail->isHTML(true);                                  			// Set email format to HTML

		$mail->Subject = $subject;
		$mail->Body    = $body;
		$mail->send();
	}
		


		public function email_notif_redeem_point_to_hr($email_to, $document_number, $link, $receipt_name, $sender_name, $status=NULL, $jk_approval=NULL, $jk_sender=NULL){
							 
			if($status == 'partial approved'){
				$keterangan = 'telah menyetujui dokumen pengajuan redeem point dengan nomor dokumen';
			}elseif($status == 'cancel'){
				$keterangan = 'tidak menyetujui pengajuan redeem point dengan nomor dokumen';
			}elseif($status == 'pending'){
				$keterangan = 'telah membuat dokumen pengajuan redeem point dengan nomor dokumen';
			}else {
				$keterangan = '';
			}

			if($jk_approval == 'Male'){
				$jk_approval = 'Bapak';
			}elseif($jk_approval == 'Female'){
				$jk_approval = 'Ibu';
			}else{
				$jk_approval = '';
			}

			if($jk_sender == 'Male'){
				$jk_sender = 'Bapak';
			}elseif($jk_sender == 'Female'){
				$jk_sender = 'Ibu';
			}else{
				$jk_sender = '';
			}
			
			
			$url_logo   = base_url().'img/uploads/LOGO5.png';
			$subject	= "Notifikasi Pengajuan Redeem Point";
			if($status != 'cancel'){
				$body		= "<div> 
								<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
								<tbody>
									<tr><td style='background-color: rgb(255, 255, 255)'>             
										<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
										<tbody>
											<tr><td align='center'>                         
												<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
													<tbody>
													<tr><td>                                     
														<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
														<tbody>
															<tr>                                             
																<td width='100%' height='63' style='max-height: 63px'>                                                 
																	<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																		<tbody>
																			<tr height='10'><td width='100%'></td></tr>
																			<tr>
																				<td align='middle'>                                                             
																					<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>
																					
																				</td>                                                                                                          
																			</tr>                                                     
																			<tr>                                                                                                                 
																				<td align='middle'>
																					<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Redeem Point  </p>
																				</td> 
																			</tr>
																		</tbody>
																	</table>                                             
																</td>                                         
															</tr>                                     
														</tbody>
													</table>                                 
												</td>                             
											</tr>                         
											</tbody>
										</table>                     
										</td>                 
										</tr>                                                   
										<tr style='font-size: 14px'>                     
										<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
											<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
												<tbody>
													<tr>                                 
														<td>                                     
															<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																<tbody>
																	<tr>                                             
																		<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																					<tr>                                                         
																						<td align='left'>                                                             
																							<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																							<strong> Dear ".$jk_approval." ".$receipt_name."</strong>
																							</p>    	
																						</td>                                                     
																					</tr>
																				</tbody>
																			</table>
																		</td>                                         
																	</tr>
																	<tr>                                             
																		<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																					<tr>                                                         
																						<td align='left'>                                                             
																							<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																							".$jk_sender." ".$sender_name." ".$keterangan." ".$document_number.". 
																							</p>                                                             
																							<p>Untuk melakukan akses dokumen ".$document_number.", silahkan klik link di bawah ini: </p>                    
																						</td>                                                     
																					</tr>                                                                                                      
																				</tbody>
																			</table>                                             
																		</td>                                         
																	</tr>                                         
																	<tr>                                             
																		<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='center'>                                                             
																					<a href='".$link."' style='padding: 7px 20px; color: rgb(255, 255, 255); background-color: rgb(0, 102, 181); text-decoration: none; border-radius: 5px' target='_blank'>LINK</a>
																				</td>
																			</tr>   
																		</tbody>
																		</table>                                             
																		</td>                                         
																	</tr>                                         
																	<tr>	
																		<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																				<tr>                                                         
																					<td align='center'>                                                             
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																					</td>  
																				</tr>  
																			</tbody>
																		</table>                                             
																		</td>
																	</tr>                                        
																	<tr height='10'><td width='100%'></td></tr>     
																</tbody>
															</table>                                 
														</td>                             
													</tr>                         
												</tbody>
												</table>                     
												</td>                 
											</tr>                              
										</tbody>
										</table>         
									</td></tr> 
								</tbody>
								</table>   
								</div>						
								";
			}else{
				$body		= "<div> 
								<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
								<tbody>
									<tr><td style='background-color: rgb(255, 255, 255)'>             
										<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
										<tbody>
											<tr><td align='center'>                         
												<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
													<tbody>
													<tr><td>                                     
														<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
														<tbody>
															<tr>                                             
																<td width='100%' height='63' style='max-height: 63px'>                                                 
																	<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																	<tbody>
																		<tr height='10'><td width='100%'></td></tr>
																		<tr>
																			<td align='middle'>                                                             
																				<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																				
																			</td>                                                                                                          
																		</tr>                                                     
																		<tr>                                                                                                                 
																			<td align='middle'>
																				<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Redeem Point  </p>
																			</td> 
																		</tr>
																	</tbody>
																	</table>                                             
																</td>                                         
															</tr>                                     
														</tbody>
													</table>                                 
												</td>                             
											</tr>                         
											</tbody>
										</table>                     
										</td>                 
										</tr>                                                   
										<tr style='font-size: 14px'>                     
										<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
											<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
												<tbody>
													<tr>                                 
														<td>                                     
															<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																<tbody>
																	<tr>                                             
																		<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																					<tr>                                                         
																						<td align='left'>                                                             
																							<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																							<strong> Dear ".$jk_approval." ".$receipt_name."</strong>
																							</p>    	
																						</td>                                                     
																					</tr>
																				</tbody>
																			</table>
																		</td>                                         
																	</tr>
																	<tr>                                             
																		<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																				<tr>                                                         
																					<td align='left'> 																					
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																						".$jk_sender." ".$sender_name." ".$keterangan." ".$document_number.". 
																						</p>                                                                    
																					</td>                                                     
																				</tr>                                                                                                      
																			</tbody>
																			</table>                                             
																		</td>                                         
																	</tr>                                        								   
																	<tr>	
																		<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='center'>                                                             
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																				</td>  
																			</tr>  
																		</tbody>
																		</table>                                             
																		</td>
																	</tr>                                        
																	<tr height='10'><td width='100%'></td></tr>     
																</tbody>
															</table>                                 
														</td>                             
													</tr>                         
												</tbody>
												</table>                     
												</td>                 
											</tr>                              
										</tbody>
										</table>         
									</td></tr> 
								</tbody>
								</table>   
								</div>						
								";
			}
			
			$this->smtp_server($email_to, $subject, $body);
			
		}


															
			public function email_notif_redeem_to_requester($email_to, $doc_number, $link, $receipt_name, $approval_name, $status_redeem, $submit_date=NULL, $jk_req=NULL){
				$keterangan = '';
				if($status_redeem == 'fully approved'){
					$keterangan = 'telah menyetujui redeem point Anda';
				}
				if($status_redeem == 'cancel'){
					$keterangan = 'tidak menyetujui redeem point Anda';
				}
					
				if($jk_req=="Male"){
					$jk_req = "Bapak";
				}else{
					$jk_req = "Ibu";
				}
				
				$url_logo   = base_url().'img/uploads/LOGO5.png';
				$subject	= "Notifikasi Pengajuan Redeem Point";
				
				$body		= "<div> 
								<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
								<tbody>
									<tr><td style='background-color: rgb(255, 255, 255)'>             
										<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
										<tbody>
											<tr><td align='center'>                         
												<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
													<tbody>
													<tr><td>                                     
														<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
														<tbody>
															<tr>                                             
																<td width='100%' height='63' style='max-height: 63px'>                                                 
																	<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																		<tbody>
																			<tr height='10'><td width='100%'></td></tr>
																			<tr>
																				<td align='middle'>                                                             
																					<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																																						
																				</td>                                                                                                          
																			</tr>                                                     
																			<tr>                                                                                                                 
																				<td align='middle'>
																					<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Redeem Point </p>
																				</td> 
																			</tr>
																		</tbody>
																	</table>                                             
																</td>                                         
															</tr>                                     
														</tbody>
													</table>                                 
												</td>                             
											</tr>                         
											</tbody>
										</table>                     
										</td>                 
										</tr>                                                   
										<tr style='font-size: 14px'>                     
										<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
											<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
												<tbody>
													<tr>                                 
														<td>                                     
															<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																<tbody>
																	<tr>                                             
																		<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																				<tr>                                                         
																					<td align='left'>                                                             
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																						<strong> Dear ".$jk_req." ".$receipt_name." </strong>
																						</p>    	
																					</td>                                                     
																				</tr>
																			</tbody>
																			</table>
																		</td>                                         
																	</tr>
																	<tr>                                             
																		<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																				<tr>                                                         
																					<td align='left'>       	
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																						Bapak/Ibu ".$approval_name." ".$keterangan." . 
																						</p>                                                                          
																					</td>                                                     
																				</tr>                                                                                                      
																			</tbody>
																			</table>                                             
																		</td>                                         
																	</tr>                                         
																																					
																	<tr>	
																		<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='center'>                                                             
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																				</td>  
																			</tr>  
																		</tbody>
																		</table>                                             
																		</td>
																	</tr>                                        
																	<tr height='10'><td width='100%'></td></tr>     
																</tbody>
															</table>                                 
														</td>                             
													</tr>                         
												</tbody>
												</table>                     
												</td>                 
											</tr>                              
										</tbody>
										</table>         
									</td></tr> 
								</tbody>
								</table>   
								</div>						
								";
		
		
				$this->smtp_server($email_to, $subject, $body);
				
			}	

			public function make_pdf_redeem_point($doc_number) {
				$data['title'] = "Redeem Point";
		
				$data['redeem_info']        = $this->application_model->get_emp_redeem_info($doc_number);
				$data['product_list_info']  = $this->application_model->get_emp_redeem_info_rows($doc_number);       
				$employee_id                = $this->application_model->get_emp_redeem_info($doc_number)->employee_id;
				$data['saldo_point']        = $this->application_model->get_saldo_point($employee_id);
		
				$this->load->helper('dompdf');
				$view_file = $this->load->view('admin/employee/employee_redeem_point_pdf', $data, true);
				return pdf_create($view_file, $data['redeem_info']->doc_number, $stream = FALSE);
				// return $view_file;
			}


			public function email_notif_to_marketing($ack_email, $ack_email_cc, $doc_number, $link, $ack_name, $approval_name, $status_redeem, $submit_date=NULL, $ack_gender=NULL, $jk_req=NULL){
				
				if($status_redeem == 'fully approved'){
					$keterangan = 'telah menyetujui redeem point mohon untuk proses pengeluaran barang di internal usage';
				}else if($status_redeem == 'cancel'){
					$keterangan = 'tidak menyetujui redeem point mohon untuk proses pengeluaran barang di internal usage';
				}else{
					$keterangan = '';
				}
					
				if($ack_gender=='Male'){
					$ack_gender = 'Bapak';
				}elseif($ack_gender=='Female'){
					$ack_gender = 'Ibu';
				}else {
					$ack_gender = '';
				}

				if($jk_req=='Male'){
					$jk_req = 'Bapak';
				}elseif($jk_req=='Female'){
					$jk_req = 'Ibu';
				}else {
					$jk_req = '';
				}
				
				$url_logo   = base_url().'img/uploads/LOGO5.png';
				$subject	= "Notifikasi Pengajuan Redeem Point";
				
				$body		= "<div> 
								<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
								<tbody>
									<tr><td style='background-color: rgb(255, 255, 255)'>             
										<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
										<tbody>
											<tr><td align='center'>                         
												<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
													<tbody>
													<tr><td>                                     
														<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
														<tbody>
															<tr>                                             
																<td width='100%' height='63' style='max-height: 63px'>                                                 
																	<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																		<tbody>
																			<tr height='10'><td width='100%'></td></tr>
																			<tr>
																				<td align='middle'>                                                             
																					<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																																						
																				</td>                                                                                                          
																			</tr>                                                     
																			<tr>                                                                                                                 
																				<td align='middle'>
																					<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Redeem Point </p>
																				</td> 
																			</tr>
																		</tbody>
																	</table>                                             
																</td>                                         
															</tr>                                     
														</tbody>
													</table>                                 
												</td>                             
											</tr>                         
											</tbody>
										</table>                     
										</td>                 
										</tr>                                                   
										<tr style='font-size: 14px'>                     
										<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
											<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
												<tbody>
													<tr>                                 
														<td>                                     
															<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																<tbody>
																	<tr>                                             
																		<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																				<tr>                                                         
																					<td align='left'>                                                             
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																						<strong> Dear ".$ack_gender." ".$ack_name." </strong>
																						</p>    	
																					</td>                                                     
																				</tr>
																			</tbody>
																			</table>
																		</td>                                         
																	</tr>
																	<tr>                                             
																		<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																				<tr>                                                         
																					<td align='left'>       	
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																						Bapak/Ibu ".$approval_name." ".$keterangan." . 
																						</p>                                                                          
																					</td>                                                     
																				</tr>                                                                                                      
																			</tbody>
																			</table>                                             
																		</td>                                         
																	</tr>                                         
																																					
																	<tr>	
																		<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='center'>                                                             
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																				</td>  
																			</tr>  
																		</tbody>
																		</table>                                             
																		</td>
																	</tr>                                        
																	<tr height='10'><td width='100%'></td></tr>     
																</tbody>
															</table>                                 
														</td>                             
													</tr>                         
												</tbody>
												</table>                     
												</td>                 
											</tr>                              
										</tbody>
										</table>         
									</td></tr> 
								</tbody>
								</table>   
								</div>						
								";
		
		
								$mail = new PHPMailer;
		
								##optional security
								$mail->SMTPOptions = array(
									'ssl' => array(
										'verify_peer' => false,
										'verify_peer_name' => false,
										'allow_self_signed' => true
									)
								);
						
								//$mail->SMTPDebug = 4;                               	    	// Enable verbose debug output
						
								$mail->isSMTP();                                      	    	// Set mailer to use SMTP
								
								$mail->Host         = 'asmara.iixcp.rumahweb.com
';  						// Specify main and backup SMTP servers
								$mail->SMTPAuth     = true;                               		// Enable SMTP authentication
								$mail->Username     = 'noreply@hrms.oneunivrs.id

								';            	    	// SMTP username
								$mail->Password     = 'Sendaljepit01';                          	// SMTP password
								$mail->SMTPSecure   = 'ssl';                            		// Enable TLS encryption, `ssl` also accepted
								$mail->Port         = 465;                                   	// TCP port to connect to
						
								$mail->setFrom('noreply@hrms.oneunivrs.id
', 'Noreply');
								$mail->addAddress($ack_email);     						// Add a recipient
								$mail->addReplyTo('noreply@hrms.oneunivrs.id
', 'Noreply');
								$mail->addCC($ack_email_cc);
								
								$mail->isHTML(true);                                  			// Set email format to HTML
						
								$mail->Subject = $subject;
								$mail->Body    = $body;
								
								//add attachment from dompdf 
								// $pdfString = $dompdf->output();
								$pdfString = $this->make_pdf_redeem_point($doc_number);
								$mail->addStringAttachment($pdfString, $doc_number.'.pdf');
								
								$mail->send();
				
			}	


			public function email_notif_permit($email_to, $document_number, $link, $receipt_name, $sender_name, $permit_status=NULL, $jk_sender=NULL, $jk_receipt=NULL, $requester_name=NULL){
							 
				
				if($permit_status == 'partial approved'){
					$keterangan = 'telah menyetujui dokumen pengajuan izin dengan nomor dokumen';
				}elseif($permit_status == 'cancel'){
					$keterangan = 'tidak menyetujui pengajuan izin dengan nomor dokumen';
				}elseif($permit_status == 'pending'){
					$keterangan = 'telah membuat dokumen pengajuan izin dengan nomor dokumen';
				}elseif($permit_status == 'partial approved 2'){
					$keterangan = 'telah menyetujui dokumen pengajuan izin dengan nomor dokumen';
				}else {
					$keterangan = '';
				}

				if(!empty($requester_name)){
					$requester_name = 'a/n '.$requester_name;
				}else {
					$requester_name = '';
				}

				if($jk_sender == 'Male'){
					$jk_sender = 'Bapak';
				}elseif($jk_sender == 'Female'){
					$jk_sender = 'Ibu';
				}else {
					$jk_sender = 'Bapak/Ibu';
				}

				if($jk_receipt == 'Male'){
					$jk_receipt = 'Bapak';
				}elseif($jk_receipt == 'Female'){
					$jk_receipt = 'Ibu';
				}else {
					$jk_receipt = 'Bapak/Ibu';
				}
				
				
				$url_logo   = base_url().'img/uploads/LOGO5.pngg';
				$subject	= "Notifikasi Pengajuan Izin";
				if($permit_status != 'cancel'){
					$body		= "<div> 
									<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
									<tbody>
										<tr><td style='background-color: rgb(255, 255, 255)'>             
											<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
											<tbody>
												<tr><td align='center'>                         
													<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
														<tbody>
														<tr><td>                                     
															<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
															<tbody>
																<tr>                                             
																	<td width='100%' height='63' style='max-height: 63px'>                                                 
																		<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																			<tbody>
																				<tr height='10'><td width='100%'></td></tr>
																				<tr>
																					<td align='middle'>                                                             
																						<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>
																						
																					</td>                                                                                                          
																				</tr>                                                     
																				<tr>                                                                                                                 
																					<td align='middle'>
																						<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Izin  </p>
																					</td> 
																				</tr>
																			</tbody>
																		</table>                                             
																	</td>                                         
																</tr>                                     
															</tbody>
														</table>                                 
													</td>                             
												</tr>                         
												</tbody>
											</table>                     
											</td>                 
											</tr>                                                   
											<tr style='font-size: 14px'>                     
											<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
												<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
													<tbody>
														<tr>                                 
															<td>                                     
																<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																	<tbody>
																		<tr>                                             
																			<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																						<tr>                                                         
																							<td align='left'>                                                             
																								<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																								<strong> Dear ".$jk_receipt."  ".$receipt_name."</strong>
																								</p>    	
																							</td>                                                     
																						</tr>
																					</tbody>
																				</table>
																			</td>                                         
																		</tr>
																		<tr>                                             
																			<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																						<tr>                                                         
																							<td align='left'>                                                             
																								<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																								".$jk_sender." ".$sender_name." ".$keterangan." ".$document_number.". 
																								</p>                                                             
																								<p>Untuk melakukan akses dokumen ".$document_number.", silahkan klik link di bawah ini: </p>                    
																							</td>                                                     
																						</tr>                                                                                                      
																					</tbody>
																				</table>                                             
																			</td>                                         
																		</tr>                                         
																		<tr>                                             
																			<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																				<tr>                                                         
																					<td align='center'>                                                             
																						<a href='".$link."' style='padding: 7px 20px; color: rgb(255, 255, 255); background-color: rgb(0, 102, 181); text-decoration: none; border-radius: 5px' target='_blank'>LINK</a>
																					</td>
																				</tr>   
																			</tbody>
																			</table>                                             
																			</td>                                         
																		</tr>                                         
																		<tr>	
																			<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																					<tr>                                                         
																						<td align='center'>                                                             
																							<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																						</td>  
																					</tr>  
																				</tbody>
																			</table>                                             
																			</td>
																		</tr>                                        
																		<tr height='10'><td width='100%'></td></tr>     
																	</tbody>
																</table>                                 
															</td>                             
														</tr>                         
													</tbody>
													</table>                     
													</td>                 
												</tr>                              
											</tbody>
											</table>         
										</td></tr> 
									</tbody>
									</table>   
									</div>						
									";
				}else{
					$body		= "<div> 
									<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
									<tbody>
										<tr><td style='background-color: rgb(255, 255, 255)'>             
											<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
											<tbody>
												<tr><td align='center'>                         
													<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
														<tbody>
														<tr><td>                                     
															<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
															<tbody>
																<tr>                                             
																	<td width='100%' height='63' style='max-height: 63px'>                                                 
																		<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																		<tbody>
																			<tr height='10'><td width='100%'></td></tr>
																			<tr>
																				<td align='middle'>                                                             
																					<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																				
																				</td>                                                                                                          
																			</tr>                                                     
																			<tr>                                                                                                                 
																				<td align='middle'>
																					<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Izin  </p>
																				</td> 
																			</tr>
																		</tbody>
																		</table>                                             
																	</td>                                         
																</tr>                                     
															</tbody>
														</table>                                 
													</td>                             
												</tr>                         
												</tbody>
											</table>                     
											</td>                 
											</tr>                                                   
											<tr style='font-size: 14px'>                     
											<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
												<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
													<tbody>
														<tr>                                 
															<td>                                     
																<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																	<tbody>
																		<tr>                                             
																			<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																						<tr>                                                         
																							<td align='left'>                                                             
																								<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																								<strong> Dear ".$jk_receipt." ".$receipt_name."</strong>
																								</p>    	
																							</td>                                                     
																						</tr>
																					</tbody>
																				</table>
																			</td>                                         
																		</tr>
																		<tr>                                             
																			<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																					<tr>                                                         
																						<td align='left'> 																					
																							<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																							".$jk_sender." ".$sender_name." ".$keterangan." ".$document_number.". 
																							</p>                                                                    
																						</td>                                                     
																					</tr>                                                                                                      
																				</tbody>
																				</table>                                             
																			</td>                                         
																		</tr>                                         
																																					 
																		<tr>	
																			<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																				<tr>                                                         
																					<td align='center'>                                                             
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																					</td>  
																				</tr>  
																			</tbody>
																			</table>                                             
																			</td>
																		</tr>                                        
																		<tr height='10'><td width='100%'></td></tr>     
																	</tbody>
																</table>                                 
															</td>                             
														</tr>                         
													</tbody>
													</table>                     
													</td>                 
												</tr>                              
											</tbody>
											</table>         
										</td></tr> 
									</tbody>
									</table>   
									</div>						
									";
				}
				
		
				$this->smtp_server($email_to, $subject, $body);
			
				}


				public function email_notif_perdin($email_to, $doc_number, $link, $receipt_name, $sender_name, $perdin_status=NULL, $jk_sender=NULL, $jk_receipt=NULL, $requester_name=NULL){
							 
				
					if($perdin_status == 'partial approved'){
						$keterangan = 'telah menyetujui dokumen pengajuan perdin dengan nomor dokumen';
					}elseif($perdin_status == 'cancel'){
						$keterangan = 'tidak menyetujui pengajuan perdin dengan nomor dokumen';
					}elseif($perdin_status == 'pending'){
						$keterangan = 'telah membuat dokumen pengajuan perdin dengan nomor dokumen';
					}elseif($perdin_status == 'partial approved 2'){
						$keterangan = 'telah menyetujui dokumen pengajuan perdin dengan nomor dokumen';
					}else {
						$keterangan = '';
					}
	
					if(!empty($requester_name)){
						$requester_name = 'a/n '.$requester_name;
					}else {
						$requester_name = '';
					}
	
					if($jk_sender == 'Male'){
						$jk_sender = 'Bapak';
					}elseif($jk_sender == 'Female'){
						$jk_sender = 'Ibu';
					}else {
						$jk_sender = 'Bapak/Ibu';
					}
	
					if($jk_receipt == 'Male'){
						$jk_receipt = 'Bapak';
					}elseif($jk_receipt == 'Female'){
						$jk_receipt = 'Ibu';
					}else {
						$jk_receipt = 'Bapak/Ibu';
					}
					
					
					$url_logo   = base_url().'img/uploads/LOGO5.png';
					$subject	= "Notifikasi Pengajuan Perdin";
					if($perdin_status != 'cancel'){
						$body		= "<div> 
										<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
										<tbody>
											<tr><td style='background-color: rgb(255, 255, 255)'>             
												<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
												<tbody>
													<tr><td align='center'>                         
														<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
															<tbody>
															<tr><td>                                     
																<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
																<tbody>
																	<tr>                                             
																		<td width='100%' height='63' style='max-height: 63px'>                                                 
																			<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																				<tbody>
																					<tr height='10'><td width='100%'></td></tr>
																					<tr>
																						<td align='middle'>                                                             
																							<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>
																							
																						</td>                                                                                                          
																					</tr>                                                     
																					<tr>                                                                                                                 
																						<td align='middle'>
																							<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Perdin  </p>
																						</td> 
																					</tr>
																				</tbody>
																			</table>                                             
																		</td>                                         
																	</tr>                                     
																</tbody>
															</table>                                 
														</td>                             
													</tr>                         
													</tbody>
												</table>                     
												</td>                 
												</tr>                                                   
												<tr style='font-size: 14px'>                     
												<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
													<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
														<tbody>
															<tr>                                 
																<td>                                     
																	<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																		<tbody>
																			<tr>                                             
																				<td align='center'>                                                 
																					<table align='center' border='0' width='90%'>                                                                            
																					<tbody>
																							<tr>                                                         
																								<td align='left'>                                                             
																									<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																									<strong> Dear ".$jk_receipt."  ".$receipt_name."</strong>
																									</p>    	
																								</td>                                                     
																							</tr>
																						</tbody>
																					</table>
																				</td>                                         
																			</tr>
																			<tr>                                             
																				<td align='center'>                                                 
																					<table align='center' border='0' width='90%'>                                                                            
																					<tbody>
																							<tr>                                                         
																								<td align='left'>                                                             
																									<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																									".$jk_sender." ".$sender_name." ".$keterangan." ".$doc_number.". 
																									</p>                                                             
																									<p>Untuk melakukan akses dokumen ".$doc_number.", silahkan klik link di bawah ini: </p>                    
																								</td>                                                     
																							</tr>                                                                                                      
																						</tbody>
																					</table>                                             
																				</td>                                         
																			</tr>                                         
																			<tr>                                             
																				<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																					<tr>                                                         
																						<td align='center'>                                                             
																							<a href='".$link."' style='padding: 7px 20px; color: rgb(255, 255, 255); background-color: rgb(0, 102, 181); text-decoration: none; border-radius: 5px' target='_blank'>LINK</a>
																						</td>
																					</tr>   
																				</tbody>
																				</table>                                             
																				</td>                                         
																			</tr>                                         
																			<tr>	
																				<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																						<tr>                                                         
																							<td align='center'>                                                             
																								<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																							</td>  
																						</tr>  
																					</tbody>
																				</table>                                             
																				</td>
																			</tr>                                        
																			<tr height='10'><td width='100%'></td></tr>     
																		</tbody>
																	</table>                                 
																</td>                             
															</tr>                         
														</tbody>
														</table>                     
														</td>                 
													</tr>                              
												</tbody>
												</table>         
											</td></tr> 
										</tbody>
										</table>   
										</div>						
										";
					}else{
						$body		= "<div> 
										<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
										<tbody>
											<tr><td style='background-color: rgb(255, 255, 255)'>             
												<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
												<tbody>
													<tr><td align='center'>                         
														<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
															<tbody>
															<tr><td>                                     
																<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
																<tbody>
																	<tr>                                             
																		<td width='100%' height='63' style='max-height: 63px'>                                                 
																			<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																			<tbody>
																				<tr height='10'><td width='100%'></td></tr>
																				<tr>
																					<td align='middle'>                                                             
																						<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																				
																					</td>                                                                                                          
																				</tr>                                                     
																				<tr>                                                                                                                 
																					<td align='middle'>
																						<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Perdin  </p>
																					</td> 
																				</tr>
																			</tbody>
																			</table>                                             
																		</td>                                         
																	</tr>                                     
																</tbody>
															</table>                                 
														</td>                             
													</tr>                         
													</tbody>
												</table>                     
												</td>                 
												</tr>                                                   
												<tr style='font-size: 14px'>                     
												<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
													<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
														<tbody>
															<tr>                                 
																<td>                                     
																	<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																		<tbody>
																			<tr>                                             
																				<td align='center'>                                                 
																					<table align='center' border='0' width='90%'>                                                                            
																					<tbody>
																							<tr>                                                         
																								<td align='left'>                                                             
																									<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																									<strong> Dear ".$jk_receipt." ".$receipt_name."</strong>
																									</p>    	
																								</td>                                                     
																							</tr>
																						</tbody>
																					</table>
																				</td>                                         
																			</tr>
																			<tr>                                             
																				<td align='center'>                                                 
																					<table align='center' border='0' width='90%'>                                                                            
																					<tbody>
																						<tr>                                                         
																							<td align='left'> 																					
																								<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																								".$jk_sender." ".$sender_name." ".$keterangan." ".$doc_number.". 
																								</p>                                                                    
																							</td>                                                     
																						</tr>                                                                                                      
																					</tbody>
																					</table>                                             
																				</td>                                         
																			</tr>                                         
																																						 
																			<tr>	
																				<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																					<tr>                                                         
																						<td align='center'>                                                             
																							<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																						</td>  
																					</tr>  
																				</tbody>
																				</table>                                             
																				</td>
																			</tr>                                        
																			<tr height='10'><td width='100%'></td></tr>     
																		</tbody>
																	</table>                                 
																</td>                             
															</tr>                         
														</tbody>
														</table>                     
														</td>                 
													</tr>                              
												</tbody>
												</table>         
											</td></tr> 
										</tbody>
										</table>   
										</div>						
										";
					}
					
			
					$this->smtp_server($email_to, $subject, $body);
				
					}



			public function email_notif_leave($email_to, $doc_number, $link, $receipt_name, $sender_name, $leave_status=NULL, $jk_sender=NULL, $jk_receipt=NULL, $requester_name=NULL){
							 
				if($leave_status == 'partial approved'){
					$keterangan = 'telah menyetujui dokumen pengajuan cuti dengan nomor dokumen';
				}elseif($leave_status == 'cancel'){
					$keterangan = 'tidak menyetujui pengajuan cuti dengan nomor dokumen';
				}elseif($leave_status == 'pending'){
					$keterangan = 'telah membuat dokumen pengajuan cuti dengan nomor dokumen';
				}elseif($leave_status == 'partial approved 2'){
					$keterangan = 'telah menyetujui dokumen pengajuan cuti dengan nomor dokumen';
				}else {
					$keterangan = '';
				}

				if(!empty($requester_name)){
					$requester_name = 'a/n '.$requester_name;
				}else {
					$requester_name = '';
				}

				if($jk_sender == 'Male'){
					$jk_sender = 'Bapak';
				}elseif($jk_sender == 'Female'){
					$jk_sender = 'Ibu';
				}else {
					$jk_sender = 'Bapak/Ibu';
				}

				if($jk_receipt == 'Male'){
					$jk_receipt = 'Bapak';
				}elseif($jk_receipt == 'Female'){
					$jk_receipt = 'Ibu';
				}else {
					$jk_receipt = 'Bapak/Ibu';
				}
				
				
				$url_logo   = base_url().'img/uploads/LOGO5.png';
				$subject	= "Notifikasi Pengajuan Cuti";
				if($leave_status != 'cancel'){
					$body		= "<div> 
									<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
									<tbody>
										<tr><td style='background-color: rgb(255, 255, 255)'>             
											<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
											<tbody>
												<tr><td align='center'>                         
													<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
														<tbody>
														<tr><td>                                     
															<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
															<tbody>
																<tr>                                             
																	<td width='100%' height='63' style='max-height: 63px'>                                                 
																		<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																			<tbody>
																				<tr height='10'><td width='100%'></td></tr>
																				<tr>
																					<td align='middle'>                                                             
																						<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>
																						
																					</td>                                                                                                          
																				</tr>                                                     
																				<tr>                                                                                                                 
																					<td align='middle'>
																						<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Cuti  </p>
																					</td> 
																				</tr>
																			</tbody>
																		</table>                                             
																	</td>                                         
																</tr>                                     
															</tbody>
														</table>                                 
													</td>                             
												</tr>                         
												</tbody>
											</table>                     
											</td>                 
											</tr>                                                   
											<tr style='font-size: 14px'>                     
											<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
												<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
													<tbody>
														<tr>                                 
															<td>                                     
																<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																	<tbody>
																		<tr>                                             
																			<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																						<tr>                                                         
																							<td align='left'>                                                             
																								<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																								<strong> Dear ".$jk_receipt." ".$receipt_name.",</strong>
																								</p>    	
																							</td>                                                     
																						</tr>
																					</tbody>
																				</table>
																			</td>                                         
																		</tr>
																		<tr>                                             
																			<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																						<tr>                                                         
																							<td align='left'>                                                             
																								<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																								 ".$jk_sender." ".$sender_name." ".$keterangan." ".$doc_number." ".$requester_name.". 
																								</p>                                                             
																								<p>Untuk melakukan akses dokumen tsb, silahkan klik link di bawah ini untuk melihat detail & proses approval. </p>                    
																							</td>                                                     
																						</tr>                                                                                                      
																					</tbody>
																				</table>                                             
																			</td>                                         
																		</tr>                                         
																		<tr>                                             
																			<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																				<tr>                                                         
																					<td align='center'>                                                             
																						<a href='".$link."' style='padding: 7px 20px; color: rgb(255, 255, 255); background-color: rgb(0, 102, 181); text-decoration: none; border-radius: 5px' target='_blank'>LINK</a>
																					</td>
																				</tr>   
																			</tbody>
																			</table>                                             
																			</td>                                         
																		</tr>                                         
																		<tr>	
																			<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																					<tr>                                                         
																						<td align='center'>                                                             
																							<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																						</td>  
																					</tr>  
																				</tbody>
																			</table>                                             
																			</td>
																		</tr>                                        
																		<tr height='10'><td width='100%'></td></tr>     
																	</tbody>
																</table>                                 
															</td>                             
														</tr>                         
													</tbody>
													</table>                     
													</td>                 
												</tr>                              
											</tbody>
											</table>         
										</td></tr> 
									</tbody>
									</table>   
									</div>						
									";
				}else{
					$body		= "<div> 
									<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
									<tbody>
										<tr><td style='background-color: rgb(255, 255, 255)'>             
											<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
											<tbody>
												<tr><td align='center'>                         
													<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
														<tbody>
														<tr><td>                                     
															<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
															<tbody>
																<tr>                                             
																	<td width='100%' height='63' style='max-height: 63px'>                                                 
																		<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																		<tbody>
																			<tr height='10'><td width='100%'></td></tr>
																			<tr>
																				<td align='middle'>                                                             
																					<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																				
																				</td>                                                                                                          
																			</tr>                                                     
																			<tr>                                                                                                                 
																				<td align='middle'>
																					<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Cuti  </p>
																				</td> 
																			</tr>
																		</tbody>
																		</table>                                             
																	</td>                                         
																</tr>                                     
															</tbody>
														</table>                                 
													</td>                             
												</tr>                         
												</tbody>
											</table>                     
											</td>                 
											</tr>                                                   
											<tr style='font-size: 14px'>                     
											<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
												<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
													<tbody>
														<tr>                                 
															<td>                                     
																<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																	<tbody>
																		<tr>                                             
																			<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																						<tr>                                                         
																							<td align='left'>                                                             
																								<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																								<strong> Dear ".$jk_receipt." ".$receipt_name.",</strong>
																								</p>    	
																							</td>                                                     
																						</tr>
																					</tbody>
																				</table>
																			</td>                                         
																		</tr>
																		<tr>                                             
																			<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																					<tr>                                                         
																						<td align='left'> 																					
																							<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																							".$jk_sender." ".$sender_name." ".$keterangan." ".$doc_number.". 
																							</p>                                                                    
																						</td>                                                     
																					</tr>                                                                                                      
																				</tbody>
																				</table>                                             
																			</td>                                         
																		</tr>                                         
																											   
																		<tr>	
																			<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																				<tr>                                                         
																					<td align='center'>                                                             
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																					</td>  
																				</tr>  
																			</tbody>
																			</table>                                             
																			</td>
																		</tr>                                        
																		<tr height='10'><td width='100%'></td></tr>     
																	</tbody>
																</table>                                 
															</td>                             
														</tr>                         
													</tbody>
													</table>                     
													</td>                 
												</tr>                              
											</tbody>
											</table>         
										</td></tr> 
									</tbody>
									</table>   
									</div>						
									";
				}
				
		
				$this->smtp_server($email_to, $subject, $body);
				
		}



    public function email_notif($email_to, $document_number, $link, $receipt_name, $sender_name, $leave_status=NULL, $jk_req=NULL){
							 
		if($leave_status == 'partial approved'){
			$keterangan = 'telah menyetujui dokumen pengajuan cuti dengan nomor dokumen';
		}elseif($leave_status == 'cancel'){
			$keterangan = 'tidak menyetujui pengajuan cuti dengan nomor dokumen';
		}elseif($leave_status == 'pending'){
			$keterangan = 'telah membuat dokumen pengajuan cuti dengan nomor dokumen';
		}elseif($leave_status == 'partial approved 2'){
			$keterangan = 'telah menyetujui dokumen pengajuan cuti dengan nomor dokumen';
		}else {
			$keterangan = '';
		}
		
		
		$url_logo   = base_url().'img/uploads/LOGO5.png';
		$subject	= "Notifikasi Pengajuan Cuti";
		if($leave_status != 'cancel'){
			$body		= "<div> 
							<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
							<tbody>
								<tr><td style='background-color: rgb(255, 255, 255)'>             
									<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
									<tbody>
										<tr><td align='center'>                         
											<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
												<tbody>
												<tr><td>                                     
													<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
													<tbody>
														<tr>                                             
															<td width='100%' height='63' style='max-height: 63px'>                                                 
																<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																	<tbody>
																		<tr height='10'><td width='100%'></td></tr>
																		<tr>
																			<td align='middle'>                                                             
																				<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>
																				
																			</td>                                                                                                          
																		</tr>                                                     
																		<tr>                                                                                                                 
																			<td align='middle'>
																				<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Cuti  </p>
																			</td> 
																		</tr>
																	</tbody>
																</table>                                             
															</td>                                         
														</tr>                                     
													</tbody>
												</table>                                 
											</td>                             
										</tr>                         
										</tbody>
									</table>                     
									</td>                 
									</tr>                                                   
									<tr style='font-size: 14px'>                     
									<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
										<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
											<tbody>
												<tr>                                 
													<td>                                     
														<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
															<tbody>
																<tr>                                             
																	<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																				<tr>                                                         
																					<td align='left'>                                                             
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																						<strong> Dear Bapak/Ibu. ".$receipt_name."</strong>
																						</p>    	
																					</td>                                                     
																				</tr>
																			</tbody>
																		</table>
																	</td>                                         
																</tr>
																<tr>                                             
																	<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																				<tr>                                                         
																					<td align='left'>                                                             
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																						 Bapak/Ibu. ".$sender_name." ".$keterangan." ".$document_number.". 
																						</p>                                                             
																						<p>Untuk melakukan akses dokumen ".$document_number.", silahkan klik link di bawah ini: </p>                    
																					</td>                                                     
																				</tr>                                                                                                      
																			</tbody>
																		</table>                                             
																	</td>                                         
																</tr>                                         
																<tr>                                             
																	<td align='center'>                                                 
																	<table align='center' border='0' width='90%'>                                                                            
																	<tbody>
																		<tr>                                                         
																			<td align='center'>                                                             
																				<a href='".$link."' style='padding: 7px 20px; color: rgb(255, 255, 255); background-color: rgb(0, 102, 181); text-decoration: none; border-radius: 5px' target='_blank'>LINK</a>
																			</td>
																		</tr>   
																	</tbody>
																	</table>                                             
																	</td>                                         
																</tr>                                         
																<tr>	
																	<td align='center'>                                                 
																	<table align='center' border='0' width='90%'>                                                                            
																	<tbody>
																			<tr>                                                         
																				<td align='center'>                                                             
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																				</td>  
																			</tr>  
																		</tbody>
																	</table>                                             
																	</td>
																</tr>                                        
																<tr height='10'><td width='100%'></td></tr>     
															</tbody>
														</table>                                 
													</td>                             
												</tr>                         
											</tbody>
											</table>                     
											</td>                 
										</tr>                              
									</tbody>
									</table>         
								</td></tr> 
							</tbody>
							</table>   
							</div>						
							";
		}else{
			$body		= "<div> 
							<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
							<tbody>
								<tr><td style='background-color: rgb(255, 255, 255)'>             
									<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
									<tbody>
										<tr><td align='center'>                         
											<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
												<tbody>
												<tr><td>                                     
													<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
													<tbody>
														<tr>                                             
															<td width='100%' height='63' style='max-height: 63px'>                                                 
																<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																<tbody>
																	<tr height='10'><td width='100%'></td></tr>
																	<tr>
																		<td align='middle'>                                                             
																			<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																				
																		</td>                                                                                                          
																	</tr>                                                     
																	<tr>                                                                                                                 
																		<td align='middle'>
																			<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Cuti  </p>
																		</td> 
																	</tr>
																</tbody>
																</table>                                             
															</td>                                         
														</tr>                                     
													</tbody>
												</table>                                 
											</td>                             
										</tr>                         
										</tbody>
									</table>                     
									</td>                 
									</tr>                                                   
									<tr style='font-size: 14px'>                     
									<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
										<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
											<tbody>
												<tr>                                 
													<td>                                     
														<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
															<tbody>
																<tr>                                             
																	<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																				<tr>                                                         
																					<td align='left'>                                                             
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																						<strong> Dear Bapak/Ibu. ".$receipt_name."</strong>
																						</p>    	
																					</td>                                                     
																				</tr>
																			</tbody>
																		</table>
																	</td>                                         
																</tr>
																<tr>                                             
																	<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='left'> 																					
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																					Bapak/Ibu. ".$sender_name." ".$keterangan." ".$document_number.". 
																					</p>                                                                    
																				</td>                                                     
																			</tr>                                                                                                      
																		</tbody>
																		</table>                                             
																	</td>                                         
																</tr>                                         
																                                       
																<tr>	
																	<td align='center'>                                                 
																	<table align='center' border='0' width='90%'>                                                                            
																	<tbody>
																		<tr>                                                         
																			<td align='center'>                                                             
																				<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																			</td>  
																		</tr>  
																	</tbody>
																	</table>                                             
																	</td>
																</tr>                                        
																<tr height='10'><td width='100%'></td></tr>     
															</tbody>
														</table>                                 
													</td>                             
												</tr>                         
											</tbody>
											</table>                     
											</td>                 
										</tr>                              
									</tbody>
									</table>         
								</td></tr> 
							</tbody>
							</table>   
							</div>						
							";
			}
	
			$this->smtp_server($email_to, $subject, $body);

		}
		

		public function email_notif_permit_to_requester($email_to, $document_number, $link, $receipt_name, $sender_name, $permit_status=NULL, $permit_start_time=NULL, $permit_end_time=NULL, $dl_name, $gender_requester=NULL){
        
			if($permit_status == 'fully approved'){
				$keterangan = 'telah menyetujui izin dari '.date('h:i A',strtotime($permit_start_time)).' s/d '.date('h:i A',strtotime($permit_end_time)).' dan di proses oleh ';
			}
			if($permit_status == 'cancel'){
				$keterangan = 'tidak menyetujui izin dari '.date('h:i A',strtotime($permit_start_time)).' s/d '.date('h:i A',strtotime($permit_end_time));
			}

				// $this->email_model->_table_name 	= "tbl_employee"; 					//table name
				// $this->email_model->_primary_key 	= "employee_id";  					//primary key
				// $data['employee_info']		    		= $this->email_model->get($employee_id, TRUE); 	// get result
			if($gender_requester=="Male"){
				$gender_requester = "Mr.";
			}else{
				$gender_requester = "Mrs.";
			}
			
			$url_logo   = base_url().'img/uploads/LOGO5.png';
			$subject	= "Notifikasi Pengajuan Izin";
			
			$body		= "<div> 
							<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
							<tbody>
								<tr><td style='background-color: rgb(255, 255, 255)'>             
									<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
									<tbody>
										<tr><td align='center'>                         
											<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
												<tbody>
												<tr><td>                                     
													<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
													<tbody>
														<tr>                                             
															<td width='100%' height='63' style='max-height: 63px'>                                                 
																<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																	<tbody>
																		<tr height='10'><td width='100%'></td></tr>
																		<tr>
																			<td align='middle'>                                                             
																				<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																																						
																			</td>                                                                                                          
																		</tr>                                                     
																		<tr>                                                                                                                 
																			<td align='middle'>
																				<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Izin </p>
																			</td> 
																		</tr>
																	</tbody>
																</table>                                             
															</td>                                         
														</tr>                                     
													</tbody>
												</table>                                 
											</td>                             
										</tr>                         
										</tbody>
									</table>                     
									</td>                 
									</tr>                                                   
									<tr style='font-size: 14px'>                     
									<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
										<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
											<tbody>
												<tr>                                 
													<td>                                     
														<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
															<tbody>
																<tr>                                             
																	<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='left'>                                                             
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																					<strong> Dear ".$gender_requester." ".$receipt_name." </strong>
																					</p>    	
																				</td>                                                     
																			</tr>
																		</tbody>
																		</table>
																	</td>                                         
																</tr>
																<tr>                                             
																	<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='left'>       	
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																					Mr./Mrs. ".$dl_name." ".$keterangan." ".$sender_name.". 
																					</p>                                                                          
																				</td>                                                     
																			</tr>                                                                                                      
																		</tbody>
																		</table>                                             
																	</td>                                         
																</tr>                                         
																																			 
																<tr>	
																	<td align='center'>                                                 
																	<table align='center' border='0' width='90%'>                                                                            
																	<tbody>
																		<tr>                                                         
																			<td align='center'>                                                             
																				<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																			</td>  
																		</tr>  
																	</tbody>
																	</table>                                             
																	</td>
																</tr>                                        
																<tr height='10'><td width='100%'></td></tr>     
															</tbody>
														</table>                                 
													</td>                             
												</tr>                         
											</tbody>
											</table>                     
											</td>                 
										</tr>                              
									</tbody>
									</table>         
								</td></tr> 
							</tbody>
							</table>   
							</div>						
							";
	
	
				$this->smtp_server($email_to, $subject, $body);
			
			}	


			public function email_notif_perdin_to_requester($email_to, $doc_number, $link, $receipt_name, $sender_name, $perdin_status=NULL, $perdin_start_date=NULL, $perdin_end_date=NULL, $dl_name, $gender_requester=NULL){
        
				if($perdin_status == 'fully approved'){
					$keterangan = 'telah menyetujui perdin dari '.date('d M y',strtotime($perdin_start_date)).' s/d '.date('d M y',strtotime($perdin_end_date)).' dan di proses oleh ';
				}
				if($perdin_status == 'cancel'){
					$keterangan = 'tidak menyetujui perdin dari '.date('d M y',strtotime($perdin_start_time)).' s/d '.date('d M y',strtotime($perdin_end_date));
				}
	
					// $this->email_model->_table_name 	= "tbl_employee"; 					//table name
					// $this->email_model->_primary_key 	= "employee_id";  					//primary key
					// $data['employee_info']		    		= $this->email_model->get($employee_id, TRUE); 	// get result
				if($gender_requester=="Male"){
					$gender_requester = "Mr.";
				}else{
					$gender_requester = "Mrs.";
				}
				
				$url_logo   = base_url().'img/uploads/LOGO5.png';
				$subject	= "Notifikasi Pengajuan Perdin";
				
				$body		= "<div> 
								<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
								<tbody>
									<tr><td style='background-color: rgb(255, 255, 255)'>             
										<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
										<tbody>
											<tr><td align='center'>                         
												<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
													<tbody>
													<tr><td>                                     
														<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
														<tbody>
															<tr>                                             
																<td width='100%' height='63' style='max-height: 63px'>                                                 
																	<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																		<tbody>
																			<tr height='10'><td width='100%'></td></tr>
																			<tr>
																				<td align='middle'>                                                             
																					<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																																						
																				</td>                                                                                                          
																			</tr>                                                     
																			<tr>                                                                                                                 
																				<td align='middle'>
																					<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Perdin </p>
																				</td> 
																			</tr>
																		</tbody>
																	</table>                                             
																</td>                                         
															</tr>                                     
														</tbody>
													</table>                                 
												</td>                             
											</tr>                         
											</tbody>
										</table>                     
										</td>                 
										</tr>                                                   
										<tr style='font-size: 14px'>                     
										<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
											<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
												<tbody>
													<tr>                                 
														<td>                                     
															<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																<tbody>
																	<tr>                                             
																		<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																				<tr>                                                         
																					<td align='left'>                                                             
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																						<strong> Dear ".$gender_requester." ".$receipt_name." </strong>
																						</p>    	
																					</td>                                                     
																				</tr>
																			</tbody>
																			</table>
																		</td>                                         
																	</tr>
																	<tr>                                             
																		<td align='center'>                                                 
																			<table align='center' border='0' width='90%'>                                                                            
																			<tbody>
																				<tr>                                                         
																					<td align='left'>       	
																						<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																						Mr./Mrs. ".$dl_name." ".$keterangan." ".$sender_name.". 
																						</p>                                                                          
																					</td>                                                     
																				</tr>                                                                                                      
																			</tbody>
																			</table>                                             
																		</td>                                         
																	</tr>                                         
																																				 
																	<tr>	
																		<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='center'>                                                             
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																				</td>  
																			</tr>  
																		</tbody>
																		</table>                                             
																		</td>
																	</tr>                                        
																	<tr height='10'><td width='100%'></td></tr>     
																</tbody>
															</table>                                 
														</td>                             
													</tr>                         
												</tbody>
												</table>                     
												</td>                 
											</tr>                              
										</tbody>
										</table>         
									</td></tr> 
								</tbody>
								</table>   
								</div>						
								";
		
		
					$this->smtp_server($email_to, $subject, $body);
				
				}	

				

		public function email_notif_overtime_to_requester($email_to, $document_number, $link, $receipt_name, $sender_name, $overtime_status=NULL, $overtime_start_time=NULL, $overtime_end_time=NULL, $dl_name, $gender_requester=NULL, $overtime_date=NULL){
			
			if($overtime_status == 'fully approved'){
				$keterangan = 'telah menyetujui overtime pada tanggal '.date('d M Y', strtotime($overtime_date)).' dari pukul '.date('h:i A',strtotime($overtime_start_time)).' s/d '.date('h:i A',strtotime($overtime_end_time)).' dan di proses oleh ';
			}
			if($overtime_status == 'cancel'){
				$keterangan = 'tidak menyetujui overtime pada tanggal '.date('d M Y', strtotime($overtime_date)).' dari pukul '.date('h:i A',strtotime($overtime_start_time)).' s/d '.date('h:i A',strtotime($overtime_end_time));
			}
				
			if($gender_requester=="Male"){
				$gender_requester = "Mr.";
			}else{
				$gender_requester = "Mrs.";
			}
			
			$url_logo   = base_url().'img/uploads/LOGO5.png';
			$subject	= "Notifikasi Pengajuan Lembur";
			
			$body		= "<div> 
							<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
							<tbody>
								<tr><td style='background-color: rgb(255, 255, 255)'>             
									<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
									<tbody>
										<tr><td align='center'>                         
											<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
												<tbody>
												<tr><td>                                     
													<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
													<tbody>
														<tr>                                             
															<td width='100%' height='63' style='max-height: 63px'>                                                 
																<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																	<tbody>
																		<tr height='10'><td width='100%'></td></tr>
																		<tr>
																			<td align='middle'>                                                             
																				<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																																						
																			</td>                                                                                                          
																		</tr>                                                     
																		<tr>                                                                                                                 
																			<td align='middle'>
																				<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Lembur </p>
																			</td> 
																		</tr>
																	</tbody>
																</table>                                             
															</td>                                         
														</tr>                                     
													</tbody>
												</table>                                 
											</td>                             
										</tr>                         
										</tbody>
									</table>                     
									</td>                 
									</tr>                                                   
									<tr style='font-size: 14px'>                     
									<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
										<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
											<tbody>
												<tr>                                 
													<td>                                     
														<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
															<tbody>
																<tr>                                             
																	<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='left'>                                                             
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																					<strong> Dear ".$gender_requester." ".$receipt_name." </strong>
																					</p>    	
																				</td>                                                     
																			</tr>
																		</tbody>
																		</table>
																	</td>                                         
																</tr>
																<tr>                                             
																	<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='left'>       	
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																					Mr./Mrs. ".$dl_name." ".$keterangan." ".$sender_name.". 
																					</p>                                                                          
																				</td>                                                     
																			</tr>                                                                                                      
																		</tbody>
																		</table>                                             
																	</td>                                         
																</tr>                                         
																																				
																<tr>	
																	<td align='center'>                                                 
																	<table align='center' border='0' width='90%'>                                                                            
																	<tbody>
																		<tr>                                                         
																			<td align='center'>                                                             
																				<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																			</td>  
																		</tr>  
																	</tbody>
																	</table>                                             
																	</td>
																</tr>                                        
																<tr height='10'><td width='100%'></td></tr>     
															</tbody>
														</table>                                 
													</td>                             
												</tr>                         
											</tbody>
											</table>                     
											</td>                 
										</tr>                              
									</tbody>
									</table>         
								</td></tr> 
							</tbody>
							</table>   
							</div>						
							";
	
					$this->smtp_server($email_to, $subject, $body);
			
			}	

	
	
	public function email_notif_to_requester($email_to, $doc_number, $link, $receipt_name, $sender_name, $leave_status=NULL, $leave_start_date=NULL, $leave_end_date=NULL, $jk_sender=NULL, $jk_receipt=NULL){
        
		if($leave_status == 'fully approved'){
			$keterangan = 'telah memproses pengajuan cuti dari tanggal '.date('d M Y',strtotime($leave_start_date)).' sampai dengan tanggal '.date('d M Y',strtotime($leave_end_date)).' dari nomor dokumen';
		}elseif($leave_status == 'cancel'){
			$keterangan = 'tidak memproses pengajuan cuti dari tanggal '.date('d M Y',strtotime($leave_start_date)).' sampai dengan tanggal '.date('d M Y',strtotime($leave_end_date)).' dari nomor dokumen';
		}else {
			$keterangan = '';
		}

		if($jk_receipt == 'Male'){
			$jk_receipt = 'Bapak';
		}elseif($jk_receipt == 'Female'){
			$jk_receipt = 'Ibu';
		}else {
			$jk_receipt = 'Bapak/Ibu';
		}

		if($jk_sender == 'Male'){
			$jk_sender = 'Bapak';
		}elseif($jk_sender == 'Female'){
			$jk_sender = 'Ibu';
		}else {
			$jk_sender = 'Bapak/Ibu';
		}
		
		$url_logo   = base_url().'img/uploads/LOGO5.png';
		$subject	= "Notifikasi Pengajuan Cuti";
		
		$body		= "<div> 
						<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
						<tbody>
							<tr><td style='background-color: rgb(255, 255, 255)'>             
								<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
								<tbody>
									<tr><td align='center'>                         
										<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
											<tbody>
											<tr><td>                                     
												<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
												<tbody>
													<tr>                                             
														<td width='100%' height='63' style='max-height: 63px'>                                                 
															<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																<tbody>
																	<tr height='10'><td width='100%'></td></tr>
																	<tr>
																		<td align='middle'>                                                             
																			<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>                                                                            
                                    									</td>                                                                                                          
																	</tr>                                                     
																	<tr>                                                                                                                 
																		<td align='middle'>
																			<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Cuti  </p>
																		</td> 
																	</tr>
																</tbody>
															</table>                                             
														</td>                                         
													</tr>                                     
												</tbody>
											</table>                                 
										</td>                             
									</tr>                         
									</tbody>
								</table>                     
								</td>                 
								</tr>                                                   
								<tr style='font-size: 14px'>                     
								<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
									<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
										<tbody>
											<tr>                                 
												<td>                                     
													<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
														<tbody>
															<tr>                                             
																<td align='center'>                                                 
																	<table align='center' border='0' width='90%'>                                                                            
																	<tbody>
																		<tr>                                                         
																			<td align='left'>                                                             
																				<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																				<strong> Dear ".$jk_receipt." ".$receipt_name.",</strong>
																				</p>    	
																			</td>                                                     
																		</tr>
																	</tbody>
																	</table>
																</td>                                         
															</tr>
															<tr>                                             
																<td align='center'>                                                 
																	<table align='center' border='0' width='90%'>                                                                            
																	<tbody>
																		<tr>                                                         
																			<td align='left'>       	
																				<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																				".$jk_sender." ".$sender_name." ".$keterangan." ".$doc_number.". 
																				</p>                                                                          
																			</td>                                                     
																		</tr>                                                                                                      
																	</tbody>
																	</table>                                             
																</td>                                         
															</tr>                                         
															                                       
															<tr>	
																<td align='center'>                                                 
																<table align='center' border='0' width='90%'>                                                                            
																<tbody>
																	<tr>                                                         
																		<td align='center'>                                                             
																			<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																		</td>  
																	</tr>  
																</tbody>
																</table>                                             
																</td>
															</tr>                                        
															<tr height='10'><td width='100%'></td></tr>     
														</tbody>
													</table>                                 
												</td>                             
											</tr>                         
										</tbody>
										</table>                     
										</td>                 
									</tr>                              
								</tbody>
								</table>         
							</td></tr> 
						</tbody>
						</table>   
						</div>						
						";

				$this->smtp_server($email_to, $subject, $body);
		
		}

		
		public function email_notif_to_hybris($email_hybris, $receipt_name, $hybris_name, $leave_start_date, $leave_end_date, $jk_hybris=NULL, $jk_receipt=NULL){

			if($jk_hybris == 'Male'){
				$jk_hybris = 'Bapak';
			}elseif($jk_hybris == 'Female'){
				$jk_hybris = 'Ibu';
			}else {
				$jk_hybris = 'Bapak/Ibu';
			}

			if($jk_receipt == 'Male'){
				$jk_receipt = 'Bapak';
			}elseif($jk_receipt == 'Female'){
				$jk_receipt = 'Ibu';
			}else {
				$jk_receipt = 'Bapak/Ibu';
			}

			$url_logo   = base_url().'img/uploads/LOGO5.png';
			$subject	= "Notifikasi Pengajuan Cuti";
			
			$body		= "<div> 
							<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
							<tbody>
								<tr><td style='background-color: rgb(255, 255, 255)'>             
									<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
									<tbody>
										<tr><td align='center'>                         
											<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
												<tbody>
												<tr><td>                                     
													<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
													<tbody>
														<tr>                                             
															<td width='100%' height='63' style='max-height: 63px'>                                                 
																<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																	<tbody>
																		<tr height='10'><td width='100%'></td></tr>
																		<tr>
																			<td align='middle'>                                                             
																				<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																			
																			</td>                                                                                                          
																		</tr>                                                     
																		<tr>                                                                                                                 
																			<td align='middle'>
																				<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Cuti  </p>
																			</td> 
																		</tr>
																	</tbody>
																</table>                                             
															</td>                                         
														</tr>                                     
													</tbody>
												</table>                                 
											</td>                             
										</tr>                         
										</tbody>
									</table>                     
									</td>                 
									</tr>                                                   
									<tr style='font-size: 14px'>                     
									<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
										<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
											<tbody>
												<tr>                                 
													<td>                                     
														<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
															<tbody>
																<tr>                                             
																	<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='left'>                                                             
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																					<strong> Dear ".$jk_hybris." ".$hybris_name.",</strong>
																					</p>    	
																				</td>                                                     
																			</tr>
																		</tbody>
																		</table>
																	</td>                                         
																</tr>
																<tr>                                             
																	<td align='center'>                                                 
																		<table align='center' border='0' width='90%'>                                                                            
																		<tbody>
																			<tr>                                                         
																				<td align='left'>      	
																					<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																					 Pengajuan cuti a/n ".$jk_receipt." ".$receipt_name." dari tanggal ".date('d M Y',strtotime($leave_start_date))." sampai dengan tanggal ".date('d M Y',strtotime($leave_end_date))." sudah fully approved. 
																					</p>                                                                          
																				</td>                                                     
																			</tr>                                                                                                      
																		</tbody>
																		</table>                                             
																	</td>                                         
																</tr>                                         																			 
																<tr>	
																	<td align='center'>                                                 
																	<table align='center' border='0' width='90%'>                                                                            
																	<tbody>
																		<tr>                                                         
																			<td align='center'>                                                             
																				<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																			</td>  
																		</tr>  
																	</tbody>
																	</table>                                             
																	</td>
																</tr>                                        
																<tr height='10'><td width='100%'></td></tr>     
															</tbody>
														</table>                                 
													</td>                             
												</tr>                         
											</tbody>
											</table>                     
											</td>                 
										</tr>                              
									</tbody>
									</table>         
								</td></tr> 
							</tbody>
							</table>   
							</div>						
							";
	
			
			//require '../PHPMailer-master/PHPMailerAutoload.php';
			
			//require './PHPMailer-master/PHPMailerAutoload.php';
	
			$mail = new PHPMailer;
	
			##optional security
			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);
	
			//$mail->SMTPDebug = 4;                               	    // Enable verbose debug output
	
			$mail->isSMTP();                                      	    // Set mailer to use SMTP
			
			$mail->Host         = 'asmara.iixcp.rumahweb.com
';  					// Specify main and backup SMTP servers
			$mail->SMTPAuth     = true;                               	// Enable SMTP authentication
			$mail->Username     = 'noreply@hrms.oneunivrs.id
';            	    // SMTP username
			$mail->Password     = 'Sendaljepit01';                         // SMTP password
			$mail->SMTPSecure   = 'ssl';                            	// Enable TLS encryption, `ssl` also accepted
			$mail->Port         = 465;                                  // TCP port to connect to
	
			$mail->setFrom('noreply@hrms.oneunivrs.id
', 'Noreply');
			$mail->addAddress($email_hybris);     						// Add a recipient
			$mail->addReplyTo('noreply@hrms.oneunivrs.id
', 'Noreply');
			//$mail->addCC('ebes.doni@gmail.com');
			
			$mail->isHTML(true);                                  		// Set email format to HTML
	
			$mail->Subject = $subject;
			$mail->Body    = $body;
			$mail->send();
			
			}



			


				public function email_notif_overtime($email_to, $document_number, $link, $receipt_name, $sender_name, $overtime_status=NULL){
							 
					if($overtime_status == 'partial approved'){
						$keterangan = 'telah menyetujui dokumen pengajuan lembur / overtime dari nomor dokumen';
					}
					if($overtime_status == 'cancel'){
						$keterangan = 'tidak menyetujui pengajuan lembur / overtime dari nomor dokumen';
					}
					if($overtime_status == 'pending'){
						$keterangan = 'telah membuat dokumen pengajuan lembur / overtime dari nomor dokumen';
					}
					
					
					$url_logo   = base_url().'img/uploads/LOGO5.png';
					$subject	= "Notifikasi Pengajuan Lembur";
					if($overtime_status != 'cancel'){
						$body		= "<div> 
										<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
										<tbody>
											<tr><td style='background-color: rgb(255, 255, 255)'>             
												<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
												<tbody>
													<tr><td align='center'>                         
														<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
															<tbody>
															<tr><td>                                     
																<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
																<tbody>
																	<tr>                                             
																		<td width='100%' height='63' style='max-height: 63px'>                                                 
																			<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																				<tbody>
																					<tr height='10'><td width='100%'></td></tr>
																					<tr>
																						<td align='middle'>                                                             
																							<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>
																							
																						</td>                                                                                                          
																					</tr>                                                     
																					<tr>                                                                                                                 
																						<td align='middle'>
																							<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Lembur  </p>
																						</td> 
																					</tr>
																				</tbody>
																			</table>                                             
																		</td>                                         
																	</tr>                                     
																</tbody>
															</table>                                 
														</td>                             
													</tr>                         
													</tbody>
												</table>                     
												</td>                 
												</tr>                                                   
												<tr style='font-size: 14px'>                     
												<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
													<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
														<tbody>
															<tr>                                 
																<td>                                     
																	<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																		<tbody>
																			<tr>                                             
																				<td align='center'>                                                 
																					<table align='center' border='0' width='90%'>                                                                            
																					<tbody>
																							<tr>                                                         
																								<td align='left'>                                                             
																									<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																									<strong> Dear Bapak/Ibu. ".$receipt_name."</strong>
																									</p>    	
																								</td>                                                     
																							</tr>
																						</tbody>
																					</table>
																				</td>                                         
																			</tr>
																			<tr>                                             
																				<td align='center'>                                                 
																					<table align='center' border='0' width='90%'>                                                                            
																					<tbody>
																							<tr>                                                         
																								<td align='left'>                                                             
																									<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																									 Bapak/Ibu. ".$sender_name." ".$keterangan." ".$document_number.". 
																									</p>                                                             
																									<p>Untuk melakukan akses dokumen ".$document_number.", silahkan klik link di bawah ini: </p>                    
																								</td>                                                     
																							</tr>                                                                                                      
																						</tbody>
																					</table>                                             
																				</td>                                         
																			</tr>                                         
																			<tr>                                             
																				<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																					<tr>                                                         
																						<td align='center'>                                                             
																							<a href='".$link."' style='padding: 7px 20px; color: rgb(255, 255, 255); background-color: rgb(0, 102, 181); text-decoration: none; border-radius: 5px' target='_blank'>LINK</a>
																						</td>
																					</tr>   
																				</tbody>
																				</table>                                             
																				</td>                                         
																			</tr>                                         
																			<tr>	
																				<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																						<tr>                                                         
																							<td align='center'>                                                             
																								<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																							</td>  
																						</tr>  
																					</tbody>
																				</table>                                             
																				</td>
																			</tr>                                        
																			<tr height='10'><td width='100%'></td></tr>     
																		</tbody>
																	</table>                                 
																</td>                             
															</tr>                         
														</tbody>
														</table>                     
														</td>                 
													</tr>                              
												</tbody>
												</table>         
											</td></tr> 
										</tbody>
										</table>   
										</div>						
										";
					}else{
						$body		= "<div> 
										<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
										<tbody>
											<tr><td style='background-color: rgb(255, 255, 255)'>             
												<table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
												<tbody>
													<tr><td align='center'>                         
														<table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
															<tbody>
															<tr><td>                                     
																<table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
																<tbody>
																	<tr>                                             
																		<td width='100%' height='63' style='max-height: 63px'>                                                 
																			<table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
																			<tbody>
																				<tr height='10'><td width='100%'></td></tr>
																				<tr>
																					<td align='middle'>                                                             
																						<img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='$url_logo'>																				
																					</td>                                                                                                          
																				</tr>                                                     
																				<tr>                                                                                                                 
																					<td align='middle'>
																						<p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Lembur  </p>
																					</td> 
																				</tr>
																			</tbody>
																			</table>                                             
																		</td>                                         
																	</tr>                                     
																</tbody>
															</table>                                 
														</td>                             
													</tr>                         
													</tbody>
												</table>                     
												</td>                 
												</tr>                                                   
												<tr style='font-size: 14px'>                     
												<td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
													<table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
														<tbody>
															<tr>                                 
																<td>                                     
																	<table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
																		<tbody>
																			<tr>                                             
																				<td align='center'>                                                 
																					<table align='center' border='0' width='90%'>                                                                            
																					<tbody>
																							<tr>                                                         
																								<td align='left'>                                                             
																									<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
																									<strong> Dear Bapak/Ibu. ".$receipt_name."</strong>
																									</p>    	
																								</td>                                                     
																							</tr>
																						</tbody>
																					</table>
																				</td>                                         
																			</tr>
																			<tr>                                             
																				<td align='center'>                                                 
																					<table align='center' border='0' width='90%'>                                                                            
																					<tbody>
																						<tr>                                                         
																							<td align='left'> 																					
																								<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
																								Bapak/Ibu. ".$sender_name." ".$keterangan." ".$document_number.". 
																								</p>                                                                    
																							</td>                                                     
																						</tr>                                                                                                      
																					</tbody>
																					</table>                                             
																				</td>                                         
																			</tr>                                         
																																						 
																			<tr>	
																				<td align='center'>                                                 
																				<table align='center' border='0' width='90%'>                                                                            
																				<tbody>
																					<tr>                                                         
																						<td align='center'>                                                             
																							<p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
																						</td>  
																					</tr>  
																				</tbody>
																				</table>                                             
																				</td>
																			</tr>                                        
																			<tr height='10'><td width='100%'></td></tr>     
																		</tbody>
																	</table>                                 
																</td>                             
															</tr>                         
														</tbody>
														</table>                     
														</td>                 
													</tr>                              
												</tbody>
												</table>         
											</td></tr> 
										</tbody>
										</table>   
										</div>						
										";
					}
					
			
					$this->smtp_server($email_to, $subject, $body);
				
					}

}
