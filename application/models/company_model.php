<?php

/**
 * @author doniebes
 */
class Company_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;

    public function get_company() {
        $this->db->select('company.*', FALSE);
        $this->db->from('company');
        $query_result   = $this->db->get();
        $result         = $query_result->result();
        return $result;
    }

    public function updateCompany($img, $id){       
        $name = $this->input->post('name');
        $logo = $img;
        $updated_at = date("Y-m-d H:i:s");
        $updated_by = $this->session->userdata('full_name');
        
        $data = [
            "name" => $name,
            "logo" => $logo,
            "updated_at" => $updated_at,
            "updated_by" => $updated_by
        ];

        $this->db->where('id', $id);
        $this->db->update('company', $data);
    }

    public function addCompany($img){       
        $name = $this->input->post('name');
        $logo = $img;
        $created_at = date("Y-m-d H:i:s");
        $created_by = $this->session->userdata('full_name');

        $data = [
            "name" => $name,
            "logo" => $logo,
            "created_at" => $created_at,
            "created_by" => $created_by
        ];

        $this->db->insert('company', $data);
    }

    public function uploadImg(){
        $config['upload_path'] = 'img/uploads/company/';
        $config['allowed_types'] = 'jpg|png|jpeg|image/png|image/jpg|image/jpeg';
        $config['max_size'] = '2048';
        $config['file_name'] = round(microtime(true)*1000);

        $this->load->library('upload', $config);
        if($this->upload->do_upload('img')){
            $return = array('result' => 'success', 'file'   => $this->upload->data(), 'error'  => '');
            return $return;
        }else{
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

}
