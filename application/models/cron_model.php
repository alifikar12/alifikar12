<?php

class Cron_Model extends MY_Model {

    public $_table_name;
    public $_order_by;
    public $_primary_key;


    // $result = $this->db->query("SELECT tbl_employee.*, tbl_saldo_cuti.*
    // FROM (`tbl_employee`)
    // LEFT JOIN `tbl_saldo_cuti` ON `tbl_saldo_cuti`.`employee_id` = `tbl_employee`.`employee_id`");

    public function get_employee_cuti() {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_saldo_cuti.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->where('tbl_employee.status =', '1');
        $this->db->join('tbl_saldo_cuti', 'tbl_employee.employee_id = tbl_saldo_cuti.employee_id', 'left');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }   

    public function get_employee_point() {
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->select('tbl_employee_point.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->join('tbl_employee_point', 'tbl_employee.employee_id = tbl_employee_point.employee_id', 'left');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }   

    // SELECT * FROM tbl_employee  WHERE `status` = '1' AND email_2 <> '' AND penempatan <> 'Area' ORDER BY employment_id ASC
    public function get_employee(){
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->where('status =', '1');
        $this->db->where('email_2 <>', '');
        $this->db->where('penempatan <>', 'Area');
        $this->db->order_by('tbl_employee.employment_id', 'ASC');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    // SELECT * FROM tbl_employee  WHERE `status` = '1' AND email_2 <> '' AND penempatan <> 'Area' ORDER BY employment_id ASC
    public function get_email_bod(){
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->where('status =', '1');
        $this->db->where('department_id =', '1');
        $this->db->where('email_2 <>', '');
        $this->db->where('penempatan <>', 'Area');
        $this->db->order_by('tbl_employee.employment_id', 'ASC');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function get_email_employee(){
        $this->db->select('tbl_employee.*', FALSE);
        $this->db->from('tbl_employee');
        $this->db->where('status =', '1');
        $this->db->where('department_id <>', '1');
        $this->db->where('email_2 <>', '');
        $this->db->where('penempatan <>', 'Area');
        $this->db->order_by('tbl_employee.employment_id', 'ASC');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    
    // WHERE PIN = '$PIN' AND `Date` = '$Date' AND StatusAbsen = '$Status_Absen' AND LokasiAbsen = '$LokasiAbsen'";
    public function cek_data_finger($PIN, $Date, $Status_Absen, $LokasiAbsen){
        $this->db->select('finger_att_logs.*', FALSE);
        $this->db->from('finger_att_logs');
        $this->db->where('PIN =', $PIN);
        $this->db->where('Date =', $Date);
        $this->db->where('StatusAbsen =', $Status_Absen);
        $this->db->where('LokasiAbsen =', $LokasiAbsen);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
    
	

}
