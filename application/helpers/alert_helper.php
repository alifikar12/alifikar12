<?php
/**
 * -------------------------------------------------------------------
 * Developed and maintained by Zaman
 * -------------------------------------------------------------------
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

    // if (!function_exists('popup_message_box')) {
    //     function popup_message_box($message_type, $close_button = TRUE)
    //     {
    //         $CI =& get_instance();
    //         $message = $CI->session->flashdata($message_type);
    //         $retval = '';

    //         if($message){
    //             switch($message_type){
    //                 case 'success':
    //                     $retval .= '<div class="modal hide fade" id="myModal">
    //                     <div class="modal-header">
    //                         <a class="close" data-dismiss="modal">×</a>
    //                         <h3>Modal header</h3>
    //                     </div>
    //                     <div class="modal-body"><span style="color:#e92127; font-size:20px; text-align:center;"><strong>';
    //                     break;
    //                 case 'error':
    //                     $retval .= '<div class="modal fade" style="opacity: 1;" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    //                     <div class="modal-dialog modal-dialog-centered" role="document">
    //                         <div class="modal-content">                            
    //                         <div class="modal-body"><span style="color:#e92127; font-size:20px; text-align:center;"><strong>';
    //                     break;
    //                 case 'info':
    //                     $retval .= '<div class="alert alert-info">';
    //                     break;
    //                 case 'warning':
    //                     $retval .= '<div class="alert alert-warning">';
    //                     break;
    //             }
    
    //             // if($close_button)
    //                 // $retval .= '<a class="close" data-dismiss="alert" href="#">&times;</a>';
    
    //             $retval .= $message;
    //             $retval .= '</strong></span></div>
    //             <div class="modal-footer">
    //                 <a href="#" class="btn">Close</a>
    //                 <a href="#" class="btn btn-primary">Save changes</a>
    //             </div>
    //         </div>
    //         <script type="text/javascript">
    //             $(window).on("load", function() {
    //                 $("#myModal").modal("show");
    //             });
    //         </script>';
    //             return $retval;
    //         }
    //     }
    // }

    if (!function_exists('popup_message_box')) {
        function popup_message_box($message_type, $close_button = TRUE)
        {
            $CI =& get_instance();
            $message = $CI->session->flashdata($message_type);
            $retval = '';

            if($message){
                switch($message_type){
                    case 'success':
                        $retval .= '<div class="modal fade" style="opacity: 1;" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">                            
                            <div class="modal-body"><span style="color:#e92127; font-size:20px; text-align:center;"><strong>';
                        break;
                    case 'error':
                        $retval .= '<div class="modal fade" style="opacity: 1;" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">                            
                            <div class="modal-body"><span style="color:#e92127; font-size:20px; text-align:center;"><strong>';
                        break;
                    case 'info':
                        $retval .= '<div class="alert alert-info">';
                        break;
                    case 'warning':
                        $retval .= '<div class="alert alert-warning">';
                        break;
                }
    
                // if($close_button)
                    // $retval .= '<a class="close" data-dismiss="alert" href="#">&times;</a>';
    
                $retval .= $message;
                $retval .= '</strong></span></div>                
                </div>
            </div>
            </div>
            <script type="text/javascript">
                $(window).on("load", function() {
                    $("#exampleModalCenter").modal("show");
                });
            </script>';
                return $retval;
            }
        }
    }

if (!function_exists('message_box')) {
    function message_box($message_type, $close_button = TRUE)
    {
        $CI =& get_instance();
        $message = $CI->session->flashdata($message_type);
        $retval = '';

        if($message){
            switch($message_type){
                case 'success':
                    $retval .= '<div class="alert alert-success">';
                    break;
                case 'error':
                    $retval .= '<div class="alert alert-danger">';
                    break;
                case 'info':
                    $retval .= '<div class="alert alert-info">';
                    break;
                case 'warning':
                    $retval .= '<div class="alert alert-warning">';
                    break;
            }

            if($close_button)
                $retval .= '<a class="close" data-dismiss="alert" href="#">&times;</a>';

            $retval .= $message;
            $retval .= '</div>';
            return $retval;
        }
    }
}

if (!function_exists('set_message')){
    function set_message($type, $message)
    {
        $CI =& get_instance();
        $CI->session->set_flashdata($type, $message);
    }
}

if (!function_exists('swal_message_box')) {
    function swal_message_box($message_type, $close_button = TRUE)
    {
        $CI =& get_instance();
        $message = $CI->session->flashdata($message_type);
        $retval = '';

        if($message){
            switch($message_type){
                case 'success':
                    $retval .= "<script type='text/javascript'>
                                $(window).on('load', function() {
                                    Swal.fire(
                                        '".$message."',
                                        'Thanks',
                                        'success'
                                      );
                                });
                                </script>";
                    break;
                case 'error':
                    $retval .= "<script type='text/javascript'>
                                $(window).on('load', function() {
                                    Swal.fire(
                                        '".$message."',
                                        'Try Again',
                                        'error'
                                    );
                                });
                                </script>";
                    break;
                case 'info':
                    $retval .= '<div class="alert alert-info">';
                    break;
                case 'warning':
                    $retval .= '<div class="alert alert-warning">';
                    break;
            }

            // if($close_button)
                // $retval .= '<a class="close" data-dismiss="alert" href="#">&times;</a>';

            // $retval .= $message;
            
            return $retval;
        }
    }
}

