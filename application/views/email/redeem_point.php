<?php
if($status == 'partial approved'){
    $keterangan = 'telah menyetujui dokumen pengajuan redeem point dengan nomor dokumen';
}elseif($status == 'cancel'){
    $keterangan = 'tidak menyetujui pengajuan redeem point dengan nomor dokumen';
}elseif($status == 'pending'){
    $keterangan = 'telah membuat dokumen pengajuan redeem point dengan nomor dokumen';
}else {
    $keterangan = '';
}
?>

<div> 
<table style='border-spacing: 0px; border: 1px solid rgb(0, 0, 0)' bgcolor='FFFFFF' width='620' align='center' cellspacing='0' cellpadding='0'>     
<tbody>
    <tr>
        <td style='background-color: rgb(255, 255, 255)'>             
        <table style='border-collapse: collapse; border-spacing: 0px' align='center' cellspacing='0' cellpadding='0' border='0'>                            
        <tbody>
            <tr><td align='center'>                         
                <table style='border-collapse: collapse; border-spacing: 0px' cellspacing='0' cellpadding='0' border='0' width='100%'>                         
                    <tbody>
                    <tr><td>                                     
                        <table width='620' cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='ffffff'>                                         
                        <tbody>
                            <tr>                                             
                                <td width='100%' height='63' style='max-height: 63px'>                                                 
                                    <table cellspacing='0' cellpadding='0' border='0' align='center' width='560'>                                               
                                        <tbody>
                                            <tr height='10'><td width='100%'></td></tr>
                                            <tr>
                                                <td align='middle'>                                                             
                                                    <img id='1549440931489110001_imgsrc_url_0' border='0' alt='dsb' width='250' height='70' src='<?= base_url().'img/uploads/logo_pmki.png'; ?>'>
                                                    
                                                </td>                                                                                                          
                                            </tr>                                                     
                                            <tr>                                                                                                                 
                                                <td align='middle'>
                                                    <p style='font-size: 16px; color: rgb(51, 51, 51); line-height: 140%'> Notifikasi Pengajuan Redeem Point  </p>
                                                </td> 
                                            </tr>
                                        </tbody>
                                    </table>                                             
                                </td>                                         
                            </tr>                                     
                        </tbody>
                    </table>                                 
                </td>                             
            </tr>                         
            </tbody>
        </table>                     
        </td>                 
        </tr>                                                   
        <tr style='font-size: 14px'>                     
        <td bgcolor='#ffffff' style='background-color: rgb(255, 255, 255)' align='center'>                         
            <table width='620' cellspacing='0' cellpadding='0' border='0' style='border-spacing: 0px'>                            
                <tbody>
                    <tr>                                 
                        <td>                                     
                            <table width='90%' cellpadding='0' cellspacing='0' border='0' align='center' style='text-align: center; font-size: 14px; background-color: rgb(255, 255, 255)'>                                         
                                <tbody>
                                    <tr>                                             
                                        <td align='center'>                                                 
                                            <table align='center' border='0' width='90%'>                                                                            
                                            <tbody>
                                                    <tr>                                                         
                                                        <td align='left'>                                                             
                                                            <p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%; margin: 0'>                      
                                                            <strong> Dear <?= $jk_approval." ".$receipt_name; ?></strong>
                                                            </p>    	
                                                        </td>                                                     
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>                                         
                                    </tr>
                                    <tr>                                             
                                        <td align='center'>                                                 
                                            <table align='center' border='0' width='90%'>                                                                            
                                            <tbody>
                                                    <tr>                                                         
                                                        <td align='left'>                                                             
                                                            <p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>                                 
                                                            <?= $jk_sender." ".$sender_name." ".$keterangan." ".$doc_number; ?> 
                                                            </p>                                                             
                                                            <p>Untuk melakukan akses dokumen <?= $doc_number;?>, silahkan klik link di bawah ini: </p>                    
                                                        </td>                                                     
                                                    </tr>                                                                                                      
                                                </tbody>
                                            </table>                                             
                                        </td>                                         
                                    </tr>                                         
                                    <tr>                                             
                                        <td align='center'>                                                 
                                        <table align='center' border='0' width='90%'>                                                                            
                                        <tbody>
                                            <tr>                                                         
                                                <td align='center'>                                                             
                                                    <a href='<?= $link;?>' style='padding: 7px 20px; color: rgb(255, 255, 255); background-color: rgb(0, 102, 181); text-decoration: none; border-radius: 5px' target='_blank'>LINK</a>
                                                </td>
                                            </tr>   
                                        </tbody>
                                        </table>                                             
                                        </td>                                         
                                    </tr>                                         
                                    <tr>	
                                        <td align='center'>                                                 
                                        <table align='center' border='0' width='90%'>                                                                            
                                            <tbody>
                                                <tr>                                                         
                                                    <td align='center'>                                                             
                                                        <p style='font-size: 14px; color: rgb(51, 51, 51); line-height: 140%'>Terimakasih</p>              
                                                    </td>  
                                                </tr>  
                                            </tbody>
                                        </table>                                             
                                        </td>
                                    </tr>                                        
                                    <tr height='10'><td width='100%'></td></tr>     
                                </tbody>
                            </table>                                 
                        </td>                             
                    </tr>                         
                </tbody>
                </table>                     
                </td>                 
            </tr>                              
        </tbody>
        </table>         
        </td>
    </tr> 
</tbody>
</table>   
</div>