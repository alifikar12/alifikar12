
<!--
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" type="text/css" media="all">
-->
<div class="col-md-12">
    <div class="row">
        <div class="col-sm-12">
            <div class="wrap-fpanel">
                <div class="panel panel-info" data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Add New Leave Application</strong>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-offset-1">
                            <form id="form" action="<?php echo base_url() ?>employee/dashboard/save_leave_application" method="post"  enctype="multipart/form-data" class="form-horizontal">
                                <div class="panel_controls">
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Leave Category<span class="required"> *</span></label>

                                        <div class="col-sm-5">
                                            <select id="leave_category" name="leave_category_id" class="form-control" required >
                                                <option value="" >Select Leave Category...</option>
                                                <?php foreach ($all_leave_category as $v_category) : ?>
                                                    <option value="<?php echo $v_category->leave_category_id ?>">
                                                        <?php echo $v_category->category ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
									
									<div class="form-group">
										<label id="lbl_saldo_cuti" style="display:none"; class="col-sm-3 control-label">Saldo Cuti <span class="required"> *</span></label>									
										<div class="col-sm-5">
											<input type="text" class="form-control" style="display:none" name="saldo_cuti" value="<?php echo $all_emplyee_info->saldo_cuti; ?>" readonly="true">
										</div>									
									</div>
									
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Start Date <span class="required"> *</span></label>

                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="text" name="leave_start_date" id="leave_start_date" required class="form-control datepicker" value="" data-format="dd-mm-yyyy">
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="entypo-calendar"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">End Date <span class="required"> *</span></label>

                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="text" name="leave_end_date" id="leave_end_date"  required class="form-control datepicker" value="" data-format="dd-mm-yyyy">
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="entypo-calendar"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Reason</label>

                                        <div class="col-sm-5">
                                            <textarea id="present" name="reason" class="form-control" rows="6"></textarea>
                                        </div>
                                    </div>
                                    <!--post parameter email notif--> 
                                    <div class="form-group">    
                                            <input type="hidden" name="email_dl" value="<?php echo $all_emplyee_info->direct_leader_email; ?>" readonly="true">                                     
                                            <input type="hidden" name="document_number" value="<?php echo $no_leave->no_leave; ?>" readonly="true">
                                            <input type="hidden" name="link" value="<?php echo base_url() ?>login?link=employee/dashboard/view_application_inquiry/<?php echo $no_leave->no_leave; ?>" readonly="true">
                                            <input type="hidden" name="receipt_name" value="<?php echo $all_emplyee_info->direct_leader_name; ?>" readonly="true">
                                            <input type="hidden" name="sender_name" value="<?php echo $all_emplyee_info->first_name .' '.$all_emplyee_info->last_name ; ?>" readonly="true">
                                            <input type="hidden" name="jk_sender" value="<?php echo $all_emplyee_info->gender; ?>" readonly="true">
                                            <input type="hidden" name="jk_receipt" value="<?php echo $all_emplyee_info->direct_leader_gender; ?>" readonly="true">
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-5">
                                            <button type="submit" id="sbtn" name="sbtn" value="1" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
    
    $("#leave_category").change(function() {
        
        if (jQuery(this).val() === '3'){ 
            jQuery('input[name=saldo_cuti]').show(); 
			jQuery('label[id="lbl_saldo_cuti"]').show(); 	
            // Pday = 7;
            // $("#leave_start_date").datepicker({ startDate:addDays(new Date(), 7)});
        } 
        else if(jQuery(this).val() === '12') {
           
            jQuery('input[name=saldo_cuti]').hide(); 
			jQuery('label[id="lbl_saldo_cuti"]').hide();
            // Pday = 14;
            // $("#leave_start_date").datepicker({ startDate:addDays(new Date(), 14)});
        }
        else {
            jQuery('input[name=saldo_cuti]').hide(); 
			jQuery('label[id="lbl_saldo_cuti"]').hide();
            // Pday = 7;
            // $("#leave_start_date").datepicker({ startDate:addDays(new Date(), 7)});
        }

    });
    
            //set bisa apply cuti minimal 7 hari dari tanggal apply 
            function addDays(dateObj, numDays) {
                dateObj.setDate(dateObj.getDate() + numDays);
                return dateObj;
            }

    // $("#leave_category").change(function() {
    //     // set jika leave category tdk sama dengan 5 atau sakit tanpa keterangan dokter 
    //     if (jQuery(this).val() !== '5'){ 
           
            // console.log('Pday : ', Pday);
            //disable past date datepicker
            <?php if($backdate_cuti->enable != 1):?>
                $("#leave_start_date").datepicker({ startDate:addDays(new Date(), 7)});
                // $("#leave_end_date").datepicker({ startDate:new Date()});  

                $("#leave_start_date").datepicker({
                    todayBtn:  1,
                    autoclose: true,
                }).on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf());
                    $('#leave_end_date').datepicker('setStartDate', minDate);
                });
                
                $("#leave_end_date").datepicker()
                .on('changeDate', function (selected) {
                    var minDate = new Date(selected.date.valueOf());
                    $('#leave_start_date').datepicker('setEndDate', minDate);
                });
            <?php endif;?>



    //     } 
    // });
        
       
        
    
});
</script>

