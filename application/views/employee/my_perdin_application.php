
<div class="col-md-12">
    <?php include_once 'asset/admin-ajax.php'; ?>
    <?php echo message_box('success'); ?>
    <?php echo message_box('error'); ?>

    
    <br/>

    <div class="row">
    
        <div class="col-sm-12">      
        <h4><?php echo anchor('employee/dashboard/apply_perdin_application', '<i class="fa fa-plus"></i> Apply New Perdin Application'); ?></h4>                      
            <div class="panel panel-info">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>My Perdin Applications</strong>
                    </div>
                </div>
                <div class="table-responsive">
                <table class="table table-bordered table-hover" id="dataTables-example">
                    <thead>                                     
                        <tr style="font-size: 13px;color: #000000">   
							<th>No</th>
							<th>Perdin ID</th>
                            <th class="col-sm-3">Perdin Category</th>
                            <th class="col-sm-1">Start Time</th>
                            <th class="col-sm-1">End Time</th>
                            <th>Reason</th>
                            <th class="col-sm-1">Applied On</th>
                            <th class="col-sm-1">Status</th>
                            <th>Action</th>   
                        </tr>
                    </thead>                
                    <tbody style="margin-bottom: 0px;background: #FFFFFF;font-size: 12px;">                                                                   
                        <?php 
							$i=1;
							if (!empty($all_permit_applications)): 
								foreach ($all_permit_applications as $v_application) : 
						?>

                                <tr>      
									<td><?php echo $i ?></td>
									<td><?php echo $v_application->perdin_id ?></td>
                                    <td><?php echo $v_application->category ?></td>
                                    <td><?php echo date('d M Y', strtotime($v_application->perdin_start_date)) ?></td>
                                    <td><?php echo date('d M Y', strtotime($v_application->perdin_end_date)) ?></td>
                                    <td><?php echo $v_application->reason ?></td>                                                                        
                                    <td><?php echo date('d M Y', strtotime($v_application->submit_date)) ?></td>
                                    <td><?php 									
                                        if ($v_application->perdin_status == 'pending') {
                                            echo '<span class="label label-warning">'.$v_application->perdin_status.'</span>';
                                        } elseif ($v_application->perdin_status == 'fully approved') {
                                            echo '<span class="label label-success">'.$v_application->perdin_status.'</span>';
                                        } elseif ($v_application->perdin_status == 'partial approved') {
                                            echo '<span class="label label-info">'.$v_application->perdin_status.'</span>';
                                        } else {
                                            echo '<span class="label label-danger">cancel</span>';
                                        }
									
                                        ?>
                                    </td>     
                                    <td><?php echo btn_view('employee/dashboard/view_my_perdin/' . $v_application->perdin_id) ?></td>                                                                                  
                                </tr>
                                <?php
									$i++;
									endforeach;
								?>
                        <?php else : ?>
                        <td colspan="3">
                            <strong>There is no data to display</strong>
                        </td>
                    <?php endif; ?>
                    </tbody>                    
                </table>
                </div>
            </div>
        </div>
    </div>
</div>



