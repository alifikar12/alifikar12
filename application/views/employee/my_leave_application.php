
<div class="col-md-12">
    <?php include_once 'asset/admin-ajax.php'; ?>
    <?php echo message_box('success'); ?>
    <?php echo message_box('error'); ?>

    

    <div class="row">   
        <div class="col-sm-12">                            
            <div class="panel panel-info">
                <br/>
                <h4><?php echo anchor('employee/dashboard/apply_leave_application', '<i class="fa fa-plus"></i> Apply New Leave Application'); ?></h4>
                
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>My Leave Applications</strong>
                    </div>
                </div>
                
                <div class="table-responsive">
                <!-- <div style="overflow-x:auto;"> -->
                <table class="table table-bordered table-hover" id="dataTables-example" >
                <!-- <table style="table-layout: fixed;  overflow-x: auto;" class="table table-bordered table-hover"  id="dataTables-example"> -->
                    <thead >                                     
                        <tr style="font-size: 13px;color: #000000">   
							<th>No</th>
							<!-- <th>ID</th> -->
                            <th class="col-sm-3">Leave Category</th>
                            <th class="col-sm-1">Start Date</th>
                            <th class="col-sm-1">End Date</th>
                            <th class="col-sm-1">Sisa Cuti</th>
                            <th>Reason</th>
                            <th class="col-sm-1">Applied On</th>
                            <th class="col-sm-1">Status</th>
                            <th>Action</th>   
                        </tr>
                    </thead>                
                    <tbody style="margin-bottom: 0px;background: #FFFFFF;font-size: 12px;">                                                                   
                        <?php 
							$i=1;
							if (!empty($all_leave_applications)): 
								foreach ($all_leave_applications as $v_application) : 
						?>

                                <tr>      
									<td><?php echo $i ?></td>
									<!-- <td><?php echo $v_application->application_list_id ?></td> -->
                                    <td><?php echo $v_application->category ?></td>
                                    <td><?php echo date('d M Y', strtotime($v_application->leave_start_date)) ?></td>
                                    <td><?php echo date('d M Y', strtotime($v_application->leave_end_date)) ?></td>
                                    <td><?php echo $v_application->sisa_cuti ?></td>      
                                    <td><?php echo $v_application->reason ?></td>                                                                        
                                    <td><?php echo date('d M Y', strtotime($v_application->application_date)) ?></td>
                                    <td><?php 									
                                        if ($v_application->leave_status == 'pending') {
                                            echo '<span class="label label-warning">'.$v_application->leave_status.'</span>';
                                        } elseif ($v_application->leave_status == 'fully approved') {
                                            echo '<span class="label label-success">'.$v_application->leave_status.'</span>';
                                        } elseif ($v_application->leave_status == 'partial approved') {
                                            echo '<span class="label label-info">'.$v_application->leave_status.'</span>';
                                        } elseif ($v_application->leave_status == 'partial approved 2') {
                                            echo '<span class="label label-primary">'.$v_application->leave_status.'</span>';
                                        } else {
                                            echo '<span class="label label-danger">cancel</span>';
                                        }
									
                                        ?>
                                    </td>     
                                    <td><?php echo btn_view('employee/dashboard/view_my_application/' . $v_application->application_list_id) ?></td>                                                                                  
                                </tr>
                                <?php
									$i++;
									endforeach;
								?>
                        <?php else : ?>
                        <td colspan="3">
                            <strong>There is no data to display</strong>
                        </td>
                    <?php endif; ?>
                    </tbody>                    
                </table>             
                </div>
            </div>
        </div>
    </div>
</div>



