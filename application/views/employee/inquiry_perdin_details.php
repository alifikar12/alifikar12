<div class="col-md-12">
    <div class="wrap-fpanel">
        <div class="panel panel-default">
            <!-- Default panel contents -->

            <div class="panel-heading">
                <div class="panel-title">                 
                    <strong>perdin Application Details</strong><span class="pull-right"><a style="cursor: pointer"onclick="history.go(-1)" class="view-all-front">Go Back</a></span>
                </div>                    
            </div>    
            <form method="post" action="<?php echo base_url() ?>employee/dashboard/proses_perdin_by_dl/<?php echo $perdin_info->perdin_id ?>">
                  <div class="panel-body form-horizontal">
                  <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>perdin ID : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->perdin_id; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Name : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->first_name . ' ' . $perdin_info->last_name; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Perdin Date : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php
                                if ($perdin_info->perdin_start_date == $perdin_info->perdin_end_date) {
                                    echo date('d M y', strtotime($perdin_info->perdin_start_date));
                                } else {
                                    echo date('d M y', strtotime($perdin_info->perdin_start_date)) . '<span class="text-danger"> To </span>' . date('d M y', strtotime($perdin_info->perdin_end_date));
                                }
                                ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>perdin Type :</strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php echo $perdin_info->category; ?></p>
                        </div>                  
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Apply On : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><span class="text-danger"><?php echo date('d M y', strtotime($perdin_info->submit_date)); ?></span></p>
                        </div>                                              
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Reason : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->reason; ?></p>
                        </div>                                              
                    </div>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Status : </strong></label>
                        </div>
                        <div class="col-sm-8">						
                            <p class="form-control-static">
								<?php if($perdin_info->perdin_status=='cancel'){?>
									<span class="label label-danger"><?php echo $perdin_info->perdin_status; ?></span>
								<?php } ?>
								<?php if($perdin_info->perdin_status=='pending'){?>
									<span class="label label-warning"><?php echo $perdin_info->perdin_status; ?></span>
								<?php } ?>
								<?php if($perdin_info->perdin_status=='fully approved'){?>
									<span class="label label-success"><?php echo $perdin_info->perdin_status; ?></span>
								<?php } ?>
								<?php if($perdin_info->perdin_status=='partial approved'){?>
									<span class="label label-info"><?php echo $perdin_info->perdin_status; ?></span>
								<?php } ?>
							</p>
                        </div>                                              
                    </div>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Created By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->first_name . ' ' . $perdin_info->last_name; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($perdin_info->submit_date)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php if($perdin_info->approve1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->approve1; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($perdin_info->date_approve1)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($perdin_info->acknowledge!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Acknowledge By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static">                            
                                <?php 
                                    if(strpos(strtoupper($perdin_info->acknowledge), 'NOVA') !== false || strpos(strtoupper($perdin_info->acknowledge), 'SUCI') !== false){
                                        echo 'HR Manager';   
                                    }else{
                                        echo $perdin_info->acknowledge; 
                                    }                                                                                       
                                ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($perdin_info->date_acknowledge)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($perdin_info->unapprove1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->unapprove1; ?>
							pada tanggal <strong><?php echo $perdin_info->date_unapprove1; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
                    
                    <div class="col-md-12">
                        <div class="col-sm-4">
                            <!-- Hidden Input ---->
                            <input type="hidden" name="employee_id" value="<?php echo $perdin_info->employee_id; ?>">
                            <input type="hidden" name="perdin_category_id" value="<?php echo $perdin_info->perdin_category_id; ?>">
                            <input type="hidden" name="perdin_start_date" value="<?php echo $perdin_info->perdin_start_date; ?>">
                            <input type="hidden" name="perdin_end_date" value="<?php echo $perdin_info->perdin_end_date; ?>">
                        </div>

                    
                        <!-- Hidden Input parameter for email notif---->
                        <input type="hidden" name="document_number" value="<?php echo $perdin_info->perdin_id; ?>" required="true">
                    <?php if(!empty($perdin_info->direct_leader_email_2)){ ?>
						<input type="hidden" name="email_to_next_approval" value="<?php echo $perdin_info->direct_leader_email_2; ?>" required="true">
						<input type="hidden" name="link" value="<?php echo base_url() ?>login?link=employee/dashboard/view_perdin_inquiry_2/<?php echo $perdin_info->perdin_id; ?>" required="true">
						<input type="hidden" name="receipt_name" value="<?php echo $perdin_info->direct_leader_name_2; ?>" required="true">
						<input type="hidden" name="sender_name" value="<?php echo $perdin_info->direct_leader_name; ?>" required="true">
                        <!-- <input type="hidden" name="jk_sender" value="<?php //echo $perdin_info->direct_leader_gender; ?>" readonly="true"> -->
                        <!-- <input type="hidden" name="jk_receipt" value="<?php //echo $perdin_info->direct_leader_gender_2; ?>" readonly="true"> -->

					<?php }else{ ?>
						<input type="hidden" name="email_to_next_approval" value="<?php echo $perdin_info->email_hr_pic; ?>" required="true">						
						<input type="hidden" name="link" value="<?php echo base_url() ?>login?link=employee/dashboard/view_perdin_inquiry/<?php echo $perdin_info->perdin_id; ?>" required="true">
						<input type="hidden" name="receipt_name" value="<?php echo $perdin_info->nama_pic_hr; ?>" required="true">
						<input type="hidden" name="sender_name" value="<?php echo $perdin_info->direct_leader_name; ?>" required="true">
                    
                    <?php } ?>

						<input type="hidden" name="email_to_requester" value="<?php echo $perdin_info->email; ?>" required="true">
						<input type="hidden" name="receipt_name_requester" value="<?php echo $perdin_info->first_name ." ". $perdin_info->last_name; ?>" required="true">
						
						<input type="hidden" name="email_dl" value="<?php echo $perdin_info->direct_leader_email; ?>" required="true">
						<?php if($perdin_info->perdin_status != 'cancel' && $perdin_info->perdin_status != 'partial approved' && $perdin_info->perdin_status != 'fully approved'){ ?>
						<div class="col-sm-4 margin">				
							<input type="submit" name="approve_btn" value="Approve" class="btn btn-primary" />
							<input type="submit" name="unapprove_btn" value="Reject" class="btn btn-danger" />	
                        </div>
						<?php } ?>
						
                    </div>
                </div>                
        </div>
    </div>
</div>






