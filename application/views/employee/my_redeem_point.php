
<div class="col-md-12">
    <?php include_once 'asset/admin-ajax.php'; ?>
    <?= message_box('success'); ?>
    <?= message_box('error'); ?>

    <h4><?= anchor('employee/dashboard/apply_redeem_point', '<i class="fa fa-plus"></i> Apply New Redeem Point'); ?></h4>
    <br/>

    <div class="row">
        <div class="col-sm-12">                            
            <div class="panel panel-info">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>My Redeem Point</strong>
                    </div>
                </div>
                
                <div class="table-responsive">
                <!-- <div style="overflow-x:auto;"> -->
                <table class="table table-bordered table-hover" id="dataTables-example" >
                <!-- <table style="table-layout: fixed;  overflow-x: auto;" class="table table-bordered table-hover"  id="dataTables-example"> -->
                    <thead >                                     
                        <tr style="font-size: 13px;color: #000000">   
							<th>No</th>
							<th>Doc. Number</th>
                            <th class="col-sm-3">Total Redeem</th>                            
                            <th class="col-sm-1">Applied On</th>
                            <th class="col-sm-1">Status</th>
                            <th>Action</th>   
                        </tr>
                    </thead>                
                    <tbody style="margin-bottom: 0px;background: #FFFFFF;font-size: 12px;">                                                                   
                        <?php 
							$i=1;
							if (!empty($all_redeem_by_user)): 
								foreach ($all_redeem_by_user as $v_redeem) : 
						?>

                                <tr>      
									<td><?= $i ?></td>
									<td><?= $v_redeem->doc_number ?></td>
                                    <td><?= number_format($v_redeem->total_redeem,0) ?></td>                                                                        
                                    <td><?= date('d M Y', strtotime($v_redeem->submit_date)) ?></td>
                                    <td><?php 									
                                        if ($v_redeem->status == 'pending') {
                                            echo '<span class="label label-warning">'.$v_redeem->status.'</span>';
                                        } elseif ($v_redeem->status == 'fully approved') {
                                            echo '<span class="label label-success">'.$v_redeem->status.'</span>';
                                        } elseif ($v_redeem->status == 'partial approved') {
                                            echo '<span class="label label-info">'.$v_redeem->status.'</span>';
                                        } else {
                                            echo '<span class="label label-danger">cancel</span>';
                                        }
									
                                        ?>
                                    </td>     
                                    <td><?= btn_view('employee/dashboard/view_my_redeem/' . $v_redeem->doc_number) ?></td>                                                                                  
                                </tr>
                                <?php
									$i++;
									endforeach;
								?>
                        <?php else : ?>
                        <td colspan="3">
                            <strong>There is no data to display</strong>
                        </td>
                    <?php endif; ?>
                    </tbody>                    
                </table>             
                </div>
            </div>
        </div>
    </div>
</div>



