<div class="col-md-12">
    <div class="wrap-fpanel">
        <div class="panel panel-default">
            <!-- Default panel contents -->

            <div class="panel-heading">
                <div class="panel-title">                 
                    <strong>Redeem Details</strong><span class="pull-right"><a style="cursor: pointer"onclick="history.go(-1)" class="view-all-front">Go Back</a></span>
                </div>                    
            </div>    
            <form method="post" action="<?= base_url() ?>employee/dashboard/proses_leave_by_dl/<?= $application_info->doc_number ?>">
                  <div class="panel-body form-horizontal">
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Application ID : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->doc_number; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Name : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->first_name . ' ' . $application_info->last_name; ?></p>
                        </div>
                    </div>
                    
                    
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Apply On : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><span class="text-danger"><?= date('d M y', strtotime($application_info->submit_date)); ?></span></p>
                        </div>                                              
                    </div>
                   
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Status : </strong></label>
                        </div>
                        <div class="col-sm-8">						
                            <p class="form-control-static">
                            
								<?php if($application_info->status_redeem=='cancel'){?>
									<span class="label label-danger"><?= $application_info->status_redeem; ?></span>
								<?php } ?>
								<?php if($application_info->status_redeem=='pending'){?>
									<span class="label label-warning"><?= $application_info->status_redeem; ?></span>
								<?php } ?>
								<?php if($application_info->status_redeem=='fully approved'){?>
									<span class="label label-success"><?= $application_info->status_redeem; ?></span>
								<?php } ?>
								<?php if($application_info->status_redeem=='partial approved'){?>
									<span class="label label-info"><?= $application_info->status_redeem; ?></span>
								<?php } ?>
							</p>
                        </div>                                              
                    </div>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Remaining Point : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->saldo_point; ?></p>
                        </div>                                              
                    </div>

                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Created By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->first_name . ' ' . $application_info->last_name; ?>
							pada tanggal <strong><?= date('d M Y', strtotime($application_info->submit_date)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>

					<?php if($application_info->approve1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->approve1; ?>
							pada tanggal <strong><?= date('d M Y', strtotime($application_info->date_approve1)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($application_info->approve2!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Acknowledge By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static">
                                <?php 
                                if(strpos(strtoupper($application_info->approve2), 'NOVA') !== false || strpos(strtoupper($application_info->approve2), 'SUCI') !== false){
                                    echo 'HR Manager';   
                                }else{
                                    echo $application_info->approve2; 
                                }                                                                                       
                                ?>
							pada tanggal <strong><?= date('d M Y', strtotime($application_info->date_approve2)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($application_info->unapprove1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->unapprove1; ?>
							pada tanggal <strong><?= $application_info->date_unapprove1; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
                    <div class="col-md-12">
                        <div class="col-sm-4">
                            <!-- Hidden Input ---->
                            <input type="hidden" name="employee_id" value="<?= $application_info->employee_id; ?>">
                        </div>
                    </div>
                </form>      
                
                
        <div class="row">
            <div class="col-sm-12" data-offset="0">                            
                <div class="panel panel-info">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Product List</strong>
                        </div>
                    </div>

                    <!-- Table -->
                    <table class="table table-bordered table-hover" id="productList">
                        <thead>
                            <tr>
                                <th class="col-sm-1">Product ID</th>
                                <th>Product Name</th>      
                                <th>Qty</th>                            
                                <th>Price</th>
                                <th>Subtotal</th>
                                <th class="col-sm-2">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php $key = 1 ;
                            // print_r($product_list_info);
                            // echo 'product name = ' .$product_list_info[0]->product_name;
                            $total = 0;
                            ?>
                            <?php if (!empty($product_list_info)): for ($i = 0; $i<count($product_list_info); $i++) { ?>
                                    <tr>
                                        <td><?= $key ?></td>
                                        <td><?= $product_list_info[$i]->product_name ?></td>
                                        <td><?= $product_list_info[$i]->qty ?></td>                                        
                                        <td><?= number_format($product_list_info[$i]->price,0) ?></td>
                                        <td><?= number_format($product_list_info[$i]->subtotal,0) ?></td>
                                        <td>
                                            <!-- <?= btn_edit('admin/settings/leave_category/' . $product_list_info[$i]->doc_number); ?>  
                                            <?= btn_delete('admin/settings/delete_leave_category/' . $product_list_info[$i]->doc_number); ?> -->
                                        </td>

                                    </tr>
                                    <?php
                                    $key++;
                                    $total += $product_list_info[$i]->subtotal ;
                            }
                                ?>
                            <?php else : ?>
                            <!-- <td colspan="3">
                                <strong>There is no data to display</strong>
                            </td> -->
                        <?php endif; ?>
                        </tbody>
                        <tfooter>
                            <thead>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th ><span style="float:right">TOTAL</span></th>                               
                                <th id="subtotal"><?= number_format($total,0);?></th>
                                <th>                                    
                                </th>
                            </thead>
                        </tfooter>
                    </table>          
                </div>
            </div>
        </div>

        </div>
    </div>
</div>






