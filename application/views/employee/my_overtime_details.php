<div class="col-md-12">
    <div class="wrap-fpanel">
        <div class="panel panel-default">
            <!-- Default panel contents -->

            <div class="panel-heading">
                <div class="panel-title">                 
                    <strong>Overtime Application  Details</strong><span class="pull-right"><a style="cursor: pointer"onclick="history.go(-1)" class="view-all-front">Go Back</a></span>
                </div>                    
            </div>    
            <form method="post" action="<?php echo base_url() ?>employee/dashboard/proses_leave_by_dl/<?php echo $overtime_info->overtime_id ?>">
                  <div class="panel-body form-horizontal">
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Overtime ID : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->overtime_id; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Name : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->first_name . ' ' . $overtime_info->last_name; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Overtime Time : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php
                                if ($overtime_info->overtime_start_time == $overtime_info->overtime_end_time) {
                                    echo date('h:i A', strtotime($overtime_info->overtime_start_time));
                                } else {
                                    echo date('h:i A', strtotime($overtime_info->overtime_start_time)) . '<span class="text-danger"> To </span>' . date('h:i A', strtotime($overtime_info->overtime_end_time));
                                }
                                ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Overtime Tools :</strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php echo $overtime_info->perlengkapan_lembur; ?></p>
                        </div>                  
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Apply On : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><span class="text-danger"><?php echo date('d M y', strtotime($overtime_info->overtime_date)); ?></span></p>
                        </div>                                              
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Keperluan/Tujuan : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->tujuan; ?></p>
                        </div>                                              
                    </div>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Status : </strong></label>
                        </div>
                        <div class="col-sm-8">						
                            <p class="form-control-static">
								<?php if($overtime_info->overtime_status=='cancel'){?>
									<span class="label label-danger"><?php echo $overtime_info->overtime_status; ?></span>
								<?php } ?>
								<?php if($overtime_info->overtime_status=='pending'){?>
									<span class="label label-warning"><?php echo $overtime_info->overtime_status; ?></span>
								<?php } ?>
								<?php if($overtime_info->overtime_status=='fully approved'){?>
									<span class="label label-success"><?php echo $overtime_info->overtime_status; ?></span>
								<?php } ?>
								<?php if($overtime_info->overtime_status=='partial approved'){?>
									<span class="label label-info"><?php echo $overtime_info->overtime_status; ?></span>
								<?php } ?>
							</p>
                        </div>                                              
                    </div>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Created By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->first_name . ' ' . $overtime_info->last_name; ?>
                            pada tanggal <strong><?php echo date('d M Y', strtotime($overtime_info->overtime_date)); ?></strong>
                            </p>
                        </div>                                              
                    </div>
					
					<?php if($overtime_info->approve!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->approve; ?>
							pada tanggal <strong><?php echo $overtime_info->date_approve; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
										
					<?php if($overtime_info->unapprove!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->unapprove; ?>
							pada tanggal <strong><?php echo $overtime_info->date_unapprove; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>

                    <?php if($overtime_info->acknowledge!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Acknowledge By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->acknowledge; ?>
							pada tanggal <strong><?php echo $overtime_info->date_acknowledge; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
                    
                    <div class="col-md-12">
                        <div class="col-sm-4">
                            <!-- Hidden Input ---->
							<!--
                            <input type="hidden" name="employee_id" value="<?php echo $application_info->employee_id; ?>">
                            <input type="hidden" name="leave_category_id" value="<?php echo $application_info->leave_category_id; ?>">
                            <input type="hidden" name="leave_start_date" value="<?php echo $application_info->leave_start_date; ?>">
                            <input type="hidden" name="leave_end_date" value="<?php echo $application_info->leave_end_date; ?>">
							-->
                        </div>
						
						
						
                    </div>
                </div>                
        </div>
    </div>
</div>






