<div id="main-header" class="clearfix">
    <header id="header" class="clearfix">
        <div class="stripe_color"></div>
        <div class="stripe_image"></div>                 
        <div class="school-logo col-sm-12">                    
            <div class="container">
                <?php
					//print_r($tbl_employee_ess_setup);
						//	die;
							
                $genaral_info = $this->session->userdata('genaral_info');
                if (!empty($genaral_info)) {
                    foreach ($genaral_info as $info) {
                        ?>                        
                        <img src="<?php echo base_url() . $info->logo ?>" alt="" class="img-circle"/>
                        <div class="head">
                            <h2><?php echo $info->name ?></h2>
                        </div>

                        <?php
                    }
                } else {
                    ?>
                    <img src="<?php echo base_url() ?>img/logo.png" class="img-circle" alt="school_logo" >                    
                    <div class="head">
                        <h2>Human Resource Management System</h2>
                    </div>
                <?php }
                ?>
            </div>
        </div>                
        <div class="container">   
            <div class="row main">
                <nav class="navbar navbar-custom" id="header_menu" role="navigation">                        
                    <div class="menu-bg">                        
                        <nav class="main-menu navbar navbar-collapse menu-bg" role="navigation">

                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header menu-bg">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="main-menu collapse navbar-collapse menu-bg" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li class="<?php if(!empty($menu['index'])){ echo $menu['index'] == 1 ? 'active' : '';} ?>">
                                        <a href="<?php echo base_url() ?>employee/dashboard">Home</a>
                                    </li> 
									<!--
                                    <li class="dropdown <?php if(!empty($menu['mailbox'])){ echo $menu['mailbox'] == 1 ? 'active' : '';} ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mailbox<b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li class="<?php if(!empty($menu['inbox'])){ echo $menu['inbox'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/inbox">Inbox</a></li>
                                            <li class="<?php if(!empty($menu['sent'])){ echo $menu['sent'] == 1 ? 'active' : '';} ?>"><a  href="<?php echo base_url() ?>employee/dashboard/sent">Sent</a></li>                                            
                                        </ul>
                                    </li>
									-->
									<?php if($tbl_employee_ess_setup[0]->leave_application == '1'):?>
                                    <li class="dropdown <?php if(!empty($menu['leave_application'])){ echo $menu['leave_application'] == 1 ? 'active' : '';} ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Leave Application<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li class="<?php if(!empty($menu['leave_application'])){ echo $menu['leave_application'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/leave_application">My Leave Application</a></li>
                                                <li class="<?php if(!empty($menu['leave_application_inquiry'])){ echo $menu['leave_application_inquiry'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/leave_application_inquiry">Approve(1) Leave Application</a></li>
                                                <li class="<?php if(!empty($menu['leave_application_inquiry_2'])){ echo $menu['leave_application_inquiry_2'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/leave_application_inquiry_2">Approve(2) Leave Application</a></li>
                                        </ul>
									</li>
									<?php endif?>
									
                                    <li class="<?php if(!empty($menu['notice'])){ echo $menu['notice'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/all_notice">Notice</a></li>
                                    <li class="<?php if(!empty($menu['events'])){ echo $menu['events'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/all_events">Events</a></li>
                                    
									<?php if($tbl_employee_ess_setup[0]->permit_application == '1'):?>
                                    <li class="dropdown <?php if(!empty($menu['permit_application'])){ echo $menu['permit_application'] == 1 ? 'active' : '';} ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Permit Application<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li class="<?php if(!empty($menu['permit_application'])){ echo $menu['permit_application'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/permit_application">My Permit Application</a></li>
                                                <li class="<?php if(!empty($menu['approve_permit_application'])){ echo $menu['approve_permit_application'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/approve_permit_application">Approve(1) Permit Application</a></li> 
                                                <li class="<?php if(!empty($menu['approve_permit_application_2'])){ echo $menu['approve_permit_application_2'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/approve_permit_application_2">Approve(2) Permit Application</a></li> 
                                            
                                            </ul>
                                    </li>
									<?php endif?>
									
									<?php if($tbl_employee_ess_setup[0]->overtime_application == '1'):?>
                                    <li class="dropdown <?php if(!empty($menu['evertime_application'])){ echo $menu['overtime_application'] == 1 ? 'active' : '';} ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Overtime Application<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li class="<?php if(!empty($menu['overtime_application'])){ echo $menu['overtime_application'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/overtime_application">My Overtime Application</a></li>
                                                <li class="<?php if(!empty($menu['approve_overtime_application'])){ echo $menu['approve_overtime_application'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/approve_overtime_application">Approve Overtime Application</a></li> 
                                            </ul>
                                    </li>
                                    <?php endif?>

                                    <?php if($tbl_employee_ess_setup[0]->redeem_point == '1'):?>
                                    <li class="dropdown <?php if(!empty($menu['redeem_point'])){ echo $menu['redeem_point'] == 1 ? 'active' : '';} ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Redeem Point<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li class="<?php if(!empty($menu['redeem_point'])){ echo $menu['redeem_point'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/redeem_point">My Redeem Point</a></li>
                                                <!-- <li class="<?php if(!empty($menu['leave_inquiry'])){ echo $menu['leave_inquiry'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/leave_application_inquiry">Approve Redeem Point</a></li> -->
                                        </ul>
									</li>
                                    <?php endif?>

                                    <?php if($tbl_employee_ess_setup[0]->perdin_application == '1'):?>
                                    <li class="dropdown <?php if(!empty($menu['perdin_application'])){ echo $menu['perdin_application'] == 1 ? 'active' : '';} ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Perdin Application<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li class="<?php if(!empty($menu['perdin_application'])){ echo $menu['perdin_application'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/perdin_application">My Perdin Application</a></li>
                                                <li class="<?php if(!empty($menu['approve_perdin_application'])){ echo $menu['approve_perdin_application'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/approve_perdin_application">Approve(1) Perdin Application</a></li> 
                                                <li class="<?php if(!empty($menu['approve_perdin_application_2'])){ echo $menu['approve_perdin_application_2'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/approve_perdin_application_2">Approve(2) Perdin Application</a></li>
                                       
                                            </ul>
                                    </li>
									<?php endif?>
                                    

                                </ul>

                              

                                <ul class="main-menu nav navbar-nav navbar-right">
                                    <li class="quickMenu"><a href="<?php echo base_url() ?>docs/HRMS_help_employee_PMKI.pdf" target="_blank"> 
                                        <img src="<?php echo base_url() ?>img/help.png"> HELP </a>
                                    </li>

                                    <li class="dropdown <?php if(!empty($menu['profile'])){ echo $menu['profile'] == 1 ? 'active' : '';} ?>">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Profile<b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li class="<?php if(!empty($menu['change_password'])){ echo $menu['change_password'] == 1 ? 'active' : '';} ?>"><a href="<?php echo base_url() ?>employee/dashboard/change_password">Change Password</a></li>
                                            <li><a href="<?php echo base_url() ?>login/logout">Logout</a></li>                                            
                                        </ul>
                                    </li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </nav>
                    </div>  
                </nav>  
            </div>                                    
        </div> 
    </header>   
</div>


