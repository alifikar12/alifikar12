<footer class="col-sm-12 footer">      
    <div class="container">
        <div class="row">

            <p class="text-center margin">
                PT. Oneuniverse Indonesia
            </p>

        </div>
    </div>
</footer>   
<!--                 
<script type="text/javascript">
    $(document).ready(function() {

    });
</script>
-->

<!-- Library Jquery Mobile -->
<!-- <script src="<?php echo base_url(); ?>asset/jquery.mobile-1.4.5/jquery.mobile-1.4.5.min.js"></script>   -->

<!-- ALl Custom Scripts -->  
<script src="<?php echo base_url(); ?>asset/js/jquery-1.10.2.min.js"></script>   
<script src="<?php echo base_url(); ?>asset/js/custom.js"></script>
<script src="<?php echo base_url() ?>asset/js/select2.js"></script>

<script src="<?php echo base_url(); ?>asset/js/custom-validation.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>asset/js/jquery.validate.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js" type="text/javascript"></script>    
<script src="<?php echo base_url() ?>asset/js/jasny-bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>asset/js/timepicker.js" ></script>  
<script src="<?php echo base_url() ?>asset/js/bootstrap-datepicker.js" ></script> 
<script src="<?php echo base_url() ?>asset/js/bootstrap-timepicker.js"></script>

        
<!-- Data Table -->
<script src="<?php echo base_url(); ?>asset/js/plugins/metisMenu/metisMenu.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>asset/js/plugins/dataTables/jquery.dataTables.js" type="text/javascript"></script>  
<script src="<?php echo base_url(); ?>asset/js/plugins/dataTables/dataTables.bootstrap.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
        
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
        setTimeout(function() {
            $(".alert").fadeOut("slow", function() {
                $(".alert").remove();
            });

        }, 3000);
        $('.datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
        });
        /*
        $('#datetimepicker3').datetimepicker({
            format: 'LT',
        });
        */
    });
</script>

<!--
<script type="text/javascript">
    $(function () {
        $('#datetimepicker3').datetimepicker({
            format: 'LT'
        });
    });
</script>
-->
</body>
</html>