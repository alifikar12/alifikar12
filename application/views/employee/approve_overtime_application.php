
<div class="col-md-12">
    <?php include_once 'asset/admin-ajax.php'; ?>
    <?php echo message_box('success'); ?>
    <?php echo message_box('error'); ?>

    
    <br/>

    <div class="row">
        <div class="col-sm-12">                            
            <div class="panel panel-info">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>List Aplication Overtime for Approve</strong>
                    </div>
                </div>
                <table class="table table-bordered table-hover" id="dataTables-example">
                    <thead>                                     
                        <tr style="font-size: 13px;color: #000000">							
							<th>Overtime ID</th>
							<th class="col-sm-1">NIK</th>
							<th class="col-sm-2">Employee Name</th>
                            <th class="col-sm-3">Perlengkapan Lembur</th>
                            <th class="col-sm-1">Start Time</th>
                            <th class="col-sm-1">End Time</th>
                            <th>Tujuan</th>
                            <th class="col-sm-1">Overtime Date</th>
                            <th class="col-sm-1">Status</th>
                            <th>Action</th>   
                        </tr>
                    </thead>                
                    <tbody style="margin-bottom: 0px;background: #FFFFFF;font-size: 12px;">                                                                   
                        <?php						
							if (!empty($all_overtime_applications)): 
							foreach ($all_overtime_applications as $v_application) : 
						?>

                                <tr>									
									<td><?php echo $v_application->overtime_id ?></td>
									<td><?php echo $v_application->employment_id ?></td>
									<td><?php echo $v_application->first_name.' '.$v_application->last_name ?></td>
                                    <td><?php echo $v_application->perlengkapan_lembur ?></td>
                                    <td><?php echo date('h:i A', strtotime($v_application->overtime_start_time)) ?></td>
                                    <td><?php echo date('h:i A', strtotime($v_application->overtime_end_time)) ?></td>
                                    <td><?php echo $v_application->tujuan ?></td>                                                                        
                                    <td><?php echo date('d M Y', strtotime($v_application->overtime_date)) ?></td>
                                    <td><?php
                                        if ($v_application->overtime_status == 'pending') {
                                            echo '<span class="label label-info">New</span>';
                                        } elseif ($v_application->overtime_status == 'partial approved' || $v_application->overtime_status == 'fully approved' ) {
                                            echo '<span class="label label-success">Approve</span>';
                                        } elseif ($v_application->overtime_status == 'cancel') {
                                            echo '<span class="label label-danger">Cancel</span>';
                                        }
                                        ?>
                                    </td>     
                                    <td><?php echo btn_view('employee/dashboard/view_overtime_inquiry/' . $v_application->overtime_id) ?></td>                                                                                    
                                </tr>
							<?php
                            endforeach;
                            ?>
                        <?php else : ?>
                        <td colspan="3">
                            <strong>There is no data to display</strong>
                        </td>
                    <?php endif; ?>
                    </tbody>                    
                </table>
            </div>
        </div>
    </div>
</div>



