
<div class="col-md-12">
    <?php include_once 'asset/admin-ajax.php'; ?>
    <?php echo message_box('success'); ?>
    <?php echo message_box('error'); ?>

    
    <br/>

    <div class="row">
        <div class="col-sm-12">                            
            <div class="panel panel-info">
                <!-- Default panel contents -->
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>List Aplication Leave for Approve</strong>
                    </div>
                </div>
                <table class="table table-bordered table-hover" id="dataTables-example">
                    <thead>                                     
                        <tr style="font-size: 12px;color: #000000">                            
                            <th class="col-sm-2">Leave Category</th>
                            <th class="col-sm-1">Start Date</th>
                            <th class="col-sm-1">End Date</th>
                            <th>Reason</th>
                            <th class="col-sm-1">Applied On</th>
                            <th class="col-sm-1">Status</th>
                            <th>Change / View</th>   
                        </tr>
                    </thead>                
                    <tbody style="margin-bottom: 0px;background: #FFFFFF;font-size: 12px;">                                                                   
                        <?php if (!empty($all_leave_applications)): foreach ($all_leave_applications as $v_application) : ?>

                                <tr>                                    
                                    <td><?php echo $v_application->category ?></td>
                                    <td><?php echo date('d M Y', strtotime($v_application->leave_start_date)) ?></td>
                                    <td><?php echo date('d M Y', strtotime($v_application->leave_end_date)) ?></td>
                                    <td><?php echo $v_application->reason ?></td>                                                                        
                                    <td><?php echo date('d M Y', strtotime($v_application->application_date)) ?></td>
                                    <td><?php
                                        if ($v_application->application_status == 1) {
                                            echo '<span class="label label-info">Pending</span>';
                                        } elseif ($v_application->application_status == 2) {
                                            echo '<span class="label label-success">Accepted</span>';
                                        } else {
                                            echo '<span class="label label-danger">Rejected</span>';
                                        }
                                        ?>
                                    </td>     
                                    <td><?php echo btn_view('employee/dashboard/view_application_inquiry/' . $v_application->application_list_id) ?></td>                                                                                    
                                </tr>
                                <?php
                            endforeach;
                            ?>
                        <?php else : ?>
                        <td colspan="3">
                            <strong>There is no data to display</strong>
                        </td>
                    <?php endif; ?>
                    </tbody>                    
                </table>
            </div>
        </div>
    </div>
</div>



