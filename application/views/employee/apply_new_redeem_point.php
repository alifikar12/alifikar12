<?php include_once 'asset/admin-ajax.php'; ?>
<?= message_box('success'); ?>
<?= message_box('error'); ?>
<div class="col-md-12">
<div class="row">
    <div class="col-sm-12"> 
        <div class="wrap-fpanel">
            <div class="panel panel-info" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>
                        Add Product
                        </strong>
                    </div>
                </div>
                <div class="panel-body">
                    
                    <div class="form-horizontal form-groups-bordered">  
                        
                        <div class="form-group">
                            <label class="col-sm-3 control-label" >Doc. Number  <span class="required"> *</span></label>
                            <div class="col-sm-5">                            
                                <input type="text" name="doc_number" id="doc_number" readonly value="<?php
                                if (!empty($doc_number_result)) {
                                    echo $doc_number_result;
                                }
                                ?>" class="form-control" placeholder="Document Number" />
                                
                            </div>
						</div>				

                        <div class="form-group">
							<label class="col-sm-3 control-label" >Product ID <span class="required"> *</span></label>
                            <div class="col-sm-5">                            
                                <input type="text" name="product_id" id="product_id" readonly value="" class="form-control" placeholder="product_id" />
                            </div>
                        </div>	

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Product Name <span class="required">*</span></label>
                            <div class="col-sm-5">
                            <select name="product_name" id="product_name" class="form-control select_box col-sm-5" >
                                <option value="" >Select Product Name...</option>
                                <?php foreach ($all_product as $v_product) : ?>
                                    <option value="<?= $v_product->product_name ?>" data-product_id="<?= $v_product->product_id;  ?>" data-price="<?= $v_product->price;  ?>" 
                                    <?php
                                        if (!empty($employee_info->product_name)) {
                                            echo $v_product->product_name == $employee_info->product_name ? 'selected' : '';
                                        }
                                    ?>><?= $v_product->product_name ?></option>
                                <?php endforeach; ?>
                            </select>
                            </div>
						</div>

                        <div class="form-group">
							<label class="col-sm-3 control-label" >Qty <span class="required"> *</span></label>
                            <div class="col-sm-5">                            
                                <input type="number" name="qty" id="qty" value="1" class="form-control" placeholder="qty" />
                            </div>
                        </div>	

						<div class="form-group">
							<label class="col-sm-3 control-label" >Price <span class="required"> *</span></label>
                            <div class="col-sm-5">                            
                                <input type="text" name="price" id="price" readonly value="<?php
                                if (!empty($leave_category->hak_cuti)) {
                                    echo $leave_category->hak_cuti;
                                }
                                ?>" class="form-control" placeholder="Price" />
                            </div>
							
								<div class="col-sm-2">
									<button id="add" class="btn btn-primary">Add</button>                            
								</div>
                        </div>							
						
                    </div>
                </div>                 
            </div>        
                                  
        </div>  
        <br/>

        <div class="row">
            <div class="col-sm-12" data-offset="0">                            
                <div class="panel panel-info">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Product List</strong>
                        </div>
                    </div>
                    <div class="table-responsive">
                    <!-- Table -->
                    <table class="table table-bordered table-hover" id="productList">
                        <thead>
                            <tr>
                                <th class="col-sm-1">Product ID</th>
                                <th>Product Name</th>    
                                <th>Qty</th>                            
                                <th>Price</th>
                                <th>Subtotal</th>
                                <th class="col-sm-2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                        <tfooter>
                            <thead>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th ><span style="float:right">TOTAL</span></th>                               
                                <th id="total">0</th>
                                <th>
                                    <form action="<?= base_url() ?>employee/dashboard/submit_redeem_point" method="post"  enctype="multipart/form-data" class="form-horizontal">
                                        <input type="hidden" name="dataArrProdList" id="dataArrProdList"  class="form-control" id="field-1" value=""/>  
                                        <input type="hidden" name="next_number" id="next_number" readonly value="<?php
                                            if (!empty($next_number)) {
                                                echo $next_number;
                                            }
                                            ?>" class="form-control" placeholder="next_number" />   
                                        <input type="hidden" name="doc_number" id="doc_number" readonly value="<?php
                                            if (!empty($doc_number_result)) {
                                                echo $doc_number_result;
                                            }
                                            ?>" class="form-control" placeholder="doc_number" />  

                                            <input type="hidden" name="saldo_point" id="saldo_point" readonly value="<?php
                                            if (!empty($saldo_point)) {
                                                echo $saldo_point;
                                            }
                                            ?>" class="form-control" placeholder="saldo_point" /> 

                                            <input type="hidden" name="ttl_redeem" id="ttl_redeem" readonly value="" class="form-control" /> 
                                            
                                            <input type="hidden" name="link" readonly value="<?= base_url() ?>login?link=employee/dashboard/view_redeem_point_inquiry/<?= $doc_number_result; ?>" class="form-control" /> 
                                            <input type="hidden" name="sender_name" readonly value="<?php  
                                                                                                if (!empty($all_emplyee_info->first_name)) {
                                                                                                    echo $all_emplyee_info->first_name.' '.$all_emplyee_info->last_name;
                                                                                                }?>" 
                                                                                                 class="form-control" /> 
                                            <input type="hidden" name="jk_sender" readonly value="<?php  
                                                                                                if (!empty($all_emplyee_info->gender)) {
                                                                                                    echo $all_emplyee_info->gender;
                                                                                                }?>" 
                                                                                                 class="form-control" /> 
                                        <button id="submit" class="btn btn-primary" style="opacity: 0.5; pointer-events: none;">Submit</button>

                                    </form>  
                                </th>
                            </thead>
                        </tfooter>
                    </table>   
                    </div>       
                </div>
            </div>
        </div>
    </div>   
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
$(document).ready(function() {

    //populate data from dropdown product
    $('#product_name').change(function(){
        //get the selected option's data-id value using jquery `.data()`
        var product_id =  $(':selected',this).data('product_id'); 
        var price =  $(':selected',this).data('price'); 
        // console.log('product name', product_name)
        //set data.
        $('#product_id').val(product_id);
        $('#price').val(price);
    });  


        //Compose template string
        String.prototype.compose = (function (){
            var re = /\{{(.+?)\}}/g;
            return function (o){
                return this.replace(re, function (_, k){
                    return typeof o[k] != 'undefined' ? o[k] : '';
                });
            }
        }());

        var i = 0;  
        var tbody = $('#productList').children('tbody');
        var table = tbody.length ? tbody : $('#productList');
        var row = '<tr>'+
                    '<td>{{product_id}}</td>'+
                    '<td>{{product_name}}</td>'+
                    '<td>{{qty}}</td>'+
                    '<td id="price">{{price}}</td>'+
                    '<td id="subtotal">{{subtotal}}</td>'+
                    '<td>{{action}}</td>'+
                '</tr>';
        i++;
        var grandtotal = 0;
        
        $('#add').click(function(){
            //Add row
            table.append(row.compose({
                'product_id':  $('#product_id').val(),
                'product_name': $('#product_name').val(),
                'qty': $('#qty').val(),
                'price': $('#price').val(),
                'subtotal' : $('#qty').val() * $('#price').val(),
                'action':  '<a class="btn btn-xs delete-record" data-id="'+i+'"><i class="glyphicon glyphicon-trash"></i></a>'     
                   
            }));     
            i++;       
            
            var total = Math.round(Number($('#qty').val() * $('#price').val()));
            grandtotal += total;
            // console.log('total', subtotal);
            $('th#total').html(grandtotal);
            
            $('#ttl_redeem').val(grandtotal);
            // console.log('saldo_point =', $('#saldo_point').val());

            var total       = Number($('th#total').html());
            var saldo_point = Number($('#saldo_point').val());
            var buttonSubmit = document.getElementById("submit");
            
            if(total <= saldo_point){
                buttonSubmit.style = "opacity : 1; pointer-events : visible;";
            }else{
                buttonSubmit.style = "opacity : 0.5; pointer-events : none;";
            }

        });

        
        // function delete row
        $(document).delegate('a.delete-record', 'click', function(e) {
            e.preventDefault();    
            var didConfirm = confirm("Are you sure You want to delete");
            if (didConfirm == true) {
            
             var price = $(this).parents("tr").find('#subtotal').html();
             grandtotal -= Math.round(price);
            
                $('th#total').html(grandtotal);
                $(this).parents("tr").remove();

            //set button submit enable/disable 
            var total       = Number($('th#total').html());
            var saldo_point = Number($('#saldo_point').val());
            var buttonSubmit = document.getElementById("submit");
            if(total <= saldo_point){
                buttonSubmit.style = "opacity : 1; pointer-events : visible;";
            }else{
                buttonSubmit.style = "opacity : 0.5; pointer-events : none;";
            }

            if(total == 0){
                buttonSubmit.style = "opacity : 0.5; pointer-events : none;";
            }
            
            return true;
        } else {
            return false;
        }
        });


        $('th#total').change(function(){
            var subtotal    = $('#total').html();
            var saldo_point = $('#saldo_point').val();
            var buttonSubmit = $('#submit');
            alert(subtotal);
            alert(saldo_point);
            if(subtotal < saldo_point){
                buttonSubmit.style = "opacity : 1; pointer-events : visible;";
            }else{
                buttonSubmit.style = "opacity : 0.5; pointer-events : none;";
            }

        });

        $('#submit').click(function(){
            // populate data array table
            var dataArrProdList_tmp = [];
            $("table#productList tr").each(function() { 
                var arrayOfThisRow = [];
                var tableData = $(this).find('td');
                if (tableData.length > 0) {
                    tableData.each(function() { arrayOfThisRow.push($(this).text()); });
                    dataArrProdList_tmp.push(arrayOfThisRow);
                }
            });
            
            $('#dataArrProdList').val(JSON.stringify(dataArrProdList_tmp));
            
        });            
    
});
</script>

