
<div class="col-md-12">
    <div class="row">
        <div class="col-sm-12">
            <div class="wrap-fpanel">
                <div class="panel panel-info" data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Add New Overtime Application</strong>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-offset-1">
                            <form id="form" action="<?php echo base_url() ?>employee/dashboard/save_overtime_application" method="post"  enctype="multipart/form-data" class="form-horizontal">
                                <div class="panel_controls">                                    

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Overtime Date <span class="required"> *</span></label>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="text" name="overtime_date"  id="overtime_date" required  class="form-control datepicker"  value="" data-format="dd-mm-yyyy">   
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="entypo-calendar"></i></a>
                                                </div>                                             
                                            </div>
                                        </div>
                                    </div>  

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Start Time <span class="required"> *</span></label>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="time" name="overtime_start_time"  required  class="form-control"  value="" >   
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="entypo-time"></i></a>
                                                </div>                                             
                                            </div>
                                        </div>
                                    </div>  

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">End Time <span class="required"> *</span></label>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="time" name="overtime_end_time"   required class="form-control" value="" >
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="entypo-time"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Keperluan/Tujuan</label>
                                        <div class="col-sm-5">
                                            <textarea id="present" name="tujuan" class="form-control" rows="4"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Kelengkapan yg perlu disiapkan <span class="required"> *</span></label>

                                        <div class="col-sm-5">
                                            <select id="perlengkapan_id" name="perlengkapan_id" class="form-control" required >
                                                <option value="" >Select Kelengkapan...</option>
                                                <?php foreach ($all_perlengkapan_lembur as $v_perlengkapan) : ?>
                                                    <option value="<?php echo $v_perlengkapan->perlengkapan_id ?>">
                                                        <?php echo $v_perlengkapan->perlengkapan_lembur ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div id="keperluan_ot" class="form-group" style="display:none";>
                                        <label for="field-1" class="col-sm-3 control-label">Lainnya (Sebutkan) </label>
                                        <div class="col-sm-5">
                                            <textarea id="present" name="perlengkapan_lainnya"  class="form-control" rows="6"></textarea>
                                        </div>
                                    </div>

                                    <!--post parameter email notif--> 
                                    <div class="form-group">    
                                            <input type="hidden" name="email_dl" value="<?php echo $all_emplyee_info->direct_leader_email; ?>" readonly="true">                                     
                                            <input type="hidden" name="document_number" value="<?php echo $no_overtime->no_overtime; ?>" readonly="true">
                                            <input type="hidden" name="link" value="<?php echo base_url() ?>login?link=employee/dashboard/view_overtime_inquiry/<?php echo $no_overtime->no_overtime; ?>" readonly="true">
                                            <input type="hidden" name="receipt_name" value="<?php echo $all_emplyee_info->direct_leader_name; ?>" readonly="true">
                                            <input type="hidden" name="sender_name" value="<?php echo $all_emplyee_info->first_name .' '.$all_emplyee_info->last_name ; ?>" readonly="true">
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-5">
                                            <button type="submit" id="sbtn" name="sbtn" value="1" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
jQuery(document).ready(function() {
    jQuery("#perlengkapan_id").change(function() {
        if (jQuery(this).val() === '3'){ 
            jQuery('div[id=keperluan_ot]').show();			
        } else {
            jQuery('div[id=keperluan_ot]').hide(); 
			
        }
    });

    //disable past date overtime date
    $("#overtime_date").datepicker({ startDate:new Date()});  

});
</script>

