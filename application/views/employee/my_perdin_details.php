<div class="col-md-12">
    <div class="wrap-fpanel">
        <div class="panel panel-default">
            <!-- Default panel contents -->

            <div class="panel-heading">
                <div class="panel-title">                 
                    <strong>Perdin Application Details</strong><span class="pull-right"><a style="cursor: pointer"onclick="history.go(-1)" class="view-all-front">Go Back</a></span>
                </div>                    
            </div>    
            <form method="post" action="">
                  <div class="panel-body form-horizontal">
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>perdin ID : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->perdin_id; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Name : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->first_name . ' ' . $perdin_info->last_name; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>perdin Date : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php
                                if ($perdin_info->perdin_start_date == $perdin_info->perdin_end_date) {
                                    echo date('d M y', strtotime($perdin_info->perdin_start_date));
                                } else {
                                    echo date('d M y', strtotime($perdin_info->perdin_start_date)) . '<span class="text-danger"> To </span>' . date('d M y', strtotime($perdin_info->perdin_end_date));
                                }
                                ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>perdin Type :</strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php echo $perdin_info->category; ?></p>
                        </div>                  
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Apply On : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><span class="text-danger"><?php echo date('d M y', strtotime($perdin_info->submit_date)); ?></span></p>
                        </div>                                              
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Reason : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->reason; ?></p>
                        </div>                                              
                    </div>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Status : </strong></label>
                        </div>
                        <div class="col-sm-8">						
                            <p class="form-control-static">
								<?php if($perdin_info->perdin_status=='cancel'){?>
									<span class="label label-danger"><?php echo $perdin_info->perdin_status; ?></span>
								<?php } ?>
								<?php if($perdin_info->perdin_status=='pending'){?>
									<span class="label label-warning"><?php echo $perdin_info->perdin_status; ?></span>
								<?php } ?>
								<?php if($perdin_info->perdin_status=='fully approved'){?>
									<span class="label label-success"><?php echo $perdin_info->perdin_status; ?></span>
								<?php } ?>
								<?php if($perdin_info->perdin_status=='partial approved'){?>
									<span class="label label-info"><?php echo $perdin_info->perdin_status; ?></span>
								<?php } ?>
							</p>
                        </div>                                              
                    </div>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Created By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->first_name . ' ' . $perdin_info->last_name; ?>
                            pada tanggal <strong><?php echo date('d M Y', strtotime($perdin_info->submit_date)); ?></strong>
                            </p>
                        </div>                                              
                    </div>
					
					<?php if($perdin_info->approve1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved (1) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->approve1; ?>
							pada tanggal <strong><?php echo $perdin_info->date_approve1; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>

                    <?php if($perdin_info->approve2!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved (2) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->approve2; ?>
							pada tanggal <strong><?php echo $perdin_info->date_approve2; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
										
					<?php if($perdin_info->unapprove1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved (1) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->unapprove1; ?>
							pada tanggal <strong><?php echo $perdin_info->date_unapprove1; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>

                    <?php if($perdin_info->unapprove2!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved (2) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->unapprove2; ?>
							pada tanggal <strong><?php echo $perdin_info->date_unapprove2; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>

                    <?php if($perdin_info->acknowledge!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Acknowledge By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $perdin_info->acknowledge; ?>
							pada tanggal <strong><?php echo $perdin_info->date_acknowledge; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
                    
                    <div class="col-md-12">
                        <div class="col-sm-4">
                            <!-- Hidden Input ---->
							<!--
                            <input type="hidden" name="employee_id" value="<?php echo $perdin_info->employee_id; ?>">
                            <input type="hidden" name="leave_category_id" value="<?php echo $perdin_info->perdin_category_id; ?>">
                            <input type="hidden" name="leave_start_date" value="<?php echo $perdin_info->perdin_start_date; ?>">
                            <input type="hidden" name="leave_end_date" value="<?php echo $perdin_info->submit_date; ?>">
							-->
                        </div>
						
						
						
                    </div>
                </div>                
        </div>
    </div>
</div>






