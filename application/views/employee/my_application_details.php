<div class="col-md-12">
    <div class="wrap-fpanel">
        <div class="panel panel-default">
            <!-- Default panel contents -->

            <div class="panel-heading">
                <div class="panel-title">                 
                    <strong>Leave Application  Details</strong><span class="pull-right">
                    <button id="resend_notif" onclick="resend_notif()" class="view-all-front">Resend Notif</button>
                    <a style="cursor: pointer"onclick="history.go(-1)" class="view-all-front">Go Back</a></span>
                </div>                    
            </div>    
            <form method="post" action="<?php echo base_url() ?>employee/dashboard/proses_leave_by_dl/<?php echo $application_info->application_list_id ?>">
                  <div class="panel-body form-horizontal">
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Application ID : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $application_info->application_list_id; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Name : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $application_info->first_name . ' ' . $application_info->last_name; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Leave Date : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php
                                if ($application_info->leave_start_date == $application_info->leave_end_date) {
                                    echo date('d M y', strtotime($application_info->leave_start_date));
                                } else {
                                    echo date('d M y', strtotime($application_info->leave_start_date)) . '<span class="text-danger"> To </span>' . date('d M y', strtotime($application_info->leave_end_date));
                                }
                                ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Leave Type :</strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php echo $application_info->category; ?></p>
                        </div>                  
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Apply On : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><span class="text-danger"><?php echo date('d M y', strtotime($application_info->application_date)); ?></span></p>
                        </div>                                              
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Reason : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $application_info->reason; ?></p>
                        </div>                                              
                    </div>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Status : </strong></label>
                        </div>
                        <div class="col-sm-8">						
                            <p class="form-control-static">
								<?php if($application_info->leave_status=='cancel'){?>
									<span class="label label-danger"><?php echo $application_info->leave_status; ?></span>
								<?php } ?>
								<?php if($application_info->leave_status=='pending'){?>
									<span class="label label-warning"><?php echo $application_info->leave_status; ?></span>
								<?php } ?>
								<?php if($application_info->leave_status=='fully approved'){?>
									<span class="label label-success"><?php echo $application_info->leave_status; ?></span>
								<?php } ?>
								<?php if($application_info->leave_status=='partial approved'){?>
									<span class="label label-info"><?php echo $application_info->leave_status; ?></span>
								<?php } ?>
							</p>
                        </div>                                              
                    </div>
                   
					<div class="col-md-12" <?php if(empty($application_info->sisa_cuti)) echo 'style="display:none"' ?>>
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Sisa Cuti : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $application_info->sisa_cuti; ?></p>
                        </div>                                              
                    </div>
                    

                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Created By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $application_info->first_name . ' ' . $application_info->last_name; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($application_info->application_date)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>

					<?php if($application_info->approve1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved 1 By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $application_info->approve1; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($application_info->date_approve1)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>

                    <?php if($application_info->approve2!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved 2 By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $application_info->approve2; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($application_info->date_approve2)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($application_info->approve3!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Acknowledge By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static">
                                <?php 
                                // if(strpos(strtoupper($application_info->approve3), 'NOVITA') !== false || strpos(strtoupper($application_info->approve3), 'SUCI') !== false){
                                //     echo 'HR Manager';   
                                // }else{
                                    echo $application_info->approve3; 
                                // }                                                                                       
                                ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($application_info->date_approve3)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($application_info->unapprove1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $application_info->unapprove1; ?>
							pada tanggal <strong><?php echo $application_info->date_unapprove1; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($application_info->unapprove2!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $application_info->unapprove2; ?>
							pada tanggal <strong><?php echo $application_info->date_unapprove2; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>

                    <?php if($application_info->unapprove3!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $application_info->unapprove3; ?>
							pada tanggal <strong><?php echo $application_info->date_unapprove3; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
                    
                    <div class="col-md-12">
                        <div class="col-sm-4">
                            <!-- Hidden Input ---->
                            <input type="hidden" name="employee_id" value="<?php echo $application_info->employee_id; ?>">
                            <input type="hidden" name="leave_category_id" value="<?php echo $application_info->leave_category_id; ?>">
                            <input type="hidden" name="leave_start_date" value="<?php echo $application_info->leave_start_date; ?>">
                            <input type="hidden" name="leave_end_date" value="<?php echo $application_info->leave_end_date; ?>">
                        </div>
						
                        <!--post parameter email notif--> 
                        <div class="form-group">    
                                <input type="hidden" id="email_dl"  value="<?php echo $all_emplyee_info->direct_leader_email; ?>" readonly="true">                                     
                                <input type="hidden" id="doc_num"  value="<?php echo $application_info->application_list_id; ?>" readonly="true">
                                <input type="hidden" id="link" value="<?php echo base_url() ?>login?link=employee/dashboard/view_application_inquiry/<?php echo $application_info->application_list_id; ?>" readonly="true">
                                <input type="hidden" id="receipt_name" value="<?php echo $all_emplyee_info->direct_leader_name; ?>" readonly="true">
                                <input type="hidden" id="sender_name" value="<?php echo $all_emplyee_info->first_name .' '.$all_emplyee_info->last_name ; ?>" readonly="true">
                                <input type="hidden" id="jk_sender" value="<?php echo $all_emplyee_info->gender; ?>" readonly="true">
                                <input type="hidden" id="jk_receipt" value="<?php echo $all_emplyee_info->direct_leader_gender; ?>" readonly="true">
                        </div>
						
						
                    </div>
                </div>                
        </div>
    </div>
</div>


<script>
    function resend_notif(){
        const email_dl = $("#email_dl").val();
        const doc_num = $("#doc_num").val();
        const link = $("#link").val();
        const receipt_name = $("#receipt_name").val();
        const sender_name = $("#sender_name").val();
        const jk_sender = $("#jk_sender").val();
        const jk_receipt = $("#jk_receipt").val();
        $.ajax({
            url: "<?= base_url(); ?>employee/dashboard/resend_notif",
            type: "post",
            data: {
                email_dl: email_dl,
                doc_num: doc_num,
                link: link,
                receipt_name: receipt_name,
                sender_name: sender_name,
                jk_sender: jk_sender,
                jk_receipt: jk_receipt 
            },
            success: function(data){
                    var result = $.parseJSON(data)
                    Swal.fire(
                        result.message,
                        'Thanks',
                        result.type
                        );
            }
        })
    }
</script>






