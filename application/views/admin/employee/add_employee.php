
<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet"> 
<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>

<?php include_once 'asset/admin-ajax.php'; ?>
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<form role="form" id="employee-form" enctype="multipart/form-data" action="<?php echo base_url() ?>admin/employee/save_employee/<?php
if (!empty($employee_info->employee_id)) {
    echo $employee_info->employee_id;
}
?>" method="post" class="form-horizontal form-groups-bordered">    
    <div class="row">
        <div class="wrap-fpanel">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong><?php echo $this->language->form_heading()[11] ?></strong>
                    </div>
                </div>
            </div>
        </div>
        <!-- ************************ Personal Information Panel Start ************************-->
        <div class="col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><?php echo $this->language->from_body()[12][0] ?></h4>
                </div>
                <div class="panel-body ">
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[12][1] ?> <span class="required"> *</span></label>
                        <input type="text" name="first_name" value="<?php
                        if (!empty($employee_info->first_name)) {
                            echo $employee_info->first_name;
                        }
                        ?>"  class="form-control">
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[12][2] ?> <span class="required"> *</span></label>
                        <input type="text" name="last_name" value="<?php
                        if (!empty($employee_info->last_name)) {
                            echo $employee_info->last_name;
                        }
                        ?>" class="form-control">
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[12][3] ?> <span class="required"> *</span></label>
                        <div class="input-group">
                            <input type="text" name="date_of_birth" value="<?php
                            if (!empty($employee_info->date_of_birth)) {
                                echo $employee_info->date_of_birth;
                            }
                            ?>" class="form-control datepicker" data-format="yyy-mm-dd">
                            <div class="input-group-addon">
                                <a href="#"><i class="entypo-calendar"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[12][4] ?> <span class="required"> *</span></label>
                        <select name="gender" class="form-control" >
                            <option value="">Select Gender ...</option>
                            <option value="Male" <?php
                            if (!empty($employee_info->gender)) {
                                echo $employee_info->gender == 'Male' ? 'selected' : '';
                            }
                            ?>>Male</option>
                            <option value="Female" <?php
                            if (!empty($employee_info->gender)) {
                                echo $employee_info->gender == 'Female' ? 'selected' : '';
                            }
                            ?>>Female</option>
                        </select>
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[12][5] ?><span class="required"> *</span></label>
                        <select name="maratial_status" class="form-control" >
                            <option value="">Select Status ...</option>
                            <option value="Menikah" <?php
                            if (!empty($employee_info->maratial_status)) {
                                echo $employee_info->maratial_status == 'Menikah' ? 'selected' : '';
                            }
                            ?>>Menikah</option>
                            <option value="Belum Menikah" <?php
                            if (!empty($employee_info->maratial_status)) {
                                echo $employee_info->maratial_status == 'Belum Menikah' ? 'selected' : '';
                            }
                            ?>>Belum Menikah</option>
                            <!--
                            <option value="Widowed" <?php
                            if (!empty($employee_info->maratial_status)) {
                                echo $employee_info->maratial_status == 'Widowed' ? 'selected' : '';
                            }
                            ?>>Widowed</option>
                            <option value="Divorced" <?php
                            if (!empty($employee_info->maratial_status)) {
                                echo $employee_info->maratial_status == 'Divorced' ? 'selected' : '';
                            }
                            ?>>Divorced</option>
                            -->
                        </select>
                    </div>
                    <div class="">
                        <label class="control-label" >Religion<span class="required"> *</span></label>
                        <select name="religion" class="form-control" >
                            <option value="">Select Religion ...</option>
                            <option value="Budha" <?php
                            if (!empty($employee_info->religion)) {
                                echo $employee_info->religion == 'Budha' ? 'selected' : '';
                            }
                            ?>>Budha</option>
                            <option value="Islam" <?php
                            if (!empty($employee_info->religion)) {
                                echo $employee_info->religion == 'Islam' ? 'selected' : '';
                            }
                            ?>>Islam</option>                            
                            <option value="Katholik" <?php
                            if (!empty($employee_info->religion)) {
                                echo $employee_info->religion == 'Katholik' ? 'selected' : '';
                            }
                            ?>>Katholik</option>
                            <option value="Kristen" <?php
                            if (!empty($employee_info->religion)) {
                                echo $employee_info->religion == 'Kristen' ? 'selected' : '';
                            }
                            ?>>Kristen Protestan</option>    
                            <option value="Kristen Protestan" <?php
                            if (!empty($employee_info->religion)) {
                                echo $employee_info->religion == 'Kristen Protestan' ? 'selected' : '';
                            }
                            ?>>Kristen</option>                    
                        </select>
                    </div>
                    <div class="">
                        <label class="control-label" >Mother's Name <span class="required"> *</span></label>
                        <input type="text" name="mother_name" value="<?php
                        if (!empty($employee_info->mother_name)) {
                            echo $employee_info->mother_name;
                        }
                        ?>"  class="form-control">
                    </div>
                    <div class="">
                        <label class="control-label"><?php echo $this->language->from_body()[12][7] ?><span class="required"> *</span></label>
                        <select name="nationality" class="form-control col-sm-5" >
                            <option value="" >Select Nationality...</option>
                            <?php foreach ($all_country as $v_country) : ?>
                                <option value="<?php echo $v_country->idCountry ?>" <?php
                                if (!empty($employee_info->country_id)) {
                                    echo $v_country->idCountry == $employee_info->country_id ? 'selected' : '';
                                }
                                ?>><?php echo $v_country->countryName ?></option>
                                    <?php endforeach; ?>
                        </select> 
                        <!--
                        <select name="country_id" class="form-control col-sm-5" >
                            <option value="" >Select Country...</option>
                            <?php foreach ($all_country as $v_country) : ?>
                                <option value="<?php echo $v_country->idCountry ?>" <?php
                                if (!empty($employee_info->country_id)) {
                                    echo $v_country->idCountry == $employee_info->country_id ? 'selected' : '';
                                }
                                ?>><?php echo $v_country->countryName ?></option>
                                    <?php endforeach; ?>
                        </select> 
                        -->
                    </div>
                    <div class="">
                        <label class="control-label" >Personal ID / KTP</label>
                        <input type="text" name="personal_id" value="<?php
                        if (!empty($employee_info->personal_id)) {
                            echo $employee_info->personal_id;
                        }
                        ?>"  class="form-control">
                    </div>

                    <div class="form-group col-sm-12">
                        <div class="form-group col-sm-12">
                            <label for="field-1" class="control-label"><?php echo $this->language->from_body()[12][9] ?> <span class="required">*</span></label>
                            <div class="input-group">     
                                <input type="hidden" name="old_path" value="<?php
                                if (!empty($employee_info->photo_a_path)) {
                                    echo $employee_info->photo_a_path;
                                }
                                ?>">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 150px; height: 150px;">
                                        <?php if (!empty($employee_info->photo)): ?>
                                            <img src="<?php echo base_url() . $employee_info->photo; ?>" >  
                                        <?php else: ?>
                                            <img src="http://placehold.it/350x260" alt="Please Connect Your Internet">     
                                        <?php endif; ?>                                 
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 150px;">
                                        <input type="file" value="<?php if (!empty($employee_info)) echo base_url() . $employee_info->photo; ?>" name="photo" size="20" /><
                                    </div>
                                    <div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new"><input type="file"  name="photo" size="20" /></span>
                                            <span class="fileinput-exists">Change</span>    
                                        </span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                                <div id="valid_msg" style="color: #e11221"></div>
                            </div>
                        </div>
                    </div>
                </div>            
            </div>            
        </div> <!-- ************************ Personal Information Panel End ************************-->       
        <div class="col-sm-6"><!-- ************************ Contact Details Start******************************* -->
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4 class="panel-title"><?php echo $this->language->from_body()[12][16] ?></h4>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[12][17] ?><span class="required"> *</span></label>
                        <textarea id="present" name="present_address" class="form-control" ><?php
                            if (!empty($employee_info->present_address)) {
                                echo $employee_info->present_address;
                            }
                            ?></textarea>
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[1][4] ?><span class="required"> *</span></label>
                        <input type="text" name="city" value="<?php
                        if (!empty($employee_info->city)) {
                            echo $employee_info->city;
                        }
                        ?>" class="form-control" >
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[1][5] ?><span class="required"> *</span></label>
                        <select name="country_id" class="form-control col-sm-5" >
                            <option value="" >Select Country...</option>
                            <?php foreach ($all_country as $v_country) : ?>
                                <option value="<?php echo $v_country->idCountry ?>" <?php
                                if (!empty($employee_info->country_id)) {
                                    echo $v_country->idCountry == $employee_info->country_id ? 'selected' : '';
                                }
                                ?>><?php echo $v_country->countryName ?></option>
                                    <?php endforeach; ?>
                        </select> 
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[1][8] ?><span class="required"> *</span></label>
                        <input type="text" name="mobile" value="<?php
                        if (!empty($employee_info->mobile)) {
                            echo $employee_info->mobile;
                        }
                        ?>" class="form-control">
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[1][7] ?></label>
                        <input type="text" name="phone" value="<?php
                        if (!empty($employee_info->phone)) {
                            echo $employee_info->phone;
                        }
                        ?>" class="form-control">
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[5][0] ?> <span class="required"> *</span></label>
                        <input type="email"  name="email" value="<?php
                        if (!empty($employee_info->email)) {
                            echo $employee_info->email;
                        }
                        ?>"  class="form-control">
                    </div>

                    <div class="">
                        <label class="control-label" >Company Email <span class="required"> *</span></label>
                        <input type="email_2"  name="email_2" value="<?php
                        if (!empty($employee_info->email_2)) {
                            echo $employee_info->email_2;
                        }
                        ?>"  class="form-control">
                    </div>
                </div>
            </div>
        </div> <!-- ************************ Contact Details End ******************************* -->

        <div class="col-sm-6"><!-- ************************ Employee Documents Start ******************************* -->
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4 class="panel-title"><?php echo $this->language->from_body()[12][18] ?></h4>
                    </div>
                </div>
                <div class="panel-body">
                    <!-- CV Upload -->
                    <div class="form-group">
                        <label for="field-1" class="col-sm-4 control-label"><?php echo $this->language->from_body()[12][19] ?></label>
                        <input type="hidden" name="resume_path" value="<?php
                        if (!empty($employee_info->resume_path)) {
                            echo $employee_info->resume_path;
                        }
                        ?>">
                        <input type="hidden" name="document_id" value="<?php
                        if (!empty($employee_info->document_id)) {
                            echo $employee_info->document_id;
                        }
                        ?>">
                        <div class="col-sm-8">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <?php if (!empty($employee_info->resume)): ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" style="display: none" >Select file</span>
                                        <span class="fileinput-exists" style="display: block">Change</span>
                                        <input type="hidden" name="resume" value="<?php echo $employee_info->resume ?>">
                                        <input type="file" name="resume" >
                                    </span>                                    
                                    <span class="fileinput-filename"> <?php echo $employee_info->resume_filename ?></span>                                          
                                <?php else: ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" >Select file</span>
                                        <span class="fileinput-exists" >Change</span>                                            
                                        <input type="file" name="resume" >
                                    </span> 
                                    <span class="fileinput-filename"></span>                                        
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none;">&times;</a>                                                                                                            
                                <?php endif; ?>

                            </div>  
                            <div id="msg_pdf" style="color: #e11221"></div>
                        </div>
                    </div>

                    <!-- Offer Letter Upload -->
                    <div class="form-group">
                        <label for="field-1" class="col-sm-4 control-label"><?php echo $this->language->from_body()[12][20] ?></label>
                        <input type="hidden" name="offer_letter_path" value="<?php
                        if (!empty($employee_info->offer_letter_path)) {
                            echo $employee_info->offer_letter_path;
                        }
                        ?>">
                        <div class="col-sm-8">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <?php if (!empty($employee_info->offer_letter)): ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" style="display: none" >Select file</span>
                                        <span class="fileinput-exists" style="display: block">Change</span>
                                        <input type="hidden" name="offer_letter" value="<?php echo $employee_info->offer_letter ?>">
                                        <input type="file" name="offer_letter" >
                                    </span>                                    
                                    <span class="fileinput-filename"> <?php echo $employee_info->offer_letter_filename ?></span>                                          
                                <?php else: ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" >Select file</span>
                                        <span class="fileinput-exists" >Change</span>                                            
                                        <input type="file" name="offer_letter" >
                                    </span> 
                                    <span class="fileinput-filename"></span>                                        
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none;">&times;</a>                                                                                                            
                                <?php endif; ?>

                            </div>  
                            <div id="msg_pdf" style="color: #e11221"></div>
                        </div>
                    </div>

                    <!-- Joining Letter Upload -->
                    <div class="form-group">
                        <label for="field-1" class="col-sm-4 control-label"><?php echo $this->language->from_body()[12][21] ?></label>
                        <input type="hidden" name="joining_letter_path" value="<?php
                        if (!empty($employee_info->joining_letter_path)) {
                            echo $employee_info->joining_letter_path;
                        }
                        ?>">
                        <div class="col-sm-8">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <?php if (!empty($employee_info->joining_letter)): ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" style="display: none" >Select file</span>
                                        <span class="fileinput-exists" style="display: block">Change</span>
                                        <input type="hidden" name="joining_letter" value="<?php echo $employee_info->joining_letter ?>">
                                        <input type="file" name="joining_letter" >
                                    </span>                                    
                                    <span class="fileinput-filename"> <?php echo $employee_info->offer_letter_filename ?></span>                                          
                                <?php else: ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" >Select file</span>
                                        <span class="fileinput-exists" >Change</span>                                            
                                        <input type="file" name="joining_letter" >
                                    </span> 
                                    <span class="fileinput-filename"></span>                                        
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none;">&times;</a>                                                                                                            
                                <?php endif; ?>

                            </div>  
                            <div id="msg_pdf" style="color: #e11221"></div>
                        </div>
                    </div>

                    <!-- Contract Paper Upload -->
                    <div class="form-group">
                        <label for="field-1" class="col-sm-4 control-label"><?php echo $this->language->from_body()[12][22] ?></label>
                        <input type="hidden" name="contract_paper_path" value="<?php
                        if (!empty($employee_info->contract_paper_path)) {
                            echo $employee_info->contract_paper_path;
                        }
                        ?>">
                        <div class="col-sm-8">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <?php if (!empty($employee_info->contract_paper)): ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" style="display: none" >Select file</span>
                                        <span class="fileinput-exists" style="display: block">Change</span>
                                        <input type="hidden" name="contract_paper" value="<?php echo $employee_info->contract_paper ?>">
                                        <input type="file" name="contract_paper" >
                                    </span>                                    
                                    <span class="fileinput-filename"> <?php echo $employee_info->offer_letter_filename ?></span>                                          
                                <?php else: ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" >Select file</span>
                                        <span class="fileinput-exists" >Change</span>                                            
                                        <input type="file" name="contract_paper" >
                                    </span> 
                                    <span class="fileinput-filename"></span>                                        
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none;">&times;</a>                                                                                                            
                                <?php endif; ?>

                            </div>  
                            <div id="msg_pdf" style="color: #e11221"></div>
                        </div>
                    </div>

                    <!-- ID / Proff Upload -->
                    <div class="form-group">
                        <label for="field-1" class="col-sm-4 control-label"><?php echo $this->language->from_body()[12][23] ?></label>
                        <input type="hidden" name="id_proff_path" value="<?php
                        if (!empty($employee_info->id_proff_path)) {
                            echo $employee_info->id_proff_path;
                        }
                        ?>">
                        <div class="col-sm-8">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <?php if (!empty($employee_info->id_proff)): ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" style="display: none" >Select file</span>
                                        <span class="fileinput-exists" style="display: block">Change</span>
                                        <input type="hidden" name="id_proff" value="<?php echo $employee_info->id_proff ?>">
                                        <input type="file" name="id_proff" >
                                    </span>                                    
                                    <span class="fileinput-filename"> <?php echo $employee_info->offer_letter_filename ?></span>                                          
                                <?php else: ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" >Select file</span>
                                        <span class="fileinput-exists" >Change</span>                                            
                                        <input type="file" name="id_proff" >
                                    </span> 
                                    <span class="fileinput-filename"></span>                                        
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none;">&times;</a>                                                                                                            
                                <?php endif; ?>

                            </div>  
                            <div id="msg_pdf" style="color: #e11221"></div>
                        </div>
                    </div>

                    <!-- Medical Upload -->
                    <div class="form-group">
                        <label for="field-1" class="col-sm-4 control-label"><?php echo $this->language->from_body()[12][24] ?> </label>
                        <input type="hidden" name="other_document_path" value="<?php
                        if (!empty($employee_info->other_document_path)) {
                            echo $employee_info->other_document_path;
                        }
                        ?>">
                        <div class="col-sm-8">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <?php if (!empty($employee_info->other_document)): ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" style="display: none" >Select file</span>
                                        <span class="fileinput-exists" style="display: block">Change</span>
                                        <input type="hidden" name="other_document" value="<?php echo $employee_info->other_document ?>">
                                        <input type="file" name="other_document" >
                                    </span>                                    
                                    <span class="fileinput-filename"> <?php echo $employee_info->other_document_filename ?></span>                                          
                                <?php else: ?>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new" >Select file</span>
                                        <span class="fileinput-exists" >Change</span>                                            
                                        <input type="file" name="other_document" >
                                    </span> 
                                    <span class="fileinput-filename"></span>                                        
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none;">&times;</a>                                                                                                            
                                <?php endif; ?>

                            </div>  
                            <div id="msg_pdf" style="color: #e11221"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- ************************ Employee Documents Start ******************************* -->

        <!-- ************************      Bank Details Start******************************* -->
        <div class="col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <h4 class="panel-title">Tax & Bank Information</h4>
                    </div>
                </div>
                <div class="panel-body">

                    <div class="">
                        <label class="control-label" >Tax Status (Family)<span class="required"> *</span></label>
                        <select name="status_pajak" id="status_pajak" class="form-control" >
                            <option value="">Select Status ...</option>
                            <option value="K/0" <?php
                            if (!empty($employee_info->status_pajak)) {
                                echo $employee_info->status_pajak == 'K/0' ? 'selected' : '';
                            }
                            ?>>K/0</option>
                             <option value="K/1" <?php
                            if (!empty($employee_info->status_pajak)) {
                                echo $employee_info->status_pajak == 'K/1' ? 'selected' : '';
                            }
                            ?>>K/1</option>
                            <option value="K/2" <?php
                            if (!empty($employee_info->status_pajak)) {
                                echo $employee_info->status_pajak == 'K/2' ? 'selected' : '';
                            }
                            ?>>K/2</option>                            
                            <option value="K/3" <?php
                            if (!empty($employee_info->status_pajak)) {
                                echo $employee_info->status_pajak == 'K/3' ? 'selected' : '';
                            }
                            ?>>K/3</option>
                            <option value="K/4" <?php
                            if (!empty($employee_info->status_pajak)) {
                                echo $employee_info->status_pajak == 'K/4' ? 'selected' : '';
                            }
                            ?>>K/4</option>
                            <option value="TK" <?php
                            if (!empty($employee_info->status_pajak)) {
                                echo $employee_info->status_pajak == 'TK' ? 'selected' : '';
                            }
                            ?>>TK</option>
                            <option value="TK/0" <?php
                            if (!empty($employee_info->status_pajak)) {
                                echo $employee_info->status_pajak == 'TK/0' ? 'selected' : '';
                            }
                            ?>>TK/0</option>
                        </select>
                    </div>
                    <div class="">
                        <label class="control-label" >NPWP</label>
                        <input type="text" name="npwp" value="<?php
                        if (!empty($employee_info->npwp)) {
                            echo $employee_info->npwp;
                        }
                        ?>" class="form-control" >
                        
                    </div>

                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[12][26] ?></label>
                        <input type="text" name="bank_name" value="<?php
                        if (!empty($employee_info->bank_name)) {
                            echo $employee_info->bank_name;
                        }
                        ?>" class="form-control" >
                        <input type="hidden" name="employee_bank_id" value="<?php
                        if (!empty($employee_info->employee_bank_id)) {
                            echo $employee_info->employee_bank_id;
                        }
                        ?>" class="form-control" >
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[12][27] ?></label>
                        <input type="text" name="branch_name" value="<?php
                        if (!empty($employee_info->branch_name)) {
                            echo $employee_info->branch_name;
                        }
                        ?>" class="form-control" >
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[12][28] ?></label>
                        <input type="text" name="account_name" value="<?php
                        if (!empty($employee_info->account_name)) {
                            echo $employee_info->account_name;
                        }
                        ?>" class="form-control">
                    </div>
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[12][29] ?></label>
                        <input type="text"  name="account_number" value="<?php
                        if (!empty($employee_info->account_number)) {
                            echo $employee_info->account_number;
                        }
                        ?>" class="form-control">
                    </div>
                </div>
            </div>
        </div><!-- ************************ Bank Details End ******************************* -->        
		
		<div class="col-sm-6"><!-- ************************** ESS Setup column Start  ****************************-->
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">Matrix Approval</h4>
                </div>
                <div class="panel-body">
                            <div class="">  
                                <input type="hidden" name="matrix_approval_id" value="<?php
                                if (!empty($employee_info->matrix_approval_id)) {
                                    echo $employee_info->matrix_approval_id;
                                }
                                ?>" class="form-control" >
                            </div>
                            <!-- <div class="">
                                <label class="control-label">Approval 1<span class="required"> *</span></label>
                                <select name="direct_leader_name" id="approval1" class="form-control col-sm-5">
                                    <option value="" >Select Approval 1...</option>
                                    <?php foreach ($all_employee as $v_employee) : ?>
                                        <option value="<?php echo $v_employee->direct_leader_name ?>"  data-id="<?php echo $v_employee->direct_leader_email ?>" <?php
                                        if (!empty($employee_info->direct_leader_name)) {
                                            echo $v_employee->direct_leader_name == $employee_info->direct_leader_name ? 'selected' : '';
                                        }
                                        ?>><?php echo $v_employee->direct_leader_name ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>

                            <div class="">
                                <label for="field-1" class="control-label">Email Approval 1 <span class="required">*</span><small id="id_error_msg"></small></label>
                                <input type="text" name="direct_leader_email" id="email_approval1"  class="form-control" id="field-1" value="<?php 
                                if (!empty($employee_info)) echo $employee_info->direct_leader_email; 
                                ?>"/>               
                            </div> 

                             <div class="">
                                <label class="control-label">Approval 2 / Acknowledge<span class="required"> *</span></label>
                                <select name="nama_pic_hr" id="approval2" class="form-control col-sm-5" >
                                    <option value="" >Select Approval 2...</option>
                                    <?php foreach ($all_employee as $v_employee) : ?>
                                        <option value="<?php echo $v_employee->nama_pic_hr ?>" data-id="<?php echo $v_employee->email_hr_pic ?>" <?php
                                        if (!empty($employee_info->nama_pic_hr)) {
                                            echo $v_employee->nama_pic_hr == $employee_info->nama_pic_hr ? 'selected' : '';
                                        }
                                        ?>><?php echo $v_employee->nama_pic_hr ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>	

                            <div class="">
                                <label for="field-1" class="control-label">Email Approval 2 / Email Acknowledge<span class="required">*</span><small id="id_error_msg"></small></label>
                                <input type="text" name="email_hr_pic" id="email_approval2"  class="form-control" id="field-1" value="<?php 
                                if (!empty($employee_info)) echo $employee_info->email_hr_pic; 
                                ?>"/>               
                            </div> 	

                            <div class="">
                                <label class="control-label">Hybris Name<span class="required"> *</span></label>
                                <select name="hybris_name" id="hybris" class="form-control col-sm-5" >
                                    <option value="" >Select Hybris...</option>
                                    <?php foreach ($all_employee as $v_employee) : ?>
                                        <option value="<?php echo $v_employee->hybris_name ?>" data-id="<?php echo $v_employee->email_hybris ?>" <?php
                                        if (!empty($employee_info->hybris_name)) {
                                            echo $v_employee->hybris_name == $employee_info->hybris_name ? 'selected' : '';
                                        }
                                        ?>><?php echo $v_employee->hybris_name ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>	

                            <div class="">
                                <label for="field-1" class="control-label">Email Hybris <span class="required">*</span><small id="id_error_msg"></small></label>
                                <input type="text" name="email_hybris"  id="email_hybris" class="form-control" id="field-1" value="<?php 
                                if (!empty($employee_info)) echo $employee_info->email_hybris; 
                                ?>"/>               
                            </div> 				 -->

                            <div class="">
                                <label class="control-label">Approval 1<span class="required"> *</span></label>
                                <select name="direct_leader_name" id="approval1" class="form-control col-sm-5">
                                    <option value="" >Select Approval 1...</option>
                                    <?php foreach ($approval_employee as $v_approval) : ?>
                                        <option value="<?php echo $v_approval->fullname ?>"  data-id="<?php echo $v_approval->email_2;  ?>" data-employmentid="<?php echo $v_approval->employment_id; ?>"
                                        data-directleadergender="<?php echo $v_approval->gender; ?>"
                                        <?php
                                        if (!empty($employee_info->direct_leader_name)) {
                                            echo $v_approval->fullname == $employee_info->direct_leader_name ? 'selected' : '';
                                        }
                                        ?>><?php echo $v_approval->fullname ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>

                            <div class="">
                            
                                <label for="field-1" class="control-label">Email Approval 1 <span class="required">*</span><small id="id_error_msg"></small></label>
                                <input type="text" name="direct_leader_email" id="email_approval1"  readonly class="form-control"  value="<?php 
                                if (!empty($approval1_employee)) echo $approval1_employee->email_2; 
                                ?>"/>  

                                <!--POST data direct leader id  -->
                                <input type="hidden" placeholder="direct leader id" name="direct_leader_id" id="direct_leader_id"  class="form-control"  value="<?php 
                                if (!empty($approval1_employee)) echo $approval1_employee->employment_id; 
                                ?>"/>           
                                <input type="hidden" placeholder="direct leader gender" name="direct_leader_gender" id="direct_leader_gender"  class="form-control"  value="<?php 
                                if (!empty($approval1_employee->gender)) echo $approval1_employee->gender; 
                                ?>"/>   
                                    
                            </div> 

                            <div class="">
                                <label class="control-label">Approval 2<span class="required"></span></label>
                                <select name="direct_leader_name_2" id="approval2" class="form-control col-sm-5">
                                    <option value="" >Select Approval 2...</option>
                                    <?php foreach ($approval_employee as $v_approval) : ?>
                                        <option value="<?php echo $v_approval->fullname ?>"  data-id="<?php echo $v_approval->email_2;  ?>" data-employmentid="<?php echo $v_approval->employment_id; ?>"
                                        data-directleadergender2="<?php echo $v_approval->gender; ?>"
                                        <?php
                                        if (!empty($employee_info->direct_leader_name_2)) {
                                            echo $v_approval->fullname == $employee_info->direct_leader_name_2 ? 'selected' : '';
                                        }
                                        ?>><?php echo $v_approval->fullname ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>

                            <div class="">
                                <label for="field-1" class="control-label">Email Approval 2 <span class="required"></span><small id="id_error_msg"></small></label>
                                <input type="text" name="direct_leader_email_2" id="email_approval2" readonly class="form-control"  
                                value="<?php if (!empty($approval2_employee->email_2)){
                                    echo $approval2_employee->email_2; 
                                        }
                                    ?>"/>  

                                <!--POST data direct leader id 2 -->
                                <input type="hidden" placeholder="direct leader id 2" name="direct_leader_id_2" id="direct_leader_id_2"  class="form-control"  value="<?php 
                                if (!empty($approval2_employee->employment_id)) echo $approval2_employee->employment_id; 
                                ?>"/>       
                                <input type="hidden" placeholder="direct leader gender 2" name="direct_leader_gender_2" id="direct_leader_gender_2"  class="form-control"  value="<?php 
                                if (!empty($approval2_employee->gender)) echo $approval2_employee->gender; 
                                ?>"/>       
                                      
                            </div> 

                             <div class="">
                                <label class="control-label">Approval 3 / Acknowledge<span class="required"> *</span></label>
                                <select name="nama_pic_hr" id="approval3" class="form-control col-sm-5" >
                                    <option value="" >Select Approval 3...</option>
                                    <?php foreach ($hrga_list as $v_hrga) : ?>
                                        <option value="<?php echo $v_hrga->fullname ?>" data-id="<?php echo $v_hrga->email_2 ?>" <?php
                                        if (!empty($employee_info->nama_pic_hr)) {
                                            echo $v_hrga->fullname == $employee_info->nama_pic_hr ? 'selected' : '';
                                        }
                                        ?>><?php echo $v_hrga->fullname ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>	

                            <div class="">
                                <label for="field-1" class="control-label">Email Approval 3 / Email Acknowledge<span class="required">*</span><small id="id_error_msg"></small></label>
                                <input type="text" name="email_hr_pic" id="email_approval3" readonly class="form-control" id="field-1" value="<?php 
                                if (!empty($employee_info)) echo $employee_info->email_hr_pic; 
                                ?>"/>         
                                
                               

                            </div> 	

                            <div class="">
                                <label class="control-label">Hybris Name<span class="required"> *</span></label>
                                <select name="hybris_name" id="hybris" class="form-control col-sm-5" >
                                    <option value="" >Select Hybris...</option>
                                    <?php foreach ($bod_list as $v_bod) : ?>
                                        <option value="<?php echo $v_bod->fullname ?>" data-id="<?php echo $v_bod->email ?>" <?php
                                        if (!empty($employee_info->hybris_name)) {
                                            echo $v_bod->fullname == $employee_info->hybris_name ? 'selected' : '';
                                        }
                                        ?>><?php echo $v_bod->fullname ?></option>
                                    <?php endforeach; ?>
                                </select> 
                            </div>	

                            <div class="">
                                <label for="field-1" class="control-label">Email Hybris <span class="required">*</span><small id="id_error_msg"></small></label>
                                <input type="text" name="email_hybris"  id="email_hybris" readonly class="form-control"  value="<?php 
                                if (!empty($employee_info)) echo $employee_info->email_hybris; 
                                ?>"/>               
                            </div> 	
                        

                </div>
            </div>
        </div><!-- ************************** official status column End  ****************************-->
		
		
        <div class="col-sm-6"><!-- ************************** official status column Start  ****************************-->
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title"><?php echo $this->language->from_body()[12][30] ?></h4>
                </div>
                <div class="panel-body">
                    <div class="">
                        <label for="field-1" class="control-label"><?php echo $this->language->from_body()[12][31] ?> <span class="required">*</span><small id="id_error_msg_emp_id"></small></label>
                        <input type="text" name="employment_id" onchange="check_duplicate_emp_id(this.value)" value="<?php
                        if (!empty($employee_info->employment_id)) {
                            echo $employee_info->employment_id;
                        }
                        ?>" class="form-control" >
                    </div> 

                    <div class="">
                        <label for="field-1" class="control-label">Finger ID <span class="required">*</span><small id="id_error_msg_finger_id"></small></label>
                        <input type="text" name="finger_id" onchange="check_duplicate_finger_id(this.value)" value="<?php
                        if (!empty($employee_info->finger_id)) {
                            echo $employee_info->finger_id;
                        }
                        ?>" class="form-control" >
                    </div> 

                    <?php if (!empty($employee_info->status)) : ?>
                        <div class="">
                            <label class="control-label" >Status <span class="required">*</span></label>
                            <select name="status" class="form-control" id="status" >
                                <option value="1" 
                                <?php
                                echo $employee_info->status == '1' ? 'selected' : '';
                                ?>><?php echo 'Active'; ?></option>                            
                                <option value="2" 
                                <?php
                                echo $employee_info->status == '2' ? 'selected' : '';
                                ?>><?php echo 'Inactive'; ?></option>                            

                            </select>
                        </div>                    
                    <?php endif; ?>

                    <div class="" id="resign_date" style="display:none">
                        <label for="field-1" class="control-label" id="resign_date" >Resign Date <span class="required">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control datepicker" name="resign_date" id="resign_date"   value="<?php
                            if (!empty($employee_info->resign_date)) {
                                echo $employee_info->resign_date;
                            }
                            ?>" data-format="yyyy/mm/dd" >

                             <div class="input-group-addon"  >
                                <a href="#"><i class="entypo-calendar"></i></a>
                            </div> 
                        </div>
                    </div> 

                    <div class="" id="resign_status" style="display:none">
                        <label for="field-1" class="control-label" id="resign_status" >Resign Status <span class="required">*</span></label>
                        <div class="">
                            <textarea id="resign_status" name="resign_status" class="form-control" ><?php
                            if (!empty($employee_info->resign_status)) {
                                echo $employee_info->resign_status;
                            }
                            ?></textarea>     
                        </div>
                    </div> 

                    <div class="">
                        <label class="control-label">Company<span class="required"> *</span></label>
                        <select name="company_id" id="selectCompany" class="form-control col-sm-5" >                        
                            <option value="" >Select Company...</option>
                            <?php foreach ($all_company as $v_company) : ?>
                                <option value="<?php echo $v_company->id ?>" <?php
                                if (!empty($employee_info->company_id)) {
                                    echo $v_company->id == $employee_info->company_id ? 'selected' : '';
                                }
                                ?>><?php echo $v_company->name ?></option>
                                    <?php endforeach; ?>                               
                        </select> 
                    </div> 

                    <div class="">
                        <label class="control-label">Department<span class="required"> *</span></label>
                        <select name="department_id" id="department" class="form-control col-sm-5" >                        
                            <option value="" >Select Department...</option>
                            <?php foreach ($all_department as $v_department) : ?>
                                <option value="<?php echo $v_department->department_id ?>" <?php
                                if (!empty($employee_info->department_id)) {
                                    echo $v_department->department_id == $employee_info->department_id ? 'selected' : '';
                                }
                                ?>><?php echo $v_department->department_name ?></option>
                                    <?php endforeach; ?>                               
                        </select> 
                    </div> 

               
                    <div class="">
                        <label class="control-label" ><?php echo $this->language->from_body()[12][32] ?> <span class="required">*</span></label>
                        <select name="designations_id" class="form-control" >                            
                            <option value="">Select Designation....</option>
                            <?php if (!empty($all_department_info)): foreach ($all_department_info as $dept_name => $v_department_info) : ?>
                                    <?php if (!empty($v_department_info)): ?>
                                       
                                        <optgroup label="<?php echo $this->db->get_where('company', ['id' => $v_department_info[0]->company_id])->row_array()['name'] .' - '. $dept_name; ?>" >
                                            <?php foreach ($v_department_info as $designation) : ?>
                                                <option value="<?php echo $designation->designations_id; ?>" 
                                                <?php
                                                if (!empty($employee_info->designations_id)) {
                                                    echo $designation->designations_id == $employee_info->designations_id ? 'selected' : '';                                                        
                                                }
                                                ?>><?php 
                                                    echo $designation->designations 
                                                    ?>
                                                </option>                            
                                                    <?php endforeach; ?>
                                        </optgroup>
                                    <?php endif; ?>                      
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>      
                    
                    <div class="">
                        <label class="control-label">Level / Jabatan<span class="required"> *</span></label>
                        <select name="level" class="form-control col-sm-5" >                        
                            <option value="" >Select Level...</option>
                            <?php foreach ($all_level as $v_level) : ?>
                                <option value="<?php echo $v_level->level_name ?>" <?php
                                if (!empty($employee_info->level)) {
                                    echo $v_level->level_name == $employee_info->level ? 'selected' : '';
                                }
                                ?>><?php echo $v_level->level_name ?></option>
                                    <?php endforeach; ?>                               
                        </select> 
                    </div>            

                     <div class="">
                        <label class="control-label">Penempatan <span class="required"> *</span></label>
                        <select name="penempatan" class="form-control col-sm-5" >                        
                            <option value="" >Select Penempatan...</option>
                            <?php foreach ($all_lokasi as $v_lokasi) : ?>
                                <option value="<?php echo $v_lokasi->lokasi_name ?>" <?php
                                if (!empty($employee_info->penempatan)) {
                                    echo $v_lokasi->lokasi_name == $employee_info->penempatan ? 'selected' : '';
                                }
                                ?>><?php echo $v_lokasi->lokasi_name ?></option>
                                    <?php endforeach; ?>                               
                        </select> 
                    </div>                   

                    <div class="">
                        <label for="field-1" class="control-label"><?php echo $this->language->from_body()[12][33] ?> <span class="required">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control datepicker" name="joining_date" id="joining_date" value="<?php
                            if (!empty($employee_info->joining_date)) {
                                echo $employee_info->joining_date;
                            }
                            ?>" data-format="yyyy/mm/dd" >
                            <div class="input-group-addon">
                                <a href="#"><i class="entypo-calendar"></i></a>
                            </div>
                        </div>
                    </div> 

                    <div class="">
                        <label class="control-label" >Employment Status<span class="required"> *</span></label>
                        <select name="employment_status" id="employment_status" class="form-control" >
                            <option value="">Select Status ...</option>
                            <option value="Permanent" <?php
                            if (!empty($employee_info->employment_status)) {
                                echo $employee_info->employment_status == 'Permanent' ? 'selected' : '';
                            }
                            ?>>Permanent</option>
                             <option value="Magang" <?php
                            if (!empty($employee_info->employment_status)) {
                                echo $employee_info->employment_status == 'Magang' ? 'selected' : '';
                            }
                            ?>>Magang</option>
                            <option value="PKWT1" <?php
                            if (!empty($employee_info->employment_status)) {
                                echo $employee_info->employment_status == 'PKWT1' ? 'selected' : '';
                            }
                            ?>>PKWT1</option>                            
                            <option value="PKWT2" <?php
                            if (!empty($employee_info->employment_status)) {
                                echo $employee_info->employment_status == 'PKWT2' ? 'selected' : '';
                            }
                            ?>>PKWT2</option>
                            <option value="PKWT3" <?php
                            if (!empty($employee_info->employment_status)) {
                                echo $employee_info->employment_status == 'PKWT3' ? 'selected' : '';
                            }
                            ?>>PKWT3</option>
                            <option value="PKWTT" <?php
                            if (!empty($employee_info->employment_status)) {
                                echo $employee_info->employment_status == 'PKWTT' ? 'selected' : '';
                            }
                            ?>>PKWTT</option>
                             <option value="Outsource" <?php
                            if (!empty($employee_info->employment_status)) {
                                echo $employee_info->employment_status == 'Outsource' ? 'selected' : '';
                            }
                            ?>>Outsource</option>
                        </select>
                    </div>

                    <div class="" id="contractperiod" style="display:none">
                        <label class="control-label" >Contract Period<span class="required"> *</span></label>
                        <select name="contract_period" id="contract_period" class="form-control" >
                            <option value="">Select Contract Period...</option>
                            <?php 						
                                 for($i=1; $i<=24; $i++):   
                            ?>
                        
                                <option value="<?php echo $i; ?>" <?php
                                    if(!empty($employee_info->contract_period)){
                                        echo $i ? 'selected' : '';
                                    }
                                ?>><?php echo $i; ?></option>

                            <?php
                            endfor;
                            ?>       
                        </select>
                    </div>

                    <?php if(!empty($employee_info->end_of_contract_date)){?>    

                        <div class="" id="end_of_contract_date" style="display:block">
                            <label for="field-1" class="control-label" id="end_of_contract_date" >End Of Contract Date <span class="required">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control datepicker" name="end_of_contract_date" id="contract_date"   value="<?php
                                if (!empty($employee_info->end_of_contract_date)) {
                                    echo $employee_info->end_of_contract_date;
                                }
                                ?>" data-format="yyyy/mm/dd" >

                                <div class="input-group-addon"  >
                                    <a href="#"><i class="entypo-calendar"></i></a>
                                </div> 
                            </div>
                        </div> 

                    <?php }else{ ?>

                        <div class="" id="end_of_contract_date" style="display:none">
                            <label for="field-1" class="control-label" id="end_of_contract_date" >End Of Contract Date <span class="required">*</span></label>
                            <div class="input-group">
                                <input type="text" class="form-control datepicker" name="end_of_contract_date" id="contract_date"   value="<?php
                                if (!empty($employee_info->end_of_contract_date)) {
                                    echo $employee_info->end_of_contract_date;
                                }
                                ?>" data-format="yyyy/mm/dd" >

                                <div class="input-group-addon"  >
                                    <a href="#"><i class="entypo-calendar"></i></a>
                                </div> 
                            </div>
                        </div> 

                    <?php } ?>

                     <!--hapus readonly utk mengaktifkan text field input saldo cuti tahunan-->
                     <div class="">
                        <label class="control-label" >Saldo Cuti Tahunan</label>
                        <input type="text"  name="saldo_cuti" <?php
                        if(!empty($employee_info->saldo_cuti)){
                            // if($employee_info->saldo_cuti=='0'){
                                echo $employee_info->saldo_cuti;
                            // } 
                        }                    
                        ?>" class="form-control">
                    </div>
                    
                    <div class="">  
                        <input type="hidden" name="saldo_cuti_id" value="<?php
                        if (!empty($employee_info->saldo_cuti_id)) {
                            echo $employee_info->saldo_cuti_id;
                        }
                        ?>" class="form-control" >
                    </div>

                <div>
                        <label class="control-label" >Saldo Point</label>
                        <input type="text"  name="saldo_point" readonly 
                        value="<?php 
                                if(!empty($employee_info->saldo_point)){
                                    echo number_format($employee_info->saldo_point,0);
                                }                    
                                ?>" 
                        class="form-control">
                    </div>
                    
                  <!--  <div>  
                        <input type="hidden" name="saldo_point_id" value="<?php
                        if (!empty($employee_info->saldo_point_id)) {
                            echo $employee_info->saldo_point_id;
                        }
                        ?>" class="form-control" >
                    </div>-->

                </div>

            </div>
        </div><!-- ************************** official status column End  ****************************-->
		
		<div class="col-sm-6"><!-- ************************** ESS Setup column Start  ****************************-->
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">ESS Setup</h4>
                </div>
                <div class="panel-body">

                     <div class="">  
                        <input type="hidden" name="ess_id" value="<?php
                        if (!empty($employee_info->ess_id)) {
                            echo $employee_info->ess_id;
                        }
                        ?>" class="form-control" >
                    </div>       
					 <div class="form-group">
                            <label for="field-1" class="col-sm-5 control-label">Overtime Application  <span class="required">*</span></label>

                            <div class="col-sm-5">                            
                                <input data-toggle="toggle" name="overtime_application" value="1" <?php
                                if (!empty($employee_info) && $employee_info->overtime_application == '1') {
                                    echo 'checked';
                                }
                                ?> data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>                                            
                        <div class="form-group">
                            <label for="field-1" class="col-sm-5 control-label">Permit Application  <span class="required">*</span></label>

                            <div class="col-sm-5">                            
                                <input  data-toggle="toggle" <?php
                                if (!empty($employee_info) && $employee_info->permit_application == '1') {
                                    echo 'checked';
                                }
                                ?> name="permit_application" value="1" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label for="field-1" class="col-sm-5 control-label">Leave Application <span class="required">*</span></label>

                            <div class="col-sm-5">                            
                                <input  data-toggle="toggle" <?php
                                if (!empty($employee_info) && $employee_info->leave_application == '1') {
                                    echo 'checked';
                                }
                                ?> name="leave_application" value="1" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-5 control-label">Redeem Application <span class="required">*</span></label>

                            <div class="col-sm-5">                            
                                <input  data-toggle="toggle" <?php
                                if (!empty($employee_info) && $employee_info->redeem_point == '1') {
                                    echo 'checked';
                                }
                                ?> name="redeem_point" value="1" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-5 control-label">Perdin Application <span class="required">*</span></label>

                            <div class="col-sm-5">                            
                                <input  data-toggle="toggle" <?php
                                if (!empty($employee_info) && $employee_info->perdin_application == '1') {
                                    echo 'checked';
                                }
                                ?> name="perdin_application" value="1" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>


                </div>
            </div>
        </div><!-- ************************** official status column End  ****************************-->
		
        <div class="col-sm-6 margin pull-right">
            <button id="btn_emp" type="submit" class="btn btn-primary btn-block"><?php echo $this->language->from_body()[1][12] ?></button>
        </div>    
    </div>    
</form>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<script>
function get_dept_id(e) {
    document.getElementById("department_id").value = e.target.value
}

$(document).ready(function() {

    var date    = new Date();
    // var today   = new Date(date.getFullYear(), date.getMonth(), date.getDate());

    var optSimple = {
        format: 'mm-dd-yyyy',
        todayHighlight: true,
        orientation: 'bottom right',
        autoclose: true,
        container: '#sandbox'
    };

    // set current date to joining date
    //$( '#joining_date' ).datepicker( optSimple );
    //$( '#joining_date' ).datepicker( 'setDate', today );
        // var status_employee = <?php //echo $employee_info->status ?>
        // alert('status_employee');

        // if (status_employee === '2'){ 
            
        //     $('div[id=resign_date]').show();
        //     $('div[id=resign_status]').show();
            
        // }

    $("#employment_status").change(function() {
        if ($(this).val() === 'PKWT1' || $(this).val() === 'PKWT2' || $(this).val() === 'PKWT3' || $(this).val() === 'PKWTT'){ 
            $('div[id=contractperiod]').show();
            
        }else {
            $('div[id=contractperiod]').hide();
        
        }
    });


    $("#employment_status").change(function() {
        if ($(this).val() === 'Permanent'){ 
            $('input[id=contract_date]').val('');
            $('div[id=end_of_contract_date]').hide();
            
        }
    });

   
    //populate data from dropdown menu
    $('#approval1').change(function(){
        //get the selected option's data-id value using jquery `.data()`
        var email_approval1 =  $(':selected',this).data('id'); 
        var direct_leader_id =  $(':selected',this).data('employmentid'); 
        var direct_leader_gender =  $(':selected',this).data('directleadergender');
        // var direct_leader_id =  JSON.parse($('#approval1').attr('data-id'))[1]; 
        
        //populate data.
        $('#email_approval1').val(email_approval1);
        $('#direct_leader_id').val(direct_leader_id);
        $('#direct_leader_gender').val(direct_leader_gender);
    });  

    $('#approval2').change(function(){
        //get the selected option's data-id value using jquery `.data()`
        var email_approval2 =  $(':selected',this).data('id'); 
        var direct_leader_id2 =  $(':selected',this).data('employmentid'); 
        var direct_leader_gender2 =  $(':selected',this).data('directleadergender2');
        //populate data.
        $('#email_approval2').val(email_approval2);
        $('#direct_leader_id_2').val(direct_leader_id2);
        $('#direct_leader_gender_2').val(direct_leader_gender2);
        
    });    

    $('#approval3').change(function(){
        //get the selected option's data-id value using jquery `.data()`
        var email_approval3 =  $(':selected',this).data('id'); 
        //populate data.
        $('#email_approval3').val(email_approval3);
    });   

    $('#hybris').change(function(){
        //get the selected option's data-id value using jquery `.data()`
        var email_hybris =  $(':selected',this).data('id'); 
        //populate data.
        $('#email_hybris').val(email_hybris);
    });      

    //show form resignation even select inactive status 
    $('#status').change(function() {
        if ($(this).val() === '2'){ 
            $('div[id=resign_date]').show();
            $('div[id=resign_status]').show();
            
        }
        else {
            $('div[id=resign_date]').hide();
            $('div[id=resign_status]').hide();

        }

    });

    $("#contract_period").change(function() {
        if ($(this).val() === '1'){ 
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth(), date.getDate()+30);	
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '2'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+1, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '3'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+2, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '4'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+3, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '5'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+4, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '6'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+5, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '7'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+6, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '8'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+7, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '9'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+8, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '10'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+9, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '11'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+10, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '12'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+11, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '13'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+12, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '14'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+13, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '15'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+14, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '16'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+15, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '17'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+16, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '18'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+17, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '19'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+18, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '20'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+19, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '21'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+20, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '22'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+21, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '23'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+22, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else if($(this).val() === '24'){
            $('div[id=end_of_contract_date]').show();
            var end_contract_date = new Date(date.getFullYear(), date.getMonth()+23, date.getDate()+30);
            $( '#contract_date' ).datepicker( 'setDate', end_contract_date );
        }else {
            $('div[id=end_of_contract_date]').hide();
            
        }

    });



    $("#selectCompany").change(function(){
        //   $("#department").select({
        //         placeholder: 'Loading..'
        //     })
        const id = $('#selectCompany').val();
        // alert(id);
        $.ajax({
          url: "<?= base_url(); ?>admin/attendance/getDepartment",
          type: "post",
          dataType: "json",
          async: true,
          data: {
              id: id
          },
            success: function(data){
                // alert(JSON.stringify(data));
                $("#department").html(data);
			}
		}) 
    });
  

});

</script>