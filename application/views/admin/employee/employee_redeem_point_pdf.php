<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
    </head>

    <body style="width: 100%;">
        <div style="width: 100%; border-bottom: 2px solid black;">
            <table style="width: 100%; vertical-align: middle;">
                <tr>
                    <?php
                    $genaral_info = $this->session->userdata('genaral_info');
                    if (!empty($genaral_info)) {
                        foreach ($genaral_info as $info) {
                            ?>
                            <td style="width: 35px; border: 0px;">
                                <img style="width: 150px;height: auto" src="<?php echo base_url() . $info->logo ?>" alt="" class="img-circle"/>
                            </td>
                            <td style="border: 0px;">
                                <p style="margin-left: 10px; font: 14px lighter;"><?php echo $info->name ?></p>
                            </td>
                            <?php
                        }
                    } else {
                        ?>
                        <td style="width: 35px; border: 0px;">
                            <img style="width: 50px;height: 50px" src="<?php echo base_url() ?>img/logo.png" alt="Logo" class="img-circle"/>
                        </td>
                        <td style="border: 0px;">
                            <p style="margin-left: 10px; font: 14px lighter;">Human Resource Management System</p>
                        </td>
                        <?php
                    }
                    ?>
                </tr>
            </table>
        </div>
        <br/>
        <br/>
        <div style="padding: 5px 0; width: 100%;">
            <div>
                <table style="width: 100%; font-size: 13px;">

                    <tr>
                        <td style="width: 30%;text-align: right;"><strong>Doc. Number :</strong></td>
                        <td style="">&nbsp; <?php echo "$redeem_info->doc_number"; ?></td>
                    </tr> 
                    <tr>
                        <td style="width: 30%;text-align: right;"><strong>Name :</strong></td>
                        <td style="">&nbsp; <?php echo $redeem_info->first_name . ' ' . $redeem_info->last_name; ?></td>
                    </tr> 
                    <tr>
                        <td style="width: 30%;text-align: right;"><strong>Redeem point Date :</strong></td>
                        <td style="">&nbsp; <span class="text-danger"><?php echo date('d M Y', strtotime($redeem_info->submit_date)); ?></span></td>
                    </tr> 
                    <tr>
                        <td style="width: 30%;text-align: right;"><strong>Sisa Point :</strong></td>
                        <td style="">&nbsp; <?php echo round($saldo_point->saldo_point,0); ?></td>
                    </tr> 
                    <tr>
                        <td style="width: 30%;text-align: right;"><strong>Total Redeem :</strong></td>
                        <td style="">&nbsp; <?php echo round($redeem_info->total_redeem,0); ?></td>
                    </tr> 
                    <tr>
                        <td style="width: 30%;text-align: right;"><strong>Status :</strong></td>
                        <td style="">&nbsp;  
								<?php if($redeem_info->status_redeem=='cancel'){?>
									<span class="label label-danger"><?php echo $redeem_info->status_redeem; ?></span>
								<?php } ?>
								<?php if($redeem_info->status_redeem=='pending'){?>
									<span class="label label-warning"><?php echo $redeem_info->status_redeem; ?></span>
								<?php } ?>
								<?php if($redeem_info->status_redeem=='fully approved'){?>
									<span class="label label-success"><?php echo $redeem_info->status_redeem; ?></span>
								<?php } ?>
								<?php if($redeem_info->status_redeem=='partial approved'){?>
									<span class="label label-info"><?php echo $redeem_info->status_redeem; ?></span>
								<?php } ?>
                        </td>
                    </tr> 

                    <tr>
                        <td style="width: 30%;text-align: right;"><strong>Created By :</strong></td>
                        <td style="">&nbsp; <?php echo $redeem_info->first_name . ' ' . $redeem_info->last_name; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($redeem_info->submit_date)); ?></strong>
							
                        </td>
                    </tr> 

                    <?php if($redeem_info->approve1!=''){?>
                    <tr>
                        <td style="width: 30%;text-align: right;"><strong>Approved By :</strong></td>
                        <td style="">&nbsp; <?php echo $redeem_info->approve1; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($redeem_info->date_approve1)); ?></strong>
							
                        </td>
                    </tr> 
                    <?php } ?>

                    <?php if($redeem_info->unapprove1!=''){?>
                    <tr>
                        <td style="width: 30%;text-align: right;"><strong>Unapproved By :</strong></td>
                        <td style="">&nbsp; <?php echo $redeem_info->unapprove1; ?>
							pada tanggal <strong><?php echo $redeem_info->date_unapprove1; ?></strong>
							
                        </td>
                    </tr> 
					<?php } ?>
                                
                </table>
            </div>
        </div>

        <div style="width: 100%; margin-top: 55px;">
            <div >
                <div style="width: 100%; background: #E3E3E3;padding: 1px 0px 1px 10px; color: black; vertical-align: middle; ">
                    <p style="margin-left: 10px; font-size: 15px; font-weight: lighter;"><strong>Product Details</strong></p>
                </div>
                <br />
                <table style="width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse;">
                        <thead>
                            <tr >
                                <th style="border: 1px solid black;">No</th>
                                <th style="border: 1px solid black;">Product ID</th>
                                <th style="border: 1px solid black;">Product Name</th>    
                                <th style="border: 1px solid black;">Qty</th>                            
                                <th style="border: 1px solid black;">Price</th>
                                <th style="border: 1px solid black;">Subtotal</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $key = 1 ;
                                  $total = 0;
                            ?>
                            <?php if (!empty($product_list_info)): for ($i = 0; $i<count($product_list_info); $i++) { ?>
                                    <tr>
                                        <td style="border: 1px solid black;"><?php echo $key ?></td>
                                        <td style="border: 1px solid black;"><?php echo $product_list_info[$i]->product_id ?></td>
                                        <td style="border: 1px solid black;"><?php echo $product_list_info[$i]->product_name ?></td>      
                                        <td style="border: 1px solid black;"><?php echo $product_list_info[$i]->qty ?></td>                                     
                                        <td style="border: 1px solid black; float:right;"><?php echo $product_list_info[$i]->price ?></td>
                                        <td style="border: 1px solid black; float:right;"><?php echo $product_list_info[$i]->subtotal ?></td>
                                        <td></td>
                                    </tr>
                                    <?php
                                    $key++;
                                    $total += $product_list_info[$i]->subtotal ;
                            } ?>
                        <?php else : ?>
                        <?php endif; ?>
                        </tbody>
                        
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="border: 1px solid black; float:right;"><span>TOTAL</span></th>                               
                            <th style="border: 1px solid black; float:right;" id="subtotal"><?php echo round($total,0);?></th>
                            <th></th>
                        </tr>
                </table>
                <br />
               
            </div>
        </div>          
    </body>
</html>