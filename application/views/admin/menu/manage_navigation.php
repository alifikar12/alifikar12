<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<h4><?php echo anchor('admin/navigation/add_navigation', '<i class="fa fa-plus"></i> Add Menu'); ?></h4>
<br/>
<div class="row">
    <div class="col-sm-12 std_print" data-spy="scroll" data-offset="0">                            
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">
                <div class="panel-title">
                    <strong>Manage Navigation</strong>
                </div>
            </div>
            <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr class="active" >
                        <th>ID</th>
                        <th>Label</th>
                        <!-- <th>Role</th> -->
                        <th style="text-align: center;">Administrator</th>
                        <th style="text-align: center;">BOD</th>
                        <th style="text-align: center;">HRGA</th>
                        <th style="text-align: center;">HRGA-Manager</th>
                        <th style="text-align: center;">IT</th>
                        <th style="text-align: center;">Payroll</th>
                        <th style="text-align: center;">Marketing</th>
                        <th>Slug</th>
                        <th style="text-align: center;">Icon</th>
                        <th style="text-align: center;">Parent</th>                                             
                        <th style="text-align: center;">Sort</th>
                        <th class="">Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php if (count($nav)): foreach ($nav as $v_nav) : ?>

                            <tr>
                                <td><?php echo $v_nav->menu_id ?></td>                                
                                <td><?php echo $v_nav->English ?></td>
                                <!-- <td><?php 
                                        if($v_nav->Administrator == 1){
                                            echo 'Administrator ';
                                        } 
                                        if($v_nav->BOD == 1){
                                            echo 'BOD ';
                                        } 
                                        if($v_nav->IT == 1){
                                            echo 'IT ';
                                        } 
                                    ?>
                                </td> -->
                                <td style="text-align: center;"> 
                                    <input  type="checkbox" name="Administrator" value="<?php echo $v_nav->Administrator ?>" 
                                    <?php
                                        if ($v_nav->Administrator == 1) {
                                            ?>
                                            checked
                                            <?php
                                        }
                                    ?> onclick="return false;" />
                                </td>
                                <td style="text-align: center;">
                                    <input  type="checkbox" name="BOD" value="<?php echo $v_nav->BOD ?>" 
                                    <?php
                                        if ($v_nav->BOD == 1) {
                                            ?>
                                            checked
                                            <?php
                                        }
                                    ?> onclick="return false;" />
                                </td>
                                <td style="text-align: center;">
                                    <input  type="checkbox" name="HRGA" value="<?php echo $v_nav->HRGA ?>" 
                                    <?php
                                        if ($v_nav->HRGA == 1) {
                                            ?>
                                            checked
                                            <?php
                                        }
                                    ?> onclick="return false;" />
                                </td>
                                <td style="text-align: center;">
                                    <input  type="checkbox" name="HRGAManager" value="<?php echo $v_nav->HRGAManager ?>" 
                                    <?php
                                        if ($v_nav->HRGAManager == 1) {
                                            ?>
                                            checked
                                            <?php
                                        }
                                    ?> onclick="return false;" />
                                </td>
                                <td style="text-align: center;">
                                    <input  type="checkbox" name="IT" value="<?php echo $v_nav->IT ?>" 
                                    <?php
                                        if ($v_nav->IT == 1) {
                                            ?>
                                            checked
                                            <?php
                                        }
                                    ?> onclick="return false;" />
                                </td>
                                <td style="text-align: center;">
                                    <input  type="checkbox" name="Payroll" value="<?php echo $v_nav->Payroll ?>" 
                                    <?php
                                        if ($v_nav->Payroll == 1) {
                                            ?>
                                            checked
                                            <?php
                                        }
                                    ?> onclick="return false;" />
                                </td>
                                <td style="text-align: center;">
                                    <input  type="checkbox" name="Marketing" value="<?php echo $v_nav->Marketing ?>" 
                                    <?php
                                        if ($v_nav->Marketing == 1) {
                                            ?>
                                            checked
                                            <?php
                                        }
                                    ?> onclick="return false;" />
                                </td>
                                <td><?php echo $v_nav->link ?></td>
                                <td style="text-align: center;"><i class="<?php echo $v_nav->icon ?>"></i></td>
                                <td style="text-align: center;"><?php echo $v_nav->parent ?></td>   
                                <td style="text-align: center;"><?php echo $v_nav->sort ?></td> 
                                <td>
                                    <div class="btn-group" role="group" aria-label="...">
                                        <a type="button" class="btn btn-success btn-xs" href="<?php echo base_url() ?>admin/navigation/add_navigation/<?php echo $v_nav->menu_id ?>"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                        <a type="button" class="btn btn-danger btn-xs" href="<?php echo base_url() ?>admin/navigation/delete_navigation/<?php echo $v_nav->menu_id ?>"><i class="fa fa-times"></i> Delete</a>

                                    </div>
                                </td> 

                            </tr>
                            <?php
                        endforeach;
                        ?>
                    <?php else : ?>
                    <td colspan="3">
                        <strong>There is no Record for display!</strong>
                    </td>
                <?php endif; ?>
                </tbody>
            </table> 
            </div>
        </div>
    </div>
</div>
