<link href="<?php echo base_url() ?>asset/css/bootstrap-toggle.min.css" rel="stylesheet"> 
<script src="<?php echo base_url() ?>asset/js/bootstrap-toggle.min.js"></script>

<div class="row">
    <div class="col-sm-12">
        <div class="wrap-fpanel">
            <div class="panel panel-default" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>Add Navigation</strong>
                        <?php
                        // print_r($fontawesome);
                        // die;
                        ?>
                    </div>                
                </div>
                <div class="panel-body">

                    <form role="form" id="form" action="<?php echo base_url(); ?>admin/navigation/save_navigation/<?php if(!empty($menu_info->menu_id)){ echo $menu_info->menu_id;} ?>" method="post" class="form-horizontal form-groups-bordered">

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Label<span class="required"> *</span></label>
                            <div class="col-sm-5">                         
                                <input type="text" name="label" value="<?php echo $menu_info->English ?>" class="form-control" placeholder="Menu Label"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Slug<span class="required"> *</span></label>
                            <div class="col-sm-5">                         
                                <input type="text" name="link" value="<?php echo $menu_info->link ?>" class="form-control" placeholder="Menu Link"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Icon<span class="required"> *</span></label>
                            <div class="col-sm-5">
                            <link href="<?php echo base_url(); ?>asset/css/font-icons/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />               
                                <!-- <input type="text" name="icon" value="<?php echo $menu_info->icon ?>" class="form-control" placeholder="Menu Icon"/> -->
                                <select name="icon" class="form-control" style="font-family: fontAwesome; font-size:16px;">
                                    <option value="">Select Icon...</option>
                                    
                                   <?php if (count($fontawesome)): foreach ($fontawesome as $v_fontawesome) : ?>
                                    <option  value="<?php echo $v_fontawesome->name ?>" <?php echo $menu_info->icon == $v_fontawesome->name ? 'selected':'' ?>>
                                            <?php echo $v_fontawesome->code."; ". $v_fontawesome->label ?>
                                            <!-- <i class="fa fa-external-link"></i> -->
                                            <!-- <span><?php echo $v_fontawesome->label; ?></span> -->
                                    </option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Parent</label>
                            <div class="col-sm-5">                         
                                <select name="parent" class="form-control">
                                    <option value="">Select Parent...</option>
                                    
                                   <?php if (count($nav)): foreach ($nav as $v_nav) : ?>
                                    <option value="<?php echo $v_nav->parent_id ?>"
                                            <?php echo $menu_info->parent == $v_nav->parent_id ? 'selected':'' ?>>
                                            <?php echo $v_nav->English ?></option>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Sort<span class="required"> *</span></label>
                            <div class="col-sm-5">                         
                                <input type="text" name="sort" value="<?php echo $menu_info->sort ?>" class="form-control" placeholder="Menu Sorting"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Administrator  <span class="required">*</span></label>
                            <div class="col-sm-5">                            
                                <input data-toggle="toggle" name="Administrator" value="1" <?php
                                if (!empty($menu_info) && $menu_info->Administrator == '1') {
                                    echo 'checked';
                                }
                                ?> data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">BOD  <span class="required">*</span></label>
                            <div class="col-sm-5">                            
                                <input data-toggle="toggle" name="BOD" value="1" <?php
                                if (!empty($menu_info) && $menu_info->BOD == '1') {
                                    echo 'checked';
                                }
                                ?> data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">IT  <span class="required">*</span></label>
                            <div class="col-sm-5">                            
                                <input data-toggle="toggle" name="IT" value="1" <?php
                                if (!empty($menu_info) && $menu_info->IT == '1') {
                                    echo 'checked';
                                }
                                ?> data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>  

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">HRGA  <span class="required">*</span></label>
                            <div class="col-sm-5">                            
                                <input data-toggle="toggle" name="HRGA" value="1" <?php
                                if (!empty($menu_info) && $menu_info->HRGA == '1') {
                                    echo 'checked';
                                }
                                ?> data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>  

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">HRGAManager  <span class="required">*</span></label>
                            <div class="col-sm-5">                            
                                <input data-toggle="toggle" name="HRGAManager" value="1" <?php
                                if (!empty($menu_info) && $menu_info->HRGAManager == '1') {
                                    echo 'checked';
                                }
                                ?> data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>  

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Payroll  <span class="required">*</span></label>
                            <div class="col-sm-5">                            
                                <input data-toggle="toggle" name="Payroll" value="1" <?php
                                if (!empty($menu_info) && $menu_info->Payroll == '1') {
                                    echo 'checked';
                                }
                                ?> data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>  

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Marketing  <span class="required">*</span></label>
                            <div class="col-sm-5">                            
                                <input data-toggle="toggle" name="Marketing" value="1" <?php
                                if (!empty($menu_info) && $menu_info->Marketing == '1') {
                                    echo 'checked';
                                }
                                ?> data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" type="checkbox">
                            </div>                            
                        </div>  

                        
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5 pull-right">
                                <button type="submit" id="sbtn" class="btn btn-primary">Save</button>                            
                            </div>
                        </div>   
                    </form>
                </div>
            </div>
        </div>
        </div>
    </div>
