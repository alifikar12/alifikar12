<div class="col-md-12">
    <div class="wrap-fpanel">
        <div class="panel panel-default">
            <!-- Default panel contents -->

            <div class="panel-heading">
                <div class="row">
                    <div class="col-lg-12 panel-title">                 
                        <h3  class="col-lg-4 col-md-4 col-sm-4">Redeem Point Application  Details</h3>
                            <div class="pull-right">         
                                <span><?php echo btn_pdf('admin/employee/make_pdf_redeem_point/' . $redeem_info->doc_number); ?></span>
                                <button class="margin btn-print" type="button" data-toggle="tooltip" title="Print" onclick="printDiv('printableArea')"><?php echo btn_print(); ?></button> 
                                <span class="pull-right"><a style="cursor: pointer"onclick="history.go(-1)" class="view-all-front">Go Back</a></span>                                                             
                            </div>
                    </div>       
                </div>             
            </div>    
            <form method="post" action="<?php echo base_url(); ?>admin/application_list/proses_redeem/<?php echo $redeem_info->doc_number; ?>" >
            <div id="printableArea">     
				  <div class="panel-body form-horizontal">
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Doc. Number : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $redeem_info->doc_number; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Name : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $redeem_info->first_name . ' ' . $redeem_info->last_name; ?></p>
                        </div>
                    </div>                  
					
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Redeem point Date : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><span class="text-danger"><?php echo date('d M Y', strtotime($redeem_info->submit_date)); ?></span></p>
                        </div>                                              
                    </div>

                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Sisa Point : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo round($saldo_point->saldo_point,0); ?></p>
                        </div>
                    </div> 
                    
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Total Redeem : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo round($redeem_info->total_redeem,0); ?></p>
                        </div>
                    </div> 
					
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Status : </strong></label>
                        </div>
                        <div class="col-sm-8">						
                            <p class="form-control-static">
								<?php if($redeem_info->status_redeem=='cancel'){?>
									<span class="label label-danger"><?php echo $redeem_info->status_redeem; ?></span>
								<?php } ?>
								<?php if($redeem_info->status_redeem=='pending'){?>
									<span class="label label-warning"><?php echo $redeem_info->status_redeem; ?></span>
								<?php } ?>
								<?php if($redeem_info->status_redeem=='fully approved'){?>
									<span class="label label-success"><?php echo $redeem_info->status_redeem; ?></span>
								<?php } ?>
								<?php if($redeem_info->status_redeem=='partial approved'){?>
									<span class="label label-info"><?php echo $redeem_info->status_redeem; ?></span>
								<?php } ?>
							</p>
                        </div>                                              
                    </div>

                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Created By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $redeem_info->first_name . ' ' . $redeem_info->last_name; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($redeem_info->submit_date)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
										
					<?php if($redeem_info->approve1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $redeem_info->approve1; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($redeem_info->date_approve1)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($redeem_info->unapprove1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $redeem_info->unapprove1; ?>
							pada tanggal <strong><?php echo $redeem_info->date_unapprove1; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
                    <div class="col-md-12">
                        <div class="col-sm-4">
                            <!-- Hidden Input ---->
                            <input type="hidden" name="employee_id" value="<?php echo $redeem_info->employee_id; ?>">
                            <input type="hidden" name="status_redeem" value="<?php echo $redeem_info->status_redeem; ?>">
                            <input type="hidden" name="submit_date" value="<?php echo $redeem_info->submit_date; ?>">
                            <input type="hidden" name="ttl_redeem" value="<?php echo round($redeem_info->total_redeem,0); ?>">
                            <input type="hidden" name="saldo_point" value="<?php echo round($saldo_point->saldo_point,0); ?>">
                        </div>
						
						<!--post parameter email notif--> 						
						<input type="hidden" name="email_to" value="<?php echo $redeem_info->email; ?>" readonly="true">                                     
						<input type="hidden" name="document_number" value="<?php echo $redeem_info->doc_number; ?>" readonly="true">
						<input type="hidden" name="link" value="<?php echo base_url() ?>login?link=employee/dashboard/view_application_inquiry/<?php echo $redeem_info->doc_number; ?>" readonly="true">
						<input type="hidden" name="receipt_name" value="<?php echo $redeem_info->first_name .' '.$redeem_info->last_name; ?>" readonly="true">                        
                        <input type="hidden" name="gender_requester" value="<?php echo $redeem_info->gender; ?>" readonly="true">
                      
						
<!-- LIST PRODUCT REDEEM -->
        <div class="row">
            <div class="col-sm-12" data-offset="0">                            
                <div class="panel panel-info">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Product List</strong>
                        </div>
                    </div>

                    <!-- Table -->
                    <table class="table table-bordered table-hover" id="productList">
                        <thead>
                            <tr>
                                <th class="col-sm-1">No</th>
                                <th class="col-sm-1">Product ID</th>
                                <th>Product Name</th>     
                                <th>Qty</th>                             
                                <th>Price</th>
                                <th>Subtotal</th>
                               
                            </tr>
                        </thead>
                        <tbody>

                            <?php $key = 1 ;
                            // print_r($product_list_info);
                            // echo 'product name = ' .$product_list_info[0]->product_name;
                            $total = 0;
                            ?>
                            <?php if (!empty($product_list_info)): for ($i = 0; $i<count($product_list_info); $i++) { ?>
                                    <tr>
                                        <td><?php echo $key ?></td>
                                        <td><?php echo $product_list_info[$i]->product_id ?></td>
                                        <td><?php echo $product_list_info[$i]->product_name ?></td>     
                                        <td><?php echo $product_list_info[$i]->qty ?></td>                                    
                                        <td><?php echo $product_list_info[$i]->price ?></td>
                                        <td><?php echo $product_list_info[$i]->subtotal ?></td>

                                    </tr>
                                    <?php
                                    $key++;
                                    $total += $product_list_info[$i]->subtotal ;
                            }
                                ?>
                            <?php else : ?>
                            <!-- <td colspan="3">
                                <strong>There is no data to display</strong>
                            </td> -->
                        <?php endif; ?>
                        </tbody>
                        <tfooter>
                            <thead>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th ><span style="float:right">TOTAL</span></th>                               
                                <th id="subtotal"><?php echo round($total,0);?></th>
                               
                            </thead>
                        </tfooter>
                    </table>          
                </div>
            </div>
        </div>
<!-- LIST PRODUCT REDEEM -->


						<?php if($redeem_info->status_redeem != 'cancel' && $redeem_info->status_redeem != 'fully approved'){ ?>
                        <div class="col-sm-2 margin" style="float:right" >
							<input type="submit" name="approve_btn" value="Approve" class="btn btn-primary" />
							<input type="submit" name="unapprove_btn" value="Reject" class="btn btn-danger" />	
							<?php //echo btn_view('admin/application_list/view_application/' . $redeem_info->application_list_id) ?>
                        </div>  
						<?php } ?>	
                    </div>

                </div>                
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    function printDiv(printableArea) {
        var printContents = document.getElementById(printableArea).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }
</script>






