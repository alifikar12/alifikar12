<div class="col-md-12">
    <div class="wrap-fpanel">
        <div class="panel panel-default">
            <!-- Default panel contents -->

            <div class="panel-heading">
                <div class="panel-title">                 
                    <strong>Overtime Application  Details</strong><span class="pull-right"><a style="cursor: pointer"onclick="history.go(-1)" class="view-all-front">Go Back</a></span>
                </div>                    
            </div>    
            <form method="post" action="<?php echo base_url(); ?>admin/application_list/proses_overtime/<?php echo $overtime_info->overtime_id; ?>" >
                  
				  <div class="panel-body form-horizontal">
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Overtime ID : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->overtime_id; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Name : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->first_name . ' ' . $overtime_info->last_name; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Overtime Time : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php
                                if ($overtime_info->overtime_start_time == $overtime_info->overtime_end_time) {
                                    echo date('h:i A', strtotime($overtime_info->overtime_start_time));
                                } else {
                                    echo date('h:i A', strtotime($overtime_info->overtime_start_time)) . '<span class="text-danger"> To </span>' . date('h:i A', strtotime($overtime_info->overtime_end_time));
                                }
                                ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Overtime Tools :</strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php echo $overtime_info->perlengkapan_lembur; ?></p>
                        </div>                  
                    </div>
					
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Overtime Date : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><span class="text-danger"><?php echo date('d M Y', strtotime($overtime_info->overtime_date)); ?></span></p>
                        </div>                                              
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Keperluan/Tujuan : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->tujuan; ?></p>
                        </div>                                              
                    </div>
					
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Status : </strong></label>
                        </div>
                        <div class="col-sm-8">						
                            <p class="form-control-static">
								<?php if($overtime_info->overtime_status=='cancel'){?>
									<span class="label label-danger"><?php echo $overtime_info->overtime_status; ?></span>
								<?php } ?>
								<?php if($overtime_info->overtime_status=='pending'){?>
									<span class="label label-warning"><?php echo $overtime_info->overtime_status; ?></span>
								<?php } ?>
								<?php if($overtime_info->overtime_status=='fully approved'){?>
									<span class="label label-success"><?php echo $overtime_info->overtime_status; ?></span>
								<?php } ?>
								<?php if($overtime_info->overtime_status=='partial approved'){?>
									<span class="label label-info"><?php echo $overtime_info->overtime_status; ?></span>
								<?php } ?>
							</p>
                        </div>                                              
                    </div>

                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Created By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->first_name . ' ' . $overtime_info->last_name; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($overtime_info->overtime_date)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
										
					<?php if($overtime_info->approve!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->approve; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($overtime_info->date_approve)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($overtime_info->acknowledge!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Acknowledge By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php 
                                                            if(strpos(strtoupper($overtime_info->acknowledge), 'NOVA') !== false || strpos(strtoupper($overtime_info->acknowledge), 'SUCI') !== false){
                                                                echo 'HR Manager'; 
                                                            }else{
                                                                echo $overtime_info->acknowledge;
                                                            }
                                                            ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($overtime_info->date_acknowledge)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($overtime_info->unapprove!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $overtime_info->unapprove; ?>
							pada tanggal <strong><?php echo $overtime_info->date_unapprove; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
                    <div class="col-md-12">
                        <div class="col-sm-4">
                            <!-- Hidden Input ---->
                            <input type="hidden" name="employee_id" value="<?php echo $overtime_info->employee_id; ?>">
                            <!--<input type="hidden" name="overtime_category_id" value="<?php echo $overtime_info->perlengkapan_id; ?>">-->
                            <input type="hidden" name="overtime_start_time" value="<?php echo $overtime_info->overtime_start_time; ?>">
                            <input type="hidden" name="overtime_end_time" value="<?php echo $overtime_info->overtime_end_time; ?>">
							<!--<input type="hidden" name="overtime_type" value="<?php echo $overtime_info->overtime_type; ?>">-->
                        </div>
						
						<!--post parameter email notif--> 						
						<input type="hidden" name="email_to" value="<?php echo $overtime_info->email; ?>" readonly="true">                                     
						<input type="hidden" name="document_number" value="<?php echo $overtime_info->overtime_id; ?>" readonly="true">
						<input type="hidden" name="link" value="<?php echo base_url() ?>login?link=employee/dashboard/view_application_inquiry/<?php echo $overtime_info->overtime_id; ?>" readonly="true">
						<input type="hidden" name="receipt_name" value="<?php echo $overtime_info->first_name .' '.$overtime_info->last_name; ?>" readonly="true">
                        <input type="hidden" name="sender_name" value="<?php echo $overtime_info->nama_pic_hr; ?>" readonly="true">
                        <input type="hidden" name="dl_name" value="<?php echo $overtime_info->direct_leader_name; ?>" readonly="true">
                        <input type="hidden" name="gender_requester" value="<?php echo $overtime_info->gender; ?>" readonly="true">
                        <input type="hidden" name="overtime_date" value="<?php echo $overtime_info->overtime_date; ?>" readonly="true">
                                                
                        <input type="hidden" name="overtime_start_time" value="<?php echo $overtime_info->overtime_start_time; ?>" readonly="true">
                        <input type="hidden" name="overtime_end_time" value="<?php echo $overtime_info->overtime_end_time; ?>" readonly="true">
                        <input type="hidden" name="email_hybris" value="<?php echo $overtime_info->email_hybris; ?>" readonly="true">
                        <input type="hidden" name="hybris_name" value="<?php echo $overtime_info->hybris_name; ?>" readonly="true">
                        <input type="hidden" name="hybris" value="<?php echo $overtime_info->hybris; ?>" readonly="true">
						
						<?php if($overtime_info->overtime_status != 'cancel' && $overtime_info->overtime_status != 'fully approved'){ ?>
                        <div class="col-sm-4 margin">
							<input type="submit" name="approve_btn" value="Approve" class="btn btn-primary" />
							<input type="submit" name="unapprove_btn" value="Reject" class="btn btn-danger" />	
							<?php //echo btn_view('admin/application_list/view_application/' . $overtime_info->application_list_id) ?>
                        </div>  
						<?php } ?>	
                    </div>

                </div>                
        </div>
    </div>
</div>






