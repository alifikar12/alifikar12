<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12 wrap-fpanel" data-offset="0">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <span>
                        <strong><?php echo $this->language->form_heading()[16] ?></strong>
                    </span>
                    <div class="pull-right hidden-print">
                        <!-- <span><?php echo btn_pdf('admin/application_list/application_list_pdf'); ?></span> -->
                        <!-- <button id="pdf_export"  class="btn btn-primary btn-xs" 
                            data-placement="top" title="" data-original-title="PDF">
                            <span> <i="" class="fa fa-file-pdf-o"></span>
                            </button>                                                               -->
                    </div>
                </div>
            </div>
            <!-- Table -->
            <div class="table-responsive">
                <table class="table table-bordered table-hover" id="example">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>#ID</th>
                            <th>EmpID</th>
                            <th>Full Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Leave Total</th>
                            <th>Leave Type</th>
                            <th>Apply Date</th>
                            <th>Status</th>
                            <th>Date Approve1</th>
                            <th>Approve1</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 1;
                        if (!empty($all_application_info)) :
                            foreach ($all_application_info as $key => $a) :
                                $tgl1 = new DateTime($a->leave_start_date);
                                $tgl2 = new DateTime($a->leave_end_date);
                                $diff = date_diff($tgl1, $tgl2);
                                // $diff = $tgl2->modify('+1 day')->diff($tgl1);
                        ?>
                                <tr>
                                    <td><?= $i; ?></td>
                                    <td><?= $a->application_list_id; ?></td>
                                    <td><?= $a->employment_id; ?></td>
                                    <td><?= $a->first_name . ' ' . $a->last_name; ?></td>
                                    <td><?= $a->leave_start_date; ?></td>
                                    <td><?= $a->leave_end_date; ?></td>
                                    <td><?= $diff->format('%R%a days') ?></td>
                                    <td><?= $a->category; ?></td>
                                    <td><?= date('d M,y', strtotime($a->application_date)) ?></td>
                                    <td><?php
                                        if ($a->leave_status == 'pending') {
                                            echo '<span class="label label-warning">' . $a->leave_status . '</span>';
                                        } elseif ($a->leave_status == 'fully approved') {
                                            echo '<span class="label label-success">' . $a->leave_status . '</span>';
                                        } elseif ($a->leave_status == 'partial approved') {
                                            echo '<span class="label label-info">' . $a->leave_status . '</span>';
                                        } elseif ($a->leave_status == 'partial approved 2') {
                                            echo '<span class="label label-primary">' . $a->leave_status . '</span>';
                                        } elseif ($a->leave_status == 'cancel') {
                                            echo '<span class="label label-danger">cancel</span>';
                                        }
                                        ?>
                                    </td>
                                    <td><?php if ($a->date_approve1 != '0000-00-00') echo date('d M,y', strtotime($a->date_approve1)) ?>
                                    </td>
                                    <td><?= $a->approve1 ?></td>
                                    <td><?= btn_view('admin/application_list/view_application/' . $a->application_list_id) ?></td>

                                </tr>
                            <?php
                                $i++;
                            endforeach;
                            ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />

<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#example').DataTable({
            lengthChange: false,
            buttons: ['copy', 'excel', 'pdf', 'colvis']
        });

        table.buttons().container()
            .appendTo('#example_wrapper .col-sm-6:eq(0)');
    });

    // $(document).ready(function() {
    //     $("#pdf_export").click(function() {
    //         var employee_id = $("#employee_id").text();
    //         console.log(employee_id);
    //         $.ajax({
    //             url: "<?= base_url() ?>admin/application_list/application_list_pdf", 
    //             type: 'POST',  // http method
    //             data:{
    //                 id: employee_id    
    //             },
    //             success: function(result){

    //                 // console.log(result);
    //                 var blob=new Blob([result]);
    //                 var link=document.createElement('a');
    //                 link.href=window.URL.createObjectURL(blob);
    //                 link.download="ApplicationList.pdf";
    //                 link.click();

    //             }
    //         });
    //     });
    // });
</script>