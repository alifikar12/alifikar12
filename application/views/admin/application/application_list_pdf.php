<!DOCTYPE html>
<html>
    <head>
        <title>Application List</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <style type="text/css">
            .table_tr1{
                background-color: rgb(224, 224, 224);
            }
            .table_tr1 td{
                padding: 7px 0px 7px 8px;
                font-weight: bold;
            }
            .table_tr2 td{
                padding: 7px 0px 7px 8px;
                border: 1px solid black;
            }            
        </style>
    </head>
    <body style="min-width: 100%; min-height: 100%; overflow: hidden; alignment-adjust: central;">
        <br />
        <div style="width: 100%; border-bottom: 2px solid black;">
            <table style="width: 100%; vertical-align: middle;">
                <tr>
                    <?php
                    $genaral_info = $this->session->userdata('genaral_info');
                    if (!empty($genaral_info)) {
                        foreach ($genaral_info as $info) {
                            ?>
                            <td style="width: 35px;">
                                <img style="width: 50px;height: 50px" src="<?php echo base_url() . $info->logo ?>" alt="" class="img-circle"/>
                            </td>
                            <td>
                                <p style="margin-left: 10px; font: 14px lighter;"><?php echo $info->name ?></p>
                            </td>
                            <?php
                        }
                    } else {
                        ?>
                        <td style="width: 35px;">
                            <img style="width: 50px;height: 50px" src="<?php echo base_url() ?>img/logo.png" alt="Logo" class="img-circle"/>
                        </td>
                        <td>
                            <p style="margin-left: 10px; font: 14px lighter;">Human Resource Management System</p>
                        </td>
                    <?php }
                    ?>                    
                </tr>
            </table>
        </div>
        <br />
        <div style="width: 100%;">
            <div style="width: 100%; background-color: rgb(224, 224, 224); padding: 1px 0px 5px 15px;">                
                <table style="width: 100%;">                    
                    <tr style="font-size: 20px;  text-align: center">
                        <td style="padding: 10px;">Application List</td>
                    </tr>                                    
                </table>
            </div>
            <br/>            
            <table style="width: 100%; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
                <tr class="table_tr1">
                    <td style="border: 1px solid black;">#</td>                    
                    <td style="border: 1px solid black;">NIK</td>                    
                    <td style="border: 1px solid black;">Full Name</td>                    
                    <td style="border: 1px solid black;">Start Date</td>                    
                    <td style="border: 1px solid black;">End Date</td>                    
                    <td style="border: 1px solid black;">Leave Type</td>
                    <!-- <td style="border: 1px solid black;">Apply Date</td>                     -->
                    <!-- <td style="border: 1px solid black;">Status</td>      -->
                    <!-- <td style="border: 1px solid black;">Date Approve1</th> -->
                    <!-- <td style="border: 1px solid black;">Direct Leader</th>                                    -->
                </tr>  
                
                <?php if (!empty($cuti_info)): foreach ($cuti_info as $c) : ?>

                        <tr class="table_tr2">
                            <td><?= $c->application_list_id ?></td>
                            <td><?= $c->employment_id ?></td>
                            <td><?= $c->first_name .' '. $c->last_name ?></td>
                            <td><?= $c->leave_start_date ?></td>
                            <td><?= $c->leave_end_date ?></td>                                
                            <td><?= $c->category ?></td>
                            <!-- <td><?php echo date('d M,y', strtotime($c->application_date)); ?></td>   -->
                            <!-- <td><?php                            
                                if ($c->leave_status == 'pending') {
                                        echo '<span class="label label-warning">'.$c->leave_status.'</span>';
                                    } elseif ($c->leave_status == 'fully approved') {
                                        echo '<span class="label label-success">'.$c->leave_status.'</span>';
                                    } elseif ($c->leave_status == 'partial approved') {
                                        echo '<span class="label label-info">'.$c->leave_status.'</span>';
                                    } elseif ($c->leave_status == 'partial approved 2') {
                                        echo '<span class="label label-primary">'.$c->leave_status.'</span>';
                                    } elseif ($c->leave_status == 'cancel') {
                                        echo '<span class="label label-danger">cancel</span>';
                                    }										
                                ?>
                            </td>                               -->
                            <!-- <td><?= date('d M,y', strtotime($c->date_approve1)) ?></td> -->
                            <!-- <td><?= $c->approve1 ?></td>                                                       -->
                        </tr>
                    <?php
                    endforeach;
                    ?>
                <?php else : ?>
                    <td colspan="3">
                        <strong>There is no data to display</strong>
                    </td>
                <?php endif; ?>
            </table>            
        </div>
    </body>
</html>