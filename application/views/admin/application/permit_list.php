
<?php echo message_box('success'); ?>
<h4><?php echo anchor('admin/permit_application/add_permit_application', '<i class="fa fa-plus"></i> Add Permit Application'); ?></h4>
<br/>
<div class="row">
    <div class="col-sm-12 wrap-fpanel" data-offset="0">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <span>
                        <strong>List of All Permit Applications</strong>
                        <div class="pull-right hidden-print">                                                                      
                            <span><?php echo btn_pdf('admin/application_list/permit_list_pdf'); ?></span>
                            <!-- <button class="btn-print" type="button" data-toggle="tooltip" title="Print" onclick="employee_list('employee_list')"><?php echo btn_print(); ?></button>                                                               -->
                        </div>
                    </span>
                </div>
            </div>
            <!-- Table -->
            <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
						<th>No</th>
                        <th>Permit ID</th>
                        <th>NIK</th>
                        <th>Full Name</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Permit Type</th>
                        <th>Tgl Apply</th>
                        <th>Status</th>
						<th>Tgl Disetujui Atasan</th>
						<th>Nama Atasan</th>
                        <th>Action</th>                        

                    </tr>
                </thead>
                <tbody>
                    <?php 
						$i=1;
						if (!empty($all_permit_info)):
						foreach ($all_permit_info as $key => $v_application): 
					?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $v_application->permit_id; ?></td>
                                <td><?php echo $v_application->employment_id; ?></td>
                                <td><?php echo $v_application->first_name . ' ' . $v_application->last_name; ?></td>
                                <td><?php echo date('h:i A', strtotime($v_application->permit_start_time)); ?></td>
                                <td><?php echo date('h:i A', strtotime($v_application->permit_end_time)); ?></td>
                                <td><?php echo $v_application->category; ?></td>
                                <td><?php echo date('d M,y', strtotime($v_application->permit_date)) ?></td>
                                <td><?php                            
									if ($v_application->permit_status == 'pending') {
                                            echo '<span class="label label-warning">'.$v_application->permit_status.'</span>';
                                        } elseif ($v_application->permit_status == 'fully approved') {
                                            echo '<span class="label label-success">'.$v_application->permit_status.'</span>';
                                        } elseif ($v_application->permit_status == 'partial approved') {
                                            echo '<span class="label label-info">'.$v_application->permit_status.'</span>';
                                        } elseif ($v_application->permit_status == 'partial approved 2') {
                                            echo '<span class="label label-primary">'.$v_application->permit_status.'</span>';
                                        } else {
                                            echo '<span class="label label-danger">cancel</span>';
                                        }										
                                    ?>
								</td>
								<td><?php
                                    if($v_application->date_approve != '0000-00-00'){  
                                        echo date('d M,y', strtotime($v_application->date_approve));
                                    }
                                    ?>
                                </td>
								<td><?php echo $v_application->approve ?></td>
                                <td><?php echo btn_view('admin/application_list/view_permit_details/' . $v_application->permit_id) ?></td>   
                                                             
                            </tr>                
                        <?php
							$i++;
							endforeach; 
						?>
                    <?php endif; ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

