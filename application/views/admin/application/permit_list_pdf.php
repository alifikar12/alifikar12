<!DOCTYPE html>
<html>
    <head>
        <title>Permit Report</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
        <style type="text/css">
            .table_tr1{
                background-color: rgb(224, 224, 224);
            }
            .table_tr1 td{
                padding: 7px 0px 7px 8px;
                font-weight: bold;
            }
            .table_tr2 td{
                padding: 7px 0px 7px 8px;
                border: 1px solid black;
            }            
        </style>
    </head>
    <body style="min-width: 100%; min-height: 100%; overflow: hidden; alignment-adjust: central;">
        <br />
        <div style="width: 100%; border-bottom: 2px solid black;">
            <table style="width: 100%; vertical-align: middle;">
                <tr>
                    <?php
                    $genaral_info = $this->session->userdata('genaral_info');
                    if (!empty($genaral_info)) {
                        foreach ($genaral_info as $info) {
                            ?>
                            <td style="width: 35px;">
                                <img style="width: 150px;height: 50px" src="<?php echo $_SERVER["DOCUMENT_ROOT"] . '/PMKI_HRMS/' . $info->logo ?>" alt="" class="img-circle"/>
                            </td>
                            <td>
                                <p style="margin-left: 10px; font: 14px lighter;"><?php echo $info->name ?></p>
                            </td>
                            <?php
                        }
                    } else {
                        ?>
                        <td style="width: 35px;">
                            <img style="width: 50px;height: 50px" src="<?php echo base_url() ?>img/logo.png" alt="Logo" class="img-circle"/>
                        </td>
                        <td>
                            <p style="margin-left: 10px; font: 14px lighter;">Human Resource Management System</p>
                        </td>
                    <?php }
                    ?>                    
                </tr>
            </table>
        </div>
        <br />
        <div style="width: 100%;">
            <div style="width: 100%; background-color: rgb(224, 224, 224); padding: 1px 0px 5px 15px;">                
                <table style="width: 100%;">                    
                    <tr style="font-size: 20px;  text-align: center">
                        <td style="padding: 10px;">Permit List</td>
                    </tr>                                    
                </table>
            </div>
            <br/>            
            <table style="width: 100%; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
                <tr class="table_tr1">
                    <td style="border: 1px solid black;">Permit ID</td>                    
                    <td style="border: 1px solid black;">NIK</td>                    
                    <td style="border: 1px solid black;">Full Name</td>                    
                    <td style="border: 1px solid black;">Start Time</td>                    
                    <td style="border: 1px solid black;">End Time</td>                    
                    <td style="border: 1px solid black;">Permit Type</td>                    
                    <td style="border: 1px solid black;">Tgl Apply</td>    
                    <td style="border: 1px solid black;">Tgl di Approve</td>  
                    <td style="border: 1px solid black;">Status</td>                                   
                </tr>  
              
                <?php if (!empty($all_permit_info)): foreach ($all_permit_info as $key => $v_application) : ?>

                        <tr class="table_tr2">
                        <td><?php echo $v_application->permit_id; ?></td>
                                <td><?php echo $v_application->employment_id; ?></td>
                                <td><?php echo $v_application->first_name . ' ' . $v_application->last_name; ?></td>
                                <td><?php echo date('h:i A', strtotime($v_application->permit_start_time)); ?></td>
                                <td><?php echo date('h:i A', strtotime($v_application->permit_end_time)); ?></td>
                                <td><?php echo $v_application->category; ?></td>
                                <td><?php echo date('d M,y', strtotime($v_application->permit_date)) ?></td>
                                <td><?php echo date('d M,y', strtotime($v_application->date_approve)) ?></td>   
                                <td><?php                            
									if ($v_application->permit_status == 'pending') {
                                            echo '<span class="label label-warning">'.$v_application->permit_status.'</span>';
                                        } elseif ($v_application->permit_status == 'fully approved') {
                                            echo '<span class="label label-success">'.$v_application->permit_status.'</span>';
                                        } elseif ($v_application->permit_status == 'partial approved') {
                                            echo '<span class="label label-info">'.$v_application->permit_status.'</span>';
                                        } elseif ($v_application->permit_status == 'partial approved 2') {
                                            echo '<span class="label label-primary">'.$v_application->permit_status.'</span>';
                                        } else {
                                            echo '<span class="label label-danger">cancel</span>';
                                        }										
                                    ?>
								</td>
							                                                       
                        </tr>
                        <?php                       
                    endforeach;
                    ?>
                <?php else : ?>
                    <td colspan="3">
                        <strong>There is no data to display</strong>
                    </td>
                <?php endif; ?>
            </table>            
        </div>
    </body>
</html>