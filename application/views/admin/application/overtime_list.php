
<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12 wrap-fpanel" data-offset="0">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <span>
                        <strong>List of All Overtime Applications</strong>
                    </span>
                </div>
            </div>
            <!-- Table -->
            <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
						<!--<th>No</th>-->
                        <th>Overtime ID</th>
                        <th>NIK</th>
                        <th>Full Name</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Perlengkapan Lembur</th>
                        <th>Tgl Pengajuan</th>
                        <th>Status</th>
						<th>Tgl Disetujui Atasan</th>
						<th>Nama Atasan</th>
                        <th>Action</th>                        

                    </tr>
                </thead>
                <tbody>
                    <?php 
						$i=1;
						if (!empty($all_overtime_info)):
						foreach ($all_overtime_info as $key => $v_application): 
					?>
                            <tr>
                                <!--<td><?php echo $i; ?></td>-->
                                <td><?php echo $v_application->overtime_id; ?></td>
                                <td><?php echo $v_application->employment_id; ?></td>
                                <td><?php echo $v_application->first_name . ' ' . $v_application->last_name; ?></td>
                                <td><?php echo date('h:i A', strtotime($v_application->overtime_start_time)); ?></td>
                                <td><?php echo date('h:i A', strtotime($v_application->overtime_end_time)); ?></td>
                                <td><?php echo $v_application->perlengkapan_lembur; ?></td>
                                <td><?php echo date('d M,y', strtotime($v_application->overtime_date)) ?></td>
                                <td><?php                            
									if ($v_application->overtime_status == 'pending') {
                                            echo '<span class="label label-warning">'.$v_application->overtime_status.'</span>';
                                        } elseif ($v_application->overtime_status == 'fully approved') {
                                            echo '<span class="label label-success">'.$v_application->overtime_status.'</span>';
                                        } elseif ($v_application->overtime_status == 'partial approved') {
                                            echo '<span class="label label-info">'.$v_application->overtime_status.'</span>';
                                        } else {
                                            echo '<span class="label label-danger">cancel</span>';
                                        }										
                                    ?>
								</td>
								<td><?php echo date('d M,y', strtotime($v_application->date_approve)) ?></td>
								<td><?php echo $v_application->approve ?></td>
                                <td><?php echo btn_view('admin/application_list/view_overtime_details/' . $v_application->overtime_id) ?></td>   
                                                             
                            </tr>                
                        <?php
							$i++;
							endforeach; 
						?>
                    <?php endif; ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

