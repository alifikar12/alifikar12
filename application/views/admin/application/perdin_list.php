
<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12 wrap-fpanel" data-offset="0">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <span>
                        <strong>List of All perdin Applications</strong>
                    </span>
                </div>
            </div>
            <!-- Table -->
            <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
						<th>No</th>
                        <th>Perdin ID</th>
                        <th>NIK</th>
                        <th>Full Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Perdin Type</th>
                        <th>Tgl Apply</th>
                        <th>Status</th>
						<th>Tgl Disetujui Atasan</th>
						<th>Nama Atasan</th>
                        <th>Action</th>                        

                    </tr>
                </thead>
                <tbody>
                    <?php 
						$i=1;
						if (!empty($all_perdin_info)):
						foreach ($all_perdin_info as $key => $v_application): 
					?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $v_application->perdin_id; ?></td>
                                <td><?php echo $v_application->employment_id; ?></td>
                                <td><?php echo $v_application->first_name . ' ' . $v_application->last_name; ?></td>
                                <td><?php echo date('d M,y', strtotime($v_application->perdin_start_date)); ?></td>
                                <td><?php echo date('d M,y', strtotime($v_application->perdin_end_date)); ?></td>
                                <td><?php echo $v_application->category; ?></td>
                                <td><?php echo date('d M,y', strtotime($v_application->submit_date)) ?></td>
                                <td><?php                            
									if ($v_application->perdin_status == 'pending') {
                                            echo '<span class="label label-warning">'.$v_application->perdin_status.'</span>';
                                        } elseif ($v_application->perdin_status == 'fully approved') {
                                            echo '<span class="label label-success">'.$v_application->perdin_status.'</span>';
                                        } elseif ($v_application->perdin_status == 'partial approved') {
                                            echo '<span class="label label-info">'.$v_application->perdin_status.'</span>';
                                        } elseif ($v_application->perdin_status == 'cancel') {
                                            echo '<span class="label label-danger">cancel</span>';
                                        } elseif ($v_application->perdin_status == 'partial approved 2') {
                                            echo '<span class="label label-primary">partial approved 2</span>';
                                        }									
                                    ?>
								</td>
								<td><?php
                                        if($v_application->date_approve1 != '0000-00-00'){ 
                                            echo date('d M,y', strtotime($v_application->date_approve1)); 
                                        }
                                    ?>
                                </td>
								<td><?php echo $v_application->approve1 ?></td>
                                <td><?php echo btn_view('admin/application_list/view_perdin_details/' . $v_application->perdin_id) ?></td>   
                                                             
                            </tr>                
                        <?php
							$i++;
							endforeach; 
						?>
                    <?php endif; ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

