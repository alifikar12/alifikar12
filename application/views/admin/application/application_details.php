<div class="col-md-12">
    <div class="wrap-fpanel">
        <div class="panel panel-default">
            <!-- Default panel contents -->

            <div class="panel-heading">
                <div class="panel-title">                 
                    <strong>Leave Application  Details</strong><span class="pull-right"><a style="cursor: pointer"onclick="history.go(-1)" class="view-all-front">Go Back</a></span>
                </div>                    
            </div>    
            <form method="post" action="<?= base_url(); ?>admin/application_list/proses_leave/<?= $application_info->application_list_id; ?>" >
                  
				  <div class="panel-body form-horizontal">
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Application ID : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->application_list_id; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Name : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->first_name . ' ' . $application_info->last_name; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Leave Date : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php
                                if ($application_info->leave_start_date == $application_info->leave_end_date) {
                                    echo date('d M y', strtotime($application_info->leave_start_date));
                                } else {
                                    echo date('d M y', strtotime($application_info->leave_start_date)) . '<span class="text-danger"> To </span>' . date('d M y', strtotime($application_info->leave_end_date));
                                }
                                ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Leave Type :</strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?= $application_info->category; ?></p>
                        </div>                  
                    </div>
					
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Apply On : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><span class="text-danger"><?= date('d M y', strtotime($application_info->application_date)); ?></span></p>
                        </div>                                              
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Reason : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->reason; ?></p>
                        </div>                                              
                    </div>
					
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Status : </strong></label>
                        </div>
                        <div class="col-sm-8">						
                            <p class="form-control-static">
								<?php if($application_info->leave_status=='cancel'){?>
									<span class="label label-danger"><?= $application_info->leave_status; ?></span>
								<?php } ?>
								<?php if($application_info->leave_status=='pending'){?>
									<span class="label label-warning"><?= $application_info->leave_status; ?></span>
								<?php } ?>
								<?php if($application_info->leave_status=='fully approved'){?>
									<span class="label label-success"><?= $application_info->leave_status; ?></span>
								<?php } ?>
								<?php if($application_info->leave_status=='partial approved'){?>
									<span class="label label-info"><?= $application_info->leave_status; ?></span>
								<?php } ?>
                                <?php if($application_info->leave_status=='partial approved 2'){?>
									<span class="label label-primary"><?= $application_info->leave_status; ?></span>
								<?php } ?>
							</p>
                        </div>                                              
                    </div>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Sisa Cuti : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->saldo_cuti; ?></p>
                        </div>                                              
                    </div>
                    
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Created By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->first_name . ' ' . $application_info->last_name; ?>
							pada tanggal <strong><?= date('d M Y', strtotime($application_info->application_date)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>

					<?php if($application_info->approve1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved (1) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->approve1; ?>
							pada tanggal <strong><?= date('d M Y', strtotime($application_info->date_approve1)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>

                    <?php if($application_info->approve2!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved (2) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->approve2; ?>
							pada tanggal <strong><?= date('d M Y', strtotime($application_info->date_approve2)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($application_info->approve3!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Acknowledge By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static">
                                <?php 
                                    if(strpos(strtoupper($application_info->approve3), 'NOVA') !== false || strpos(strtoupper($application_info->approve3), 'SUCI') !== false){
                                        echo 'HR Manager';   
                                    }else{
                                        echo $application_info->approve3; 
                                    }                                                                                       
                                ?>
							pada tanggal <strong><?= date('d M Y', strtotime($application_info->date_approve3)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($application_info->unapprove1!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved (1) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->unapprove1; ?>
							pada tanggal <strong><?= $application_info->date_unapprove1; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($application_info->unapprove2!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved (2) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->unapprove2; ?>
							pada tanggal <strong><?= $application_info->date_unapprove2; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>

                    <?php if($application_info->unapprove3!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unacknowledge By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?= $application_info->unapprove3; ?>
							pada tanggal <strong><?= $application_info->date_unapprove3; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
                    <div class="col-md-12">
                        <div class="col-sm-4">
                            <!-- Hidden Input ---->
                            <input type="hidden" name="employee_id" value="<?= $application_info->employee_id; ?>" readonly="true">
                            <input type="hidden" name="leave_category_id" value="<?= $application_info->leave_category_id; ?>" readonly="true">
                            <input type="hidden" name="leave_start_date" value="<?= $application_info->leave_start_date; ?>" readonly="true">
                            <input type="hidden" name="leave_end_date" value="<?= $application_info->leave_end_date; ?>" readonly="true">
							<input type="hidden" name="leave_type" value="<?= $application_info->leave_type; ?>" readonly="true">
                        </div>
						
						<!--post parameter email notif--> 						
						<input type="hidden" name="email_to" value="<?= $application_info->email_2; ?>" readonly="true">                                     
						<input type="hidden" name="document_number" value="<?= $application_info->application_list_id; ?>" readonly="true">
						<input type="hidden" name="link" value="<?= base_url() ?>login?link=employee/dashboard/view_application_inquiry/<?= $application_info->application_list_id; ?>" readonly="true">
						<input type="hidden" name="receipt_name" value="<?= $application_info->first_name .' '.$application_info->last_name; ?>" readonly="true">
                        <input type="hidden" name="sender_name" value="<?= $application_info->nama_pic_hr; ?>" readonly="true">
                        <input type="hidden" name="jk_sender" value="<?= $application_info->gender_pic_hr; ?>" readonly="true">
                        <input type="hidden" name="jk_receipt" value="<?= $application_info->gender; ?>" readonly="true">
                        
                        <input type="hidden" name="leave_start_date" value="<?= $application_info->leave_start_date; ?>" readonly="true">
                        <input type="hidden" name="leave_end_date" value="<?= $application_info->leave_end_date; ?>" readonly="true">
                        <input type="hidden" name="email_hybris" value="<?= $application_info->email_hybris; ?>" readonly="true">
                        <input type="hidden" name="hybris_name" value="<?= $application_info->hybris_name; ?>" readonly="true">
                        <input type="hidden" name="hybris" value="<?= $application_info->hybris; ?>" readonly="true">
                        <input type="hidden" name="jk_hybris" value="<?= $application_info->hybris_gender; ?>" readonly="true">
						
						<?php if($application_info->leave_status != 'cancel' && $application_info->leave_status != 'fully approved'){ ?>
                        <div class="col-sm-4 margin">
							<input type="submit" name="approve_btn" value="Approve" class="btn btn-primary" />
							<input type="submit" name="unapprove_btn" value="Reject" class="btn btn-danger" />	
                        </div>  
						<?php } ?>	

                         <!-- JIKA fully approved tampilkan button cancel -->
                         <?php if($application_info->leave_status == 'fully approved'){ ?>
                        <div class="col-sm-4 margin">
							<input type="submit" name="cancel_btn" value="Cancel" class="btn btn-primary" />
                        </div>  
						<?php } ?>	

                    </div>

                </div>                
        </div>
    </div>
</div>






