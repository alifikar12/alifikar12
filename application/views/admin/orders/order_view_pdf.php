<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
    </head>

    <body style="width: 100%;">
    <div class="panel-heading">
                <div class="row">
                    <div  class="col-lg-12 panel-title">
                        <h3 class="col-lg-4 col-md-4 col-sm-4">Order <?php echo '#'.$order_info['number']; ?> details</h3>
                        <div class="pull-right">                               
                            <span><?php echo btn_edit('admin/orders/add_order/' . $order_info['id']); ?></span>
                            <span><?php echo btn_pdf('admin/orders/make_pdf/' . $order_info['id']); ?></span>
                            <button class="margin btn-print" type="button" data-toggle="tooltip" title="Print" onclick="printDiv('printableArea')"><?php echo btn_print(); ?></button>                                                              
                        </div>
                    </div>
                    <div  class="col-lg-12">
                        <h5 class="col-lg-6 col-md-4 col-sm-4">
                            <?php echo 'Payment via ' .$order_info['payment_method_title'].'. '; 
                                if($order_info['date_paid'] != null){ 
                                    echo 'Paid on' .date('M d, Y H:i A', strtotime($order_info['date_paid'])).'. ';
                                }  
                                echo 'Customer IP: ' .$order_info['customer_ip_address'] 
                            ?> 
                        </h5>
                    </div>
                </div>
            </div>
            <br />
            <div id="printableArea"> 
                <div class="show_print" style="width: 100%; border-bottom: 2px solid black;">
                    <table style="width: 100%; vertical-align: middle;">
                        <tr>
                            <?php
                            $genaral_info = $this->session->userdata('genaral_info');
                            if (!empty($genaral_info)) {
                                foreach ($genaral_info as $info) {
                                    ?>
                                    <td style="width: 75px; border: 0px;">
                                        <img style="width: 150px;height: 50px" src="<?php echo base_url() . $info->logo ?>" alt="" class="img-circle"/>
                                    </td>
                                    <td style="border: 0px;">
                                        <p style="margin-left: 10px; font: 14px lighter;"><?php echo $info->name ?></p>
                                    </td>
                                    <?php
                                }
                            } else {
                                ?>
                                <td style="width: 75px; border: 0px;">
                                    <img style="width: 50px;height: 50px" src="<?php echo base_url() ?>img/logo.png" alt="Logo" class="img-circle"/>
                                </td>
                                <td style="border: 0px;">
                                    <p style="margin-left: 10px; font: 14px lighter;">Human Resource Management System</p>
                                </td>
                                <?php
                            }
                            ?>
                        </tr>
                    </table>
                </div><!--show when print start-->
                <br/>
                <div class="col-lg-12 well">
                    <div class="row">                            
                        <!-- start column billing -->
                        <div class="col-lg-4 col-sm-4 ">
                            <div>
                                <div style="margin-left: 20px;">                                        
                                    <h3>General</h3>
                                    <hr />
                                    <table class="table-hover">
                                    <tr>
                                            <td><strong>Date created:</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['date_created']; ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Status:</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['status']; ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Customer:</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><a href="<?php echo $order_info['_links']['customer'][0]['href'] ?>"> <?php echo '(#'.$order_info['customer_id'].') - '.$order_info['billing']['email']; ?> </a></td>
                                        </tr>                                                                                
                                       

                                    </table>                                                                           
                                </div>
                            </div>
                        </div>
                        <!-- start column billing -->
                        <div class="col-lg-4 col-sm-4 ">
                            <div>
                                <div style="margin-left: 20px;">                                        
                                    <h3>Billing</h3>
                                    <hr />
                                    <table class="table-hover">
                                    <tr>
                                            <td><strong>Shipping Name</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['billing']['first_name'].' '.$order_info['billing']['last_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Company</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['billing']['company']; ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>address_1</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['billing']['address_1']; ?></td>
                                        </tr>                                                                                
                                        <tr>
                                            <td><strong>address_2</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['billing']['address_2']; ?></td>
                                        </tr> 
										<tr>
                                            <td><strong>city</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['billing']['city']; ?></td>
                                        </tr> 
                                        <tr>
                                            <td><strong>state</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['billing']['state']; ?></td>
                                        </tr> 
                                        <tr>
                                            <td><strong>postcode</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['billing']['postcode']; ?></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td></td>
                                        </tr> 
                                        <tr>
                                            <td><strong>Email address:</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['billing']['email']; ?></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td></td>
                                        </tr> 
                                        <tr>
                                            <td><strong>Phone:</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['billing']['phone']; ?></td>
                                        </tr>

                                    </table>                                                                           
                                </div>
                            </div>
                        </div>
                        <!-- start column shipping -->
                        <div class="col-lg-4 col-sm-4 ">
                            <div>
                                <div style="margin-left: 20px;">                                        
                                    <h3>Shipping</h3>
                                    <hr />
                                    <table class="table-hover">
                                        <tr>
                                            <td><strong>Shipping Name</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['shipping']['first_name'].' '.$order_info['shipping']['last_name']; ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>Company</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['shipping']['company']; ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong>address_1</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['shipping']['address_1']; ?></td>
                                        </tr>                                                                                
                                        <tr>
                                            <td><strong>address_2</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['shipping']['address_2']; ?></td>
                                        </tr> 
										<tr>
                                            <td><strong>city</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['shipping']['city']; ?></td>
                                        </tr> 
                                        <tr>
                                            <td><strong>state</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['shipping']['state']; ?></td>
                                        </tr> 
                                        <tr>
                                            <td><strong>postcode</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['shipping']['postcode']; ?></td>
                                        </tr> 
										<tr>
                                            <td></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td></td>
                                        </tr> 
                                        <tr>
                                            <td><strong>Customer provided note:</strong></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td><?php echo $order_info['customer_note']; ?></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                            <td></td>
                                        </tr> 
                                        
                                    </table>                                                                           
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">Line Item</h4>
                        </div>
                        <div class="panel-body form-horizontal">  

                        <div class="table-responsive">
                        <table class="table table-hover" id="dataTables-example">

                            <thead>
                                <tr>
                                    <th class="col-sm-1" colspan="2">Item</th>
                                    <th>Cost</th>
                                    <th>Qty</th>
                                    <th class="show_print">Total</th>
                                    <th>Tax</th>
                                    <th width="1%">&nbsp;</th>
                                   
                                </tr>
                            </thead>

                            
                            <tbody id="order_line_items">

                            <?php if (!empty($order_info['line_items'])): foreach ($order_info['line_items'] as $v_line_items) : ?>


                                <?php //echo '<pre>'; print_r($product_info); echo '</pre>'; die;?>

                                    <tr class="item " data-order_item_id="1713">
                                        <td class="">
                                            <div class="wc-order-item-thumbnail">
                                                <!-- <img width="150" height="150" src="<?php //echo $product_info[$v_line_items['product_id']]['images'][0]['src']; ?>" class="attachment-thumbnail size-thumbnail" alt="" title=""> -->
                                                <img style="width: 150px;height: 150px" src="<?php echo str_replace('https', 'http', $product_info[$v_line_items['product_id']]['images'][0]['src']); ?>" alt="" class="img-circle"/>
                                                <!-- <img style="width: 150px;height: 150px" src="https://www.joylabbeauty.com/public/uploads/2020/04/skinotic-moisture-gel.png" alt="" class="img-circle"/> -->
                                            </div>	
                                        </td>
                                        <td class="name" data-sort-value="Eye On Diet - 20gr">
                                        <a href="#" class="wc-order-item-name"><?php echo $v_line_items['name'];?></a><div class="wc-order-item-sku"><strong>SKU:</strong> <?php echo $v_line_items['sku'];?></div><div class="wc-order-item-variation"><strong>Variation ID:</strong> <?php echo $v_line_items['variation_id'];?></div>		
                                        <input type="hidden" class="order_item_id" name="order_item_id[]" value="1713">
                                        <input type="hidden" name="order_item_tax_class[1713]" value="">
                                        <div class="view">
                                        </div>
                                        <div class="edit" style="display: none;">
                                        <table class="meta" cellspacing="0">
                                            <tbody class="meta_items">
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="4"><button class="add_order_item_meta button">Add&nbsp;meta</button></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        </div>
                                        </td>

                        
                                        <td class="item_cost" width="1%" data-sort-value="72727.27">
                                            <div class="view">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">IDR </span><?php echo number_format($v_line_items['total']);?></span></br>
                                                <span class="wc-order-item-discount">-<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">IDR </span><?php echo number_format($v_line_items['subtotal']);?></span></span>		
                                            </div>
                                        </td>

                                        <td class="quantity" width="1%">
                                            <div class="view">
                                                <small class="times">×</small> <?php echo $v_line_items['quantity'];?></div>
                                            <div class="edit" style="display: none;">
                                                <input type="number" step="1" min="0" autocomplete="off" name="order_item_qty[1713]" placeholder="0" value="1" data-qty="1" size="4" class="quantity">
                                            </div>
                                            <div class="refund" style="display: none;">
                                                <input type="number" step="1" min="0" max="1" autocomplete="off" name="refund_order_item_qty[1713]" placeholder="0" size="4" class="refund_order_item_qty">
                                            </div>
                                        </td>

                                        <td class="line_cost" width="1%" data-sort-value="65454.545455">
                                            <div class="view">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">IDR </span><?php echo number_format($v_line_items['total']);?></span></br>
                                                <span class="wc-order-item-discount">-<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">IDR </span><?php echo number_format($v_line_items['subtotal']);?></span></span>		
                                            </div>
                                            <div class="edit" style="display: none;">
                                                <div class="split-input">
                                                    <div class="input">
                                                        <label>Pre-discount:</label>
                                                        <input type="text" name="line_subtotal[1713]" placeholder="0" value="72727.272727" class="line_subtotal wc_input_price" data-subtotal="72727.272727">
                                                    </div>
                                                    <div class="input">
                                                        <label>Total:</label>
                                                        <input type="text" name="line_total[1713]" placeholder="0" value="65454.545455" class="line_total wc_input_price" data-tip="After pre-tax discounts." data-total="65454.545455">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="refund" style="display: none;">
                                                <input type="text" name="refund_line_total[1713]" placeholder="0" class="refund_line_total wc_input_price">
                                            </div>
                                        </td>


                                        <td class="line_tax" width="1%">
                                            <div class="view">
                                                <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">IDR </span><?php echo number_format($v_line_items['taxes'][0]['total']);?></span></br>
                                                <span class="wc-order-item-discount">-<span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">IDR </span><?php echo number_format($v_line_items['taxes'][0]['subtotal']);?></span></span>				
                                            </div>
                                            <div class="edit" style="display: none;">
                                                <div class="split-input">
                                                    <div class="input">
                                                        <label>Pre-discount:</label>
                                                        <input type="text" name="line_subtotal_tax[1713][1]" placeholder="0" value="7272.727273" class="line_subtotal_tax wc_input_price" data-subtotal_tax="7272.727273" data-tax_id="1">
                                                    </div>
                                                    <div class="input">
                                                        <label>Total:</label>
                                                        <input type="text" name="line_tax[1713][1]" placeholder="0" value="6545.454545" class="line_tax wc_input_price" data-total_tax="6545.454545" data-tax_id="1">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="refund" style="display: none;">
                                                <input type="text" name="refund_line_tax[1713][1]" placeholder="0" class="refund_line_tax wc_input_price" data-tax_id="1">
                                            </div>
                                        </td>
                                        <td class="wc-order-edit-line-item" width="1%">
                                            <div class="wc-order-edit-line-item-actions">
                                        </div>
                                        </td>
                                    </tr>
                                </tbody>

                                <?php
                                    endforeach;
                                    ?>
                                <?php else : ?>
                                <td colspan="3">
                                    <strong>There is no data to display</strong>
                                </td>
                            <?php endif; ?>

                               <?php 
                            //    echo '<pre>';
                            //    print_r($order_info); 
                            //    echo '</pre>';
                               ?>
                                <tbody id="order_shipping_line_items">
                                
                                <tr class="shipping " data-order_item_id="1714">
                                    <td class="thumb">
                                        <div>

                                        </div>
                                    </td>
    
                                    <td class="name">
                                        <div class="view">
                                            <?php echo $order_info['shipping_lines'][0]['method_title'];?>		
                                        </div>
                                        <div class="edit" style="display: none;">
                                            <input type="hidden" name="shipping_method_id[]" value="1714">
                                            <input type="text" class="shipping_method_name" placeholder="Shipping name" name="shipping_method_title[1714]" value="J&amp;T - EZ">
                                            <select class="shipping_method" name="shipping_method[1714]">
                                                <optgroup label="Shipping method">
                                                    <option value="">N/A</option>
                                                    <option value="flat_rate">Flat rate</option><option value="free_shipping">Free shipping</option><option value="local_pickup">Local pickup</option><option value="woongkir" selected="selected">Woongkir</option><option value="other">Other</option>				
                                                </optgroup>
                                            </select>
                                        </div>
                                                <div class="view">
                                        </div>
    
                                        <div class="edit" style="display: none;">
                                            <table class="meta" cellspacing="0">
                                                <tbody class="meta_items">
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4"><button class="add_order_item_meta button">Add&nbsp;meta</button></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </td>
    
                            
                                    <td class="item_cost" width="1%">&nbsp;</td>
                                    <td class="quantity" width="1%">&nbsp;</td>
    
                                    <td class="line_cost" width="1%">
                                        <div class="view">
                                            <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">IDR </span><?php echo number_format($order_info['shipping_lines'][0]['total']);?></span></div>
                                        <div class="edit" style="display: none;">
                                            <input type="text" name="shipping_cost[1714]" placeholder="0" value="10000.00" class="line_total wc_input_price">
                                        </div>
                                        <div class="refund" style="display: none;">
                                            <input type="text" name="refund_line_total[1714]" placeholder="0" class="refund_line_total wc_input_price">
                                        </div>
                                    </td>
    
                                    <td class="line_tax" width="1%">
                                        <div class="view">
                                            –				</div>
                                        <div class="edit" style="display: none;">
                                            <input type="text" name="shipping_taxes[1714][1]" placeholder="0" value="" class="line_tax wc_input_price">
                                        </div>
                                        <div class="refund" style="display: none;">
                                            <input type="text" name="refund_line_tax[1714][1]" placeholder="0" class="refund_line_tax wc_input_price" data-tax_id="1">
                                        </div>
                                    </td>
                                        <td class="wc-order-edit-line-item">
                                    </td>
                                </tr>
                                </tbody>
                                <tbody id="order_fee_line_items"></tbody>
                                <tbody id="order_refunds"></tbody>


                                <!--  -->
                                <tbody id="order_shipping_line_items">
                                
                                <tr class="shipping " data-order_item_id="1714">
                                    <td class="thumb">
                                        <div>
                                        <div class="wc-order-data-row wc-order-totals-items wc-order-items-editable">
                                            <div class="wc-used-coupons">
                                            <ul class="wc_coupon_list">
                                                <li><strong>Coupon(s)</strong></li>
                                                                    <li class="code">
                                                                        <a href="#" class="tips">
                                                                <span><?php if(!empty($order_info['coupon_lines'][0]['code'])){echo $order_info['coupon_lines'][0]['code'];}?></span>
                                                            </a>
                                                </li>
                                            </ul>
                                        </div>
                                        </div>
                                    </td>
    
                                    <td class="name">
                                        <div class="view">
                                        </div>
                                        <div class="edit" style="display: none;">
                                            <input type="hidden" name="shipping_method_id[]" value="1714">
                                            <input type="text" class="shipping_method_name" placeholder="Shipping name" name="shipping_method_title[1714]" value="J&amp;T - EZ">
                                            <select class="shipping_method" name="shipping_method[1714]">
                                                <optgroup label="Shipping method">
                                                    <option value="">N/A</option>
                                                    <option value="flat_rate">Flat rate</option><option value="free_shipping">Free shipping</option><option value="local_pickup">Local pickup</option><option value="woongkir" selected="selected">Woongkir</option><option value="other">Other</option>				</optgroup>
                                            </select>
                                        </div>
                                                <div class="view">
                                        </div>
    
                                        <div class="edit" style="display: none;">
                                            <table class="meta" cellspacing="0">
                                                <tbody class="meta_items">
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4"><button class="add_order_item_meta button">Add&nbsp;meta</button></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </td>
    
                            
                                    <td class="item_cost" width="1%">&nbsp;</td>
                                    <td class="quantity" width="1%">&nbsp;</td>
    
                                    <td class="line_cost" width="1%">
                                        <div class="view">
                                        </div>
                                        <div class="edit" style="display: none;">
                                            <input type="text" name="shipping_cost[1714]" placeholder="0" value="10000.00" class="line_total wc_input_price">
                                        </div>
                                        <div class="refund" style="display: none;">
                                            <input type="text" name="refund_line_total[1714]" placeholder="0" class="refund_line_total wc_input_price">
                                        </div>
                                    </td>
    
                                    <td class="line_tax" width="1%">
                                        <div class="view">
                                        <table class="wc-order-totals">
                                            <tbody>
                                            <tr>
                                                <td>Discount:</td>
                                                <td width="1%"></td>
                                                <td class="total">
                                                    <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">IDR </span><?php echo number_format($order_info['discount_total']);?></span>				
                                                </td>
                                            </tr>
                                        
                                            <tr>
                                                <td>Shipping:</td>
                                                <td width="1%"></td>
                                                <td class="total">
                                                    <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">IDR </span><?php echo number_format($order_info['shipping_total']);?></span>				
                                                </td>
                                            </tr>
                                        
                                        
                                                <tr>
                                                    <td>Tax:</td>
                                                    <td width="1%"></td>
                                                    <td class="total">
                                                        <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">IDR </span><?php echo number_format($order_info['total_tax']);?></span>					
                                                    </td>
                                                </tr>
                                                    
                                        
                                            <tr>
                                                <td>Total:</td>
                                                <td width="1%"></td>
                                                <td class="total">
                                                    <span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">IDR </span><?php echo number_format($order_info['total']);?></span>			
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>				
                                        </div>
                                        <div class="edit" style="display: none;">
                                            <input type="text" name="shipping_taxes[1714][1]" placeholder="0" value="" class="line_tax wc_input_price">
                                        </div>
                                        <div class="refund" style="display: none;">
                                            <input type="text" name="refund_line_tax[1714][1]" placeholder="0" class="refund_line_tax wc_input_price" data-tax_id="1">
                                        </div>
                                    </td>
                                        <td class="wc-order-edit-line-item">
                                    </td>
                                </tr>
                                </tbody>
                                <div class="clear"></div>


                                <tbody id="">
                                <tr class="">
                                    <td class="thumb">
                                        <div>
                                        <div>
                                            <div class="">
                                        </div>
                                        </div>
                                    </td>
    
                                    <td class="name">
                                        <div class="view">
                                        </div>
                                        <div class="edit" style="display: none;">
                                            <input type="hidden" >
                                            <input type="text" class="" >
                                        </div>
                                                <div class="view">
                                        </div>
    
                                        <div class="" style="display: none;">
                                            <table class="" cellspacing="0">
                                                <tbody class="">
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="4"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </td>
    
                            
                                    <td class="" width="1%">&nbsp;</td>
                                    <td class="" width="1%">&nbsp;</td>
    
                                    <td class="line_cost" width="1%">
                                        <div class="view">
                                        </div>
                                        <div class="edit" style="display: none;">
                                            <input type="text" >
                                        </div>
                                        <div class="" style="display: none;">
                                            <input type="text">
                                        </div>
                                    </td>
    
                                    <td class="" width="1%">
                                        <div class="view">
                                       			
                                        </div>
                                        <div class="" style="display: none;">
                                            <input type="text" >
                                        </div>
                                        <div class="" style="display: none;">
                                            <input type="text" >
                                        </div>
                                    </td>
                                        <td>
                                    </td>
                                </tr>
                                </tbody>
    
                        </table>          
                        </div>
      
                </div>
            </div>
        </div>
    </div>


                             
            </div>         
    </body>
</html>