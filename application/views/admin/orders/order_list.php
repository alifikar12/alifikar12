
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<div class="row">
    <div class="col-sm-12"> 
        
        <br/>

        <div class="row">
            <div class="col-sm-12" data-offset="0">                            
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Orders List</strong>
                        </div>
                    </div>
                    <div class="table-responsive">            
                    <!-- Table -->
                    <table class="table table-bordered table-hover" id="dataorder">
                        <thead>
                            <tr>
                                <th class="col-sm-2">Order</th>
                                <th>Date</th> 
                                <th>Status</th> 
                                <th style="text-align: right;">Total</th> 
                                <th class="col-sm-1 hidden-print" style="text-align: center;">View</th>       
                                <?php if($role == 'Administrator'):?>
                                    <th class="col-sm-2" style="text-align: right;">Action</th>
                                <?php endif?>
                            
                            </tr>
                        </thead>
                        <tbody>
                           
                            <?php if (!empty($all_orders_info)): foreach ($all_orders_info as $v_category) :  //echo '<pre>'; print_r($v_category); echo '</pre>' ?>
                           
                                    <tr>
                                        <td><?php echo $v_category['number']." ".$v_category['billing']['first_name']." ".$v_category['billing']['last_name'] ?></td>
                                        <td><?php echo date('M d, Y H:i:s', strtotime($v_category['date_created'])) ?></td>   
                                        <td><?php echo $v_category['status'] ?></td> 
                                        <td style="text-align: right;"><?php echo number_format($v_category['total'], 2) ?></td>
                                        <td class="hidden-print" style="text-align: center;"><?php echo btn_view('admin/orders/view_order/' . $v_category['id']); ?></td>          
                                        <?php if($role == 'Administrator'):?>
                                            <td style="text-align: right;">
                                                <?php echo btn_edit('admin/customer/customer_list/' . $v_category['id']); ?>  
                                                <?php echo btn_delete('admin/customer/delete_customer/' . $v_category['id']); ?>
                                            </td>
                                        <?php endif?>
                                    </tr>
                                    <?php
                                endforeach;
                                ?>
                            <?php else : ?>
                            <td colspan="3">
                                <strong>There is no data to display</strong>
                            </td>
                        <?php endif; ?>
                        </tbody>
                    </table>   
                    </div>       
                </div>
            </div>
        </div>
    </div>   
</div>



<script>
   
$(document).ready(function() {
    var versionNo = $.fn.dataTable.version;
    var versionBootstrap = $.fn.tooltip.Constructor.VERSION;

    //alert("dataTable versi : "+versionNo);       // => 1.10.0-dev
    //alert("Bootsrap versi : "+versionBootstrap); // => "3.2.0"

    $('#dataorder').DataTable( {
        "order": [[ 0, "desc" ]]
    } );

});    

</script>
