<?php $this->load->view('admin/components/header'); ?>
<body>
    <?php $this->load->view('admin/components/user_profile'); ?>
    <div class="wrapper row-offcanvas row-offcanvas-left">	            
        <?php $this->load->view('admin/components/navigation'); ?>	
        <!-- Right side column. Contains the navbar and content of the page -->

        <div class="right-side">
            
        <!-- Content Header (Page header) -->
        <section class="content-header">
                <!-- AdminLTE App -->
                <script src="<?php echo base_url(); ?>asset/js/bootstrap.min.js" type="text/javascript"></script> 
                <script src="<?php echo base_url(); ?>asset/js/menu.js" type="text/javascript"></script>  
                <script src="<?php echo base_url(); ?>asset/js/custom-validation.js" type="text/javascript"></script>
                <script src="<?php echo base_url(); ?>asset/js/jquery.validate.js" type="text/javascript"></script>
                <!-- Jasny Bootstrap for NIce Image Change -->
                <script src="<?php echo base_url() ?>asset/js/jasny-bootstrap.min.js"></script>
                <script src="<?php echo base_url() ?>asset/js/bootstrap-datepicker.js" ></script>      
                <script src="<?php echo base_url() ?>asset/js/timepicker.js" ></script>  

                <!-- Data Table -->
                <script src="<?php echo base_url(); ?>asset/js/plugins/metisMenu/metisMenu.min.js" type="text/javascript"></script> 
                <script src="<?php echo base_url(); ?>asset/js/plugins/dataTables/jquery.dataTables.js" type="text/javascript"></script>  
                <script src="<?php echo base_url(); ?>asset/js/plugins/dataTables/dataTables.bootstrap.js" type="text/javascript"></script>
                    <script>
                    $(document).ready(function() {
                        $("[id^=dataTables-example]").dataTable();        
                    });
                    </script>
                <ol class="breadcrumb">
                    <?php echo $this->breadcrumbs->build_breadcrumbs(); ?>
                </ol>
            </section>
            <br/>
            <div class="container-fluid">
                <?php echo $subview ?>
            </div>

            <br />
            <?php $this->load->view('admin/components/footer'); ?> 

        