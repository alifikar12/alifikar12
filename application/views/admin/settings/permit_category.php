
<?= message_box('success'); ?>
<?= message_box('error'); ?>
<div class="row">
    <div class="col-sm-12"> 
        <div class="wrap-fpanel">
            <div class="panel panel-default" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>Add Permit Category</strong>
                    </div>
                </div>
                <div class="panel-body">

                    <form id="form" action="<?= base_url() ?>admin/settings/save_permit_category/<?php
                        if (!empty($permit_category->permit_category_id)) {
                            echo $permit_category->permit_category_id;
                        }
                    ?>" method="post" class="form-horizontal form-groups-bordered">
					
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Permit Category <span class="required">*</span></label>
                            <div class="col-sm-5">                            
                                <input type="text" name="category" value="<?php
                                if (!empty($permit_category->category)) {
                                    echo $permit_category->category;
                                }
                                ?>" class="form-control" placeholder="Enter Your Permit Category Name" />
                            </div>
						</div>	
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Status <span class="required">*</span></label>
                            <div class="col-sm-5">                            
                                <select class="form-control" name="status">
                                    <option value="active" <?= (!empty($permit_category->status) AND $permit_category->status == 'active') ? 'selected' : '' ?>> Active </option>
                                    <option value="inactive" <?= (!empty($permit_category->status) AND $permit_category->status == 'inactive') ? 'selected' : '' ?>> Inactive </option>
                                </select>                                
                            </div>
						</div>	
						<div class="form-group">							
                            <div class="col-sm-3">               
                            </div>							
                            <div class="col-sm-2">
                                <button type="submit" id="sbtn" class="btn btn-primary"><?= $this->language->from_body()[1][12] ?></button>                            
                            </div>
                        </div>					
						
                    </form>
                </div>                 
            </div>                 
        </div>  
        <br/>

        <div class="row">
            <div class="col-sm-12" data-offset="0">                            
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Permit Category List</strong>
                        </div>
                    </div>

                    <!-- Table -->
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="col-sm-1">SL</th>
                                <th>Category Name</th>   
                                <th>Status</th>                                
                                <th class="col-sm-2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $key = 1 ?>
                            <?php if (!empty($all_permit_category_info)): foreach ($all_permit_category_info as $v_category) : ?>
                                    <tr>
                                        <td><?= $key ?></td>
                                        <td><?= $v_category->category ?></td>   
                                        <td><?= $v_category->status ?></td>                                      
                                        <td>
                                            <?= btn_edit('admin/settings/permit_category/' . $v_category->permit_category_id); ?>  
                                            <?= btn_delete('admin/settings/delete_permit_category/' . $v_category->permit_category_id); ?>
                                        </td>

                                    </tr>
                                    <?php
                                    $key++;
                                endforeach;
                                ?>
                            <?php else : ?>
                            <td colspan="3">
                                <strong>There is no data to display</strong>
                            </td>
                        <?php endif; ?>
                        </tbody>
                    </table>          
                </div>
            </div>
        </div>
    </div>   
</div>
