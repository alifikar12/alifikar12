<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>

<div class="row">
    <div class="col-sm-12"> 
        <div class="wrap-fpanel">
            <div class="panel panel-default" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>Set Approval Redeem</strong>                    
                    </div>                
                </div>
                <div class="panel-body">

                    <form id="set_approval_redeem" action="<?php echo base_url(); ?>admin/settings/save_approval_redeem/<?php if (!empty($approval_redeem)) echo $approval_redeem->approval_redeem_id; ?>" method="post"  class="form-horizontal form-groups-bordered">                                                           
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Approval <span class="required"> *</span></label>
                        <div class="col-sm-3">
                            <input type="text" name="approval_email" id="approval_email" value="<?php if (!empty($approval_redeem)) echo $approval_redeem->approval_email; ?>" 
                            class="form-control" placeholder="Enter Approval Email"  id="field-1">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="approval_name" id="approval_name" value="<?php if (!empty($approval_redeem)) echo $approval_redeem->approval_name; ?>" 
                            class="form-control" placeholder="Enter Approval Name"  id="field-1">
                        </div>
                        <div class="col-sm-2">
                            <select name="approval_gender" class="form-control col-sm-5" >                                                          
                                <option value="">Select Gender ...</option>
                                <option value="Male" <?php
                                if (!empty($approval_redeem->approval_gender)) {
                                    echo $approval_redeem->approval_gender == 'Male' ? 'selected' : '';
                                }
                                ?>>Male</option>
                                <option value="Female" <?php
                                if (!empty($approval_redeem->approval_gender)) {
                                    echo $approval_redeem->approval_gender == 'Female' ? 'selected' : '';
                                }
                                ?>>Female</option>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Acknowledge<span class="required"> *</span></label>
                        <div class="col-sm-3">
                            <input type="text" name="acknowledge_email" id="acknowledge_email" value="<?php if (!empty($approval_redeem)) echo $approval_redeem->acknowledge_email; ?>" 
                            class="form-control" placeholder="Enter Acknowledge Email"  id="field-1">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="acknowledge_name" id="acknowledge_name" value="<?php if (!empty($approval_redeem)) echo $approval_redeem->acknowledge_name; ?>" 
                            class="form-control" placeholder="Enter Acknowledge Name"  id="field-1">
                        </div>
                        <div class="col-sm-2">
                            <select name="acknowledge_gender" class="form-control col-sm-5" >                                                          
                                <option value="">Select Gender ...</option>
                                <option value="Male" <?php
                                if (!empty($approval_redeem->acknowledge_gender)) {
                                    echo $approval_redeem->acknowledge_gender == 'Male' ? 'selected' : '';
                                }
                                ?>>Male</option>
                                <option value="Female" <?php
                                if (!empty($approval_redeem->acknowledge_gender)) {
                                    echo $approval_redeem->acknowledge_gender == 'Female' ? 'selected' : '';
                                }
                                ?>>Female</option>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Acknowledge CC</label>
                        <div class="col-sm-3">
                            <input type="text" name="acknowledge_email_cc" value="<?php if (!empty($approval_redeem)) echo $approval_redeem->acknowledge_email_cc; ?>" 
                            class="form-control" placeholder="Enter Acknowledge CC Email"  id="field-1">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" name="acknowledge_name_cc" value="<?php if (!empty($approval_redeem)) echo $approval_redeem->acknowledge_name_cc; ?>" 
                            class="form-control" placeholder="Enter Acknowledge CC Name"  id="field-1">
                        </div>
                        <div class="col-sm-2">
                            <select name="acknowledge_gender_cc" class="form-control col-sm-5" >                                                          
                                <option value="">Select Gender ...</option>
                                <option value="Male" <?php
                                if (!empty($approval_redeem->acknowledge_gender_cc)) {
                                    echo $approval_redeem->acknowledge_gender_cc == 'Male' ? 'selected' : '';
                                }
                                ?>>Male</option>
                                <option value="Female" <?php
                                if (!empty($approval_redeem->acknowledge_gender_cc)) {
                                    echo $approval_redeem->acknowledge_gender_cc == 'Female' ? 'selected' : '';
                                }
                                ?>>Female</option>
                            </select> 
                        </div>
                    </div>
                                                            
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" id="sbtn" class="btn btn-primary" id="i_submit" ><?php echo $this->language->from_body()[1][12] ?></button>                            
                        </div>
                    </div>  
                    </form>
                </div>            
            </div>          
        </div>          

    </div>   
</div>