<?php echo message_box('success'); ?>
<style type="text/css">
    .datepicker{z-index:1151 !important;}
</style>
<?php if (!empty($matrix_info) || !empty($add_matrix)): ?>
    <div class="row">
        <div class="col-sm-12" data-spy="scroll" data-offset="0">                            
            <div class="wrap-fpanel" >                            
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Edit Matrix Approval</strong>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form role="form" id="form" action="<?php echo base_url(); ?>admin/settings/save_matrix/<?php if (!empty($matrix_info)) echo $matrix_info->employee_id; ?> " method="post" class="form-horizontal form-groups-bordered">

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Employee Name<span class="required">*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" name="employee_name"  class="form-control" id="field-1" value="<?php if (!empty($matrix_info)) echo $matrix_info->first_name.' '.$matrix_info->last_name; ?>" readonly="true"/>                
                                    <input type="hidden" name="employee_id"  class="form-control" id="field-1" value="<?php if (!empty($matrix_info)) echo $matrix_info->employee_id; ?>"/>                
                                </div>
                            </div>
							
							<div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Email Approval 1<span class="required">*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" name="direct_leader_name"  class="form-control" id="field-1" value="<?php if (!empty($matrix_info)) echo $matrix_info->direct_leader_email; ?>"/>               
                                </div>
                            </div>
							
							<div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Email Approval 2<span class="required">*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" name="direct_leader_name_2"  class="form-control" id="field-1" value="<?php if (!empty($matrix_info)) echo $matrix_info->direct_leader_email_2; ?>"/>               
                                </div>
                            </div>
														<div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Email Approval 3<span class="required">*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" name="nama_pic_hr"  class="form-control" id="field-1" value="<?php if (!empty($matrix_info)) echo $matrix_info->email_hr_pic; ?>"/>               
                                </div>
                            </div>
							<!--<div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Email Hybris<span class="required">*</span></label>
                                <div class="col-sm-5">
                                    <input type="text" name="email_hybris"  class="form-control" id="field-1" value="<?php if (!empty($matrix_info)) echo $matrix_info->email_hybris; ?>"/>               
                                </div>
                            </div>-->
							
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-5 pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo $this->language->from_body()[1][12] ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>        
        </div>        
    </div> 
<?php endif; ?>
<!--
<form method="post" action="<?php echo base_url() ?>admin/settings/matrix_approval">
    <h4><i style="color:#428BCA;" class="fa fa-plus"></i><input style="color:#428BCA;background: none;;border: none" type="submit" name="add_matrix" value="Add Matrix Approval "/></h4>    
</form>
-->
<div class="row">
    <div class="col-sm-12 scroll-box" data-spy="scroll" data-offset="0">                            
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">
                <div class="panel-title">
                    <strong>Matrix Approval List</strong>
                </div>
            </div>
            <!-- Table -->
            <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
                        <th class="col-sm-1">Employee ID</th>
                        <th>Employee Name</th>
                        <th>Approval(1)</th>
						<th>Email Approval(1)</th>
                        <th> Approval(2)</th>
                        <th>Email Approval(2)</th>
                        <th>Acknowledge/ Approval(3)</th>
						<th>Email Approval(3)</th>
						<!--<th>Hybris Name</th>
						<th>Email Hybris</th>-->

                        <th class="col-sm-2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($all_matrix_info)): $sl = 1;
                        foreach ($all_matrix_info as $v_matrix) :
                            ?>                            
                            <tr>
                                <td><?php echo $sl ?></td>
                                <td>
                                    <?php echo $v_matrix->first_name.' '.$v_matrix->last_name; ?>
                                </td>
                                <td><?php echo $v_matrix->direct_leader_name; ?></td>
								<td><?php echo $v_matrix->direct_leader_email; ?></td>
                                <td><?php echo $v_matrix->direct_leader_name_2; ?></td>
								<td><?php echo $v_matrix->direct_leader_email_2; ?></td>
                                <td><?php echo $v_matrix->nama_pic_hr; ?></td>
								<td><?php echo $v_matrix->email_hr_pic; ?></td>
								<td><?php echo $v_matrix->hybris_name; ?></td> 
								<td><?php echo $v_matrix->email_hybris; ?></td>	
                                <td>   
                                    <div class="btn-group" role="group" aria-label="...">
                                        <?php echo btn_edit('admin/settings/matrix_approval/' . $v_matrix->employee_id); ?>  
                                        <?php //echo btn_delete('admin/settings/delete_matrix_approval/' . $v_matrix->employee_id); ?>        
                                    </div>
                                </td>                            
                            </tr>
                            <?php $sl++ ?>
                        <?php endforeach; ?> 
                    <?php else: ?> 
                    <td colspan="4">
                    <tr>
                    <strong>There is no data for display</strong>
                    </tr>
                    </td>
                <?php endif; ?>   


                </tbody>
            </table>  
            </div>        
        </div>
    </div>
</div>



