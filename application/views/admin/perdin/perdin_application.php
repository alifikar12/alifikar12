
<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-12 wrap-fpanel" data-offset="0">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <span>
                        <strong>List of All perdin for Approve(1)</strong>
                    </span>
                </div>
            </div>
            <!-- Table -->
            <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTables-example">
                    <thead>                                     
                        <tr style="font-size: 13px;color: #000000">  
							<!--<th>No</th>-->
							<th>perdin ID</th>
							<th class="col-sm-1">NIK</th>
							<th class="col-sm-2">Employee Name</th>
                            <th class="col-sm-3">perdin Category</th>
                            <th class="col-sm-1">Start Time</th>
                            <th class="col-sm-1">End Time</th>
                            <th>Reason</th>
                            <th class="col-sm-1">Applied On</th>
                            <th class="col-sm-1">Status</th>
                            <th>Action</th>   
                        </tr>
                    </thead>                
                    <tbody style="margin-bottom: 0px;background: #FFFFFF;font-size: 12px;">                                                                   
                        <?php 
							$i=1;
							if (!empty($all_perdin_applications)): 
							foreach ($all_perdin_applications as $v_application) : 
						?>

                                <tr>      
									<!--<td><?php echo $i ?></td>-->
									<td><?php echo $v_application->perdin_id ?></td>
									<td><?php echo $v_application->employment_id ?></td>
									<td><?php echo $v_application->first_name.' '.$v_application->last_name ?></td>
                                    <td><?php echo $v_application->category ?></td>
                                    <td><?php echo date('d M Y', strtotime($v_application->perdin_start_date)) ?></td>
                                    <td><?php echo date('d M Y', strtotime($v_application->perdin_end_date)) ?></td>
                                    <td><?php echo $v_application->reason ?></td>                                                                        
                                    <td><?php echo date('d M Y', strtotime($v_application->submit_date)) ?></td>
                                    <td><?php
                                        if ($v_application->perdin_status == 'pending') {
                                            echo '<span class="label label-warning">pending</span>';
                                        } elseif ($v_application->perdin_status == 'partial approved') {
                                            echo '<span class="label label-info">partial approve</span>';
                                        } elseif ($v_application->perdin_status == 'fully approved' ) {
                                            echo '<span class="label label-success">partial approve</span>';
                                        } elseif ($v_application->perdin_status == 'cancel') {
                                            echo '<span class="label label-danger">cancel</span>';
                                        }
                                        ?>
                                    </td>     
                                    <td><?php echo btn_view('admin/perdin_application/view_perdin_details/' . $v_application->perdin_id) ?></td>                                                                                    
                                </tr>
							<?php
							$i++;
                            endforeach;
                            ?>
                        <?php else : ?>
                        <td colspan="3">
                            <strong>There is no data to display</strong>
                        </td>
                    <?php endif; ?>
                    </tbody>                    
                </table>
            </div>
        </div>
    </div>
</div>

