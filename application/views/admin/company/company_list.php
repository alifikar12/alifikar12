<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<div class="row">
    <div class="col-sm-12"> 
        <div class="wrap-fpanel">
            <div class="panel panel-default" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>Add Company</strong>
                    </div>
                </div>
                <div class="panel-body">
                    <form id="form" enctype="multipart/form-data" action="<?php echo base_url() ?>admin/company/save" 
                        method="post" class="form-horizontal form-groups-bordered">

                        <div class="form-group" style="display:none">
                            <label for="field-1" class="col-sm-3 control-label">Company ID <span class="required">*</span></label>
                            <div class="col-sm-5">                            
                                <input type="text" name="id" value="<?php
                                if (!empty($company->id)) {
                                    echo $company->id;
                                }
                                ?>" class="form-control" placeholder="Enter Company ID" readonly />
                            </div>
						</div>	

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Company Name <span class="required">*</span></label>
                            <div class="col-sm-5">                            
                                <input type="text" name="name" value="<?php
                                if (!empty($company->name)) {
                                    echo $company->name;
                                }
                                ?>" class="form-control" placeholder="Enter Company Name" />
                            </div>
						</div>	
                        
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Logo <span class="required">*</span></label>
                            <div class="col-sm-5">    
                                <input type="hidden" name="old_path" value="<?php if (!empty($company)) echo $company->logo; ?>">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail g-logo-img" >
                                        <?php if (!empty($company->logo)): ?>
                                            <img src="<?php echo base_url() . $company->logo; ?>" >  
                                        <?php else: ?>
                                            <!-- <img src="http://placehold.it/350x260" alt="Please Connect Your Internet">      -->
                                        <?php endif; ?>                                 
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail g-logo-img" ></div>
                                    <div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new"><input type="file"  name="img" size="20" /></span>
                                            <span class="fileinput-exists">Change</span>    
                                        </span>
                                        <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                    <div id="valid_msg" style="color: #e11221"></div>
                                </div>
                            </div>
                        </div>

						<div class="form-group">	
                            <div class="col-sm-3"></div>
                            <div class="col-sm-2">
                                <button type="submit" id="sbtn" class="btn btn-primary">Save</button>                            
                            </div>
                        </div>
							
                    </form>
                </div>                 
            </div>                 
        </div>  
        <br/>
        <div class="row">
            <div class="col-sm-12" data-offset="0">                            
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Company List</strong>
                        </div>
                    </div>

                    <!-- Table -->
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="col-sm-1">ID</th>
                                <th>Company Name</th>     
                                <th>Logo</th>                           
                                <th class="col-sm-2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($company_info)): foreach ($company_info as $row) : ?>
                                    <tr>
                                        <td><?php echo $row->id ?></td>
                                        <td><?php echo $row->name ?></td>         
                                        <td><img src="<?php echo base_url() . 'img/uploads/company/' . $row->logo ?>" style="height:20px; width:65px;"></td>                                 
                                        <td>
                                            <?php echo btn_edit('admin/company/index/' . $row->id); ?>  
                                            <?php echo btn_delete('admin/company/delete_company/' . $row->id); ?>
                                        </td>
                                    </tr>
                                    <?php
                                endforeach;
                                ?>
                            <?php else : ?>
                            <td colspan="3">
                                <strong>There is no data to display</strong>
                            </td>
                        <?php endif; ?>
                        </tbody>
                    </table>          
                </div>
            </div>
        </div>
    </div>   
</div>

<!-- popup sweetalert2 -->
<?= swal_message_box('success');?>
<?= swal_message_box('error');?>
