
<div class="row">
    <div class="col-sm-12"> 
         
        <h4><a href="#" data-toggle="modal" data-target="#addProduct" id="btnAddProduct"><i class="fa fa-plus"></i> Add Product</a></h4>

        <!-- modal add product -->
        <div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="<?= base_url() ?>admin/product/save_product" method="post">
                            <div class="form-group">
                                <input type="hidden" class="form-control" id="id" name="id" autocomplete="off" required readonly>
                            </div>
                            <div class="form-group">
                                <label for="product_id">Product ID</label>
                                <input type="text" class="form-control" id="product_id" name="product_id" autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <label for="product_name">Product Name</label>
                                <input type="text" class="form-control" id="product_name" name="product_name" autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="text" class="form-control" id="price" name="price" autocomplete="off" required>
                            </div>
                            <div class="form-group">
                                <select name="active" id="active" class="form-control" > 
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>                         
                                </select>
                            </div>
                           
                            <button type="submit" class="btn btn-primary">
                                Tambahkan
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--  -->

        <div class="row">          
            <div class="col-sm-12" data-offset="0">                            
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Product List</strong>
                        </div>
                    </div>
                    <div class="table-responsive">            
                    <!-- Table -->
                    <table class="table table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th class="col-sm-1">Product ID</th>
                                <th>Product Name</th> 
                                <th>Price</th>   
                                <th>Active</th>      
                                <?php if($role == 'Administrator' || $role == 'HRGAManager'):?>
                                    <th class="col-sm-2">Action</th>
                                <?php endif?>
                            
                            </tr>
                        </thead>
                        <tbody>                           
                            <?php $i=1;
                                    if (!empty($product_info)): foreach ($product_info as $d) : ?>
                                    <tr>
                                        <td><?= $i ?></td>
                                        <td><?= $d->product_id ?></td>
                                        <td><?= $d->product_name ?></td>   
                                        <td><?= number_format($d->price,0) ?></td>     
                                        <td><?php if($d->active == 1){ echo 'Yes'; }else{ echo 'No'; } ?></td>  
                                        <?php if($role == 'Administrator' || $role == 'HRGAManager'):?>
                                            <td>
                                                <button id="editButton<?= $d->id;?>"
                                                    data-toggle="modal"
                                                    data-target="#addProduct"
                                                    data-id="<?= $d->id;?>"
                                                    data-product_id="<?= $d->product_id;?>"
                                                    data-product_name="<?= $d->product_name;?>"
                                                    data-price="<?= $d->price;?>"
                                                    data-active="<?= $d->active;?>">Edit
                                                </button>
                                                <?php echo btn_delete('admin/product/delete_product/' . $d->id); ?>
                                            </td>
                                        <?php endif?>
                                    </tr>
                                    <?php
                                    $i++;
                                    endforeach;
                                    ?>
                            <?php else : ?>
                            <td colspan="3">
                                <strong>There is no data to display</strong>
                            </td>
                        <?php endif; ?>
                        </tbody>
                    </table>   
                    </div>       
                </div>
            </div>
        </div>
    </div>   
</div>
<script>
$(document).ready(function(){
    <?php foreach($product_info as $d):?>
    $('#editButton<?= $d->id;?>').click(function(){
        var id              = $(this).data('id');
        var product_id      = $(this).data('product_id');
        var product_name    = $(this).data('product_name');
        var price           = $(this).data('price');
        var active          = $(this).data('active');
        $('#id').val(id);
        $('#product_id').val(product_id);
        $('#product_name').val(product_name);
        $('#price').val(price);
        $('#active').val(active);
    });
    <?php endforeach;?>
});

    $('#btnAddProduct').click(function(){
        $('#id').val('');
        $('#product_id').val('');
        $('#product_name').val('');
        $('#price').val('');
        $('#active').val('');
    });

</script>
<!-- popup sweetalert2 -->
<?= swal_message_box('success');?>
<?= swal_message_box('error');?>

