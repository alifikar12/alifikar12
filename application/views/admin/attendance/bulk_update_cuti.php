<?php include_once 'asset/admin-ajax.php'; ?>
<?= message_box('success'); ?>
<?= message_box('error'); ?>
<div class="row">
    <div class="col-sm-12"> 

        <div class="row">
            <div class="col-sm-12" data-offset="0">    
                <div class="wrap-fpanel">
                    <div class="panel panel-default" data-collapsed="0">                    
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong>Bulk Update Cuti Bersama</strong>
                            </div>
                        </div>
                        <div class="panel-body">
                            <form id="form" action="<?= base_url() ?>admin/attendance/bulk_update_cuti" method="post"  enctype="multipart/form-data" class="form-horizontal">   
                                <div class="panel_controls">                         
                                    <div class="form-group margin">
                                       
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Leave Category<span class="required"> *</span></label>

                                            <div class="col-sm-5">
                                                <select id="leave_category" name="leave_category_id" class="form-control" required >
                                                    <option value="" >Select Leave Category...</option>
                                                    <?php foreach ($all_leave_category_info as $v_category) : ?>
                                                        <option value="<?= $v_category->leave_category_id ?>"
                                                        <?php
                                                        if (!empty($leave_category_id)) {
                                                            echo $v_category->leave_category_id == $leave_category_id ? 'selected' : '';
                                                        }
                                                        ?>    >                                                
                                                                <?= $v_category->category ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group"> 
                                            <label class="col-sm-3 control-label">Start Date <span class="required"> *</span></label>

                                            <div class="col-sm-5">
                                                <div class="input-group">
                                                    <input type="text" name="leave_start_date"  required class="form-control datepicker" value="<?php if(!empty($leave_start_date)) echo $leave_start_date;?>" data-format="dd-mm-yyyy">
                                                    <div class="input-group-addon">
                                                        <a href="#"><i class="entypo-calendar"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">End Date <span class="required"> *</span></label>

                                            <div class="col-sm-5">
                                                <div class="input-group">
                                                    <input type="text" name="leave_end_date"   required class="form-control datepicker" value="<?php if(!empty($leave_end_date)) echo $leave_end_date;?>" data-format="dd-mm-yyyy">
                                                    <div class="input-group-addon">
                                                        <a href="#"><i class="entypo-calendar"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Reason</label>

                                            <div class="col-sm-5">
                                                <textarea id="present" name="reason" class="form-control" rows="6"><?php if(!empty($reason)) echo $reason ?></textarea>
                                            </div>
                                        </div>   

                                        <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Department<span class="required"> *</span></label>

                                            <div class="col-sm-5">
                                                <select name="department_id" id="department" class="form-control">
                                                    <option value="" >Select Department...</option>
                                                    <?php foreach ($all_department as $v_department) : ?>
                                                        <option value="<?= $v_department->department_id ?>"                                                     
                                                        <?php
                                                        if (!empty($department_id)) {
                                                            echo $v_department->department_id == $department_id ? 'selected' : '';
                                                        }
                                                        ?>                                                    
                                                                ><?php 
                                                                echo $v_department->department_name;
                                                                ?></option>

                                                        
                                                    <?php endforeach; ?>
                                                    
                                                </select>    
                                            </div>                        
                                        </div>

                                    </div>
                                   

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-5">
                                            <button type="submit" id="sbtn" name="sbtn" value="1" class="btn btn-primary">Go</button>                            
                                        </div>
                                    </div>
                                </div>
                            </form>  
                        </div>
                    </div>


                    <?php if (!empty($employee_info)): ?>
                    <form action="<?= base_url() ?>admin/attendance/save_bulk_cuti" method="post"  enctype="multipart/form-data" class="form-horizontal">  
                        <!-- hidden post -->
                        <table class="table table-bordered">
                            <div style="display:none;">
                                <select id="leave_category" name="leave_category_id" class="form-control" required >
                                    <option value="" >Select Leave Category...</option>
                                    <?php foreach ($all_leave_category_info as $v_category) : ?>
                                        <option value="<?= $v_category->leave_category_id ?>"
                                        <?php
                                        if (!empty($leave_category_id)) {
                                            echo $v_category->leave_category_id == $leave_category_id ? 'selected' : '';
                                        }
                                        ?>    >                                                
                                                <?= $v_category->category ?></option>
                                    <?php endforeach; ?>
                                </select>
                                
                                <input type="text" name="leave_start_date"  required class="form-control datepicker" value="<?php if(!empty($leave_start_date)) echo $leave_start_date;?>" data-format="dd-mm-yyyy">
                                <input type="text" name="leave_end_date"   required class="form-control datepicker" value="<?php if(!empty($leave_end_date)) echo $leave_end_date;?>" data-format="dd-mm-yyyy">
                                <textarea id="present" name="reason" class="form-control" rows="6"><?php if(!empty($reason)) echo $reason ?></textarea>
                                
                                <select name="department_id" id="department" class="form-control">
                                    <option value="" >Select Department...</option>
                                    <?php foreach ($all_department as $v_department) : ?>
                                        <option value="<?= $v_department->department_id ?>"                                                     
                                        <?php
                                        if (!empty($department_id)) {
                                            echo $v_department->department_id == $department_id ? 'selected' : '';
                                        }
                                        ?>                                                    
                                                ><?= $v_department->department_name ?></option>
                                    <?php endforeach; ?>

                                </select>  

                                    
                            </div>
                        </table>
                        <!-- end hidden post -->

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Employee Name</th>                                
                                        <th>Designation</th>
                                    </tr>
                                </thead>                             
                                <tbody>
                                
                                <?php foreach ($employee_info as $v_employee) { ?>
                                    <tr><td> 
                                            <input type="hidden" name="employee_id[]"  value="<?= $v_employee->employee_id ?>"> <input type="hidden" name="employment_id[]"  value="<?= $v_employee->employment_id ?>"> <?= $v_employee->first_name . ' ' . $v_employee->last_name; ?></td>                                                                             
                                        <td><?= $v_employee->designations ?></td>
                                    </tr>
                                <?php }
                                ?>  
                                </tbody>
                            </table>                               
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-11">
                            <button type="submit" id="sbtn" class="btn btn-primary">Update</button>                            
                        </div>
                    </div>
                </form>
                <?php endif; ?>
            </div>
        </div>
    </div>                                            
</div>   
<script type="text/javascript">
    $(document).ready(function() {
        $(':checkbox').on('change', function() {
            var th = $(this), id = th.prop('id');

            if (th.is(':checked')) {
                $(':checkbox[id="' + id + '"]').not($(this)).prop('checked', false);
            }
        });
    });
</script>
<script>
    $(function() {
        $('#date').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
        });
    });

</script>