<?php include_once 'asset/admin-ajax.php'; ?>
<?= message_box('success'); ?>
<?= message_box('error'); ?>
<div class="row">
    <div class="col-sm-12"> 

        <div class="row">
            <div class="col-sm-12" data-offset="0">    
                <div class="wrap-fpanel">
                    <div class="panel panel-default" data-collapsed="0">                    
                        <div class="panel-heading">
                            <div class="panel-title">
                                <strong>Entry No Show</strong>
                            </div>
                        </div>
                        <div class="panel-body">
                            <form id="form" action="<?= base_url() ?>admin/attendance/save_noshow" method="post"  enctype="multipart/form-data" class="form-horizontal">   
                                <div class="panel_controls">                         
                                    <div class="form-group margin">     

                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Leave Category<span class="required"> *</span></label>
                                            <div class="col-sm-5">
                                                <select id="leave_category" class="form-control" required >
                                                    <option value="" >Select Leave Category...</option>
                                                    <?php foreach ($all_leave_category_info as $v_category) : ?>
                                                        <option value="<?= $v_category->leave_category_id ?>" data-category="<?= $v_category->category;  ?>"
                                                        <?php
                                                        if (!empty($leave_category_id)) {
                                                            echo $v_category->leave_category_id == $leave_category_id ? 'selected' : '';
                                                        }
                                                        ?>    >                                                
                                                                <?= $v_category->category ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <input type="hidden" name="leave_category_id" id="leave_category_id" class="form-control" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group"> 
                                            <label class="col-sm-3 control-label">Start Date <span class="required"> *</span></label>
                                            <div class="col-sm-5">
                                                <div class="input-group">
                                                    <input type="text" name="leave_start_date"  required class="form-control datepicker" value="<?php if(!empty($leave_start_date)) echo $leave_start_date;?>" data-format="dd-mm-yyyy">
                                                    <div class="input-group-addon">
                                                        <a href="#"><i class="entypo-calendar"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">End Date <span class="required"> *</span></label>
                                            <div class="col-sm-5">
                                                <div class="input-group">
                                                    <input type="text" name="leave_end_date"   required class="form-control datepicker" value="<?php if(!empty($leave_end_date)) echo $leave_end_date;?>" data-format="dd-mm-yyyy">
                                                    <div class="input-group-addon">
                                                        <a href="#"><i class="entypo-calendar"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label for="field-1" class="col-sm-3 control-label">Reason</label>
                                            <div class="col-sm-5">
                                                <textarea id="reason" name="reason" class="form-control" rows="6"><?php if(!empty($reason)) echo $reason ?></textarea>
                                            </div>
                                        </div>   

                                        <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Employee<span class="required"> *</span></label>
                                            <div class="col-sm-5">
                                                <select name="employee_id" id="employee_id" class="form-control">
                                                    <option value="" >Select Employee...</option>
                                                    <?php foreach ($all_employee as $v_employee) : ?>
                                                        <option value="<?= $v_employee->employee_id ?>"  data-employment_id="<?= $v_employee->employment_id;  ?>"                                                  
                                                        <?php
                                                        if (!empty($employee_id)) {
                                                            echo $v_employee->employee_id == $employee_id ? 'selected' : '';
                                                        }
                                                        ?>                                                    
                                                                ><?php 
                                                                echo $v_employee->first_name.' '.$v_employee->last_name;
                                                                ?></option>
                                                    <?php endforeach; ?>                                                    
                                                </select>    
                                            </div>                        
                                        </div>
                                        <div class="form-group" style="display:none">
                                            <label for="field-1" class="col-sm-3 control-label">Employment id <span class="required">*</span><small id="id_error_msg"></small></label>
                                            <div class="col-sm-5">
                                            <input type="text" name="employment_id" id="employment_id"  readonly class="form-control"  
                                                    value="<?php if (!empty($v_employee)) echo $v_employee->employment_id; ?>"/> 
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-5">
                                            <button type="submit" id="sbtn" name="sbtn" value="1" class="btn btn-primary">Save</button>                            
                                        </div>
                                    </div>

                                </div>
                            </form>  
                        </div>
                    </div>
                                        
            </div>
        </div>
    </div>                                            
</div>   
<script type="text/javascript">
    $(document).ready(function() {
        $(':checkbox').on('change', function() {
            var th = $(this), id = th.prop('id');

            if (th.is(':checked')) {
                $(':checkbox[id="' + id + '"]').not($(this)).prop('checked', false);
            }
        });
    });
</script>
<script>
    $(function() {
        $('#date').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
        });
    });

    //populate data from dropdown menu
    $('#employee_id').change(function(){
        //get the selected option's data-id value using jquery `.data()`
        var employment_id =  $(':selected',this).data('employment_id'); 
        //populate data.
        $('#employment_id').val(employment_id);
    });  

    $('#leave_category').change(function(){
        var leave_category = $(':selected',this).data('category');
        // alert(leave_category);
        if(leave_category=='No Show'){
            $('#reason').val(leave_category);
            // $('#reason').attr('readonly',true);
        }else{
            $('#reason').val('');
            // $('#reason').attr('readonly',false);
        }
    })

</script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#leave_category").val(14).change();
        $("#leave_category").prop('disabled', true);
        $("#leave_category_id").val(14);        
    });
</script>

