<style type="text/css" media="print">
    @media print{@page {size: landscape}}
</style>
<!--CSS progress bar-->
<style> 
#Progress_Status { 
  width: 100%; 
  background-color: #ddd; 
} 
  
#myprogressBar { 
  width: 0%; 
  height: 20px; 
  background-color: #4CAF50; 
} 
</style> 
<!--CSS progress bar-->

<div class="row">
    <div class="col-sm-12">
        <div class="wrap-fpanel">
            <div class="panel panel-default" data-collapsed="0">
				<!--progress bar-->
				<div id="Progress_Status"> 
				  <div id="myprogressBar"></div> 
				</div> 
				<!--progress bar-->
				
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong><?php echo $this->language->form_heading()[15] ?></strong>
						<div class="pull-right">
							<a href="<?php echo base_url(); ?>admin/api_attendance/download_attendance" class="btn btn-primary btn-xs" id="getAttLogs" onclick="progressBar()" ><span>Download Data Finger</span></a>
						</div>
					</div>   	
                </div>

                <div class="panel-body">
                    <form id="attendance-form" role="form" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/attendance/get_att_report_by_emp" method="post" class="form-horizontal form-groups-bordered">                    
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Employee Name<span class="required">*</span></label>

                            <div class="col-sm-5">
                                <select name="employee_id" class="form-control" >
                                    <option value="" >Select Employee Name...</option>                                  
                                    <?php if (!empty($all_employee)): foreach ($all_employee as $employee): ?>
                                            <option value="<?php echo $employee->employee_id; ?>"
                                            <?php if (!empty($employee_id)): ?>
                                                <?php echo $employee->employee_id == $employee_id ? 'selected ' : '' ?>                                                            
                                                    <?php endif; ?>>
                                                        <?php echo $employee->first_name. ' ' .$employee->last_name; ?>
                                            </option>                                            
                                            <?php
                                        endforeach;                                        
                                    endif;                                    
                                    ?> 
                                    <!-- <option value="0">ALL</option> -->
                                </select>                            
                            </div>
                        </div> 

                        <!-- <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Start Month <span class="required">*</span></label>
                            <div class="input-group col-sm-5">
                                <input type="text" class="form-control monthyear" value="<?php
                                if (!empty($start_date)) {
                                    echo date('Y-n', strtotime($start_date));
                                }
                                ?>" name="start_date" >
                                <div class="input-group-addon">
                                    <a href="#"><i class="entypo-calendar"></i></a>
                                </div>
                            </div>
                        </div>  

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">End Month <span class="required">*</span></label>
                            <div class="input-group col-sm-5">
                                <input type="text" class="form-control monthyear" value="<?php
                                if (!empty($end_date)) {
                                    echo date('Y-n', strtotime($end_date));
                                }
                                ?>" name="end_date" >
                                <div class="input-group-addon">
                                    <a href="#"><i class="entypo-calendar"></i></a>
                                </div>
                            </div>
                        </div>   -->

                        <!-- <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Periode<span class="required">*</span></label>
                            <div class="col-sm-5">
                                <select name="periode" class="form-control" >
                                    <option value="" >Select Periode...</option>                                  
                                    <option value="quarterly">Quarterly</option>
                                    <option value="yearly">Yearly</option>
                                    
                                </select>                            
                            </div>
                        </div>  -->
                       
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Start Date<span class="required">*</span></label>
                                <div class="input-group col-sm-5">
                                    <input type="text" class="form-control datepicker" name="start_date" value="<?php
                                    if (!empty($start_date)) {
                                        echo date('Y-m-d', strtotime($start_date));
                                    }
                                    ?>" >

                                    <div class="input-group-addon">
                                        <a href="#"><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">End Date<span class="required">*</span></label>
                                <div class="input-group col-sm-5">
                                    <input type="text" class="form-control datepicker" name="end_date" value="<?php
                                    if (!empty($end_date)) {
                                        echo date('Y-m-d', strtotime($end_date));
                                    }
                                    ?>" >

                                    <div class="input-group-addon">
                                        <a href="#"><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                            </div> 
                                                 
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5 pull-right">
                                <!-- button POST data -->
                                <input type="submit" name="Search2" value="Generate" class="btn btn-primary" />                                                  
                            </div>
                        </div>   
                    </form>
                </div>                        
            </div>                        
        </div>                
    </div>   
</div>
<div id="EmpprintReport">
    <div class="show_print" style="width: 100%; border-bottom: 2px solid black;">
        <table style="width: 100%; vertical-align: middle;">
            <tr>
                <?php
                $genaral_info = $this->session->userdata('genaral_info');
                if (!empty($genaral_info)) {
                    foreach ($genaral_info as $info) {
                        ?>
                        <td style="width: 35px; border: 0px;">
                            <img style="width: 50px;height: 50px" src="<?php echo base_url() . $info->logo ?>" alt="" class="img-circle"/>
                        </td>
                        <td style="border: 0px;">
                            <p style="margin-left: 10px; font: 14px lighter;"><?php echo $info->name ?></p>
                        </td>
                        <?php
                    }
                } else {
                    ?>
                    <td style="width: 35px; border: 0px;">
                        <img style="width: 50px;height: 50px" src="<?php echo base_url() ?>img/logo.png" alt="Logo" class="img-circle"/>
                    </td>
                    <td style="border: 0px;">
                        <p style="margin-left: 10px; font: 14px lighter;">Human Resource Management System</p>
                    </td>
                    <?php
                }
                ?>
            </tr>
        </table>
    </div>
    <br/>
    <br/>
		
	<!--Attendance Report v2 -->
    <?php if (!empty($attendance_v2)): ?>
        <div class="std_heading" hidden style="background-color: rgb(224, 224, 224);margin-bottom: 5px;padding: 5px;">           
            <table style="margin: 3px 10px 0px 24px; width:100%;">                    
                <tr>
                    <td style="font-size: 15px"><strong>Department : </strong><?php echo $dept_name->department_name ?></td>                    
                    <td style="font-size: 15px"><strong>Periode :</strong><?php echo $start_date .' s/d '. $end_date ?></td>
                </tr>                                      
            </table>
        </div>
        <div class="row">
            <div class="col-sm-12 std_print"> 
                <div class="wrap-fpanel">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><strong>Attendance List </strong>
                                <div class="pull-right hidden-print" >
                                    <a href="<?php echo base_url() ?>admin/attendance/create_pdf_att_report_by_emp/<?php echo $employee_id . '/' . date('Y-m-d',strtotime($start_date)) . '/' . date('Y-m-d',strtotime($end_date)) ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Pdf"><span><i class="fa fa-file-pdf-o"></i></span></a>
                                    <!-- <a href="<?php echo base_url() ?>admin/attendance/create_excel_att_report_by_emp/<?php echo $employee_id . '/' . date('Y-m-d',strtotime($start_date)) . '/' . date('Y-m-d',strtotime($end_date)) ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Excel"><span><i class="fa fa-file-excel-o"></i></span></a>                                                 -->
                                    <button  class="btn-print" title="Print" data-toggle="tooltip" type="button" onclick="printEmp_report('EmpprintReport')"><?php echo btn_print(); ?></button>                                                              
                                </div>
                            </h4>
                        </div>            
                        
                        <div class="table-responsive">
                        <table id="" class="table table-bordered std_table">
                            <!-- jan -->
                            <?php if(!empty($dateSl['Jan'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                

                                    <?php foreach ($dateSl['Jan'] as $edate) : 
                                        //  echo '<pre>';
                                        //  print_r($dateSl);
                                        //  echo '</pre>';
                                        //die;
                                        ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php 
                                        
                            
                                        foreach ($attendance_v2 as $v_employee): 
                                        //     echo '<pre>';
                                        //     print_r($v_employee['Jan']);
                                        //     echo '</pre>';
                                        //    die;
                                        
                                ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">January</td>  
                                        <?php foreach ($v_employee['Jan'] as $v_result): 
                                                //  echo '<pre>';
                                                //  print_r($v_result);
                                                //  echo '</pre>';
                                            ?>   

                                                <td>
                                                    <?php
                                                        if(!empty($v_result)){
                                                            $cekin  = '';
                                                            $cekout = '';
                                                            $absen  = '';
                                                            foreach ($v_result as $emp_attendance):
                                                            //print_r($emp_attendance);
                                                            //if(date('m',strtotime($emp_attendance->Date))=='01'){       

                                                                if($emp_attendance->StatusAbsen == 'C/In'){

                                                                    $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                                    //set jam telat
                                                                    if($cekin_tmp > '10:00:00'){
                                                                        $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                                    }else{
                                                                        $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                                    }
                                                                }
                                                                if($emp_attendance->StatusAbsen == 'C/Out'){
                                                                    $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                                }

                                                                if ($emp_attendance->StatusAbsen == 'L') {
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'C'){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'I'){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'W'){
                                                                    $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'O'){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                                }elseif($emp_attendance->StatusAbsen == ''){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'S'){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'D'){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                                }else{                                                           
                                                                    $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                                }    
                                                            //}        

                                                            endforeach;      
                                                    }
                                                      echo $absen;
                                                                                                              
                                                    ?>
                                                    <?php endforeach; ?> 

                                                </td>               
                                    </tr>

                                    
    
                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end jan -->

                            <!-- feb -->
                            <?php if(!empty($dateSl['Feb'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Feb'] as $edate) : ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">February</td>  
                                        <?php foreach ($v_employee['Feb'] as $v_result): ?>   
                                            <td>
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end feb -->

                            <!-- mar -->
                            <?php if(!empty($dateSl['Mar'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Mar'] as $edate) : ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">March</td>  
                                        <?php foreach ($v_employee['Mar'] as $v_result): ?>   
                                            <td>
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end March -->

                            <!-- apr -->
                            <?php if(!empty($dateSl['Apr'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Apr'] as $edate) : ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">April</td>  
                                        <?php foreach ($v_employee['Apr'] as $v_result): ?>   
                                            <td>
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end apr -->

                             <!-- may -->
                             <?php if(!empty($dateSl['May'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['May'] as $edate) : ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">May</td>  
                                        <?php foreach ($v_employee['May'] as $v_result): ?>   
                                            <td>
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end may -->

                             <!-- jun -->
                             <?php if(!empty($dateSl['Jun'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Jun'] as $edate) : ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">June</td>  
                                        <?php foreach ($v_employee['Jun'] as $v_result): ?>   
                                            <td>
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end jun -->

                             <!-- jul -->
                             <?php if(!empty($dateSl['Jul'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Jul'] as $edate) : ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">July</td>  
                                        <?php foreach ($v_employee['Jul'] as $v_result): ?>   
                                            <td>
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end jul -->

                             <!-- aug -->
                             <?php if(!empty($dateSl['Aug'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Aug'] as $edate) : ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">August</td>  
                                        <?php foreach ($v_employee['Aug'] as $v_result): ?>   
                                            <td>
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end aug -->

                            <!-- sep -->
                            <?php if(!empty($dateSl['Sep'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Sep'] as $edate) : ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">September</td>  
                                        <?php foreach ($v_employee['Sep'] as $v_result): ?>   
                                            <td>
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end sep -->

                             <!-- oct -->
                             <?php if(!empty($dateSl['Oct'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Oct'] as $edate) : ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">October</td>  
                                        <?php foreach ($v_employee['Oct'] as $v_result): ?>   
                                            <td>
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end oct -->

                            <!-- nov -->
                            <?php if(!empty($dateSl['Nov'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Nov'] as $edate) : ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">November</td>  
                                        <?php foreach ($v_employee['Nov'] as $v_result): ?>   
                                            <td>
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end nov -->

                             <!-- dec -->
                             <?php if(!empty($dateSl['Dec'])):?>
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Dec'] as $edate) : ?>  
                                        <th class="std_p"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="width: 100%; font-size:7pt" class="col-sm-3">December</td>  
                                        <?php foreach ($v_employee['Dec'] as $v_result): ?>   
                                            <td>
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'H') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">H</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'L'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'P'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">P</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">C</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end dec -->



                        </table>
                        </div>
                    </div>
                </div>    
            </div>
        </div>    
    <?php endif; ?>
</div>


<script type="text/javascript">
    function printEmp_report(EmpprintReport) {
        var printContents = document.getElementById(EmpprintReport).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }


    //TODO Create Progress bar Get Data Finger
    function progressBar() {
        alert('Ambil data logs finger, klik OK & Mohon tunggu sampai proses download selesai !');
		var element = document.getElementById("myprogressBar");    
		  var width = 1; 
		  var identity = setInterval(scene, 1000); 
		  function scene() { 
			if (width >= 1000) { 
			  clearInterval(identity); 
			} else { 
			  width++;  
			  element.style.width = width + '%';  
			} 
		  } 
		  
    }

    
	

</script>
