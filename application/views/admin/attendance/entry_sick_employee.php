

<div class="col-md-12">
    <div class="row">
        <div class="col-sm-12">
            <div class="wrap-fpanel">
                <div class="panel panel-info" data-collapsed="0">
                <?php echo message_box('success'); ?>
                <?php echo message_box('error'); ?>
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Entry Sick Employee</strong>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-offset-1">
                            <form id="form" action="<?php echo base_url() ?>admin/attendance/save_sick_employee" method="post"  enctype="multipart/form-data" class="form-horizontal">
                                <div class="panel_controls">

                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Employee Name<span class="required"> *</span></label>
                                        <div class="col-sm-5">
                                            <select id="employment_id" name="employment_id" class="form-control" required >
                                                <option value="" >Select Employee Name...</option>
                                                <?php foreach ($all_emplyee_info as $v_employee) : ?>
                                                    <option value="<?php echo $v_employee->employment_id ?>" data-employee_id="<?php echo $v_employee->employee_id; ?>" data-saldo_cuti="<?php echo $v_employee->saldo_cuti; ?>"> 
                                                        <?php echo $v_employee->first_name.' '.$v_employee->last_name ?></option>
                                                <?php endforeach; ?>                                                
                                            </select>
                                        </div>
                                    </div>

                                    
									
									<div class="form-group">
										<label id="lbl_saldo_cuti" style="display:none"; class="col-sm-3 control-label">Saldo Cuti <span class="required"> *</span></label>									
										<div class="col-sm-5">
											<input type="text" class="form-control" style="display:none" name="saldo_cuti" id="saldo_cuti" value="" readonly="true">
										</div>									
									</div>

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Start Date <span class="required"> *</span></label>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="text" name="sick_start_date"  required class="form-control datepicker" value="" data-format="dd-mm-yyyy">
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="entypo-calendar"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">End Date <span class="required"> *</span></label>

                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="text" name="sick_end_date"   required class="form-control datepicker" value="" data-format="dd-mm-yyyy">
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="entypo-calendar"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Note</label>

                                        <div class="col-sm-5">
                                            <textarea id="present" name="note" class="form-control" rows="6"></textarea>
                                        </div>
                                    </div>   

                                    <div class="form-group">																		
										<div class="col-sm-5">
											<input type="hidden" class="form-control" name="employee_id" id="employeeid" value="" readonly="true">
										</div>									
									</div>

                                    <!-- start upload file SKD  -->
                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">SKD</label>
                                        <input type="hidden" name="skd_path" value="<?php
                                        if (!empty($employee_info->skd_path)) {
                                            echo $employee_info->skd_path;
                                        }
                                        ?>">
                                        <input type="hidden" name="document_id" value="<?php
                                        if (!empty($employee_info->document_id)) {
                                            echo $employee_info->document_id;
                                        }
                                        ?>">
                                        <div class="col-sm-5">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <?php if (!empty($employee_info->skd)): ?>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new" style="display: none" >Select file</span>
                                                        <span class="fileinput-exists" style="display: block">Change</span>
                                                        <input type="hidden" name="skd" value="<?php echo $employee_info->skd ?>">
                                                        <input type="file" name="skd" >
                                                    </span>                                    
                                                    <span class="fileinput-filename"> <?php echo $employee_info->skd_filename ?></span>                                          
                                                <?php else: ?>
                                                    <span class="btn btn-default btn-file"><span class="fileinput-new" >Select file</span>
                                                        <span class="fileinput-exists" >Change</span>                                            
                                                        <input type="file" name="skd" >
                                                    </span> 
                                                    <span class="fileinput-filename"></span>                                        
                                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none;">&times;</a>                                                                                                            
                                                <?php endif; ?>

                                            </div>  
                                            <div id="msg_pdf" style="color: #e11221"></div>
                                        </div>
                                    </div>
                                    <!--  -->

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-5">
                                            <button type="submit" id="sbtn" name="sbtn" value="1" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
                <br/>

<div class="row">
    <div class="col-sm-12" data-offset="0">                            
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading">
                <div class="panel-title">
                    <strong>Sick Application List</strong>
                </div>
            </div>

            <!-- Table -->
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="col-sm-1">No</th>
                        <th>NIK</th>
                        <th>Name</th>
                        <th>Sick Start Date</th>
                        <th>Sick End Date</th>
                        <th>Note</th>
                        <th>SKD</th>
                        <th class="col-sm-2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $key = 1 ?>
                    <?php if (!empty($all_sick_application)): foreach ($all_sick_application as $v_result) : ?>
                            <tr>
                                <td><?php echo $key ?></td>
                                <td><?php echo $v_result->employment_id ?></td>      
                                <td><?php echo $v_result->first_name.' '.$v_result->last_name ?></td>                             
                                <td><?php echo $v_result->sick_start_date ?></td>
                                <td><?php echo $v_result->sick_end_date ?></td>
                                <td><?php echo $v_result->note ?></td>
                                <td>                                                        
                                <p class="form-control-static">
                                    <a href="<?php echo base_url() . $v_result->skd; ?>" target="_blank" style="text-decoration: underline;">View SKD</a>
                                </p>
                                </td>
                                <td>
                                    <?php
                                    if($this->session->userdata('role') == 'Administrator'){
                                        echo btn_delete('admin/attendance/delete_sick_application/' . $v_result->id); 
                                    } 
                                       
                                    ?>
                                </td>

                            </tr>
                            <?php
                            $key++;
                        endforeach;
                        ?>
                    <?php else : ?>
                    <td colspan="3">
                        <strong>There is no data to display</strong>
                    </td>
                <?php endif; ?>
                </tbody>
            </table>          
        </div>
    </div>
</div>




            </div>
        </div>
    </div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<script>
jQuery(document).ready(function() {
    jQuery("#leave_category").change(function() {
        if (jQuery(this).val() === '3'){ 
            jQuery('input[name=saldo_cuti]').show(); 
			jQuery('label[id="lbl_saldo_cuti"]').show(); 	
        } else {
            jQuery('input[name=saldo_cuti]').hide(); 
			jQuery('label[id="lbl_saldo_cuti"]').hide();
        }
    });
});


    //populate data from dropdown menu
    $('#employment_id').change(function(){
        //get the selected option's data-id value using jquery `.data()`
        var saldo_cuti =  $(':selected',this).data('saldo_cuti'); 
        var employee_id =  $(':selected',this).data('employee_id'); 
        // var direct_leader_id =  JSON.parse($('#approval1').attr('data-id'))[1]; 
        
        //populate data.
        $('#saldo_cuti').val(saldo_cuti);
        $('#employeeid').val(employee_id);
    });  

</script>

