<!DOCTYPE html>
<html>
    <head>
        <title>Employee Attendance</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
    </head>
    <body style="min-width: 100%; min-height: 100%; overflow: hidden; alignment-adjust: central;">
        <br />
        <div style="width: 100%; border-bottom: 2px solid black;">
            <table style="width: 100%; vertical-align: middle;">
                <tr>
                    <?php
                    $genaral_info = $this->session->userdata('genaral_info');
                    if (!empty($genaral_info)) {
                        foreach ($genaral_info as $info) {
                            ?>
                            <td style="width: 35px;">
                                <img style="width: 300px;height: 86px" src="<?php echo $_SERVER["DOCUMENT_ROOT"] . '/PMKI_HRMS/'. $info->logo ?>" alt="" class="img-circle"/>
                            </td>
                            <td>
                                <p style="margin-left: 10px; font: 14px lighter;"><?php //echo $info->name ?></p>
                            </td>
                            <?php
                        }
                    } else {
                        ?>
                        <td style="width: 35px;">
                            <img style="width: 50px;height: 50px" src="<?php echo base_url() ?>img/logo.png" alt="Logo" class="img-circle"/>
                        </td>
                        <td>
                            <p style="margin-left: 10px; font: 14px lighter;">Human Resource Management System</p>
                        </td>
                    <?php }
                    ?>                    
                </tr>
            </table>
        </div>
        <br />
        <div style="width: 100%;">
            <div style="height: 50px; width: 99%; background-color: rgb(224, 224, 224); padding: 1px 0px 5px 15px;">                
                <table style="margin: 3px 10px 0px 24px; width: 65%;">                    
                    <tr>                        
                        <td style="font-size: 15px"><strong>Department:</strong> <?php 
                                                                                    //print_r($dept_name);
                                                                                    if(!empty($dept_name->department_name)){
																						echo $dept_name->department_name; 
																					}
																				?>
                        </td>      
                    </tr>     
                    <tr>                
                        <td style="font-size: 15px"><strong>Periode:</strong> <?php echo $start_date .' s/d '. $end_date ?></td>
                    </tr>                                      
                </table>
            </div>
            <div align="center">
                <table style="width: 100%; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
                    <tr style="font-size: 20px;  text-align: center">
                        <td colspan="32" style=" padding: 10px 0;  color: black;">Employee Attendance</td>
                    </tr>
                    <tr style="background-color: rgb(224, 224, 224);">
                        <th style="text-align: center; font-size: 12px; border: 1px solid black;">Name</th>
                        <?php foreach ($dateSl as $edate) : ?>  
                            <th style="text-align: center; font-size: 12px; border: 1px solid black;"><?php echo $edate ?></th>
                        <?php endforeach; ?>

                    </tr>
                    <?php foreach ($attendance_v2 as $key => $v_employee): ?>
                        <tr>
                            <td style="text-align: left; border: 1px solid black; font-size: 12px;"><?php echo $employee[$key]->first_name.' '.$employee[$key]->last_name ?> </td>
                            <?php foreach ($v_employee as $v_result): ?>
                                <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                
                                <?php
                                    if(!empty($v_result)){
                                        $cekin  = '';
                                        $cekout = '';
                                        $absen  = '';
                                        foreach ($v_result as $emp_attendance):                                            
                                        
                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                            if($cekin_tmp > '09:01:00'){
                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                            }else{
                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                            }
                                        }
                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                        }

                                        if ($emp_attendance->StatusAbsen == 'L') {
                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                        }elseif($emp_attendance->StatusAbsen == 'N'){
                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">N</span>';
                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p"><font color="blue">W</font></span>';
                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                        }elseif($emp_attendance->StatusAbsen == ''){
                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                        }else{                                                           
                                            $absen = '<span style="font-size:5pt">'.$cekin.' '.$cekout.'</span>';
                                        }    

                                        endforeach;
                                    }    
                                        echo $absen;             
                                    
                                    ?>                                
                                </td>       
                            <?php endforeach; ?>       
                        </tr>
                    <?php endforeach; ?>                    
                </table>
            </div>
        </div>
    </body>
</html>