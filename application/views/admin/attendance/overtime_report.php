<style type="text/css" media="print">
    @media print{@page {size: landscape}}
</style>
<!--CSS progress bar-->
<style> 
#Progress_Status { 
  width: 100%; 
  background-color: #ddd; 
} 
  
#myprogressBar { 
  width: 0%; 
  height: 20px; 
  background-color: #4CAF50; 
} 
</style> 
<!--CSS progress bar-->

<div class="row">
    <div class="col-sm-12">
        <div class="wrap-fpanel">
            <div class="panel panel-default" data-collapsed="0">
				<!--progress bar-->
				<div id="Progress_Status"> 
				  <div id="myprogressBar"></div> 
				</div> 
				<!--progress bar-->
				
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong><?php echo $this->language->form_heading()[15] ?></strong>
						<div class="pull-right">
							<!--<a href="../../get_att_logs_finger.php" class="btn btn-primary btn-xs" id="getAttLogs" onclick="progressBar()" ><span>Download Data Finger</span></a>-->
						</div>
					</div>   	
                </div>

                <div class="panel-body">
                    <form id="attendance-form" role="form" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/attendance/get_overtime_report" method="post" class="form-horizontal form-groups-bordered">                    
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label"><?php echo $this->language->from_body()[16][0] ?><span class="required">*</span></label>

                            <div class="col-sm-5">
                                <select name="department_id" class="form-control" >
                                    <option value="" >Select Department...</option>                                  
                                    <?php if (!empty($all_department)): foreach ($all_department as $department): ?>
                                            <option value="<?php echo $department->department_id; ?>"
                                            <?php if (!empty($department_id)): ?>
                                                <?php echo $department->department_id == $department_id ? 'selected ' : '' ?>
                                                    <?php endif; ?>>
                                                        <?php echo $department->department_name; ?>
                                            </option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?> 
                                </select>                            
                            </div>
                        </div> 
                        <!--
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label"><?php echo $this->language->from_body()[16][1] ?> <span class="required">*</span></label>
                            <div class="input-group col-sm-5">
                                <input type="text" class="form-control monthyear" value="<?php
                                if (!empty($date)) {
                                    echo date('Y-n', strtotime($date));
                                }
                                ?>" name="date" >
                                <div class="input-group-addon">
                                    <a href="#"><i class="entypo-calendar"></i></a>
                                </div>
                            </div>
                        </div>   
                        -->
                        <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Start Date<span class="required">*</span></label>
                                <div class="input-group col-sm-5">
                                    <input type="text" class="form-control datepicker" name="start_date" value="<?php
                                    if (!empty($start_date)) {
                                        echo date('Y-m-d', strtotime($start_date));
                                    }
                                    ?>" >

                                    <div class="input-group-addon">
                                        <a href="#"><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">End Date<span class="required">*</span></label>
                                <div class="input-group col-sm-5">
                                    <input type="text" class="form-control datepicker" name="end_date" value="<?php
                                    if (!empty($end_date)) {
                                        echo date('Y-m-d', strtotime($end_date));
                                    }
                                    ?>" >

                                    <div class="input-group-addon">
                                        <a href="#"><i class="entypo-calendar"></i></a>
                                    </div>
                                </div>
                            </div>                      
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5 pull-right">
                                
                                <input type="submit" value="Generate" class="btn btn-primary" />
                                    
                                                  
                            </div>
                        </div>   
                    </form>
                </div>                        
            </div>                        
        </div>                
    </div>   
</div>
<div id="EmpprintReport">
    <div class="show_print" style="width: 100%; border-bottom: 2px solid black;">
        <table style="width: 100%; vertical-align: middle;">
            <tr>
                <?php
                $genaral_info = $this->session->userdata('genaral_info');
                if (!empty($genaral_info)) {
                    foreach ($genaral_info as $info) {
                        ?>
                        <td style="width: 35px; border: 0px;">
                            <img style="width: 50px;height: 50px" src="<?php echo base_url() . $info->logo ?>" alt="" class="img-circle"/>
                        </td>
                        <td style="border: 0px;">
                            <p style="margin-left: 10px; font: 14px lighter;"><?php echo $info->name ?></p>
                        </td>
                        <?php
                    }
                } else {
                    ?>
                    <td style="width: 35px; border: 0px;">
                        <img style="width: 50px;height: 50px" src="<?php echo base_url() ?>img/logo.png" alt="Logo" class="img-circle"/>
                    </td>
                    <td style="border: 0px;">
                        <p style="margin-left: 10px; font: 14px lighter;">Human Resource Management System</p>
                    </td>
                    <?php
                }
                ?>
            </tr>
        </table>
    </div>
    <br/>
    <br/>
		
	
    <?php if (!empty($overtime)): ?>
        <div class="std_heading" hidden style="background-color: rgb(224, 224, 224);margin-bottom: 5px;padding: 5px;">           
            <table style="margin: 3px 10px 0px 24px; width:100%;">                    
                <tr>

                    <td style="font-size: 15px"><strong>Department : </strong><?php echo $dept_name->department_name ?></td>                    
                    <td style="font-size: 15px"><strong>Periode :</strong><?php echo $start_date .' s/d '. $end_date ?></td>
                </tr>                                      
            </table>
        </div>
        <div class="row">
            <div class="col-sm-12 std_print"> 
                <div class="wrap-fpanel">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><strong>Overtime Report </strong>
                                <div class="pull-right hidden-print" >
                                    <a href="<?php echo base_url() ?>admin/attendance/create_pdf_overtime/<?php echo $department_id . '/' . date('Y-m-d',strtotime($start_date)) . '/' . date('Y-m-d',strtotime($end_date)) ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Pdf"><span><i class="fa fa-file-pdf-o"></i></span></a>
                                    <a href="<?php echo base_url() ?>admin/attendance/create_excel_overtime/<?php echo $department_id . '/' . date('Y-m-d',strtotime($start_date)) . '/' . date('Y-m-d',strtotime($end_date)) ?>" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="Excel"><span><i class="fa fa-file-excel-o"></i></span></a>                                                
                                    <button  class="btn-print" title="Print" data-toggle="tooltip" type="button" onclick="printEmp_report('EmpprintReport')"><?php echo btn_print(); ?></button>                                                              
                                </div>
                            </h4>
                        </div>    

                        <div class="table-responsive">                                              
                        <table id="" class="table table-bordered std_table">
                            <thead>
                                <tr>
                                    <th style="width: 100%" class="col-sm-3">Name</th>                

                                    <?php foreach ($dateSl as $edate) : ?>                                
                                        <th class="std_p"><?php 
															echo $edate 
															//echo '<span style="padding:2px; 4px" class="label label-danger std_p">'.$edate.'</span>';
														?>
										</th>
                                    <?php endforeach; ?>                        
                                    <th style="width: 100%" class="col-sm-3">Total Overtime Hours</th> 
                                </tr>  

                            </thead>      

                            <tbody>

                                <?php foreach ($overtime as $key => $v_employee): ?>
                                    <tr>  

                                        <td style="width: 100%; font-size:7pt" class="col-sm-3"><?php echo $employee[$key]->first_name . ' ' . $employee[$key]->last_name ?></td>   
                                        <?php $total_lembur_jam = 0; $total_lembur_menit = 0;
                                            foreach ($v_employee as $v_result): ?>                            
                                                <td>
                                                    <?php
                                                    foreach ($v_result as $emp_attendance):	
                                                                
                                                        // $awal  = strtotime('2017-08-10 10:05:25');
                                                        // $akhir = strtotime('2017-08-11 11:07:33');
                                                        // $diff  = $akhir - $awal;

                                                        // $jam   = floor($diff / (60 * 60));
                                                        // $menit = $diff - $jam * (60 * 60);
                                                        // echo 'Waktu tinggal: ' . $jam .  ' jam, ' . floor( $menit / 60 ) . ' menit';

														//if(!empty($emp_attendance->overtime_start_time)){
														//	if ($emp_attendance->StatusAbsen == 'H') {
														//		echo '<span style="padding:2px; 4px" class="label label-danger std_p">'.date('h:i A', strtotime($emp_attendance->overtime_start_time)).' - '.date('h:i A',strtotime($emp_attendance->overtime_end_time)).'</span>';
														//	} else{
																echo '<span style="font-size:5pt">'.date('h:i A', strtotime($emp_attendance->overtime_start_time)).' - '.date('h:i A',strtotime($emp_attendance->overtime_end_time)).'</span>';
														//	}
														//}
													   /*
														$jml_jam = strtotime($emp_attendance->overtime_end_time) - strtotime($emp_attendance->overtime_start_time);   
                                                        $total_lembur += $jml_jam;   
														*/
                                                         $awal  = strtotime($emp_attendance->overtime_start_time);
                                                         $akhir = strtotime($emp_attendance->overtime_end_time);
                                                         $diff  = $akhir - $awal;

                                                         $jam   = floor($diff / (60 * 60));
                                                         $menit = $diff - $jam * (60 * 60);
                                                         //echo 'Waktu tinggal: ' . $jam .  ' jam, ' . floor( $menit / 60 ) . ' menit';
                                                         $total_lembur_jam += $jam;
														 $total_lembur_menit += $menit;

                                                        // $jml_jam  = date_diff( $emp_attendance->overtime_start_time, $emp_attendance->overtime_end_time );
                                                        // $total_lembur += $jml_jam->h;
													endforeach;
                                                    ?>
                                                </td>
                                                    
                                        <?php endforeach; ?> 
                                        <td style="width: 100%" class="col-sm-3">
                                            <?php
                                                //$jam   = floor($jml_jam / (60 * 60));
                                                //$menit = $jml_jam - $jam * (60 * 60);
                                                
                                                //echo date('h:i',$jam) .  ' jam, ' . floor( $menit / 60 ) . ' menit';
												echo $total_lembur_jam .' jam, '. floor($total_lembur_menit / 60) .' menit ';
                                            ?>        
                                        </td>        
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                        </div>                            
                    </div>
                </div>    
            </div>
        </div>    
    <?php endif; ?>
</div>


<script type="text/javascript">
    function printEmp_report(EmpprintReport) {
        var printContents = document.getElementById(EmpprintReport).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    }


    //TODO Create Progress bar Get Data Finger
    function progressBar() {
        alert('Ambil data logs finger, klik OK & Mohon tunggu sampai proses download selesai !');
		var element = document.getElementById("myprogressBar");    
		  var width = 1; 
		  var identity = setInterval(scene, 1000); 
		  function scene() { 
			if (width >= 1000) { 
			  clearInterval(identity); 
			} else { 
			  width++;  
			  element.style.width = width + '%';  
			} 
		  } 
		  
    }

    
	

</script>
