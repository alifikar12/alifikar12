<!DOCTYPE html>
<html>
    <head>
        <title>Employee Attendance</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">        
    </head>
    <body style="min-width: 100%; min-height: 100%; overflow: hidden; alignment-adjust: central;">
        <br />
        <div style="width: 100%; border-bottom: 2px solid black;">
            <table style="width: 100%; vertical-align: middle;">
                <tr>
                    <?php
                    $genaral_info = $this->session->userdata('genaral_info');
                    if (!empty($genaral_info)) {
                        foreach ($genaral_info as $info) {
                            ?>
                            <td style="width: 35px;">
                                <img style="width: 160px;height: 50px" src="<?php echo $_SERVER["DOCUMENT_ROOT"] . '/PMKI_HRMS/'. $info->logo ?>" alt="" class="img-circle"/>
                            </td>
                            <td>
                                <!-- <p style="margin-left: 10px; font: 14px lighter;"><?php echo $info->name ?></p> -->
                            </td>
                            <?php
                        }
                    } else {
                        ?>
                        <td style="width: 35px;">
                            <img style="width: 50px;height: 50px" src="<?php echo base_url() ?>img/logo.png" alt="Logo" class="img-circle"/>
                        </td>
                        <td>
                            <p style="margin-left: 10px; font: 14px lighter;">Human Resource Management System</p>
                        </td>
                    <?php }
                    ?>                    
                </tr>
            </table>
        </div>
        <br />
        <div style="width: 100%;">
            <div style="height: 25px; width: 99%; background-color: rgb(224, 224, 224); padding: 1px 0px 5px 15px;">                
                <table style="margin: 3px 10px 0px 24px; width: 100%;">                    
                    <tr>                        
                        <td style="font-size: 15px"><strong>Nama :</strong> <?php  echo $full_name; ?></td>      
                        <td style="font-size: 15px"><strong>Periode:</strong> <?php echo $start_date .' s/d '. $end_date ?></td>
                    </tr>                                      
                </table>
            </div>
            <div align="center">
                <table style="width: 100%; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
               
                            <!-- jan -->
                            <?php if(!empty($dateSl['Jan'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Jan'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>   
                            <tbody>
                            <?php 
                                        
                                foreach ($attendance_v2 as $v_employee): 
                                             
                            ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">January</td>  
                                        <?php foreach ($v_employee['Jan'] as $v_result): ?>   

                                                <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                    <?php
                                                        if(!empty($v_result)){
                                                            $cekin  = '';
                                                            $cekout = '';
                                                            $absen  = '';
                                                            foreach ($v_result as $emp_attendance):

                                                                if($emp_attendance->StatusAbsen == 'C/In'){

                                                                    $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                                    //set jam telat
                                                                    if($cekin_tmp > '10:00:00'){
                                                                        $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                                    }else{
                                                                        $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                                    }
                                                                }
                                                                if($emp_attendance->StatusAbsen == 'C/Out'){
                                                                    $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                                }

                                                                if ($emp_attendance->StatusAbsen == 'L') {
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'C'){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'I'){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'W'){
                                                                    $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'O'){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                                }elseif($emp_attendance->StatusAbsen == ''){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'S'){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                                }elseif($emp_attendance->StatusAbsen == 'D'){
                                                                    $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                                }else{                                                           
                                                                    $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                                }                                                                 

                                                            endforeach;      
                                                    }
                                                      echo $absen;
                                                                                                              
                                                    ?>
                                                    <?php endforeach; ?> 

                                                </td>               
                                    </tr>

                                    
    
                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end jan -->

                            <!-- feb -->
                            <?php if(!empty($dateSl['Feb'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Feb'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>    
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">February</td>  
                                        <?php foreach ($v_employee['Feb'] as $v_result): ?>   
                                            <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }       
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end feb -->

                            <!-- mar -->
                            <?php if(!empty($dateSl['Mar'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Mar'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>      
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">March</td>  
                                        <?php foreach ($v_employee['Mar'] as $v_result): ?>   
                                            <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }     
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end March -->

                            <!-- apr -->
                            <?php if(!empty($dateSl['Apr'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Apr'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>    
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">April</td>  
                                        <?php foreach ($v_employee['Apr'] as $v_result): ?>   
                                            <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end apr -->

                            <!-- may -->
                            <?php if(!empty($dateSl['May'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['May'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>    
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">May</td>  
                                        <?php foreach ($v_employee['May'] as $v_result): ?>   
                                            <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end may -->

                             <!-- jun -->
                             <?php if(!empty($dateSl['Jun'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Jun'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>      
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">June</td>  
                                        <?php foreach ($v_employee['Jun'] as $v_result): ?>   
                                            <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }       
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end jun -->

                             <!-- jul -->
                             <?php if(!empty($dateSl['Jul'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Jul'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>          
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">July</td>  
                                        <?php foreach ($v_employee['Jul'] as $v_result): ?>   
                                            <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }     
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end jul -->

                             <!-- aug -->
                             <?php if(!empty($dateSl['Aug'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Aug'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>      
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">August</td>  
                                        <?php foreach ($v_employee['Aug'] as $v_result): ?>   
                                            <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end aug -->

                            <!-- sep -->
                            <?php if(!empty($dateSl['Sep'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Sep'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>         
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">September</td>  
                                        <?php foreach ($v_employee['Sep'] as $v_result): ?>   
                                            <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end sep -->

                             <!-- oct -->
                             <?php if(!empty($dateSl['Oct'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Oct'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>        
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">October</td>  
                                        <?php foreach ($v_employee['Oct'] as $v_result): ?>   
                                            <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end oct -->

                            <!-- nov -->
                            <?php if(!empty($dateSl['Nov'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Nov'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>         
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">November</td>  
                                        <?php foreach ($v_employee['Nov'] as $v_result): ?>   
                                            <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end nov -->

                             <!-- dec -->
                             <?php if(!empty($dateSl['Dec'])):?>
                            <thead>
                                <tr>
                                    <th style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">Date</th>                
                                    <?php foreach ($dateSl['Dec'] as $edate) : ?>  
                                        <th style="text-align: center; border: 1px solid black; font-size: 10px;"><?php echo $edate ?></th>
                                    <?php endforeach; ?>   
                                </tr>  
                            </thead>        
                            <tbody>
                            <?php foreach ($attendance_v2 as $v_employee): ?>
                               
                                <tr>  
                                    <td style="text-align: left; border: 1px solid black; font-size: 12px;" class="col-sm-3">December</td>  
                                        <?php foreach ($v_employee['Dec'] as $v_result): ?>   
                                            <td style="padding: 2px;text-align: center;font-size: 10px; border: 1px solid black;">
                                                <?php
                                                if(!empty($v_result)){
                                                    $cekin  = '';
                                                    $cekout = '';
                                                    $absen  = '';
                                                    foreach ($v_result as $emp_attendance):
                                                    
                                                        if($emp_attendance->StatusAbsen == 'C/In'){

                                                            $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                                            //set jam telat
                                                            if($cekin_tmp > '10:00:00'){
                                                                $cekin = '<font color="red">'.date("H:i",strtotime($emp_attendance->Time)).'</font>';
                                                            }else{
                                                                $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                                            }
                                                        }
                                                        if($emp_attendance->StatusAbsen == 'C/Out'){
                                                            $cekout = date('H:i',strtotime($emp_attendance->Time));
                                                        }

                                                        if ($emp_attendance->StatusAbsen == 'L') {
                                                            $absen = '<span style="padding:2px; 4px" class="label label-default std_p">L</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'C'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-info std_p">C</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'I'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-warning std_p">I</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'W'){
                                                            $absen = '<span style="padding:2px; 4px; font-size 1px;" class="label label-primary std_p">W</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'O'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-success std_p">O</span>';
                                                        }elseif($emp_attendance->StatusAbsen == ''){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-danger std_p">A</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'S'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">S</span>';
                                                        }elseif($emp_attendance->StatusAbsen == 'D'){
                                                            $absen = '<span style="padding:2px; 4px" class="label label-primary std_p">D</span>';
                                                        }else{                                                           
                                                            $absen = '<span style="font-size:7pt">'.$cekin.' '.$cekout.'</span>';
                                                        }    
                                                    
                                                    endforeach;      
                                                }
                                                    echo $absen;
                                                                                                            
                                                ?>
                                                <?php endforeach; ?> 
                                            </td>               
                                    </tr>

                                 <?php endforeach; ?>  
                            </tbody>
                            <?php endif;?>
                            <!-- end dec -->         
                </table>
            </div>
        </div>
    </body>
</html>