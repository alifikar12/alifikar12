
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<div class="row">
    <div class="col-sm-12"> 
        <div class="wrap-fpanel">
            <div class="panel panel-default" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>Add Level Jabatan</strong>
                    </div>
                </div>
                <div class="panel-body">

                    <form id="form" action="<?php echo base_url() ?>admin/level_jabatan/save_level_jabatan/<?php
                    if (!empty($level_jabatan->level_id)) {
                        echo $level_jabatan->level_id;
                    }
                    ?>" method="post" class="form-horizontal form-groups-bordered">
					
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Level Jabatan <span class="required">*</span></label>

                            <div class="col-sm-5">                            
                                <input type="text" name="level_name" value="<?php
                                if (!empty($level_jabatan->level_name)) {
                                    echo $level_jabatan->level_name;
                                }
                                ?>" class="form-control" placeholder="Enter Level Jabatan" />
                            </div>
						</div>	
						<div class="form-group">
							
                                <div class="col-sm-3">               
                                </div>
							
								<div class="col-sm-2">
									<button type="submit" id="sbtn" class="btn btn-primary"><?php echo $this->language->from_body()[1][12] ?></button>                            
								</div>
                        </div>
							
						
                    </form>
                </div>                 
            </div>                 
        </div>  
        <br/>

        <div class="row">
            <div class="col-sm-12" data-offset="0">                            
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Level Jabatan List</strong>
                        </div>
                    </div>

                    <!-- Table -->
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="col-sm-1">ID</th>
                                <th>Level Jabatan Name</th>                                
                                <th class="col-sm-2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                            <?php if (!empty($all_level_jabatan_info)): foreach ($all_level_jabatan_info as $v_category) : ?>
                                    <tr>
                                        <td><?php echo $v_category->level_id ?></td>
                                        <td><?php echo $v_category->level_name ?></td>                                        
                                        <td>
                                            <?php echo btn_edit('admin/level_jabatan/level_jabatan_list/' . $v_category->level_id); ?>  
                                            <?php echo btn_delete('admin/level_jabatan/delete_level_jabatan/' . $v_category->level_id.'/'. $v_category->level_name); ?>
                                        </td>

                                    </tr>
                                    <?php
                                endforeach;
                                ?>
                            <?php else : ?>
                            <td colspan="3">
                                <strong>There is no data to display</strong>
                            </td>
                        <?php endif; ?>
                        </tbody>
                    </table>          
                </div>
            </div>
        </div>
    </div>   
</div>
