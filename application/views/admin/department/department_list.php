
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<div class="row">
    <div class="col-sm-12" data-offset="0"> 

    <?php //foreach ($all_company_info as $row_company): ?>
        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading" style="background: #466472;color: #FFFFFF;">
                <div class="panel-title">
                    <strong>Department List</strong>
                </div>
            </div>
            <?php if (!empty($all_department_info)): foreach ($all_department_info as $akey => $v_department_info) : ?>                 
                <?php
                // echo '<pre>';
                // print_r($all_dept_info);
                // echo '</pre>';
                // die;
                /**
                 * $row_company->id == $all_dept_info->company_id 
                 */

                ?>
                    <?php if (!empty($v_department_info)): ?>
                        <div class="panel-heading" >
                            <div class="panel-title">
                                <strong><?php echo $this->db->get_where('company', ['id' => $all_dept_info[$akey]->company_id])->row_array()['name'] .' - '. $all_dept_info[$akey]->department_name ?>
                                    <div class="pull-right">
                                        <?php echo btn_edit('admin/department/add_department/' . $all_dept_info[$akey]->department_id); ?>  
                                        <a data-original-title="Delete" href="<?php echo base_url() ?>admin/department/delete_department/<?php echo $all_dept_info[$akey]->department_id; ?>" class="btn btn-danger btn-xs" title="" data-toggle="tooltip" data-placement="top" onclick="return confirm('You are about to delete This Department. All Designation Under This Department Will Be Deleted. Are you sure?');"><i class="fa fa-trash-o"></i> Delete</a>
                                    </div>
                                </strong>
                            </div>
                        </div>
                    
                        <?php //if($row_company == $all_dept_info->company_id): ?>
                        <!-- Table -->                    
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="col-sm-1">SL</th>
                                    <th>Designations</th>                                            
                                </tr>
                            </thead>
                            <tbody>                                                        
                                <?php foreach ($v_department_info as $key => $v_department) : ?>
                                    <tr>
                                        <td><?php echo $key + 1 ?></td>
                                        <td><?php echo $v_department->designations ?></td>                                                

                                    </tr>
                                    <?php
                                endforeach;
                                ?>
                            <?php endif; ?>                                    
                            </tbody>
                        </table> 
                    <?php //endif ?>

                    <?php
                    endforeach;
                    ?>

            <?php else : ?>
                <div class="panel-body">
                    <strong>There is no data to display</strong>
                </div>
            <?php endif; ?>
        </div>
        <?php //endforeach ?>

    </div>
</div>