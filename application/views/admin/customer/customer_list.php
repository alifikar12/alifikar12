
<?php echo message_box('success'); ?>
<?php echo message_box('error'); ?>
<div class="row">
    <div class="col-sm-12"> 
        <div class="wrap-fpanel">
            <div class="panel panel-default" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>Add Customer</strong>
                    </div>
                </div>
                <div class="panel-body">

                    <form id="form" action="<?php echo base_url() ?>admin/customer/save_customer/<?php
                    if (!empty($level_jabatan->ID_CUST)) {
                        echo $level_jabatan->ID_CUST;
                    }
                    ?>" method="post" class="form-horizontal form-groups-bordered">
					
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Nama <span class="required">*</span></label>

                            <div class="col-sm-5">                            
                                <input type="text" name="name" value="<?php
                                if (!empty($level_jabatan->NAME)) {
                                    echo $level_jabatan->NAME;
                                }
                                ?>" class="form-control" placeholder="Enter Name" required="true" />
                            </div>
						</div>	

                        
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Email <span class="required">*</span></label>

                            <div class="col-sm-5">                            
                                <input type="email" name="email" value="<?php
                                if (!empty($level_jabatan->EMAIL)) {
                                    echo $level_jabatan->EMAIL;
                                }
                                ?>" class="form-control" placeholder="Enter Email"  required="true" />
                            </div>
						</div>

                        
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">No Telp <span class="required">*</span></label>

                            <div class="col-sm-5">                            
                                <input type="text" name="no_telp" value="<?php
                                if (!empty($level_jabatan->NO_TELP)) {
                                    echo $level_jabatan->NO_TELP;
                                }
                                ?>" class="form-control" placeholder="Enter No Telp" required="true" />
                            </div>
						</div>

						<div class="form-group">							
                                <div class="col-sm-3">               
                                </div>
							
								<div class="col-sm-2">
									<button type="submit" id="sbtn" class="btn btn-primary"><?php echo $this->language->from_body()[1][12] ?></button>                            
								</div>
                        </div>
						
						
                    </form>
                </div>                 
            </div>                 
        </div>  
        <br/>

        <div class="row">
            <div class="col-sm-12" data-offset="0">                            
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Customer List</strong>
                        </div>
                    </div>
                    <div class="table-responsive">            
                    <!-- Table -->
                    <table class="table table-bordered table-hover" id="example">
                        <thead>
                            <tr>
                                <th class="col-sm-1">ID</th>
                                <th>Nama</th> 
                                <th>Email</th> 
                                <th>No Telp</th>      
                                <?php if($role == 'Administrator'):?>
                                    <th class="col-sm-2">Action</th>
                                <?php endif?>
                            
                            </tr>
                        </thead>
                        <tbody>
                           
                            <?php if (!empty($all_level_jabatan_info)): foreach ($all_level_jabatan_info as $v_category) : ?>
                                    <tr>
                                        <td><?php echo $v_category->ID_CUST ?></td>
                                        <td><?php echo $v_category->NAME ?></td>   
                                        <td><?php echo $v_category->EMAIL ?></td> 
                                        <td><?php echo $v_category->NO_TELP ?></td>      
                                        <?php if($role == 'Administrator'):?>
                                            <td>
                                                <?php echo btn_edit('admin/customer/customer_list/' . $v_category->ID_CUST); ?>  
                                                <?php echo btn_delete('admin/customer/delete_customer/' . $v_category->ID_CUST); ?>
                                            </td>
                                        <?php endif?>
                                    </tr>
                                    <?php
                                endforeach;
                                ?>
                            <?php else : ?>
                            <td colspan="3">
                                <strong>There is no data to display</strong>
                            </td>
                        <?php endif; ?>
                        </tbody>
                    </table>   
                    </div>       
                </div>
            </div>
        </div>
    </div>   
</div>

<link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />

<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script> 
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.colVis.min.js"></script>

<script type="text/javascript">  
    $(document).ready(function() {
        var table = $('#example').DataTable( {
            lengthChange: false,
            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
        } );
    
        table.buttons().container()
            .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
    } );
</script>
