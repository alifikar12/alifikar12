<?php
if (isset($user)) {
	$id = $user['user_id'];
	$inputFullnameValue = $user['first_name'];
	$inputRoleValue = $user['user_role_role_id'];
	$inputEmailValue = $user['user_email'];
	$inputDescValue = $user['user_description'];
} else {
	$inputFullnameValue = set_value('first_name');
	$inputRoleValue = set_value('user_role_role_id');
	$inputEmailValue = set_value('user_email');
	$inputDescValue = set_value('user_description');
}
?>

<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/kendo.default.min.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>asset/css/kendo.common.min.css" />
<script type="text/javascript" src="<?= base_url(); ?>asset/js/kendo.all.min.js"></script>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <strong>Tambah Admin</strong>
                </div>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <?= form_open_multipart(current_url()); ?>
                        <!-- Small cardes (Stat card) -->
                        <div class="row">
                            <div class="col-md-9">
                                <div class="card card-primary">
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <?= validation_errors(); ?>
                                        <div class="form-group mb-2">
                                            <label>Username <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                            <input name="user_name" type="text" class="form-control" value="" placeholder="username">
                                        </div> 
                                        <div class="form-group mb-2">
                                            <label>Email <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                            <input name="email" type="text" class="form-control" value="" placeholder="email">
                                        </div> 

                                        <div class="form-group mb-2">
                                            <label>Nama lengkap <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                            <input name="first_name" type="text" class="form-control" value="" placeholder="Nama">
                                        </div>

                                        <?php if (!isset($user)) { ?>
                                        <div class="form-group mb-2">
                                            <label>Password <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                            <input name="password" type="password" class="form-control" placeholder="Password">
                                        </div>            

                                        <div class="form-group mb-2">
                                            <label>Konfirmasi Password <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                            <input name="passconf" type="password" class="form-control" placeholder="Konfirmasi Password">
                                        </div>       
                                        <?php } ?>

                                        <div class="form-group mb-2">
                                            <label>Hak Akses <small data-toggle="tooltip" title="Wajib diisi">*</small></label>
                                            <select name="role" class="form-control form-select">
                                                <option value="">-Pilih Hak Akses-</option>
                                                <?php foreach ($roles as $row): ?> 
                                                    <option value="<?= $row['role_name']; ?>" <?= ($inputRoleValue == $row['role_id']) ? 'selected' : '' ?>><?= $row['role_name']; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <p class="text-muted">*) Kolom wajib diisi.</p>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-primary">
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <label >Foto</label>
                                        <a href="#" class="thumbnail">
                                            <?php if (isset($user) AND $user['user_image'] != NULL) { ?>
                                            <img src="<?= upload_url('admin/' . $user['user_image']) ?>" class="img-responsive avatar">
                                            <?php } else { ?>
                                            <img id="target" alt="Choose image to upload">
                                            <?php } ?>
                                        </a>
                                        <input type='file' id="user_image" name="user_image" class="mb-3">
                                        <br>
                                        <button type="submit" class="btn btn-block btn-success">Simpan</button>
                                        <a href="<?= site_url('admin/admin/admin_list'); ?>" class="btn btn-block btn-info">Batal</a>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>
                    <?= form_close(); ?>

                </div>
            </div>
        </div>
    </div>  

    <script>
        $("#treeview").kendoTreeView({
            checkboxes: {
                checkChildren: true,
                template: "<input type='checkbox' #= item.check# name='menu[]' value='#= item.value #'  />"
            },
            check: onCheck,
            
            dataSource: [
        <?php foreach ($result as $parent => $v_parent): ?>                   
            <?php if(is_array($v_parent)):?>
                <?php foreach ($v_parent as $parent_id => $v_child): ?>            
                {
                id: "", text: "<?= $parent; ?>",value: "<?php if(!empty ($parent_id)){ echo $parent_id;}?>", expanded: false, items: [                                                                                                   
                 <?php foreach ($v_child as $child => $v_sub_child) :?>         
                   <?php if(is_array($v_sub_child)):?> 
                    <?php foreach ($v_sub_child as  $sub_chld =>  $v_sub_chld): ?>
                   {
                        id: "", text: "<?= $child;?>",value: "<?php if(!empty ($sub_chld)){ echo $sub_chld;}?>",expanded: false, items: [
                            <?php foreach($v_sub_chld as $sub_chld_name => $sub_chld_id):?>
                            {
                              id: "", text: "<?= $sub_chld_name;?>",<?php if(!empty($roll[$sub_chld_id])){echo $roll[$sub_chld_id] ?'check: "checked",':'' ;} ?> value: "<?php if(!empty ($sub_chld_id)){ echo $sub_chld_id;}?>",  
                            },  
                            <?php endforeach;?>
                       ]     
                    },                        
                    <?php endforeach;?>   
                    <?php else:?>
                        {
                        id: "", text: "<?= $child;?>", <?php if(!is_array($v_sub_child)){if(!empty($roll[$v_sub_child])){echo $roll[$v_sub_child] ?'check: "checked",':'' ;}} ?> value: "<?php if(!empty ($v_sub_child)){ echo $v_sub_child;}?>",
                    },
                    <?php endif;?>
                   <?php endforeach;?>         
                ]
            },                        
            <?php endforeach;?>
            <?php else: ?>                
                  {
                        id: "", text: "<?= $parent?>", <?php if(!is_array($v_parent)){if(!empty($roll[$v_parent])){echo $roll[$v_parent] ?'check: "checked",':'' ;}} ?> value: "<?php if(!is_array($v_parent)) {echo $v_parent;}?>"
                    },                  
            <?php endif;?>
            <?php endforeach;?>                
            ]
        });
        

        // show checked node IDs on datasource change
        function onCheck() {
            var checkedNodes = [],
                treeView = $("#treeview").data("kendoTreeView"),
                message;

            checkedNodeIds(treeView.dataSource.view(), checkedNodes);

            $("#result").html(message);
        }
    </script>


    <script type="text/javascript">
        $(function () {
            $('form').submit(function () {
                $('#treeview :checkbox').each(function () {
                    if (this.indeterminate) {
                        this.checked = true;
                    }
                });
            })
        })
    </script>

