<?= message_box('success'); ?>
<?= message_box('error'); ?>

<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="wrap-fpanel">
            <div class="card-header">
                <h4 class="pull-left holiday-vertical">
                    <a href="<?= site_url('admin/admin/admin_add') ?>">
                        <i class="fa fa-plus"></i> Add Admin
                    </a>
                </h4>
                <!-- <a href="<?= site_url('admin/admin/admin_add') ?>" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah</a> -->
                <!-- <a href="<?= site_url('admin/admin/admin_role')?>" class="btn btn-sm btn-danger"><i class="fa fa-cogs"></i> Role</a> -->
            </div>         
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12" data-spy="scroll" data-offset="0">
        <div class="wrap-fpanel">
            
            <div class="panel panel-default">
                <!-- Default panel contents -->    
                <div class="panel-heading">
                    <div class="panel-title">
                        <strong>Admin List</strong>
                    </div>
                </div>
                <br />
                <!-- Table -->
                <table class="table table-bordered table-hover" id="dataTables-example">
                    <thead>
                        <tr class="active" >
                            <th>No</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Hak Akses</th>                                             
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $key = 1 ?>
                        <?php if (count($users)): foreach ($users as $row) : ?>

                                <tr>
                                    <td><?= $key ?></td>
                                    <td><?= $row->user_name ?></td>
                                    <td><?= $row->email ?></td>
                                    <td><?= $row->role ?></td>                                 
                                    <td>
                                        <!-- <a href="<?= site_url('manage/users/view/' . $row->user_id) ?>" 
                                            class="btn btn-xs btn-info" 
                                            data-toggle="tooltip" title="Lihat"><i class="fa fa-eye"></i>
                                        </a> -->

                                        <?php if ($this->session->userdata('role') == 'Administrator') { ?>
                                            <a href="<?= site_url('admin/admin/admin_rpw/' . $row->user_id) ?>" 
                                                class="btn btn-xs btn-warning"><i class="fa fa-lock" 
                                                data-toggle="tooltip" 
                                                title="Reset Password"></i>
                                            </a>                                       
                                                                                    
                                            <button type="button" 
                                                    onclick="getId(<?= $row->user_id ?>)" 
                                                    class="btn btn-danger btn-xs" 
                                                    data-toggle="modal" 
                                                    data-target="#delModal">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        <?php } ?>
                                        
                                    </td>
                                </tr>
                                <?php
                                $key++;
                            endforeach;
                            ?>
                        <?php else : ?>
                        <td colspan="3">
                            <strong>There is no Record for display!</strong>
                        </td>
                    <?php endif; ?>
                    </tbody>
                </table>          
            </div>
        </div>
    </div>
</div>

<!-- modal delete data -->
<div class="modal fade" id="delModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Konfirmasi Hapus</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="<?= site_url('admin/admin/admin_delete') ?>" method="POST">
                <div class="modal-body">
                    <p>Apakah anda akan menghapus data ini?</p>
                    <input type="hidden" name="id" id="userID" readonly>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modal delete -->

<script>
    function getId(id) {
		$('#userID').val(id)
	}
</script>

