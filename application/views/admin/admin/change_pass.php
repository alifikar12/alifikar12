<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <strong>Reset Password</strong>
                </div>
            </div>
            <div class="panel-body">
                <div class="panel-body">
                    <?php echo form_open(current_url()); ?>
                        <!-- Small cardes (Stat card) -->
                        <div class="row">
                            <div class="col-md-9">
                                <div class="card card-primary">
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <?php echo form_open(current_url()); ?>
                                        <?php echo validation_errors(); ?>
                                        <div class="form-group mb-2">
                                            <?php if ($this->uri->segment(3) == 'cpw') { ?>
                                            <label >Password lama *</label>
                                            <input type="password" name="user_current_password" class="form-control" placeholder="Password lama">
                                            <?php } ?>
                                        </div>
                                        <div class="form-group mb-2">
                                            <label >Password baru*</label>
                                            <input type="password" name="user_password" class="form-control" placeholder="Password baru">
                                            <?php if ($this->uri->segment(3) == 'cpw') { ?>
                                            <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('uid'); ?>" >
                                            <?php } else { ?>
                                            <input type="hidden" name="user_id" value="<?php echo $user['user_id'] ?>" >
                                            <?php } ?>
                                        </div>
                                        <div class="form-group mb-2">
                                            <label > Konfirmasi password baru*</label>
                                            <input type="password" name="passconf" class="form-control" placeholder="Konfirmasi password baru" >
                                        </div>
                                        <p class="text-muted">*) Kolom wajib diisi.</p>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="card card-primary">
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-block btn-success">Simpan</button>
                                        <a href="<?php echo site_url('admin/admin/admin_list'); ?>" class="btn btn-block btn-info">Batal</a>

                                    </div>
                                    <!-- /.card-body -->
                                </div>
                            </div>
                        </div>
                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
    </div>  
