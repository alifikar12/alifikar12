
<div class="col-md-12">
    <div class="row">
        <div class="col-sm-12">
            <div class="wrap-fpanel">
                <div class="panel panel-info" data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <strong>Add New Permit Application</strong>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-offset-1">
                            <form id="form" action="<?php echo base_url() ?>admin/permit_application/save_permit_application_by_admin" method="post"  enctype="multipart/form-data" class="form-horizontal">
                                <div class="panel_controls">

                                <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Employee Name<span class="required"> *</span></label>
                                        <div class="col-sm-5">
                                            <select id="employment_id" name="employment_id" class="form-control" required >
                                                <option value="" >Select Employee Name...</option>
                                                <?php foreach ($all_emplyee_info as $v_employee) : ?>
                                                    <option value="<?php echo $v_employee->employment_id ?>" data-employee_id="<?php echo $v_employee->employee_id; ?>"> 
                                                        <?php echo $v_employee->first_name.' '.$v_employee->last_name ?></option>
                                                <?php endforeach; ?>                                                
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">																		
										<div class="col-sm-5">
											<input type="hidden" class="form-control" name="employee_id" id="employeeid" value="" readonly="true">
										</div>									
									</div>
									

                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Permit Category<span class="required"> *</span></label>

                                        <div class="col-sm-5">
                                            <select id="permit_category" name="permit_category_id" class="form-control" required >
                                                <option value="" >Select Permit Category...</option>
                                                <?php foreach ($all_permit_category as $v_category) : ?>
                                                    <option value="<?php echo $v_category->permit_category_id ?>">
                                                        <?php echo $v_category->category ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
									
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Permit Date <span class="required"> *</span></label>

                                        <div class="col-sm-5">
                                        <div class="input-group">
                                                <input type="text" name="permit_date"  required class="form-control datepicker" value="" data-format="dd-mm-yyyy">
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="entypo-calendar"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Start Time <span class="required"> *</span></label>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="time" name="permit_start_time"  required  class="form-control"  value="" >   
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="entypo-time"></i></a>
                                                </div>                                             
                                            </div>
                                        </div>
                                    </div>  

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">End Time <span class="required"> *</span></label>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <input type="time" name="permit_end_time"   required class="form-control" value="" >
                                                <div class="input-group-addon">
                                                    <a href="#"><i class="entypo-time"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 

                                    <div class="form-group">
                                        <label for="field-1" class="col-sm-3 control-label">Reason</label>

                                        <div class="col-sm-5">
                                            <textarea id="present" name="reason" class="form-control" rows="6"></textarea>
                                        </div>
                                    </div>   
                                                       

                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-5">
                                            <button type="submit" id="sbtn" name="sbtn" value="1" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
    <script>
    //populate data from dropdown menu
    $('#employment_id').change(function(){
        //get the selected option's data-id value using jquery `.data()`
        var employee_id =  $(':selected',this).data('employee_id'); 
        
        //populate data.
        $('#employeeid').val(employee_id);
    }); 
    </script>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
<!-- <script>
jQuery(document).ready(function() {
    jQuery("#permit_category").change(function() {
        if (jQuery(this).val() === '3'){ 
            jQuery('input[name=saldo_cuti]').show(); 
			jQuery('label[id="lbl_saldo_cuti"]').show(); 	
        } else {
            jQuery('input[name=saldo_cuti]').hide(); 
			jQuery('label[id="lbl_saldo_cuti"]').hide();
        }
    });
});
</script> -->

