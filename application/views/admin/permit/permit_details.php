<div class="col-md-12">
    <div class="wrap-fpanel">
        <div class="panel panel-default">
            <!-- Default panel contents -->

            <div class="panel-heading">
                <div class="panel-title">                 
                    <strong>Permit Application Details</strong><span class="pull-right"><a style="cursor: pointer"onclick="history.go(-1)" class="view-all-front">Go Back</a></span>
                </div>                    
            </div>    
            <form method="post" action="<?php echo base_url() ?>admin/permit_application/proses_permit_by_dl/<?php echo $permit_info->permit_id ?>">
                  <div class="panel-body form-horizontal">
                  <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Permit ID : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $permit_info->permit_id; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Name : </strong></label>
                        </div>                    
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $permit_info->first_name . ' ' . $permit_info->last_name; ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Permit Time : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php
                                if ($permit_info->permit_start_time == $permit_info->permit_end_time) {
                                    echo date('h:i A', strtotime($permit_info->permit_start_time));
                                } else {
                                    echo date('h:i A', strtotime($permit_info->permit_start_time)) . '<span class="text-danger"> To </span>' . date('h:i A', strtotime($permit_info->permit_end_time));
                                }
                                ?></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Permit Type :</strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static text-justify"><?php echo $permit_info->category; ?></p>
                        </div>                  
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Apply On : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><span class="text-danger"><?php echo date('d M y', strtotime($permit_info->permit_date)); ?></span></p>
                        </div>                                              
                    </div>
                    <div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Reason : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $permit_info->reason; ?></p>
                        </div>                                              
                    </div>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Status : </strong></label>
                        </div>
                        <div class="col-sm-8">						
                            <p class="form-control-static">
								<?php if($permit_info->permit_status=='cancel'){?>
									<span class="label label-danger"><?php echo $permit_info->permit_status; ?></span>
								<?php } ?>
								<?php if($permit_info->permit_status=='pending'){?>
									<span class="label label-warning"><?php echo $permit_info->permit_status; ?></span>
								<?php } ?>
								<?php if($permit_info->permit_status=='fully approved'){?>
									<span class="label label-success"><?php echo $permit_info->permit_status; ?></span>
								<?php } ?>
								<?php if($permit_info->permit_status=='partial approved'){?>
									<span class="label label-info"><?php echo $permit_info->permit_status; ?></span>
								<?php } ?>
							</p>
                        </div>                                              
                    </div>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Created By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $permit_info->first_name . ' ' . $permit_info->last_name; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($permit_info->permit_date)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php if($permit_info->approve!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved (1) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $permit_info->approve; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($permit_info->date_approve)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>

                    <?php if($permit_info->approve2!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Approved (2) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $permit_info->approve2; ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($permit_info->date_approve2)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($permit_info->acknowledge!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Acknowledge By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static">                            
                                <?php 
                                    if(strpos(strtoupper($permit_info->acknowledge), 'NOVA') !== false || strpos(strtoupper($permit_info->acknowledge), 'SUCI') !== false){
                                        echo 'HR Manager';   
                                    }else{
                                        echo $permit_info->acknowledge; 
                                    }                                                                                       
                                ?>
							pada tanggal <strong><?php echo date('d M Y', strtotime($permit_info->date_acknowledge)); ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
					<?php if($permit_info->unapprove!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved (1) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $permit_info->unapprove; ?>
							pada tanggal <strong><?php echo $permit_info->date_unapprove; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>

                    <?php if($permit_info->unapprove2!=''){?>
					<div class="col-md-12">
                        <div class="col-sm-4 text-right">
                            <label class="control-label"><strong>Unapproved (2) By : </strong></label>
                        </div>
                        <div class="col-sm-8">
                            <p class="form-control-static"><?php echo $permit_info->unapprove2; ?>
							pada tanggal <strong><?php echo $permit_info->date_unapprove2; ?></strong>
							</p>							 
                        </div>                                              
                    </div>
					<?php } ?>
					
                    <div class="col-md-12">
                        <div class="col-sm-4">
                            <!-- Hidden Input ---->
                            <input type="hidden" name="employee_id" value="<?php echo $permit_info->employee_id; ?>">
                            <input type="hidden" name="leave_category_id" value="<?php echo $permit_info->leave_category_id; ?>">
                            <input type="hidden" name="leave_start_date" value="<?php echo $permit_info->leave_start_date; ?>">
                            <input type="hidden" name="leave_end_date" value="<?php echo $permit_info->leave_end_date; ?>">
							<input type="hidden" name="leave_type" value="<?php echo $permit_info->leave_type; ?>">
                        </div>
						
						<!--post parameter email notif--> 		
                        <input type="hidden" name="document_number" value="<?php echo $permit_info->application_list_id; ?>" readonly="true">

                        <?php if(!empty($permit_info->direct_leader_email_2)){ ?>
      
                            <input type="hidden" name="email_to_next_approval" value="<?php echo $permit_info->direct_leader_email_2; ?>" readonly="true">                                        
                            <input type="hidden" name="link" value="<?php echo base_url() ?>login?link=employee/dashboard/view_permit_inquiry_2/<?php echo $permit_info->application_list_id; ?>" readonly="true">
                            <input type="hidden" name="receipt_name" value="<?php echo $permit_info->direct_leader_name_2; ?>" readonly="true">
                            <input type="hidden" name="sender_name" value="<?php echo $permit_info->direct_leader_name; ?>" readonly="true">
                            <input type="hidden" name="jk_sender" value="<?php echo $permit_info->direct_leader_gender; ?>" readonly="true">
                            <input type="hidden" name="jk_receipt" value="<?php echo $permit_info->direct_leader_gender_2; ?>" readonly="true">
						
                        <?php }else{ ?>
                            <input type="hidden" name="email_to_next_approval" value="<?php echo $permit_info->email_hr_pic; ?>" required="true">
                            <input type="hidden" name="link" value="<?php echo base_url() ?>login?link=employee/dashboard/view_permit_inquiry_2/<?php echo $permit_info->application_list_id; ?>" required="true">
                            <input type="hidden" name="receipt_name" value="<?php echo $permit_info->nama_pic_hr; ?>" required="true">
                            <input type="hidden" name="sender_name" value="<?php echo $permit_info->direct_leader_name; ?>" required="true">
                            <input type="hidden" name="jk_sender" value="<?php echo $permit_info->direct_leader_gender; ?>" required="true">
                            <input type="hidden" name="jk_receipt" value="<?php echo $permit_info->gender_pic_hr; ?>" required="true">

                        <?php } ?>
						<input type="hidden" name="email_requester" value="<?php echo $permit_info->email; ?>" readonly="true"> 
						<input type="hidden" name="requester_name" value="<?php echo $permit_info->first_name ." ". $permit_info->last_name; ?>" readonly="true">
						
						<?php if($permit_info->permit_status != 'cancel' && $permit_info->permit_status != 'fully approved' && $permit_info->permit_status != 'partial approved' ){ ?>
                        <div class="col-sm-4 margin">
							<input type="submit" name="approve_btn" value="Approve" class="btn btn-primary" />
							<input type="submit" name="unapprove_btn" value="Reject" class="btn btn-danger" />	
                        </div>  
						<?php } ?>	
                    </div>

                </div>                
        </div>
    </div>
</div>






