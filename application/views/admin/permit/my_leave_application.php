
<?php echo message_box('success'); ?>
<div class="row">
    <div class="col-sm-9">
        <h4 class="pull-left holiday-vertical"><?php echo anchor('admin/leave_application/create_leave_application', '<i class="fa fa-plus"></i> Apply New Leave Application'); ?></h4>        
    </div>
    <div class="col-sm-12 wrap-fpanel" data-offset="0">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <span>
                        <strong>List of All My Leave</strong>
                    </span>
                </div>
            </div>
            <!-- Table -->
            <div class="table-responsive">
            <table class="table table-bordered table-hover" id="dataTables-example">
                <thead>
                    <tr>
						<th>No</th>
                        <th>ID</th>
						<th>NIK</th>
                        <th>Full Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Leave Type</th>
                        <th>Tgl Apply</th>
                        <th>Status</th>
						<th>Tgl Disetujui Atasan</th>
						<th>Nama Atasan</th>
                        <th>Action</th>                        

                    </tr>
                </thead>
                <tbody>
                    <?php 
						$i=1;
						if (!empty($all_application_info)):
						foreach ($all_application_info as $key => $v_application): 
					?>
                            <tr>
								<td><?php echo $i; ?></td>
								<td><?php echo $v_application->application_list_id; ?></td>
                                <td><?php echo $v_application->employment_id; ?></td>
                                <td><?php echo $v_application->first_name . ' ' . $v_application->last_name; ?></td>
                                <td><?php echo $v_application->leave_start_date; ?></td>
                                <td><?php echo $v_application->leave_end_date; ?></td>
                                <td><?php echo $v_application->category; ?></td>
                                <td><?php echo date('d M,y', strtotime($v_application->application_date)) ?></td>
                                <td><?php                            
									if ($v_application->leave_status == 'pending') {
                                            echo '<span class="label label-warning">'.$v_application->leave_status.'</span>';
                                        } elseif ($v_application->leave_status == 'fully approved') {
                                            echo '<span class="label label-success">'.$v_application->leave_status.'</span>';
                                        } elseif ($v_application->leave_status == 'partial approved') {
                                            echo '<span class="label label-info">'.$v_application->leave_status.'</span>';
                                        } else {
                                            echo '<span class="label label-danger">cancel</span>';
                                        }										
                                    ?>
								</td>
								<td><?php echo date('d M,y', strtotime($v_application->date_approve1)) ?></td>
								<td><?php echo $v_application->approve1 ?></td>
                                <td><?php echo btn_view('admin/leave_application/view_my_application/' . $v_application->application_list_id) ?></td>   
                                                             
                            </tr>                
                        <?php
							$i++;
							endforeach; 
						?>
                    <?php endif; ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

