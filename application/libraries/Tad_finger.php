<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : Muhammad Surya Ikhsanudin 
 *  License    : Protected 
 *  Email      : mutofiyah@gmail.com 
 *   
 *  Dilarang merubah, mengganti dan mendistribusikan 
 *  ulang tanpa sepengetahuan Author 
 *  ======================================= 
 */  
require_once APPPATH."/third_party/tad/lib/TADFactory.php";
require_once APPPATH."/third_party/tad/lib/TAD.php";
require_once APPPATH."/third_party/tad/lib/TADResponse.php";
require_once APPPATH."/third_party/tad/lib/Providers/TADSoap.php";
require_once APPPATH."/third_party/tad/lib/Providers/TADZKLib.php";
require_once APPPATH."/third_party/tad/lib/Exceptions/ConnectionError.php";
require_once APPPATH."/third_party/tad/lib/Exceptions/FilterArgumentError.php";
require_once APPPATH."/third_party/tad/lib/Exceptions/UnrecognizedArgument.php";
require_once APPPATH."/third_party/tad/lib/Exceptions/UnrecognizedCommand.php";

use TADPHP\TADFactory;
use TADPHP\TAD; 

class Tad_finger extends TADFactory { 
    public function __construct() { 
        parent::__construct(); 
    } 
}
