<?php
 /**Define variable SMTP */ 
 define('SMTP_HOST', 'hrms.oneunivrs.id');
 define('USERNAME', 'noreply@hrms.oneunivrs.id');
 define('PASSWORD', '~w.IdafX,EwW');
 define('FROM', 'noreply@hrms.oneunivrs.id');

class Smtp_server {

    public function itpkg($to, $subject, $body, $cc=NULL, $bcc=NULL){
              
        $mail = new PHPMailer;
        ##optional security
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

         //$debug = $mail->SMTPDebug = 2;                               	// Enable verbose debug output

        $mail->isSMTP();                                      	    	// Set mailer to use SMTP
        $mail->Host         = SMTP_HOST;  						        // Specify main and backup SMTP servers
        $mail->SMTPAuth     = true;                               		// Enable SMTP authentication
        $mail->Username     = USERNAME;            	    	            // SMTP username
        $mail->Password     = PASSWORD;                          	    // SMTP password
        $mail->SMTPSecure   = 'ssl';                            		// Enable TLS encryption, `ssl` also accepted
        $mail->Port         = 465;                                   	// TCP port to connect to

        $mail->setFrom(FROM, 'no-reply');
        $mail->addAddress($to);     								    // Add a recipient
        $mail->addReplyTo(FROM, 'no-reply');

        if($cc!=NULL){
            $mail->addCC($cc);        
        }
        if($bcc!=NULL){
            $mail->addBCC($bcc); 
        }
        
        $mail->isHTML(true);                                  			// Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $body;

        if($mail->send()){
            return TRUE;
        }else{
            return FALSE;
        }
        
    }
}
