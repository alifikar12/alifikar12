<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : Muhammad Surya Ikhsanudin 
 *  License    : Protected 
 *  Email      : mutofiyah@gmail.com 
 *   
 *  Dilarang merubah, mengganti dan mendistribusikan 
 *  ulang tanpa sepengetahuan Author 
 *  ======================================= 
 */  
// require_once APPPATH."/third_party/PHPExcel.php"; 

require_once APPPATH.'/third_party/TADFactory.php';
require_once APPPATH.'/third_party/TAD.php';
require_once APPPATH.'/third_party/TADResponse.php';
require_once APPPATH.'/third_party/Providers/TADSoap.php';
require_once APPPATH.'/third_party/Providers/TADZKLib.php';
require_once APPPATH.'/third_party/Exceptions/ConnectionError.php';
require_once APPPATH.'/third_party/Exceptions/FilterArgumentError.php';
require_once APPPATH.'/third_party/Exceptions/UnrecognizedArgument.php';
require_once APPPATH.'/third_party/Exceptions/UnrecognizedCommand.php';
 
class TadPHP extends TAD { 
    public function __construct() { 
        parent::__construct(); 
		/*
		//panggil instance class example
		$this->CI = &get_instance();
        $this->CI->load->library('email');
		*/
    } 
}