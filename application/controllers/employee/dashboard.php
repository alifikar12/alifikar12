<?php

class Dashboard extends Employee_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->model('emp_model');
        $this->load->model('global_model');
        $this->load->model('mailbox_model');
		$this->load->model('application_model');
        $this->load->model('permit_model');
        $this->load->helper('ckeditor');
        $this->data['ckeditor'] = array(
            'id' => 'ck_editor',
            'path' => 'asset/js/ckeditor',
            'config' => array(
                'toolbar' => "Full",
                'width' => "100%",
                'height' => "350px"
            )
        );
        $this->load->model('email_model');

         //Load email library
         $this->load->library('email');

    }

    

    public function index() {
        //DEBUG
        // $this->output->enable_profiler(TRUE);
        $data['menu'] = array("index" => 1);
        $data['title'] = "Employee Panel";
        $employee_id = $this->session->userdata('employee_id');
        $employment_id = $this->session->userdata('employment_id');
        $yymm = date('Y-m');
        $start_date = date('Y-m-d');
        $end_date   = date('Y-m-d');

        //Total Attendance
        //$this->emp_model->_table_name   = "tbl_attendance"; //table name
        //$this->emp_model->_order_by     = "employee_id";
        $data['total_attendance']       = count($this->total_attendace_in_month($employee_id));

        
        
        // get working days holiday
        //$holidays = count($this->global_model->get_holidays()); //tbl working Days Holiday
       
        
        // get public holiday
        $public_holiday = count($this->global_model->get_public_holidays($yymm));
        //echo $weekends;
        //die;

        // set function employee point
        $data_point['saldo_point_info'] = $this->emp_model->get_saldo_point($employee_id);


        
        // //auto update saldo cuti jika expirednya sama dengan today dan expired tdk sama dengan tgl topup
        // //sisanya akan hangus setelah 3 bulan reload
        // $data_cuti['saldo_cuti_info'] = $this->emp_model->get_saldo_cuti($employee_id); 

        // $expired        = $data_cuti['saldo_cuti_info']->expired;
        // $date_reload    = $data_cuti['saldo_cuti_info']->date_reload;
        // $saldo_cuti     = $data_cuti['saldo_cuti_info']->saldo_cuti;

        // $this->auto_reload_saldo_cuti($expired, $date_reload, $saldo_cuti);


        // //kurangi sisa cuti tahun sblmnya jika sudah lebih 3 bulan dari tanggal reload
        // $data_after_reload['saldo_cuti_info'] = $this->emp_model->get_saldo_cuti($employee_id);

        // $sisa_cuti_tahun_lalu = $data_after_reload['saldo_cuti_info']->sisa_cuti_thn_lalu;
        // $date_reload2         = $data_after_reload['saldo_cuti_info']->date_reload;
        // if($sisa_cuti_tahun_lalu != 0){
        //     $this->auto_cleareance_saldo_cuti_lebih_dari_3_bln($date_reload2, $sisa_cuti_tahun_lalu);
        // }    

        //get employee details by employee id
        $data['employee_details'] = $this->emp_model->all_emplyee_info($employee_id);

        // get total days in a month
        $month  = date('m');
        $year   = date('Y');
        $days   = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        //get total weekend in a month
        $start_date_in_month = $year.'-'.$month.'-01';
        $end_date_in_month   = $year.'-'.$month.'-'.$days;
        $weekends = $this->get_total_weekend($start_date_in_month, $end_date_in_month);
        
        // total attend days in a month without public holiday and working days
        $data['total_days'] = $days - $weekends - $public_holiday;
        //echo $public_holiday; 


        //leave applied         
        $data['total_leave_applied'] = count($this->emp_model->get_approved_leave($employee_id));

        //award received
        $this->emp_model->_table_name = "tbl_employee_award"; //table name
        $this->emp_model->_order_by = "employee_id";
        $data['total_award_received'] = count($this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE));
		
		//tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        //get all notice by flag       
        $data['notice_info'] = $this->emp_model->get_all_notice(TRUE);

        //get all upcomming events
        $data['event_info'] = $this->emp_model->get_all_events();
        // $this->emp_model->_table_name = "tbl_holiday"; //table name
        // $this->emp_model->_where = "YEAR(start_date) = YEAR(NOW())"; 
        // $this->emp_model->_order_by = "start_date DESC";
        // $data['event_info'] = $this->emp_model->get();

        $data['total_event'] = count($this->emp_model->get_all_events());

        // get upcoming birthday
        $this->admin_model->_table_name = "tbl_employee"; //table name
        $this->admin_model->_order_by = "employee_id"; // order by
        $data['employee'] = $this->admin_model->get_by(array('status' => 1)); // get resutl 
        // get recent email          
        $data['get_inbox_message'] = $this->mailbox_model->get_inbox_message($data['employee_details']->email, $flag = NULL, TRUE);
        $data['subview'] = $this->load->view('employee/main_content', $data, TRUE);
		//$data['subview'] = $this->load->view('employee/main_content', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }


    public function get_total_weekend($start_date_in_month, $end_date_in_month){
        $current    = strtotime( $start_date_in_month );
        $last       = strtotime( $end_date_in_month );
        $step       = '+1 day';
        $format     = 'Y-m-d';
       
        $weekends = $this->global_model->get_holidays(); //tbl working Days Holiday
        $array_weekend = array();
        while( $current <= $last ) {
    
            $sdate   = date( $format, $current );
            $current = strtotime( $step, $current );

            $day_name = date('l', strtotime($sdate));
            if (!empty($weekends)) {   
                
                foreach ($weekends as $v_weekends) {

                    if ($v_weekends->day == $day_name) {
                        $array_weekend[] = $v_weekends->day;
                        
                    }     
                }
            }  
        }
        
        $total_weekend = count($array_weekend);
        return $total_weekend;
    }


    public function total_attendace_in_month($employee_id, $flag = NULL) {
        $month  = date('m');
        $year   = date('Y');

        if ($month >= 1 && $month <= 9) { // if i<=9 concate with Mysql.becuase on Mysql query fast in two digit like 01.
            $start_date = $year . "-" . '0' . $month . '-' . '01';
            $end_date   = $year . "-" . '0' . $month . '-' . '31';
        } else {
            $start_date = $year . "-" . $month . '-' . '01';
            $end_date   = $year . "-" . $month . '-' . '31';
        }
        if (!empty($flag)) { // if flag is not empty that means get pulic holiday
            $get_public_holiday = $this->emp_model->get_public_holiday($start_date, $end_date);
            if (!empty($get_public_holiday)) { // if not empty the public holiday
                foreach ($get_public_holiday as $v_holiday) {
                    if ($v_holiday->start_date == $v_holiday->end_date) { // if start date and end date is equal return one data
                        $total_holiday[] = $v_holiday->start_date;
                    } else { // if start date and end date not equan return all date
                        for ($j = $v_holiday->start_date; $j <= $v_holiday->end_date; $j++) {
                            $total_holiday[] = $j;
                        }
                    }
                }
                return $total_holiday;
            }
        } else {
            $get_total_attendance = $this->emp_model->get_total_attendace_by_date($start_date, $end_date, $employee_id); // get all attendace by start date and in date 
            return $get_total_attendance;
        }
    }

    public function redeem_point() {
		//DEBUG
		// $this->output->enable_profiler(TRUE);		
        $data['menu'] = array("redeem_point" => 1);
        $data['title'] = "Employee Panel";

        //get leave applied with category name
        $employee_id = $this->session->userdata('employee_id');
        $data['all_redeem_by_user'] = $this->emp_model->get_trans_point_applied_by_user($employee_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/my_redeem_point', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function view_my_redeem($id) {
        // $this->output->enable_profiler(TRUE);
        $data['title'] = "Application List";
        $data['application_info'] = $this->emp_model->get_emp_redeem_info($id);

        $data['product_list_info'] = $this->emp_model->get_emp_redeem_info_rows($id);
        // set view status by id
        // $where = array('application_list_id' => $id);
        // $updata['view_status'] = '1';
        // $this->emp_model->set_action($where, $updata, 'tbl_application_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/my_redeem_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    } 

    public function leave_application() {
		//DEBUG
		//$this->output->enable_profiler(TRUE);		
        $data['menu'] = array("leave_application" => 1);
        $data['title'] = "Employee Panel";

        //get leave applied with category name
        $employee_id = $this->session->userdata('employee_id');
        $data['all_leave_applications'] = $this->emp_model->get_leave_applied_by_user($employee_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/my_leave_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function permit_application() {
		//DEBUG
		//$this->output->enable_profiler(TRUE);		
        $data['menu'] = array("permit_application" => 1);
        $data['title'] = "Employee Panel";

        //get leave applied with category name
        $employee_id = $this->session->userdata('employee_id');
        $data['all_permit_applications'] = $this->emp_model->get_permit_applied_by_user($employee_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/my_permit_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function perdin_application() {
		//DEBUG
		//$this->output->enable_profiler(TRUE);		
        $data['menu'] = array("perdin_application" => 1);
        $data['title'] = "Perdin Form";

        //get leave applied with category name
        $employee_id = $this->session->userdata('employee_id');
        $data['all_permit_applications'] = $this->emp_model->get_perdin_applied_by_user($employee_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/my_perdin_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function overtime_application() {		
        $data['menu'] = array("overtime_application" => 1);
        $data['title'] = "Employee Panel";

        //get overtime applied with category name
        $employee_id = $this->session->userdata('employee_id');
        $data['all_overtime_applications'] = $this->emp_model->get_overtime_applied_by_user($employee_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/my_overtime_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function apply_overtime_application() {
        $data['title']  = "New Overtime Application";

        //get kelengkapan category for dropdown
        $this->emp_model->_table_name   = "tbl_perlengkapan_lembur"; // table name
        $this->emp_model->_order_by     = "perlengkapan_id"; // $id
        $data['all_perlengkapan_lembur']    = $this->emp_model->get(); // get result

        $employee_id                = $this->session->userdata('employee_id');
        $data['all_emplyee_info']   = $this->emp_model->all_emplyee_info($employee_id); // get result
        $data['no_overtime']        = $this->emp_model->get_no_overtime();

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview']            = $this->load->view('employee/apply_new_overtime_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }


    public function save_overtime_application() {

        $this->emp_model->_table_name = "tbl_overtime_list"; // table name
        $this->emp_model->_primary_key = "overtime_id"; // $id
		
        //receive form input by post
        $data['employee_id'] 		    = $this->session->userdata('employee_id');
        $data['perlengkapan_id']        = $this->input->post('perlengkapan_id');
        $data['perlengkapan_lainnya']   = $this->input->post('perlengkapan_lainnya');
        $data['overtime_date'] 	        = $this->input->post('overtime_date');
        $data['overtime_start_time'] 	= $this->input->post('overtime_start_time');
        $data['overtime_end_time'] 	    = $this->input->post('overtime_end_time');
        $data['tujuan'] 			    = $this->input->post('tujuan');
		$data['employment_id'] 		    = $this->session->userdata('employment_id');
        $data['overtime_status'] 		= 'pending';
        
        //save data in database
        $this->emp_model->save($data);

        //send email notif to approval
        $email_to 			= $this->input->post('email_dl');
        $document_number 	= $this->input->post('document_number');
        $link 				= $this->input->post('link');
        $receipt_name 		= $this->input->post('receipt_name');
        $sender_name 		= $this->input->post('sender_name');
				
        $this->email_model->email_notif_overtime($email_to, $document_number, $link, $receipt_name, $sender_name, $overtime_status='pending');
        

        // messages for user
        $type       = "success";
        $message    = "Overtime Application Successfully Submitted !";
        set_message($type, $message);
        redirect('employee/dashboard/overtime_application');
    }

    public function apply_permit_application() {
        $data['title']  = "New Permit Application";

        //get leave category for dropdown
        $data['permit_category']    = $this->permit_model->get_permit_by_status('active'); // get result

        $employee_id                = $this->session->userdata('employee_id');
        $data['all_emplyee_info']   = $this->emp_model->all_emplyee_info($employee_id); // get result
        $data['no_permit']          = $this->emp_model->get_no_permit();

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview']            = $this->load->view('employee/apply_new_permit_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function apply_perdin_application() {
        $data['title']  = "New Perdin Application";

        //get leave category for dropdown
        $this->emp_model->_table_name   = "tbl_perdin_category"; // table name
        $this->emp_model->_order_by     = "perdin_category_id"; // $id
        $data['all_perdin_category']    = $this->emp_model->get(); // get result

        $employee_id                = $this->session->userdata('employee_id');
        $data['all_emplyee_info']   = $this->emp_model->all_emplyee_info($employee_id); // get result
        $data['no_perdin']          = $this->emp_model->get_no_perdin();

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview']            = $this->load->view('employee/apply_new_perdin_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function save_permit_application() {
        //DEBUG
		//$this->output->enable_profiler(TRUE);	
        $this->emp_model->_table_name = "tbl_permit_list"; // table name
        $this->emp_model->_primary_key = "permit_id"; // $id
		
        //receive form input by post
        $data['employee_id'] 		= $this->session->userdata('employee_id');
        $data['permit_category_id'] = $this->input->post('permit_category_id');
        $data['permit_start_time'] 	= $this->input->post('permit_start_time');
        $data['permit_end_time'] 	= $this->input->post('permit_end_time');
        $data['reason'] 			= $this->input->post('reason');
		$data['employment_id'] 		= $this->session->userdata('employment_id');
        $data['permit_status'] 		= 'pending';
        // $data['permit_date']        = date('Y-m-d');
        $data['permit_date'] 	= $this->input->post('permit_date');
        
        //save data in database
        $this->emp_model->save($data);

        
        //send email notif to approval
        $email_to 			= $this->input->post('email_dl');
        $document_number 	= $this->input->post('document_number');
        $link 				= $this->input->post('link');
        $receipt_name 		= $this->input->post('receipt_name');
        $sender_name 		= $this->input->post('sender_name');
				
        $this->email_model->email_notif_permit($email_to, $document_number, $link, $receipt_name, $sender_name, $permit_status='pending');
        

        // messages for user
        $type       = "success";
        $message    = "Leave Application Successfully Submitted !";
        set_message($type, $message);
        redirect('employee/dashboard/permit_application');
    }


    public function save_perdin_application() {
        //DEBUG
		//$this->output->enable_profiler(TRUE);	
        $this->emp_model->_table_name = "tbl_perdin_list"; // table name
        $this->emp_model->_primary_key = "perdin_id"; // $id
		
        //receive form input by post
        $data['employee_id'] 		= $this->session->userdata('employee_id');
        $data['perdin_category_id'] = $this->input->post('perdin_category_id');
        $data['perdin_start_date'] 	= $this->input->post('perdin_start_date');
        $data['perdin_end_date'] 	= $this->input->post('perdin_end_date');
        $data['reason'] 			= $this->input->post('reason');
		$data['employment_id'] 		= $this->session->userdata('employment_id');
        $data['perdin_status'] 		= 'pending';
        $data['submit_date']        = date('Y-m-d');
        
        //save data in database
        $this->emp_model->save($data);
        
        //send email notif to approval
        $email_to 			= $this->input->post('email_dl');
        $doc_number 	    = $this->input->post('document_number');
        $link 				= $this->input->post('link');
        $receipt_name 		= $this->input->post('receipt_name');
        $sender_name 		= $this->input->post('sender_name');
				
        $this->email_model->email_notif_perdin($email_to, $doc_number, $link, $receipt_name, $sender_name, $perdin_status='pending');
        

        // messages for user
        $type       = "success";
        $message    = "Application Successfully Submitted !";
        set_message($type, $message);
        redirect('employee/dashboard/perdin_application');
    }


    public function proses_overtime_by_dl($id) {
        
        if (isset($_POST['approve_btn'])) {
			$data['overtime_status']    = 'partial approved';
			$data['approve']            = $this->session->userdata('user_name');
            $data['date_approve']       = date('Y-m-d');
            
            //send email notif to approval
            $email_to 			= $this->input->post('email_to_next_approval');
            $document_number 	= $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name');
            $sender_name 		= $this->input->post('sender_name');

			
        }else if(isset($_POST['unapprove_btn'])){
			$data['overtime_status']   = 'cancel';
			$data['unapprove']       = $this->session->userdata('user_name');
			$data['date_unapprove']  = date('Y-m-d');
			
			 //send email notif to approval
            $email_to 			= $this->input->post('email_to_requester');
            $document_number 	= $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name_requester');
            $sender_name 		= $this->input->post('sender_name');
			
		}
        $where = array('overtime_id' => $id);
        $this->application_model->set_action($where, $data, 'tbl_overtime_list');
		
		$overtime_info	    = $this->application_model->get_overtime_info($document_number);
		$overtime_status	= $overtime_info->overtime_status;
		
		$this->email_model->email_notif_overtime($email_to, $document_number, $link, $receipt_name, $sender_name, $overtime_status);
		
        $type       = "success";
        $message    = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('employee/dashboard/approve_overtime_application'); //redirect page
    }


    public function proses_permit_by_dl($id) {
        //DEBUG
		//$this->output->enable_profiler(TRUE);
        if (isset($_POST['approve_btn'])) {
			$data['permit_status']  = 'partial approved';
			$data['approve']        = $this->session->userdata('user_name');
            $data['date_approve']   = date('Y-m-d');
            
            //send email notif to approval
            $email_to 			= $this->input->post('email_to_next_approval');
            $document_number 	= $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name');
            $sender_name 		= $this->input->post('sender_name');

			
        }else if(isset($_POST['unapprove_btn'])){
			$data['permit_status']   = 'cancel';
			$data['unapprove']       = $this->session->userdata('user_name');
			$data['date_unapprove']  = date('Y-m-d');
			
			 //send email notif to approval
            $email_to 			= $this->input->post('email_to_requester');
            $document_number 	= $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name_requester');
            $sender_name 		= $this->input->post('sender_name');
			
		}
        $where = array('permit_id' => $id);
        $this->application_model->set_action($where, $data, 'tbl_permit_list');
		
		$permit_info	    = $this->application_model->get_permit_info($document_number);
		$permit_status 		= $permit_info->permit_status;
		
		$this->email_model->email_notif_permit($email_to, $document_number, $link, $receipt_name, $sender_name, $permit_status);
		
        $type       = "success";
        $message    = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('employee/dashboard/approve_permit_application'); //redirect page
    }

    public function proses_permit_by_dl_2($id) {
        //DEBUG
		//$this->output->enable_profiler(TRUE);
        if (isset($_POST['approve_btn'])) {
			$data['permit_status']      = 'partial approved 2';
			$data['approve2']        = $this->session->userdata('user_name');
            $data['date_approve2']   = date('Y-m-d');
            
            //send email notif to approval
            $email_to 			= $this->input->post('email_to_next_approval');
            $document_number 	= $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name');
            $sender_name 		= $this->input->post('sender_name');

			
        }else if(isset($_POST['unapprove_btn'])){
			$data['permit_status']    = 'cancel';
			$data['unapprove2']       = $this->session->userdata('user_name');
			$data['date_unapprove2']  = date('Y-m-d');
			
			 //send email notif to approval
            $email_to 			= $this->input->post('email_to_requester');
            $document_number 	= $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name_requester');
            $sender_name 		= $this->input->post('sender_name');
			
		}
        $where = array('permit_id' => $id);
        $this->application_model->set_action($where, $data, 'tbl_permit_list');
		
		$permit_info	    = $this->application_model->get_permit_info($document_number);
		$permit_status 		= $permit_info->permit_status;
		
		$this->email_model->email_notif_permit($email_to, $document_number, $link, $receipt_name, $sender_name, $permit_status);
		
        $type       = "success";
        $message    = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('employee/dashboard/approve_permit_application'); //redirect page
    }

    public function proses_perdin_by_dl_2($id) {
        //DEBUG
		//$this->output->enable_profiler(TRUE);
        if (isset($_POST['approve_btn'])) {
			$data['perdin_status']  = 'partial approved 2';
			$data['approve2']        = $this->session->userdata('user_name');
            $data['date_approve2']   = date('Y-m-d');
            
            //send email notif to approval
            $email_to 			= $this->input->post('email_to_next_approval');
            $doc_number 	    = $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name');
            $sender_name 		= $this->input->post('sender_name');

			
        }else if(isset($_POST['unapprove_btn'])){
			$data['perdin_status']   = 'cancel';
			$data['unapprove2']       = $this->session->userdata('user_name');
			$data['date_unapprove2']  = date('Y-m-d');
			
			 //send email notif to approval
            $email_to 			= $this->input->post('email_to_requester');
            $doc_number 	    = $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name_requester');
            $sender_name 		= $this->input->post('sender_name');
			
		}
        $where = array('perdin_id' => $id);
        $this->application_model->set_action($where, $data, 'tbl_perdin_list');
		
		$perdin_status 		= $this->application_model->get_perdin_info($doc_number)->perdin_status;
		
		$this->email_model->email_notif_perdin($email_to, $doc_number, $link, $receipt_name, $sender_name, $perdin_status);
		
        $type       = "success";
        $message    = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('employee/dashboard/approve_perdin_application_2'); //redirect page
    }

    public function proses_perdin_by_dl($id) {
        //DEBUG
		//$this->output->enable_profiler(TRUE);
        if (isset($_POST['approve_btn'])) {
			$data['perdin_status']  = 'partial approved';
			$data['approve1']        = $this->session->userdata('user_name');
            $data['date_approve1']   = date('Y-m-d');
            
            //send email notif to approval
            $email_to 			= $this->input->post('email_to_next_approval');
            $doc_number 	    = $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name');
            $sender_name 		= $this->input->post('sender_name');

			
        }else if(isset($_POST['unapprove_btn'])){
			$data['perdin_status']   = 'cancel';
			$data['unapprove1']       = $this->session->userdata('user_name');
			$data['date_unapprove1']  = date('Y-m-d');
			
			 //send email notif to approval
            $email_to 			= $this->input->post('email_to_requester');
            $doc_number 	    = $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name_requester');
            $sender_name 		= $this->input->post('sender_name');
			
		}
        $where = array('perdin_id' => $id);
        $this->application_model->set_action($where, $data, 'tbl_perdin_list');
		
		$perdin_status 		= $this->application_model->get_perdin_info($doc_number)->perdin_status;
		
		$this->email_model->email_notif_perdin($email_to, $doc_number, $link, $receipt_name, $sender_name, $perdin_status);
		
        $type       = "success";
        $message    = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('employee/dashboard/approve_perdin_application'); //redirect page
    }



    public function approve_permit_application() {			
        $data['menu']   = array("approve_permit_application" => 1);
        $data['title']  = "Approve(1) Permit Application";

        //get leave applied with category name
        $employment_id = $this->session->userdata('employment_id');
        $data['all_permit_applications'] = $this->emp_model->get_all_permit_applied_for_approved($employment_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);
        
        $data['subview'] = $this->load->view('employee/approve_permit_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function approve_permit_application_2() {			
        $data['menu']   = array("approve_permit_application_2" => 1);
        $data['title']  = "Approve(2) Permit Application";

        //get leave applied with category name
        $employment_id = $this->session->userdata('employment_id');
        $data['all_permit_applications'] = $this->emp_model->get_all_permit_applied_for_approved_2($employment_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);
        
        $data['subview'] = $this->load->view('employee/approve_permit_application_2', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function approve_perdin_application() {			
        $data['menu']   = array("approve_perdin_application" => 1);
        $data['title']  = "Employee Panel";

        //get leave applied with category name
        $employment_id = $this->session->userdata('employment_id');
        $data['all_perdin_applications'] = $this->emp_model->get_all_perdin_applied_for_approved($employment_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);
        
        $data['subview'] = $this->load->view('employee/approve_perdin_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function approve_perdin_application_2() {			
        $data['menu']   = array("approve_perdin_application_2" => 1);
        $data['title']  = "Approve Perdin Application 2";

        //get leave applied with category name
        $employment_id = $this->session->userdata('employment_id');
        $data['all_perdin_applications'] = $this->emp_model->get_all_perdin_applied_for_approved_2($employment_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);
        
        $data['subview'] = $this->load->view('employee/approve_perdin_application_2', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }


    public function approve_overtime_application() {			
        $data['menu']   = array("approve_overtime_application" => 1);
        $data['title']  = "Employee Panel";

        //get overtime applied with category name
        $employment_id = $this->session->userdata('employment_id');
        $data['all_overtime_applications'] = $this->emp_model->get_all_overtime_applied_for_approved($employment_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);
        
        $data['subview'] = $this->load->view('employee/approve_overtime_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }


    public function leave_application_inquiry() {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
		
        $data['menu'] = array("leave_inquiry" => 1);
        $data['title'] = "Employee Panel";

        //get leave applied with category name
        $employment_id = $this->session->userdata('employment_id');
        $data['all_leave_applications'] = $this->emp_model->get_all_leave_applied_for_approved($employment_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);
        
        $data['subview'] = $this->load->view('employee/approve_leave_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }


    public function leave_application_inquiry_2() {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
        $data['menu'] = array("leave_inquiry_2" => 1);
        $data['title'] = "Employee Panel";

        //get leave applied with category name
        $employment_id = $this->session->userdata('employment_id');
        $data['all_leave_applications'] = $this->emp_model->get_all_leave_applied_for_approved_2($employment_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);
        
        $data['subview'] = $this->load->view('employee/approve_leave_application_2', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }


    public function view_my_application($id) {
        //DEBUG
		// $this->output->enable_profiler(TRUE);
        $data['title'] = "Application List";
        $data['application_info'] = $this->emp_model->get_emp_leave_info($id);
        // set view status by id
        $where = array('application_list_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_application_list');

        /**get data params email */
        $employee_id = $this->session->userdata('employee_id');
        $data['all_emplyee_info'] = $this->emp_model->all_emplyee_info($employee_id); // get result
        $data['no_leave'] = $this->emp_model->get_no_leave();

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/my_application_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    } 

    

    public function view_my_permit($id) {
        $data['title'] = "Permit List";
        $data['permit_info'] = $this->emp_model->get_emp_permit_info($id);
        // set view status by id
        $where = array('permit_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_permit_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/my_permit_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    } 

    public function view_my_perdin($id) {
        $data['title'] = "Perdin Detail";
        $data['perdin_info'] = $this->emp_model->get_emp_perdin_info($id);
        // set view status by id
        $where = array('perdin_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_perdin_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/my_perdin_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    } 

    public function view_my_overtime($id) {
        $data['title'] = "Overtime List";
        $data['overtime_info'] = $this->emp_model->get_emp_overtime_info($id);
        // set view status by id
        $where = array('overtime_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_overtime_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/my_overtime_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    } 

    public function view_permit_inquiry($id) {
        $data['title'] = "Permit Inquiry Detail(1)";
        $data['permit_info'] = $this->emp_model->get_emp_permit_info($id);
        // set view status by id
        $where = array('permit_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_permit_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/inquiry_permit_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function view_permit_inquiry_2($id) {
        $data['title'] = "Permit Inquiry Detail(2)";
        $data['permit_info'] = $this->emp_model->get_emp_permit_info($id);
        // set view status by id
        $where = array('permit_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_permit_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/inquiry_permit_details_2', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function view_perdin_inquiry($id) {
        $data['title'] = "Perdin List";
        $data['perdin_info'] = $this->emp_model->get_emp_perdin_info($id);
        // set view status by id
        $where = array('perdin_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_perdin_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/inquiry_perdin_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function view_perdin_inquiry_2($id) {
        $data['title'] = "Perdin List";
        $data['perdin_info'] = $this->emp_model->get_emp_perdin_info($id);
        // set view status by id
        $where = array('perdin_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_perdin_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/inquiry_perdin_details_2', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function view_overtime_inquiry($id) {
        $data['title'] = "Overtime List";
        $data['overtime_info'] = $this->emp_model->get_emp_overtime_info($id);
        // set view status by id
        $where = array('overtime_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_overtime_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/inquiry_overtime_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

   
	
	public function view_application_inquiry($id) {
        $data['title'] = "Application List";
        $data['application_info'] = $this->emp_model->get_emp_leave_info($id);
        // set view status by id
        $where = array('application_list_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_application_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/inquiry_application_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }


    public function view_application_inquiry_2($id) {
        $data['title'] = "Application List";
        $data['application_info'] = $this->emp_model->get_emp_leave_info_2($id);
        // set view status by id
        $where = array('application_list_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_application_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);
        $data['subview'] = $this->load->view('employee/inquiry_application_details_2', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
        
    }
    

    public function auto_reload_saldo_cuti($expired, $date_reload, $saldo_cuti){
        $now  = date('Y-m-d');
        $employee_id = $this->session->userdata('employee_id');
        if($expired == $now && $date_reload != $expired){
            // UPDATE tbl_saldo_cuti SET expired=DATE_ADD(expired,INTERVAL 1 YEAR) WHERE `employment_id` =  '2-160038';
            $this->db->where('employee_id', $employee_id);
            $this->db->set('saldo_cuti', 'saldo_cuti+12', FALSE);
            $this->db->set('date_reload', $now);
            $this->db->set('sisa_cuti_thn_lalu', intval($saldo_cuti));
            $this->db->set('expired','DATE_ADD(expired,INTERVAL 1 YEAR)',FALSE);
            $this->db->update('tbl_saldo_cuti');

            // $where = array('employment_id' => $employment_id);
            // $update = array('saldo_cuti' => 'saldo_cuti + 12', FALSE);
            // $update['saldo_cuti']        = 'saldo_cuti+12';
            // $update['date_topup']   = $now;
            // $this->emp_model->set_action($where, $update, 'tbl_saldo_cuti');
        }

    }

    public function auto_cleareance_saldo_cuti_lebih_dari_3_bln($date_reload2, $sisa_cuti_tahun_lalu){
        // $date_reload = "2007-03-24";
        $now    = date('Y-m-d');
        // $now    = "2019-06-29";

        $diff   = abs(strtotime($now) - strtotime($date_reload2));

        $years  = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

        // printf("%d years, %d months, %d days\n", $years, $months, $days);
        $employee_id = $this->session->userdata('employment_id');
        if($sisa_cuti_tahun_lalu != 0){
            if($years >= 0 && $months >= 3 && $days >= 0){
                $this->db->where('employee_id', $employee_id);
                $this->db->set('saldo_cuti', 'saldo_cuti-'.$sisa_cuti_tahun_lalu, FALSE);
                $this->db->set('sisa_cuti_thn_lalu', 0);
                $this->db->update('tbl_saldo_cuti');
            }
        }
    }

	public function apply_leave_application() {
        //DEBUG
        // $this->output->enable_profiler(TRUE);
        $data['title'] = "New Leave Application";

        //get leave category for dropdown
        $this->emp_model->_table_name = "tbl_leave_category"; // table name
        $this->emp_model->_order_by = "leave_category_id"; // $id
        $data['all_leave_category'] = $this->emp_model->get(); // get result

        $employee_id = $this->session->userdata('employee_id');
        $data['all_emplyee_info'] = $this->emp_model->all_emplyee_info($employee_id); // get result
        $data['no_leave'] = $this->emp_model->get_no_leave();

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $this->emp_model->_table_name = "backdate_cuti"; //table name
        $this->emp_model->_order_by = "id";
        $data['backdate_cuti'] = $this->emp_model->get_by(array('id' => 1), TRUE);


        $data['subview'] = $this->load->view('employee/apply_new_leave_application', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);

    }

    public function apply_redeem_point() {
        //DEBUG
        //$this->output->enable_profiler(TRUE);
        $data['title'] = "New Redeem Point";

        //set numbering document
        // $this->emp_model->_table_name = "tbl_numbering"; // table name
        // $this->emp_model->_order_by = "numbering_id"; // $id
        $data['tbl_numbering'] = $this->emp_model->get_data_numbering('redeem_point'); // get result

        $prefix      = $data['tbl_numbering']->prefix;
        $next_number = $data['tbl_numbering']->next_number;
        $length      = $data['tbl_numbering']->length;

        $data['doc_number']         = sprintf('%0'.$length.'d', $next_number);
        $data['doc_number_result']  = $prefix.$data['doc_number'];

        $data['next_number'] = $next_number;

        // get last saldo point
        $data['saldo_point'] = $this->emp_model->get_saldo_point($this->session->userdata('employee_id'))->saldo_point;

        //get leave category for dropdown
        $this->emp_model->_table_name = "tbl_leave_category"; // table name
        $this->emp_model->_order_by = "leave_category_id"; // $id
        $data['all_leave_category'] = $this->emp_model->get(); // get result

         // get product
         $this->emp_model->_table_name = "tbl_product"; //table name
         $this->emp_model->_order_by = "product_name";
         $data['all_product'] = $this->emp_model->get_by(array('active' => 1));

        //  get email from general setting
        $this->emp_model->_table_name = "tbl_gsettings"; //table name
        $this->emp_model->_order_by = "id_gsettings";
        $data['gsetting_info'] = $this->emp_model->get();

        $employee_id = $this->session->userdata('employee_id');
        $data['all_emplyee_info'] = $this->emp_model->all_emplyee_info($employee_id); // get result
       
        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/apply_new_redeem_point', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function submit_redeem_point() {
        $this->emp_model->_table_name = "tbl_transaksi_point"; // table name
        $this->emp_model->_primary_key = "transaksi_point_id"; // $id
		
        $data_trans_redeem = json_decode($this->input->post('dataArrProdList'));
        // print_r($data_trans_redeem[0][0]);
        // print_r($this->input->post('dataArrProdList'));
        // die;
        $doc_number    = $this->input->post('doc_number');
        $this->emp_model->_table_name = "tbl_transaksi_point"; //table name
        $this->emp_model->_order_by = "doc_number";
        if($this->emp_model->get_by(array('doc_number'=> $doc_number), TRUE)){
            $cek_existing_doc_number = $this->emp_model->get_by(array('doc_number'=> $doc_number), TRUE)->doc_number;
            // print_r($cek_existing_doc_number);
            // die;
            if($cek_existing_doc_number == $doc_number){
                $type = "error";
                $message = "Nomor Dokumen ".$this->input->post('doc_number')." sudah ada !";
                set_message($type, $message);
                redirect('employee/dashboard/apply_redeem_point');
            }
        }

        for($i=0; $i<count($data_trans_redeem); $i++){
            //receive form input by post
            $data['employee_id'] 		= $this->session->userdata('employee_id');
            $data['employment_id'] 		= $this->session->userdata('employment_id');
            $data['doc_number']         = $doc_number;
            $data['product_id'] 	    = $data_trans_redeem[$i][0];
            $data['product_name'] 	    = $data_trans_redeem[$i][1];
            $data['qty'] 	            = $data_trans_redeem[$i][2];
            $data['price'] 	            = $data_trans_redeem[$i][3];
            $data['subtotal']           = $data_trans_redeem[$i][4];
            
            $data['status'] 		= 'pending';
            
            //save data in database
            $this->emp_model->save($data);
               
        }

        $next_number = $this->input->post('next_number');
        if($next_number!=0){
            $set_next_number = array('next_number' => $next_number+1);
            $where = array('doc_type' => 'redeem_point');
            $this->emp_model->set_action($where, $set_next_number, 'tbl_numbering');
        }

        // // update saldo employee point
        // $saldo_point     = $this->input->post('saldo_point');
        // $ttl_redeem      = $this->input->post('ttl_redeem');
        // $set_saldo_point = array('saldo_point' => $saldo_point-$ttl_redeem);
        // $where = array('employment_id' => $this->session->userdata('employment_id'));
        // $this->emp_model->set_action($where, $set_saldo_point, 'tbl_employee_point');
        
        //send email notif to hrd

        //get data form tbl_approval_redeem 
        $this->emp_model->_table_name = "tbl_approval_redeem"; //table name
        $this->emp_model->_order_by   = "approval_redeem_id";
        $data['approval_redeem'] = $this->emp_model->get_by(array('approval_redeem_id' => 1,), TRUE);

        $email_to 			= $data['approval_redeem']->approval_email;
        $document_number 	= $this->input->post('doc_number');
        $link 				= $this->input->post('link');
        $receipt_name 		= $data['approval_redeem']->approval_name;
        $sender_name 		= $this->input->post('sender_name');
        $jk_approval        = $data['approval_redeem']->approval_gender;
        $jk_sender          = $this->input->post('jk_sender');
		
        //set template email
        // $d['doc_number']     = $this->input->post('doc_number');
        // $d['link']           = $this->input->post('link');
        // $d['receipt_name']   = $data['approval_redeem']->approval_name;
        // $d['jk_sender']      = $this->input->post('jk_sender');
        // $d['jk_approval']    = $data['approval_redeem']->approval_gender;
        // $d['status']         = 'pending';
       

        //$template_message = $this->load->view('email/redeem_point', $d, TRUE);
        ////Send mail, config ada di directory config/email.php
        //$this->email->
        //$this->email->from('postmaster@mg.pmki.co.id', 'postmaster@mg.pmki.co.id');
        //$this->email->to($email_to);
        //$this->email->subject('Notifikasi Pengajuan Redeem Point');
        //$this->email->message($template_message);
		
        $this->email_model->email_notif_redeem_point_to_hr($email_to, $document_number, $link, $receipt_name, $sender_name, $status='pending', $jk_approval, $jk_sender);


        // messages for user
        $type = "success";
        $message = "Redeem Point Successfully Submitted !";
        set_message($type, $message);
        redirect('employee/dashboard/redeem_point');
    }


    public function view_redeem_point_inquiry($id) {
        $data['title'] = "Redeem List";
        $data['application_info'] = $this->emp_model->get_emp_leave_info($id);
        // set view status by id
        $where = array('application_list_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_application_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/inquiry_application_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function resend_notif(){
         //send email notif to approval
         $email_to 			= $this->input->post('email_dl');
         $doc_number 	    = $this->input->post('doc_number');
         $link 				= $this->input->post('link');
         $receipt_name 		= $this->input->post('receipt_name');
         $sender_name 		= $this->input->post('sender_name');
         $jk_sender          = $this->input->post('jk_sender');
         $jk_receipt         = $this->input->post('jk_receipt');
         
         $this->email_model->email_notif_leave($email_to, $doc_number, $link, $receipt_name, $sender_name, $leave_status='pending', $jk_sender, $jk_receipt);
       
         // messages for user
         $data = array('type'       => "success",
                        'message'   => "Email notif successfully send !"
                      );
       
         echo json_encode($data);
    }

	public function save_leave_application() {
        $this->emp_model->_table_name = "tbl_application_list"; // table name
        $this->emp_model->_primary_key = "application_list_id"; // $id
		
        //receive form input by post
        $data['employee_id'] 		= $this->session->userdata('employee_id');
        $data['leave_category_id'] 	= $this->input->post('leave_category_id');
        $data['leave_start_date'] 	= $this->input->post('leave_start_date');
        $data['leave_end_date'] 	= $this->input->post('leave_end_date');
        $data['reason'] 			= $this->input->post('reason');
		$data['employment_id'] 		= $this->session->userdata('employment_id');
        $data['leave_status'] 		= 'pending';
        $data['application_date']   = date('Y-m-d H:i:s');
        
        //save data in database
        $this->emp_model->save($data);

        // //send email notif to approval
        // $email_to 			= $this->input->post('email_dl');
        // $doc_number 	    = $this->input->post('document_number');
        // $link 				= $this->input->post('link');
        // $receipt_name 		= $this->input->post('receipt_name');
        // $sender_name 		= $this->input->post('sender_name');
        // $jk_sender          = $this->input->post('jk_sender');
        // $jk_receipt         = $this->input->post('jk_receipt');
		
        // $this->email_model->email_notif_leave($email_to, $doc_number, $link, $receipt_name, $sender_name, $leave_status='pending', $jk_sender, $jk_receipt);

        /**set new template email */
        $data['email_to']	    = $this->input->post('email_dl');
        $data['doc_number']     = $this->input->post('document_number');
        $data['link']		    = $this->input->post('link');
        $data['receipt_name']	= $this->input->post('receipt_name');
        $data['sender_name'] 	= $this->input->post('sender_name');
        $data['jk_sender']      = $this->input->post('jk_sender');
        $data['jk_receipt']     = $this->input->post('jk_receipt');
        $data['subject']        = 'Notifikasi Pengajuan Cuti';

        $template_leave_approval   = $this->load->view('email/notif_leave_approval', $data, TRUE);   
        $notif_leave_approval = $this->smtp_server->itpkg($data['email_to'], $data['subject'], $template_leave_approval);     
        /**end set net template email */

        // messages for user
        $type = "success";
        $message = "Leave Application Successfully Submitted !";
        set_message($type, $message);
        redirect('employee/dashboard/leave_application');

    }

   
    
	public function proses_leave_by_dl($id) {
        //DEBUG
		//$this->output->enable_profiler(TRUE);
        if (isset($_POST['approve_btn'])) {
			$data['leave_status']   = 'partial approved';
			$data['approve1']       = $this->session->userdata('user_name');
            $data['date_approve1']  = date('Y-m-d');
            
            //send email notif to approval
            $email_to 			= $this->input->post('email_to_next_approval');
            $doc_number 	    = $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name');
            $sender_name 		= $this->input->post('sender_name');
            $jk_sender 		    = $this->input->post('jk_sender');
			$jk_receipt         = $this->input->post('jk_receipt');
            $requester_name     = $this->input->post('requester_name');
			
        }else if(isset($_POST['unapprove_btn'])){
			$data['leave_status']       = 'cancel';
			$data['unapprove1']         = $this->session->userdata('user_name');
			$data['date_unapprove1']    = date('Y-m-d');
			
			//  //send email notif to approval
            // $email_to 			= $this->input->post('requester_mail');
            // $doc_number 	        = $this->input->post('document_number');
            // $link 				= $this->input->post('link');
            // $receipt_name 		= $this->input->post('receipt_name');
            // $sender_name 		= $this->input->post('sender_name');
            // $jk_sender 		    = $this->input->post('jk_sender');
			// $jk_receipt          = $this->input->post('jk_receipt');
            // $requester_name      = $this->input->post('requester_name');
			
		}

        // $where = array('application_list_id' => $id);
        // $this->application_model->set_action($where, $data, 'tbl_application_list');
		// $application_info	= $this->application_model->get_application_info($doc_number);
		// $leave_status 		= $application_info->leave_status;
        // $this->email_model->email_notif_leave($email_to, $doc_number, $link, $receipt_name, $sender_name, $leave_status, $jk_sender, $jk_receipt, $requester_name);
		

        /**set new template email */
        $params['email_to'] 		= $this->input->post('requester_mail');
        $params['doc_number'] 	    = $this->input->post('document_number');
        $params['link'] 			= $this->input->post('link');
        $params['receipt_name'] 	= $this->input->post('receipt_name');
        $params['sender_name'] 		= $this->input->post('sender_name');
        $params['jk_sender'] 		= $this->input->post('jk_sender');
        $params['jk_receipt']       = $this->input->post('jk_receipt');
        $params['requester_name']   = $this->input->post('requester_name');
        $params['subject']          = 'Notifikasi Pengajuan Cuti';

        $where = array('application_list_id' => $id);
        $this->application_model->set_action($where, $data, 'tbl_application_list');
		$application_info	    = $this->application_model->get_application_info($params['doc_number']);
		$data['leave_status']	= $application_info->leave_status;

        $template_leave_approval    = $this->load->view('email/notif_leave_approval', $params, TRUE);   
        $notif_leave_approval       = $this->smtp_server->itpkg($data['email_to'], $data['subject'], $template_leave_approval);     
        /**end set net template email */


        $type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('employee/dashboard/leave_application_inquiry'); //redirect page
    }


    //action for approve & unapprove direct leader 2
    public function proses_leave_by_dl_2($id) {
        //DEBUG
		//$this->output->enable_profiler(TRUE);
        if (isset($_POST['approve_btn'])) {
			$data['leave_status']   = 'partial approved 2';
			$data['approve2']       = $this->session->userdata('user_name');
            $data['date_approve2']  = date('Y-m-d');
            
            //send email notif to approval
            $email_to 			= $this->input->post('email_to_next_approval');
            $doc_number 	    = $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name');
            $sender_name 		= $this->input->post('sender_name');
            $jk_sender 		    = $this->input->post('jk_sender');
			$jk_receipt         = $this->input->post('jk_receipt');
            $requester_name     = $this->input->post('requester_name');
			
        }else if(isset($_POST['unapprove_btn'])){
			$data['leave_status']       = 'cancel';
			$data['unapprove2']         = $this->session->userdata('user_name');
			$data['date_unapprove2']    = date('Y-m-d');
			
			//  //send email notif to approval
            // $email_to 			= $this->input->post('requester_mail');
            // $doc_number 	    = $this->input->post('document_number');
            // $link 				= $this->input->post('link');
            // $receipt_name 		= $this->input->post('requester_name');
            // $sender_name 		= $this->input->post('sender_name');
			// $jk_sender 		    = $this->input->post('jk_sender');
			// $jk_receipt         = $this->input->post('jk_receipt');
            // $requester_name     = $this->input->post('requester_name');            
		}

        // $where = array('application_list_id' => $id);
        // $this->application_model->set_action($where, $data, 'tbl_application_list');
		// $application_info	= $this->application_model->get_application_info($doc_number);
		// $leave_status 		= $application_info->leave_status;
        // $this->email_model->email_notif_leave($email_to, $doc_number, $link, $receipt_name, $sender_name, $leave_status, $jk_sender, $jk_receipt, $requester_name);


        /**set new template email */
        $params['email_to'] 		= $this->input->post('requester_mail');
        $params['doc_number'] 	    = $this->input->post('document_number');
        $params['link'] 			= $this->input->post('link');
        $params['receipt_name'] 	= $this->input->post('receipt_name');
        $params['sender_name'] 		= $this->input->post('sender_name');
        $params['jk_sender'] 		= $this->input->post('jk_sender');
        $params['jk_receipt']       = $this->input->post('jk_receipt');
        $params['requester_name']   = $this->input->post('requester_name');
        $params['subject']          = 'Notifikasi Pengajuan Cuti';

        $where = array('application_list_id' => $id);
        $this->application_model->set_action($where, $data, 'tbl_application_list');
		$application_info	    = $this->application_model->get_application_info($params['doc_number']);
		$data['leave_status']	= $application_info->leave_status;

        $template_leave_approval    = $this->load->view('email/notif_leave_approval', $params, TRUE);   
        $notif_leave_approval       = $this->smtp_server->itpkg($data['email_to'], $data['subject'], $template_leave_approval);     
        /**end set net template email */

        $type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('employee/dashboard/leave_application_inquiry_2'); //redirect page
    }
	
	//update aplication leave
	public function set_action($id) {
		// $this->output->enable_profiler(TRUE);
        $data['application_status'] = $this->input->post('application_status', TRUE);
		$data['approve_by_dl'] = $this->session->userdata('employment_id');
		
        if ($data['application_status'] == 'Approve') {
            $atdnc_data = $this->application_model->array_from_post(array('employee_id', 'leave_category_id'));
            $leave_start_date = $this->input->post('leave_start_date', TRUE);
            $leave_end_date = $this->input->post('leave_end_date', TRUE);
            if ($leave_start_date == $leave_end_date) {
                $this->admin_model->_table_name = 'tbl_attendance';
                $this->admin_model->_order_by = 'attendance_id';
                $check_leave_date = $this->admin_model->get_by(array('employee_id' => $atdnc_data['employee_id'], 'date' => $leave_end_date), FALSE);
                if (empty($check_leave_date)) {
                    $atdnc_data['date'] = $leave_start_date;
                    $atdnc_data['attendance_status'] = '3';
                    $this->admin_model->_table_name = 'tbl_attendance';
                    $this->admin_model->_primary_key = "attendance_id";
                    $this->admin_model->save($atdnc_data);
                }
            } else {
                for ($l = $leave_start_date; $l <= $leave_end_date; $l++) {
                    $this->admin_model->_table_name = 'tbl_attendance';
                    $this->admin_model->_order_by = 'attendance_id';
                    $check_leave_date = $this->admin_model->get_by(array('employee_id' => $atdnc_data['employee_id'], 'date' => $l), FALSE);
                    if (empty($check_leave_date)) {
                        $atdnc_data['date'] = $l;
                        $atdnc_data['attendance_status'] = '3';
                        $this->admin_model->_table_name = 'tbl_attendance';
                        $this->admin_model->_primary_key = "attendance_id";
                        $this->admin_model->save($atdnc_data);
                    }
                }
            }
        }
		
        $where = array('application_list_id' => $id);
        $this->application_model->set_action($where, $data, 'tbl_application_list');
        $type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('employee/dashboard/leave_application_inquiry'); //redirect page
    }
	
	///
	
	
	

    public function view_application($id) {
        $data['title'] = "Application List";
        $data['application_info'] = $this->emp_model->get_emp_leave_info($id);
        // set view status by id
        $where = array('application_list_id' => $id);
        $updata['view_status'] = '1';
        $this->emp_model->set_action($where, $updata, 'tbl_application_list');

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/inquiry_application_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }


    public function all_notice() {
        $data['menu'] = array("notice" => 1);
        $data['title'] = "All Notice";

        // get all notice by flag       
        $data['notice_info'] = $this->emp_model->get_all_notice();

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/all_notice', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function notice_detail($id) {
        $data['title'] = "Notice Details";

        //get leave category for dropdown
        $this->emp_model->_table_name = "tbl_notice"; // table name
        $this->emp_model->_order_by = "notice_id"; // $id
        $data['full_notice_details'] = $this->emp_model->get_by(array('notice_id' => $id,), TRUE); // get result

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/notice_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function all_events() {
        // $this->output->enable_profiler(TRUE);

        $data['menu'] = array("events" => 1);
        $data['title'] = "All Events";

        // get all notice by flag       
        $data['event_info'] = $this->emp_model->get_all_events();

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);
        
        $data['subview'] = $this->load->view('employee/events', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function event_detail($id) {
        $data['title'] = "Event Details";

        //get leave category for dropdown
        $this->emp_model->_table_name = "tbl_holiday"; // table name
        $this->emp_model->_order_by = "holiday_id"; // $id
        $data['event_details'] = $this->emp_model->get_by(array('holiday_id' => $id,), TRUE); // get result

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/event_details', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function all_award() {
        $data['menu'] = array("awards" => 1);
        $data['title'] = "All Awards";

        // get all notice by flag       
        $data['award_info'] = $this->emp_model->get_all_awards();

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/all_awards', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function award_detail($id) {
        $data['title'] = "All Awards";

        //get award detail info for particular employee    
        $data['employee_award_info'] = $this->emp_model->get_all_awards($id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);    

        $data['subview'] = $this->load->view('employee/award_details_page', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function profile() {
        $data['title'] = "User Profile";
        $employee_id = $this->session->userdata('employee_id');

        //get employee details
        $data['employee_details'] = $this->emp_model->all_emplyee_info($employee_id);

        $data['subview'] = $this->load->view('employee/user_profile', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    /*
     * Mailbox Controllers starts ------
     */

    public function inbox() {
        $data['menu'] = array("mailbox" => 1, "inbox" => 1);
        $data['title'] = "Employee Panel";
        $employee_id = $this->session->userdata('employee_id');

        //get leave category for dropdown
        $this->emp_model->_table_name = "tbl_employee"; // table name
        $this->emp_model->_order_by = "employee_id"; // $id
        $data['employee_details'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), TRUE); // get result        
        $email = $data['employee_details']->email;

        // get all inbox by email 
        $data['get_inbox_message'] = $this->mailbox_model->get_inbox_message($email);
        $data['unread_mail'] = count($this->mailbox_model->get_inbox_message($email, TRUE));

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/emp_inbox', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function read_inbox_mail($id) {
        $data['title'] = "Inbox";
        $this->mailbox_model->_table_name = 'tbl_inbox';
        $this->mailbox_model->_order_by = 'inbox_id';
        $data['read_mail'] = $this->mailbox_model->get_by(array('inbox_id' => $id), true);

        $this->mailbox_model->_primary_key = 'inbox_id';
        $updata['view_status'] = '1';
        $this->mailbox_model->save($updata, $id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);    

        $data['subview'] = $this->load->view('employee/emp_read_mail', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function delete_inbox_mail() {
        // get sellected id into inbox email page
        $selected_inbox_id = $this->input->post('selected_inbox_id', TRUE);
        if (!empty($selected_inbox_id)) { // check selected message is empty or not
            foreach ($selected_inbox_id as $v_inbox_id) {
                $this->mailbox_model->_table_name = 'tbl_inbox';
                $this->mailbox_model->delete_multiple(array('inbox_id' => $v_inbox_id));
            }
            $type = "success";
            $message = "Your message has been Deleted.";
        } else {
            $type = "error";
            $message = "Please Select a Message to Delete.";
        }
        set_message($type, $message);
        redirect('employee/dashboard/inbox');
    }

    public function sent() {
        $data['menu'] = array("mailbox" => 1, "sent" => 1);
        $data['title'] = "Send Email";

        $employee_id = $this->session->userdata('employee_id');
        $data['get_sent_message'] = $this->mailbox_model->get_sent_message($employee_id);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/emp_sent', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function read_send_mail($id) {
        $data['title'] = "Inbox";
        $this->mailbox_model->_table_name = 'tbl_send';
        $this->mailbox_model->_order_by = 'send_id';
        $data['read_mail'] = $this->mailbox_model->get_by(array('send_id' => $id), true);

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);
        
        $data['subview'] = $this->load->view('employee/dashboard/emp_read_mail', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function delete_send_mail() {
        // get sellected id into send email page
        $selected_send_id = $this->input->post('selected_send_id', TRUE);

        if (!empty($selected_send_id)) {
            foreach ($selected_send_id as $v_send_id) {
                $this->mailbox_model->_table_name = 'tbl_send';
                $this->mailbox_model->delete_multiple(array('send_id' => $v_send_id));
            }
            $type = "success";
            $message = "Your message has been Deleted.";
        } else {
            $type = "error";
            $message = "Please Select a Message to Delete.";
        }
        set_message($type, $message);
        redirect('employee/dashboard/sent');
    }

    public function compose() {
        $data['title'] = "Inbox";
        $this->mailbox_model->_table_name = 'tbl_employee';
        $this->mailbox_model->_order_by = 'employee_id';
        $data['get_employee_email'] = $this->mailbox_model->get_by(array('status' => '1'), FALSE);
        $data['editor'] = $this->data;

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/emp_compose_mail', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function send_mail() {

        $discard = $this->input->post('discard', TRUE);
        if ($discard) {
            redirect('employee/dashboard/inbox');
        }
        $all_email = $this->input->post('to', TRUE);

        // get all email address
        foreach ($all_email as $v_email) {
            $data = $this->mailbox_model->array_from_post(array('subject', 'message_body'));
            if (!empty($_FILES['attach_file']['name'])) {
                $old_path = $this->input->post('attach_file_path');
                if ($old_path) {
                    unlink($old_path);
                }
                $val = $this->mailbox_model->uploadAllType('attach_file');
                $val == TRUE || redirect('employee/emp_compose_mail');
                // save into send table
                $data['attach_filename'] = $val['fileName'];
                $data['attach_file'] = $val['path'];
                $data['attach_file_path'] = $val['fullPath'];
                // save into inbox table
                $idata['attach_filename'] = $val['fileName'];
                $idata['attach_file'] = $val['path'];
                $idata['attach_file_path'] = $val['fullPath'];
            } else {
                $data['attach_filename'] = NULL;
                $data['attach_file'] = NULL;
                $data['attach_file_path'] = NULL;
                // save into inbox table
                $idata['attach_filename'] = NULL;
                $idata['attach_file'] = NULL;
                $idata['attach_file_path'] = NULL;
            }
            $data['to'] = $v_email;
            /*
             * Email Configuaration 
             */
            $employee_id = $this->session->userdata('employee_id');

            //get employee email address by employee id
            $this->emp_model->_table_name = "tbl_employee"; // table name
            $this->emp_model->_order_by = "employee_id"; // $id
            $employee_details = $this->emp_model->get_by(array('employee_id' => $employee_id,), TRUE); // get result   

            $name = $employee_details->email;
            $info = $data['subject'];
            // set from email
            $from = array($name, $info);
            // set sender email
            $to = $v_email;
            //set subject
            $subject = $data['subject'];
            $data['employee_id'] = $employee_id;
            $data['message_time'] = date('Y-m-d H:i:s');
            // save into send 
            $this->mailbox_model->_table_name = 'tbl_send';
            $this->mailbox_model->_primary_key = 'send_id';
            $send_id = $this->mailbox_model->save($data);

            // get mail info by send id to send
            $this->mailbox_model->_table_name = 'tbl_send';
            $this->mailbox_model->_order_by = 'send_id';
            $data['read_mail'] = $this->mailbox_model->get_by(array('send_id' => $send_id), true);

            // set view page
            $view_page = $this->load->view('employee/read_mail', $data, TRUE);
            $send_email = $this->mail->sendEmail($from, $to, $subject, $view_page);
            // save into inbox table procees 
            $idata['to'] = $employee_details->email;
            $idata['from'] = $data['to'];
            $idata['subject'] = $data['subject'];
            $idata['message_body'] = $data['message_body'];
            $idata['message_time'] = date('Y-m-d H:i:s');
            // save into inbox
            $this->mailbox_model->_table_name = 'tbl_inbox';
            $this->mailbox_model->_primary_key = 'inbox_id';
            $this->mailbox_model->save($idata);
        }
        if ($send_email) {
            $type = "success";
            $message = "Your message has been sent.";
            set_message($type, $message);
            redirect('employee/dashboard/sent');
        } else {
            show_error($this->email->print_debugger());
        }
    }

    /*
     * Mailbox Controllers ends ------
     */

    public function change_password() {
        $data['menu'] = array("profile" => 1, "change_password" => 1);
        $data['title'] = "Change Password";

        //tbl_employee_ess_setup
        $this->emp_model->_table_name = "tbl_employee_ess_setup"; //table name
        $this->emp_model->_order_by = "employee_id";
        $employee_id = $this->session->userdata('employee_id');
        $data['tbl_employee_ess_setup'] = $this->emp_model->get_by(array('employee_id' => $employee_id,), FALSE);

        $data['subview'] = $this->load->view('employee/change_password', $data, TRUE);
        $this->load->view('employee/_layout_main', $data);
    }

    public function check_employee_password($val) {
        $password = $this->hash($val);
        $check_dupliaction_id = $this->emp_model->check_by(array('password' => $password), 'tbl_employee_login');
        if (empty($check_dupliaction_id)) {
            $result = '<small style="padding-left:10px;color:red;font-size:10px">Your Entered Password Do Not Match !<small>';
        } else {
            $result = NULL;
        }
        echo $result;
    }

    public function set_password() {
        $employee_id                     = $this->session->userdata('employee_id');
        $data['password']                = $this->hash($this->input->post('new_password'));
        $this->emp_model->_table_name    = 'tbl_employee_login';
        $this->emp_model->_primary_key   = 'employee_id';
        $this->emp_model->save($data, $employee_id);
        $type = "success";
        $message = "Password Successfully Changed!";
        set_message($type, $message);
        redirect('employee/dashboard/change_password'); //redirect page
    }

    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }

    public function perbaruiSaldo($nik, $idCuti) {
        $masaBerlaku = Cuti::find($idCuti)->masa_berlaku;
        $expired = Carbon::createFromFormat(
                    'Y-m-d', 
                    $this->where('nik', '=', $nik)->where('id_cuti', '=', $idCuti)->first()->expired
                );
        $sekarang = Carbon::now();
        
        if ($sekarang->gte($expired)) {
            $expired->addDays($masaBerlaku);
            
        }
    }

    

}
