<?php
 // APPPATH . 'libraries/' . $classname . '.php';
require APPPATH . 'third_party/tad/lib/TADFactory.php';
require APPPATH . 'third_party/tad/lib/TAD.php';
require APPPATH . 'third_party/tad/lib/TADResponse.php';
require APPPATH . 'third_party/tad/lib/Providers/TADSoap.php';
require APPPATH . 'third_party/tad/lib/Providers/TADZKLib.php';
require APPPATH . 'third_party/tad/lib/Exceptions/ConnectionError.php';
require APPPATH . 'third_party/tad/lib/Exceptions/FilterArgumentError.php';
require APPPATH . 'third_party/tad/lib/Exceptions/UnrecognizedArgument.php';
require APPPATH . 'third_party/tad/lib/Exceptions/UnrecognizedCommand.php';
use TADPHP\TADFactory;
use TADPHP\TAD;


class Synch_fingerprint_patal extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('cron_model');

    }

	public function index() {
    $this->output->enable_profiler(TRUE);

	$LokasiAbsen = 'Patal';

	$options = [
		'ip'            => '192.168.250.5',     // '169.254.0.1' by default (totally useless!!!).
		'internal_id'   => 1,                   // 1 by default.
		'com_key'       => 0,                   // 0 by default.
		'description'   => 'N/A',               // 'N/A' by default.
		'soap_port'     => 80,                  // 80 by default,
		'udp_port'      => 4370,                // 4370 by default.
		'encoding'      => 'utf-8'              // iso8859-1 by default.
	  ];
	  
	  $tad_factory      = new TADFactory($options);
	  $tad              = $tad_factory->get_instance();  
//  * $logs = $b1->get_att_log(['pin'=>'99999999']);
//  * $logs = $logs->filter_by_date(['start_date'=>'2014-11-27', 'end_date'=>'2014-12-02']);

      $now = date('Y-m-d');
     // get last month from now
      $last_month   = date('Y-m-d',strtotime("-1 months"));
    //   echo 'date start : ' .$last_month.'<br/>';
    //   echo 'date end   : ' .$now;
    //   die;
      
    //   get data logs fingerprint 1bln kebelakang
	  $att_logs         = $tad->get_att_log()->filter_by_date(['start'=>$last_month, 'end'=>$now]);
      $array_att_logs   = $att_logs->to_array();
      
	  
        $awal_cekin 		= strtotime('06:00:00');
        $terakhir_cekin 	= strtotime('12:00:00');
        $awal_cekout 		= strtotime('13:00:00');
        $terakhir_cekout 	= strtotime('23:30:00');
			
	 	$total_r = count($array_att_logs['Row']);
        //  echo '<pre>';
        //  print_r($array_att_logs);
        //  echo '</pre>';
        //  die;

	  	$arr_push_data = array();	
        $type = 'success';
        $message = 'No synchronize data !';
		//TODO Insert data finger attendance logs
		for($i=0; $i<$total_r; $i++){
				$DateTime   = explode(' ', $array_att_logs['Row'][$i]['DateTime']);
				$temp_date  = $DateTime[0];
				$temp_time  = $DateTime[1];

				$PIN 			= $array_att_logs['Row'][$i]['PIN'];
				$DateTime       = $array_att_logs['Row'][$i]['DateTime'];
				$Verified       = $array_att_logs['Row'][$i]['Verified'];
				$Status 	    = $array_att_logs['Row'][$i]['Status'];
				$WorkCode       = $array_att_logs['Row'][$i]['WorkCode'];
				$Date 		    = $temp_date;
                $Time 		    = $temp_time;
                
				if(strtotime($temp_time) > $awal_cekin && strtotime($temp_time) < $terakhir_cekin){
					$Status_Absen	= 'C/In';
				}elseif(strtotime($temp_time) > $awal_cekout && strtotime($temp_time) < $terakhir_cekout){
					$Status_Absen = 'C/Out';
				}else{
					$Status_Absen = 'Break';
				}
                
                //insert data
                $data['PIN'] = $PIN;
                $data['DateTime'] = $DateTime;
                $data['Verified'] = $Verified;
                $data['Status'] = $Status;
                $data['WorkCode'] = $WorkCode;
                $data['Date'] = $Date;
                $data['Time'] = $Time;
                $data['StatusAbsen'] = $Status_Absen;
                $data['LokasiAbsen'] = $LokasiAbsen;

                $this->cron_model->_table_name  = "finger_att_logs"; //table name        
                // $this->cron_model->_primary_key = "PIN";    //id
                
                // cek datanya ada atau tidak klo ada jgn execute insert
                $cek_data = $this->cron_model->cek_data_finger($PIN, $Date, $Status_Absen, $LokasiAbsen);

                if(!$cek_data){
                    //save data jika blm ada record 
                    $save_data = $this->cron_model->save($data);
                    // $save_data = 1;
                    if($save_data = 1){
                        array_push($arr_push_data, $data);
                        $type       = "success";
                        $message    = "Insert Data " .json_encode($arr_push_data)." Succesfully !";
                    }else{
                        $type       = "error";
                        $message    = "Insert Data Error !";
                    }

                   
                
                }else{
                    //update datanya
                    $where = "PIN = '$PIN' AND `Date` = '$Date' AND StatusAbsen = '$Status_Absen' AND LokasiAbsen = '$LokasiAbsen'";
                    
                    $update_data = $this->cron_model->set_action($where, $data, 'finger_att_logs');
                    // $update_data = 1;
                    if($update_data = 1){
                        array_push($arr_push_data, $data);
                        $type       = "success";
                        $message    = "Update Data " .json_encode($arr_push_data)." Succesfully !";
                    }else{
                        $type       = "error";
                        $message    = "Update Data Error !";
                    }
                    
                }
            
				
		}

        $log = "Date Synchronize: ".date("F j, Y, g:i a").PHP_EOL.
        "Status: ".$type.PHP_EOL.
        "Message: ".$message.PHP_EOL.
        "-------------------------".PHP_EOL;

        //Save string to log, use FILE_APPEND to append.
        file_put_contents('logs/synch_fingerprint_patal.log', $log, FILE_APPEND);
    }

}

		



