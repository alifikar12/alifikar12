<?php

class Reload_cuti extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('cron_model');

    }


    public function index() {
        //DEBUG
        //$this->output->enable_profiler(TRUE);

        $data_employee = $this->cron_model->get_employee_cuti();
        // echo '<pre>';
        // print_r($data_employee); 
        // echo '</pre>';
        // die;

        $arr_reload_cuti                    = array();
        $arr_cleareance_grace_period_cuti   = array();

        for($i=0; $i<count($data_employee); $i++){
            
            $employee_id            = $data_employee[$i]->employee_id; //employee_id
            $saldo_cuti             = $data_employee[$i]->saldo_cuti; //saldo cuti
            $tgl_join               = date('m-d', strtotime($data_employee[$i]->joining_date));
            $date_reload            = date('Y-m-d', strtotime($data_employee[$i]->date_reload));//tanggal reload cuti
            $sisa_cuti_tahun_lalu   = $data_employee[$i]->sisa_cuti_thn_lalu;
            $joining_date           = $data_employee[$i]->joining_date;

            if($this->reload_cuti($employee_id, $saldo_cuti, $tgl_join, $date_reload, $joining_date) == TRUE){
                array_push($arr_reload_cuti, array( 'employee_id' => $employee_id, 
                                                    'saldo cuti' => $saldo_cuti,
                                                    'tanggal join' => $joining_date
                                                ));
            }
           
                
            if($this->cleareance_grace_period_cuti($employee_id, $date_reload, $sisa_cuti_tahun_lalu, $saldo_cuti) == TRUE){
                array_push($arr_cleareance_grace_period_cuti, array( 'employee_id' => $employee_id, 
                                                                     'saldo cuti' => $saldo_cuti,
                                                                     'sisa_cuti_tahun_lalu' => $sisa_cuti_tahun_lalu
                                                                    ));
            }
            
            
        }

        $log = "Date Update: ".date("F j, Y, g:i a").PHP_EOL.
                "Reload Cuti: ".json_encode($arr_reload_cuti).PHP_EOL.
                "Cleareance Grace Period: ".json_encode($arr_cleareance_grace_period_cuti).PHP_EOL.
                "-------------------------".PHP_EOL;

        //Save string to log, use FILE_APPEND to append.
        file_put_contents('logs/reload_cuti.log', $log, FILE_APPEND);
        
    }


    public function reload_cuti($employee_id, $saldo_cuti, $tgl_join, $date_reload, $joining_date){
        $now            = date('Y-m-d');
        $joiningdate    = new DateTime($joining_date); //set date_join
        $datenow        = new DateTime($now); //set date_now
        $difference     = $joiningdate->diff($datenow);
        $ttl_join_month = $difference->m;

        if(date('m-d') == $tgl_join ){

            //update jika join sudah 12 bln 
            if($ttl_join_month = 12){

                $date = new DateTime($date_reload);
                // set interval 12 month
                $interval = new DateInterval('P12M');
                $date_expired = $date->add($interval)->format('Y-m-d');
                if($saldo_cuti < 0){
                    $sisa_cuti = 0;
                }else{
                    $sisa_cuti = $saldo_cuti;
                }

                $data['saldo_cuti']         = $saldo_cuti+12;
                $data['date_reload']        = $now;
                $data['sisa_cuti_thn_lalu'] = $sisa_cuti;
                $data['expired']            = $date_expired;
                $data['updated_by']         = 'cron';
                $data['last_updated']       = date('Y-m-d H:i:s');

                $this->cron_model->_table_name  = "tbl_saldo_cuti"; //table name        
                $this->cron_model->_primary_key = "employee_id";    //id
                $this->cron_model->save($data, $employee_id);
                
                return TRUE;

            }

        }
        
    }


    //kurangi sisa cuti tahun sblmnya (grace period) jika sudah lebih 3 bulan dari tanggal reload
    public function cleareance_grace_period_cuti($employee_id, $date_reload, $sisa_cuti_tahun_lalu, $saldo_cuti){

        // cek sisa masa cuti
        $now            = date('Y-m-d');
        $datereload     = new DateTime($date_reload); //set date_join
        $datenow        = new DateTime($now); //set date_now
        $difference     = $datereload->diff($datenow);
        $grace_period   = $difference->days;
        $date_time      = date('Y-m-d H:i:s');

        $data['saldo_cuti']         = $saldo_cuti - $sisa_cuti_tahun_lalu;
        // $data['date_reload']        = $now;
        $data['sisa_cuti_thn_lalu'] = 0;
        $data['updated_by']         = 'cron';
        $data['last_updated']       = $date_time;

       
        //run cleareance jika sudah 90 hari atau 3 bulan
        if($grace_period == 90){            
            $this->cron_model->_table_name  = "tbl_saldo_cuti"; //table name        
            $this->cron_model->_primary_key = "employee_id";    //id
            $this->cron_model->save($data, $employee_id);

            return TRUE;
        }

        
    }
           

}
