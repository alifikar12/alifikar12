<?php

class Reload_point extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('cron_model');

    }


    public function index() {
        //DEBUG
        $this->output->enable_profiler(TRUE);

        $data_employee      = $this->cron_model->get_employee_point();
        $arr_reload_point   = array();

        for($i=0; $i<count($data_employee); $i++){
            
            $employee_id            = $data_employee[$i]->employee_id; //employee_id
            $saldo_point            = $data_employee[$i]->saldo_point; //saldo cuti
            $date_reload            = date('Y-m-d', strtotime($data_employee[$i]->date_reload));//tanggal reload cuti
            $joining_date           = $data_employee[$i]->joining_date;

              // cek masa kerja karyawan
              $now          = date('Y-m-d');
              $datetime1    = new DateTime($joining_date); //set date_join
              $datetime2    = new DateTime($now); //set date_now
              $difference   = $datetime1->diff($datetime2);
              $masa_kerja   = $difference->days;

            if($this->reload_point($employee_id, $saldo_point, $masa_kerja, $date_reload) == TRUE){
                array_push($arr_reload_point, array('employee_id' => $employee_id, 
                                                    'saldo point' => $saldo_point,
                                                    'masa kerja'  => $masa_kerja
                                                ));
            }
           
            
        }

        $log = "Date Update: ".date("F j, Y, g:i a").PHP_EOL.
                "Reload Point: ".json_encode($arr_reload_point).PHP_EOL.
                "-------------------------".PHP_EOL;

        //Save string to log, use FILE_APPEND to append.
        file_put_contents('logs/reload_point.log', $log, FILE_APPEND);
        
    }


    public function reload_point($employee_id, $saldo_point, $masa_kerja, $date_reload){
        
        $now        = date('Y-m-d');
        $date_time  = date('Y-m-d H:i:s');

        //set update employee point per tgl. 1 setiap bulan 
        if(date('d')=='01'){
            
            // update jika masa kerja lebih dari 3bln / 90 hari
            if($masa_kerja > 90){    

                $date = new DateTime($date_reload);
                // set interval 1 month
                $interval = new DateInterval('P1M');
                $date_expired = $date->add($interval)->format('Y-m-d');

                $data['saldo_point']            = $saldo_point+75000;
                $data['date_reload']            = $now;
                $data['sisa_point_bln_lalu']    = $saldo_point;
                $data['expired']                = $date_expired;
                $data['updated_by']             = 'cron';
                $data['last_updated']           = $date_time;

                $this->cron_model->_table_name  = "tbl_employee_point"; //table name        
                $this->cron_model->_primary_key = "employee_id";    //id
                $this->cron_model->save($data, $employee_id);

                return TRUE;
            }
        }
        
    }

          

}
