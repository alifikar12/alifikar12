<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of department
 *
 * @author NaYeM
 */
class Orders extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('level_jabatan_model');
    }

    public function add_level_jabatan($id = NULL) {
        $this->level_jabatan_model->_table_name = "tbl_level_jabatan"; //table name
        $this->level_jabatan_model->_order_by = "level_id";
        $data['title'] = "Add Level Jabatan";

        if ($id) { // retrive data from db by id
            // get all department by id
            $data['department_info'] = $this->level_jabatan_model->get_by(array('level_id' => $id), TRUE);
            // get all designation by department id
            $this->level_jabatan_model->_table_name = "tbl_level_jabatan"; //table name
            $data['designation_info'] = $this->level_jabatan_model->get_by(array('level_id' => $id), FALSE);

            if (empty($data['department_info'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/level_jabatan/add_level_jabatan');
            }
        }

        //page load
        $data['subview'] = $this->load->view('admin/level_jabatan/add_level_jabatan', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
   

    public function order_list($id=NULL) {
        //DEBUG
        // $this->output->enable_profiler(TRUE);

        $data['title'] = "Order List";

        $this->level_jabatan_model->_table_name  = "tb_cust"; //table name
        $this->level_jabatan_model->_order_by    = "id_cust";

        // retrive data from db by id
        if (!empty($id)) {
            $data['level_jabatan'] = $this->level_jabatan_model->get_by(array('id_cust' => $id,), TRUE);

            if (empty($data['level_jabatan'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/customer/customer_list');
            }
        }

        // retrive all data from db
        include('api-joylabbeauty/get_request_orders.php');
        $all_orders = get_all_orders();
        //print_r($response[0][id]);
        //die;
        $data['all_orders_info'] = $all_orders;

        $data['role'] = $this->session->userdata('role');

        $data['subview'] = $this->load->view('admin/orders/order_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function view_order($id = NULL) {
        $data['title'] = "View Order";
        if (!empty($id)) {// retrive data from db by id

            // retrive all data from db
            include('api-joylabbeauty/get_request_orders.php');
            $get_orders_by_id = get_order_by_id($id);
            $data['order_info'] = $get_orders_by_id;

            include('api-joylabbeauty/get_request_products.php');
            if (!empty($data['order_info']['line_items'])){ 
                foreach ($data['order_info']['line_items'] as $v_line_items) {
                    
                    //echo $v_line_items['id'];
                    $get_product_by_id = get_product_by_id($v_line_items['product_id']);
                    $product_id = $v_line_items['product_id'];
                    $data['product_info'][$product_id] = $get_product_by_id;
                    
                }
                    // echo '<pre>';
                    // print_r($data['product_info']);
                    // echo '</pre>';
                    // die;

            } 
            
            if (empty($data['order_info'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/orders/order_list');
            }
        }
        $data['subview'] = $this->load->view('admin/orders/view_order', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }


    public function make_pdf($id) {
        $data['title'] = "Sales Order";

         // retrive all data from db
         include('api-joylabbeauty/get_request_orders.php');
         $get_orders_by_id = get_order_by_id($id);
         $data['order_info'] = $get_orders_by_id;

         include('api-joylabbeauty/get_request_products.php');
         if (!empty($data['order_info']['line_items'])){ 
             foreach ($data['order_info']['line_items'] as $v_line_items) {
                 
                 //echo $v_line_items['id'];
                 $get_product_by_id = get_product_by_id($v_line_items['product_id']);
                 $product_id = $v_line_items['product_id'];
                 $data['product_info'][$product_id] = $get_product_by_id;
                 
             }

         } 

        $this->load->helper('dompdf');
        //$this->dompdf->set_option('isRemoteEnabled', TRUE);
        $view_file = $this->load->view('admin/orders/order_view_pdf', $data, true);
        pdf_create($view_file, $data['order_info']['number']);
    }


    public function save_order($id = NULL) {
        $this->level_jabatan_model->_table_name  = "tb_cust"; //table name        
        $this->level_jabatan_model->_primary_key = "id_cust";    //id
        // input data
        $data = $this->level_jabatan_model->array_from_post(array('name', 'email', 'no_telp')); //input post

        // dublicacy check 
        if (!empty($id)) {
            $id_cust = array('id_cust !=' => $id);
        } else {
            $id_cust = null;
        }
        // check check_leave_category by where        
        // if not empty show alert message else save data
        $check_by_no_telp = $this->level_jabatan_model->check_update('tb_cust', $where = array('no_telp' => $data['no_telp']), $id_cust);

        if (!empty($check_by_no_telp)) {
            $type = "error";
            $message = "No Telp Already Exist!";
            set_message($type, $message);
        } else {
            $this->level_jabatan_model->save($data, $id);
            // messages for user
            $type = "success";
            $message = "Data Successfully Saved!";
            set_message($type, $message);
        }

        redirect('admin/customer/customer_list'); //redirect page
    }


    public function delete_order($id) {    
        $this->level_jabatan_model->_table_name = "tb_cust"; //table name        
        $this->level_jabatan_model->_primary_key = "id_cust";    //id
        $this->level_jabatan_model->delete($id);
        $type       = "success";
        $message    = "Data Successfully Delete!";
        set_message($type, $message);
    
        redirect('admin/customer/customer_list'); //redirect page
    }

   

}
