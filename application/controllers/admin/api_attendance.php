<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of attendance
 *
 * @author doniebes
 */
 

require_once APPPATH."/third_party/tad/lib/TADFactory.php";
require_once APPPATH."/third_party/tad/lib/TAD.php";
require_once APPPATH."/third_party/tad/lib/TADResponse.php";
require_once APPPATH."/third_party/tad/lib/Providers/TADSoap.php";
require_once APPPATH."/third_party/tad/lib/Providers/TADZKLib.php";
require_once APPPATH."/third_party/tad/lib/Exceptions/ConnectionError.php";
require_once APPPATH."/third_party/tad/lib/Exceptions/FilterArgumentError.php";
require_once APPPATH."/third_party/tad/lib/Exceptions/UnrecognizedArgument.php";
require_once APPPATH."/third_party/tad/lib/Exceptions/UnrecognizedCommand.php";

use TADPHP\TADFactory;
use TADPHP\TAD;
		

class api_attendance extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('attendance_model');
    }
	
	public function download_attendance() {
			
			
			
			//$this->load->library('Tad_finger');
			//set to connect finger print
			$options = [
				'ip' => '117.54.108.171',   // '169.254.0.1' by default (totally useless!!!).
				'internal_id' => 1,    // 1 by default.
				'com_key' => 445465,        // 0 by default.
				'description' => 'N/A', // 'N/A' by default.
				'soap_port' => 80,     // 80 by default,
				'udp_port' => 4370,      // 4370 by default.
				'encoding' => 'utf-8'    // iso8859-1 by default.
			  ];
			  
		  $tad_factory = new TADFactory($options);
		  $tad = $tad_factory->get_instance(); 

		  $att_logs = $tad->get_att_log();
		  
		  $array_att_logs = $att_logs->to_array();
		  
			$awal_cekin 		= strtotime('06:00:00');
			$terakhir_cekin 	= strtotime('12:00:00');
			$awal_cekout 		= strtotime('13:00:00');
			$terakhir_cekout 	= strtotime('23:30:00');

			$total_r = count($array_att_logs['Row']);
			
			$this->attendance_model->_table_name 	= "finger_att_logs"; //table name        
			$this->attendance_model->_primary_key 	= "PIN";    //id
		
			//TODO Insert data finger attendance logs
			for($i=0; $i<$total_r; $i++){
					$DateTime = explode(' ', $array_att_logs['Row'][$i]['DateTime']);
					$temp_date = $DateTime[0];
					$temp_time = $DateTime[1];

					$PIN 			= $array_att_logs['Row'][$i]['PIN'];
					$DateTime 		= $array_att_logs['Row'][$i]['DateTime'];
					$Verified 		= $array_att_logs['Row'][$i]['Verified'];
					$Status 		= $array_att_logs['Row'][$i]['Status'];
					$WorkCode 		= $array_att_logs['Row'][$i]['WorkCode'];
					$Date 			= $temp_date;
					$Time 			= $temp_time;
					
					if(strtotime($temp_time) > $awal_cekin && strtotime($temp_time) < $terakhir_cekin){
						$Status_Absen	= 'C/In';
					}elseif(strtotime($temp_time) > $awal_cekout && strtotime($temp_time) < $terakhir_cekout){
						$Status_Absen = 'C/Out';
					}else{
						$Status_Absen = '';
					}
					
					/*
					$query_insert = "INSERT INTO `pmki_hris`.`finger_att_logs`
									(`PIN`,	`DateTime`, `Verified`,	`Status`,	`WorkCode`,	`Date`,	`Time`, `StatusAbsen`)
									VALUES ('$PIN', '$DateTime', '$Verified', '$Status', '$WorkCode', '$Date', '$Time', '$Status_Absen')";
									// WHERE NOT EXISTS (SELECT * FROM `pmki_hris`.`finger_att_logs` 
									// 									WHERE PIN = '$PIN' AND `Date` = '$Date' AND StatusAbsen = '$Status_Absen')";

					// echo $query_insert;
					
					//cek datanya ada atau tidak klo ada jgn execute insert				
					$sql_cek = "SELECT * FROM `pmki_hris`.`finger_att_logs` 
								WHERE PIN = '$PIN' AND `Date` = '$Date' AND StatusAbsen = '$Status_Absen'";

					$cek_data = mysqli_query($con, $sql_cek);				
					
					// echo $sql_cek;
					// die;

					if(!$cek_data->EOF){
						// echo $sql_cek;
						mysqli_query($con, $query_insert);
					}
					*/
					
					//cek datanya ada atau tdk dengan 3 parameter PIN, Date Status Absen
					$where = array('PIN' => $PIN, 'Date' => $Date, 'StatusAbsen' => $Status_Absen);	
					$check_attendance = $this->attendance_model->check_update('finger_att_logs', $where);				
					/*
					echo '<br/>';
					print_r($check_attendance);
					echo '<br/>';
					*/
					/*
					if(empty($check_attendance)){	
						$data['PIN'] 			= $PIN;
						$data['DateTime'] 		= $DateTime;
						$data['Verified'] 		= $Verified;
						$data['Status'] 		= $Status;
						$data['WorkCode'] 		= $WorkCode;
						$data['Date'] 			= $Date;
						$data['Time'] 			= $Time;
						$data['StatusAbsen'] 	= $StatusAbsen;
						
						$this->attendance_model->save($data);
					}
					*/
					if (!empty($check_attendance)) {
						//$type = "error";
						//$message = "Holiday Information Already Exist!";
						//set_message($type, $message);
						$data_att = array(
							'PIN'			=>$PIN,
							'DateTime'		=>$DateTime,
							'Verified'		=>$Verified,
							'Status'		=>$Status,
							'WorkCode'		=>$WorkCode,
							'Date'			=>$Date,
							'Time'			=>$Time,
							'StatusAbsen'	=>$Status_Absen
						);

						$where_update	= array('PIN' => $PIN, 'Date' => $Date, 'StatusAbsen' => 'C/Out');
						
						$this->db->set($data_att);
						$this->db->where($where_update);
						$this->db->update('finger_att_logs');
						
					} else {
						 //DEBUG
						//$this->output->enable_profiler(TRUE);
						//echo 'Insert data !';
						/*
						$data['PIN'] 			= $PIN;
						$data['DateTime'] 		= $DateTime;
						$data['Verified'] 		= $Verified;
						$data['Status'] 		= $Status;
						$data['WorkCode'] 		= $WorkCode;
						$data['Date'] 			= $Date;
						$data['Time'] 			= $Time;
						$data['StatusAbsen'] 	= $Status_Absen;
						
						$sql = $this->attendance_model->save($data);
						print_r($sql);
						*/
						
						$data_att = array(
							'PIN'=>$PIN,
							'DateTime'=>$DateTime,
							'Verified'=>$Verified,
							'Status'=>$Status,
							'WorkCode'=>$WorkCode,
							'Date'=>$Date,
							'Time'=>$Time,
							'StatusAbsen'=>$Status_Absen
						);

						$this->db->insert('finger_att_logs',$data_att);
						
						// messages for user
						//$type = "success";
						//$message = "Holiday Information Successfully Saved!";
						//set_message($type, $message);
					}	
					
			}

			$type = "success";
			$message = "Attendance Information Successfully Saved!";
			set_message($type, $message);
			redirect('admin/attendance/attendance_report'); //redirect page
	}

}
