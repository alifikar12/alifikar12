<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Perdin_Application extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('leave_model');
        $this->load->model('employee_model');

        $this->load->model('emp_model');
		$this->load->model('email_model');
		
		$this->load->model('application_model');
    }

    public function approve_perdin_application() {
        //DEBUG
        // $this->output->enable_profiler(TRUE);  
        $data['title'] = "Perdin Application List";
        $employment_id = $this->session->userdata('employment_id');
        $data['all_perdin_applications'] = $this->emp_model->get_all_perdin_applied_for_approved($employment_id);

        $data['subview'] = $this->load->view('admin/perdin/perdin_application', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function approve_perdin_application_2() {
        //DEBUG
        // $this->output->enable_profiler(TRUE);  
        $data['title'] = "Perdin Application List (2)";
        $employment_id = $this->session->userdata('employment_id');
        $data['all_perdin_applications'] = $this->emp_model->get_all_perdin_applied_for_approved_2($employment_id);

        $data['subview'] = $this->load->view('admin/perdin/perdin_application_2', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function my_perdin_application() {
        $data['title'] = "Application List";
        $employment_id =  $this->session->userdata('employment_id');
        $data['all_application_info'] = $this->leave_model->get_my_leave_info($employment_id);

        $data['subview'] = $this->load->view('admin/leave/my_perdin_application', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function create_perdin_application() {
		//DEBUG
        //$this->output->enable_profiler(TRUE);       
        $data['title'] = "New Leave Application";

        //get leave category for dropdown
        $this->leave_model->_table_name = "tbl_leave_category"; // table name
        $this->leave_model->_order_by = "leave_category_id"; // $id
        $data['all_leave_category'] = $this->leave_model->get(); // get result

        $employment_id = $this->session->userdata('employment_id');
        $data['all_emplyee_info'] = $this->leave_model->all_emplyee_info($employment_id); // get result
        $data['no_leave'] = $this->emp_model->get_no_leave();

        $data['subview'] = $this->load->view('admin/leave/create_perdin_application', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_perdin_application() {
        $this->leave_model->_table_name = "tbl_application_list"; // table name
        $this->leave_model->_primary_key = "application_list_id"; // $id
	
        //receive form input by post
        $data['employee_id'] 		= $this->session->userdata('employee_id');
        $data['leave_category_id'] 	= $this->input->post('leave_category_id');
        $data['leave_start_date'] 	= $this->input->post('leave_start_date');
        $data['leave_end_date'] 	= $this->input->post('leave_end_date');
        $data['reason'] 			= $this->input->post('reason');
		$data['employment_id'] 		= $this->session->userdata('employment_id');
		$data['leave_status'] 		= 'pending';

        //save data in database
        $this->leave_model->save($data);

        ////send email notif to approval
        $email_to 			= $this->input->post('email_dl');
        $document_number	= $this->input->post('document_number');
        $link 				= $this->input->post('link');
        $receipt_name 		= $this->input->post('receipt_name');
		$sender_name		= $this->input->post('sender_name');
        
        $this->email_model->email_notif($email_to, $document_number, $link, $receipt_name, $sender_name, $leave_status='pending');

        // messages for user
        $type = "success";
        $message = "Leave Application Successfully Submitted !";
        set_message($type, $message);
        redirect('admin/perdin_application/my_perdin_application');
    }


    public function proses_perdin_by_dl($id) {
        //DEBUG
		//$this->output->enable_profiler(TRUE);
        if (isset($_POST['approve_btn'])) {
			$data['perdin_status']  = 'partial approved';
			$data['approve1']        = $this->session->userdata('user_name');
            $data['date_approve1']   = date('Y-m-d');
            
            //send email notif to approval
            $email_to 			= $this->input->post('email_to_next_approval');
            $doc_number 	    = $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name');
            $sender_name 		= $this->input->post('sender_name');
            $jk_sender 		    = $this->input->post('jk_sender');
			$jk_receipt         = $this->input->post('jk_receipt');
            $requester_name     = $this->input->post('requester_name');

			
        }else if(isset($_POST['unapprove_btn'])){
			$data['perdin_status']   = 'cancel';
			$data['unapprove1']       = $this->session->userdata('user_name');
			$data['date_unapprove1']  = date('Y-m-d');
			
			 //send email notif to approval
            $email_to 			= $this->input->post('email_to_requester');
            $doc_number 	    = $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name_requester');
            $sender_name 		= $this->input->post('sender_name');
            $jk_sender 		    = $this->input->post('jk_sender');
			$jk_receipt         = $this->input->post('jk_receipt');
            $requester_name     = $this->input->post('requester_name');

		}
        
        $where = array('perdin_id' => $id);
        $this->application_model->set_action($where, $data, 'tbl_perdin_list');
		
		$perdin_info	    = $this->application_model->get_perdin_info($doc_number);
		$perdin_status 		= $perdin_info->perdin_status;
		
		$this->email_model->email_notif_perdin($email_to, $doc_number, $link, $receipt_name, $sender_name, $perdin_status, $jk_sender, $jk_receipt, $requester_name);
		
        $type       = "success";
        $message    = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('admin/perdin_application/approve_perdin_application'); //redirect page
    }

    public function proses_perdin_by_dl_2($id) {
        //DEBUG
		//$this->output->enable_profiler(TRUE);
        if (isset($_POST['approve_btn'])) {
			$data['perdin_status']  = 'partial approved 2';
			$data['approve2']        = $this->session->userdata('user_name');
            $data['date_approve2']   = date('Y-m-d');
            
            //send email notif to approval
            $email_to 			= $this->input->post('email_to_next_approval');
            $doc_number 	    = $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name');
            $sender_name 		= $this->input->post('sender_name');
            $jk_sender 		    = $this->input->post('jk_sender');
			$jk_receipt         = $this->input->post('jk_receipt');
            $requester_name     = $this->input->post('requester_name');

			
        }else if(isset($_POST['unapprove_btn'])){
			$data['perdin_status']   = 'cancel';
			$data['unapprove2']       = $this->session->userdata('user_name');
			$data['date_unapprove2']  = date('Y-m-d');
			
			 //send email notif to approval
            $email_to 			= $this->input->post('email_to_requester');
            $doc_number 	    = $this->input->post('document_number');
            $link 				= $this->input->post('link');
            $receipt_name 		= $this->input->post('receipt_name_requester');
            $sender_name 		= $this->input->post('sender_name');
            $jk_sender 		    = $this->input->post('jk_sender');
			$jk_receipt         = $this->input->post('jk_receipt');
            $requester_name     = $this->input->post('requester_name');

		}
        
        $where = array('perdin_id' => $id);
        $this->application_model->set_action($where, $data, 'tbl_perdin_list');
		
		$perdin_info	    = $this->application_model->get_perdin_info($doc_number);
		$perdin_status 		= $perdin_info->perdin_status;
		
		$this->email_model->email_notif_perdin($email_to, $doc_number, $link, $receipt_name, $sender_name, $perdin_status, $jk_sender, $jk_receipt, $requester_name);
		
        $type       = "success";
        $message    = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('admin/perdin_application/approve_perdin_application_2'); //redirect page
    }


    public function view_perdin_details($id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
        $data['title'] = "Perdin Details (1)";
        $data['perdin_info'] = $this->emp_model->get_emp_perdin_info($id);
        // set view status by id
        $where = array('perdin_id' => $id);
        $updata['view_status'] = '1';
        $this->leave_model->set_action($where, $updata, 'tbl_perdin_list');
		//$this->employee_model->set_action($where, $updata, 'tbl_employee');
		$data['no_leave'] = $this->emp_model->get_no_leave();
		
        $data['subview'] = $this->load->view('admin/perdin/perdin_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function view_perdin_details_2($id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
        $data['title'] = "Perdin Details (2)";
        $data['perdin_info'] = $this->emp_model->get_emp_perdin_info($id);
        // set view status by id
        $where = array('perdin_id' => $id);
        $updata['view_status'] = '1';
        $this->leave_model->set_action($where, $updata, 'tbl_perdin_list');
		//$this->employee_model->set_action($where, $updata, 'tbl_employee');
		$data['no_leave'] = $this->emp_model->get_no_leave();
		
        $data['subview'] = $this->load->view('admin/perdin/perdin_details_2', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function view_application_2($id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
        $data['title'] = "Application List";
        $data['application_info'] = $this->leave_model->get_emp_leave_info_2($id);
        // set view status by id
        $where = array('application_list_id' => $id);
        $updata['view_status'] = '1';
        $this->leave_model->set_action($where, $updata, 'tbl_application_list');
		//$this->employee_model->set_action($where, $updata, 'tbl_employee');
		$data['no_leave'] = $this->emp_model->get_no_leave();
		
        $data['subview'] = $this->load->view('admin/leave/application_details_2', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function view_my_application($id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
        $data['title'] = "Application List";
        $data['application_info'] = $this->leave_model->get_emp_leave_info($id);
        // set view status by id
        $where = array('application_list_id' => $id);
        $updata['view_status'] = '1';
        $this->leave_model->set_action($where, $updata, 'tbl_application_list');
		//$this->employee_model->set_action($where, $updata, 'tbl_employee');

        $data['subview'] = $this->load->view('admin/leave/my_application_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function set_action($id) {
        $data['application_status'] = $this->input->post('application_status', TRUE);
        if ($data['application_status'] == 2) {
            $atdnc_data = $this->leave_model->array_from_post(array('employee_id', 'leave_category_id'));
            $leave_start_date = $this->input->post('leave_start_date', TRUE);
            $leave_end_date = $this->input->post('leave_end_date', TRUE);
            if ($leave_start_date == $leave_end_date) {
                $this->admin_model->_table_name = 'tbl_attendance';
                $this->admin_model->_order_by = 'attendance_id';
                $check_leave_date = $this->admin_model->get_by(array('employee_id' => $atdnc_data['employee_id'], 'date' => $leave_end_date), FALSE);
                if (empty($check_leave_date)) {
                    $atdnc_data['date'] = $leave_start_date;
                    $atdnc_data['attendance_status'] = '3';
                    $this->admin_model->_table_name = 'tbl_attendance';
                    $this->admin_model->_primary_key = "attendance_id";
                    $this->admin_model->save($atdnc_data);
                }
            } else {
                for ($l = $leave_start_date; $l <= $leave_end_date; $l++) {
                    $this->admin_model->_table_name = 'tbl_attendance';
                    $this->admin_model->_order_by = 'attendance_id';
                    $check_leave_date = $this->admin_model->get_by(array('employee_id' => $atdnc_data['employee_id'], 'date' => $l), FALSE);
                    if (empty($check_leave_date)) {
                        $atdnc_data['date'] = $l;
                        $atdnc_data['attendance_status'] = '3';
                        $this->admin_model->_table_name = 'tbl_attendance';
                        $this->admin_model->_primary_key = "attendance_id";
                        $this->admin_model->save($atdnc_data);
                    }
                }
            }
        }
        $where = array('application_list_id' => $id);
        $this->leave_model->set_action($where, $data, 'tbl_application_list');
        $type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('admin/application_list'); //redirect page
    }
	
    public function proses_leave($id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
        if (isset($_POST['approve_btn'])) {
			$update_application_list['leave_status'] 	= 'partial approved';
			$update_application_list['approve1'] 		= $this->session->userdata('full_name');
			$update_application_list['date_approve1'] 	= date('Y-m-d');
			
			//TODO cek saldo cuti
			//$application_details = $this->leave_model->array_from_post(array('employee_id', 'leave_category_id'));
			$employee_id        = $this->input->post('employee_id', TRUE);
			$leave_start_date   = $this->input->post('leave_start_date', TRUE);
            $leave_end_date     = $this->input->post('leave_end_date', TRUE);	
			$leave_type         = $this->input->post('leave_type', TRUE);				
			
			
			//send email notif to approval
			$email_to 			= $this->input->post('email_to_next_approval');
			$doc_number	        = $this->input->post('document_number');
			$link 				= $this->input->post('link');
			$receipt_name 		= $this->input->post('receipt_name');
			$sender_name 		= $this->input->post('sender_name');
            $jk_sender 		    = $this->input->post('jk_sender');
			$jk_receipt         = $this->input->post('jk_receipt');
            $requester_name     = $this->input->post('requester_name');
			
        }else if(isset($_POST['unapprove_btn'])){
			$update_application_list['leave_status'] 	= 'cancel';
			$update_application_list['unapprove1'] 		= $this->session->userdata('full_name');
			$update_application_list['date_unapprove1'] = date('Y-m-d');
			
			//send email notif to approval
			$email_to 			= $this->input->post('email_requester');
			$doc_number	        = $this->input->post('document_number');
			$link 				= $this->input->post('link');
			$receipt_name 		= $this->input->post('requester_name');
			$sender_name 		= $this->input->post('sender_name');
            $jk_sender 		    = $this->input->post('jk_sender');
			$jk_receipt         = $this->input->post('jk_receipt');
            $requester_name     = $this->input->post('requester_name');
		}
		//update status application list karyawan
        $where = array('application_list_id' => $id);
        $this->leave_model->set_action($where, $update_application_list, 'tbl_application_list');
		
		
		$application_info	= $this->leave_model->get_application_info($doc_number);
		$leave_status 		= $application_info->leave_status;
        
		$this->email_model->email_notif_leave($email_to, $doc_number, $link, $receipt_name, $sender_name, $leave_status, $jk_sender, $jk_receipt, $requester_name);
		
		
		$type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('admin/perdin_application/approve_perdin_application/'); //redirect page
		
    }

    public function proses_leave_2($id) {
		//DEBUG
		//$this->output->enable_profiler(TRUE);
        if (isset($_POST['approve_btn'])) {
			$update_application_list['leave_status'] 	= 'partial approved 2';
			$update_application_list['approve2'] 		= $this->session->userdata('full_name');
			$update_application_list['date_approve2'] 	= date('Y-m-d');
			
			//TODO cek saldo cuti
			//$application_details = $this->leave_model->array_from_post(array('employee_id', 'leave_category_id'));
			$employee_id = $this->input->post('employee_id', TRUE);
			$leave_start_date = $this->input->post('leave_start_date', TRUE);
            $leave_end_date = $this->input->post('leave_end_date', TRUE);	
			$leave_type = $this->input->post('leave_type', TRUE);				
			
			
			//send email notif to approval
			$email_to 			= $this->input->post('email_to_next_approval');
			$doc_number     	= $this->input->post('document_number');
			$link 				= $this->input->post('link');
			$receipt_name 		= $this->input->post('receipt_name');
			$sender_name 		= $this->input->post('sender_name');
			$jk_sender 		    = $this->input->post('jk_sender');
			$jk_receipt         = $this->input->post('jk_receipt');
            $requester_name     = $this->input->post('requester_name');
			
        }else if(isset($_POST['unapprove_btn'])){
			$update_application_list['leave_status'] 	= 'cancel';
			$update_application_list['unapprove2'] 		= $this->session->userdata('full_name');
			$update_application_list['date_unapprove2'] = date('Y-m-d');
			
			//send email notif to approval
			$email_to 			= $this->input->post('email_requester');
			$doc_number	        = $this->input->post('document_number');
			$link 				= $this->input->post('link');
			$receipt_name 		= $this->input->post('requester_name');
			$sender_name 		= $this->input->post('sender_name');
            $jk_sender 		    = $this->input->post('jk_sender');
			$jk_receipt         = $this->input->post('jk_receipt');
            $requester_name     = $this->input->post('requester_name');
		}
		//update status application list karyawan
        $where = array('application_list_id' => $id);
        $this->leave_model->set_action($where, $update_application_list, 'tbl_application_list');
		
		
		$application_info	= $this->leave_model->get_application_info($doc_number);
		$leave_status 		= $application_info->leave_status;
        
		// $this->email_model->email_notif($email_to, $document_number, $link, $receipt_name, $sender_name, $leave_status);
		$this->email_model->email_notif_leave($email_to, $doc_number, $link, $receipt_name, $sender_name, $leave_status, $jk_sender, $jk_receipt, $requester_name);
		
		$type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('admin/perdin_application/approve_perdin_application_2/'); //redirect page
		
    }

}
