<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of attendance
 *
 * @author ebes
 */
class Attendance extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('attendance_model');
        $this->load->model('leave_model');
        $this->load->model('emp_model');
        $this->load->model('employee_model');
    }

    public function entry_noshow() {
		//DEBUG
        // $this->output->enable_profiler(TRUE);       
        $data['title'] = "Entry No Show";
        $this->attendance_model->_table_name = "tbl_leave_category"; //table name
        $this->attendance_model->_order_by = "leave_category_id";
        $data['all_leave_category_info'] = $this->attendance_model->get();

        $this->attendance_model->_table_name = "tbl_employee"; //table name
        $this->attendance_model->_order_by = "employee_id";
        $data['all_employee'] = $this->attendance_model->get();

        $data['leave_category_id']  = $this->input->post('leave_category_id', TRUE);
        $data['department_id']      = $this->input->post('department_id', TRUE);
        $data['leave_start_date']   = $this->input->post('leave_start_date', TRUE);
        $data['leave_end_date']     = $this->input->post('leave_end_date', TRUE);
        $data['reason']             = $this->input->post('reason', TRUE);
        $sbtnType   = $this->input->post('sbtn');
        $flag       = $this->session->userdata('flag');
        if ($sbtnType == 1 || $flag == 1) {
            if ($flag) {
                $data['leave_category_id']  = $this->session->userdata('leave_category_id');
                $data['leave_start_date']   = $this->session->userdata('leave_start_date');
                $data['leave_end_date']     = $this->session->userdata('leave_end_date');
                $data['department_id']      = $this->session->userdata('department_id');
                $this->session->unset_userdata('leave_category_id');
                $this->session->unset_userdata('leave_start_date');
                $this->session->unset_userdata('leave_end_date');
                $this->session->unset_userdata('department_id');
                $this->session->unset_userdata('flag');
            } else {
                $data['leave_category_id']  = $this->input->post('leave_category_id');
                $data['leave_start_date']   = $this->input->post('leave_start_date');
                $data['leave_end_date']     = $this->input->post('leave_end_date');
                $data['department_id']      = $this->input->post('department_id');
            }
        }
        $data['employee_info'] = $this->attendance_model->get_employee_id_by_dept_id($data['department_id']);

        $data['subview'] = $this->load->view('admin/attendance/entry_noshow', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_noshow() {
        // save to table noshow list
        $this->attendance_model->_table_name = "tbl_noshow_list"; // table name
        $this->attendance_model->_primary_key = "noshow_id"; // $id

        $employee_id            = $this->input->post('employee_id', TRUE);
        $employment_id 	        = $this->input->post('employment_id', TRUE);
        $leave_category_id      = $this->input->post('leave_category_id', TRUE);
        $leave_start_date       = $this->input->post('leave_start_date', TRUE);
        $leave_end_date         = $this->input->post('leave_end_date', TRUE);
        $reason                 = $this->input->post('reason', TRUE);

        $data['leave_category_id']  = $leave_category_id;
        $data['leave_start_date'] 	= $leave_start_date;
        $data['leave_end_date']	    = $leave_end_date;
        $data['leave_status'] 	    = 'fully approved';
        $data['reason'] 		    = $reason; 
        $data['approve3']           = $this->session->userdata('full_name');
        $data['date_approve3']      = date('Y-m-d');
        $data['application_date']   = date('Y-m-d H:i:s');
        $data['employee_id']    = $employee_id;
        $data['employment_id']  = $employment_id;

        //save data in database
        $this->attendance_model->save($data);
        $this->potong_cuti($leave_category_id, $leave_start_date, $leave_end_date, $employee_id);
            
        $arr_employment_id = json_encode($employment_id);
        // echo $arr_employment_id;
        // die;
        // messages for user
        $type = "success";
        $message = "Data NIK ".$arr_employment_id." Successfully Updated !";
        // echo $message;
        // die;

        set_message($type, $message);
        redirect('admin/attendance/entry_noshow');

    }


    public function bulk_update_cuti() {
        // $this->output->enable_profiler(TRUE);       
        $data['title'] = "Bulk Update Cuti Bersama";
        $this->attendance_model->_table_name = "tbl_leave_category"; //table name
        $this->attendance_model->_order_by = "leave_category_id";
        $data['all_leave_category_info'] = $this->attendance_model->get();

        $this->attendance_model->_table_name = "tbl_department"; //table name
        $this->attendance_model->_order_by = "department_id";
        $data['all_department'] = $this->attendance_model->get();

        $data['leave_category_id']  = $this->input->post('leave_category_id', TRUE);
        $data['department_id']      = $this->input->post('department_id', TRUE);
        $data['leave_start_date']   = $this->input->post('leave_start_date', TRUE);
        $data['leave_end_date']     = $this->input->post('leave_end_date', TRUE);
        $data['reason']             = $this->input->post('reason', TRUE);
        $sbtnType   = $this->input->post('sbtn');
        $flag       = $this->session->userdata('flag');
        if ($sbtnType == 1 || $flag == 1) {
            if ($flag) {
                $data['leave_category_id']  = $this->session->userdata('leave_category_id');
                $data['leave_start_date']   = $this->session->userdata('leave_start_date');
                $data['leave_end_date']     = $this->session->userdata('leave_end_date');
                $data['department_id']      = $this->session->userdata('department_id');
                $this->session->unset_userdata('leave_category_id');
                $this->session->unset_userdata('leave_start_date');
                $this->session->unset_userdata('leave_end_date');
                $this->session->unset_userdata('department_id');
                $this->session->unset_userdata('flag');
            } else {
                $data['leave_category_id']  = $this->input->post('leave_category_id');
                $data['leave_start_date']   = $this->input->post('leave_start_date');
                $data['leave_end_date']     = $this->input->post('leave_end_date');
                $data['department_id']      = $this->input->post('department_id');
            }
        }
        $data['employee_info'] = $this->attendance_model->get_employee_id_by_dept_id($data['department_id']);

        $data['subview'] = $this->load->view('admin/attendance/bulk_update_cuti', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }


    public function delete_sick_application($id) {
        // ************* Delete into Employee Table 
        $this->employee_model->_table_name = "tbl_sick_application"; // table name
        $this->employee_model->_primary_key = "id"; // $id
        $this->employee_model->delete($id);
       

        // messages for user
        $type = "success";
        $message = "Application Successfully Delete!";
        set_message($type, $message);
        redirect('admin/attendance/entry_sick_employee');//redirect page
    }

    public function save_sick_employee() {
        $this->leave_model->_table_name = "tbl_sick_application"; // table name
        $this->leave_model->_primary_key = "id"; // $id
	
        //receive form input by post
        $data['employee_id'] 		= $this->input->post('employee_id');
        $data['sick_start_date'] 	= $this->input->post('sick_start_date');
        $data['sick_end_date'] 	    = $this->input->post('sick_end_date');
        $data['note'] 			    = $this->input->post('note');
		$data['employment_id'] 		= $this->input->post('employment_id');
        $data['status'] 		    = 'fully approved';
        $data['approve3']           = $this->session->userdata('full_name');
        $data['date_approve3']      = date('Y-m-d');
        $data['application_date']   = date('Y-m-d H:i:s');

        // skd File upload
        if (!empty($_FILES['skd']['name'])) {
            $old_path = $this->input->post('skd_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_model->uploadAllType('skd');
            $val == TRUE || redirect('admin/attendance/entry_sick_employee');
            $data['skd_filename'] = $val['fileName'];
            $data['skd'] = $val['path'];
            $data['skd_path'] = $val['fullPath'];
        }

        //save data in database
        $this->leave_model->save($data);

        // messages for user
        $type = "success";
        $message = "Application Successfully Submitted !";
        set_message($type, $message);
        redirect('admin/attendance/entry_sick_employee');

    }

   

    public function entry_sick_employee() {
		//DEBUG
        // $this->output->enable_profiler(TRUE);       
        $data['title'] = "Entry Sick Employee";

        //get leave category for dropdown
        $this->leave_model->_table_name = "tbl_sick_application"; // table name
        $this->leave_model->_order_by = "id"; // $id
        $data['all_sick_application'] = $this->leave_model->get_application_sick_info(); // get result
        // retrive all data from db
        // $data['all_leave_category_info'] = $this->settings_model->get();

        $employment_id = $this->session->userdata('employment_id');
        // $data['all_emplyee_info'] = $this->leave_model->all_emplyee_info($employment_id); // get result
        $data['all_emplyee_info'] = $this->leave_model->all_emplyee_info(); // get result
        $data['no_leave'] = $this->emp_model->get_no_leave();

        $data['subview'] = $this->load->view('admin/attendance/entry_sick_employee', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function manage_attendance() {
        //author By DSB        
        $data['title'] = "Set Attendance";
        $this->attendance_model->_table_name = "tbl_leave_category"; //table name
        $this->attendance_model->_order_by = "leave_category_id";
        $data['all_leave_category_info'] = $this->attendance_model->get();
        $this->attendance_model->_table_name = "tbl_department"; //table name
        $this->attendance_model->_order_by = "department_id";
        $data['all_department'] = $this->attendance_model->get();
        $data['department_id'] = $this->input->post('department_id');
        $data['date'] = $this->input->post('date', TRUE);
        $sbtnType = $this->input->post('sbtn');
        $flag = $this->session->userdata('flag');
        if ($sbtnType == 1 || $flag == 1) {
            if ($flag) {
                $data['date'] = $this->session->userdata('date');
                $data['department_id'] = $this->session->userdata('department_id');
                $this->session->unset_userdata('date');
                $this->session->unset_userdata('flag');
                $this->session->unset_userdata('department_id');
            } else {

                $data['date'] = $this->input->post('date');
                $data['department_id'] = $this->input->post('department_id');
            }
        }
        $data['employee_info'] = $this->attendance_model->get_employee_id_by_dept_id($data['department_id']);

        foreach ($data['employee_info'] as $v_employee) {
            $where = array('employee_id' => $v_employee->employee_id, 'date' => $data['date']);
            $data['atndnce'][] = $this->attendance_model->check_by($where, 'tbl_attendance');
        }
        $data['subview'] = $this->load->view('admin/attendance/manage_attendance', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }


    public function save_bulk_cuti() {
        $this->attendance_model->_table_name = "tbl_application_list"; // table name
        $this->attendance_model->_primary_key = "application_list_id"; // $id

        $employee_id            = $this->input->post('employee_id', TRUE);
        $employment_id 	        = $this->input->post('employment_id', TRUE);
        $leave_category_id      = $this->input->post('leave_category_id', TRUE);
        $leave_start_date       = $this->input->post('leave_start_date', TRUE);
        $leave_end_date         = $this->input->post('leave_end_date', TRUE);
        $reason                 = $this->input->post('reason', TRUE);

        if(!empty($employee_id)){
            $key = 0;
            foreach ($employee_id as $empID) {
                $data['leave_category_id']  = $leave_category_id;
                $data['leave_start_date'] 	= $leave_start_date;
                $data['leave_end_date']	    = $leave_end_date;
                $data['leave_status'] 	    = 'fully approved';
                $data['reason'] 		    = $reason; 
                $data['approve3']           = $this->session->userdata('full_name');
                $data['date_approve3']      = date('Y-m-d');
                $data['application_date']   = date('Y-m-d H:i:s');

                $data['employee_id']    = $empID;
                $data['employment_id']  = $employment_id[$key];

                $data['sisa_cuti'] =  $this->potong_cuti($leave_category_id, $leave_start_date, $leave_end_date, $empID);

                //save data in database
                $this->attendance_model->save($data);
                
                $key++;
            }

        }
       
        $arr_employment_id = json_encode($employment_id);
        // echo $arr_employment_id;
        // die;
        // messages for user
        $type = "success";
        $message = "Data NIK ".$arr_employment_id." Successfully Updated !";
        // echo $message;
        // die;

        set_message($type, $message);
        redirect('admin/attendance/bulk_update_cuti');

    }


    function potong_cuti($leave_category_id, $leave_start_date, $leave_end_date, $empID){

        //ambil data sisa cuti dari tabel;			
        $saldo_cuti 		= $this->attendance_model->cek_saldo_cuti($empID)[0]->saldo_cuti;		
        $sisa_cuti_thn_lalu = $this->attendance_model->cek_saldo_cuti($empID)[0]->sisa_cuti_thn_lalu;

        if($leave_category_id==3 || $leave_category_id==4 || $leave_category_id==14){
            if($leave_start_date == $leave_end_date){
                $jumlah_cuti = 1;
                //kurangi dari grace period saldo cuti dulu jika tdk sama dengan 0 
                if($sisa_cuti_thn_lalu > 0){
                    $update_saldo_cuti = $sisa_cuti_thn_lalu - $jumlah_cuti;
                }else{
                    $update_saldo_cuti = $saldo_cuti - $jumlah_cuti;
                }

            }else{
               
                // weekend sabtu/minggu tdk di hitung
                $awal_cuti = strtotime($leave_start_date);
                $akhir_cuti = strtotime($leave_end_date);
    
                $haricuti = array();
                $sabtuminggu = array();
    
                for ($i=$awal_cuti; $i <= $akhir_cuti; $i += (60 * 60 * 24)) {
                    if (date('w', $i) !== '0' && date('w', $i) !== '6') {
                        $haricuti[] = $i;
                    } else {
                        $sabtuminggu[] = $i;
                    }
                 
                }
                $jumlah_cuti        = count($haricuti);
                $jumlah_sabtuminggu = count($sabtuminggu);
                $abtotal            = $jumlah_cuti + $jumlah_sabtuminggu;
    
                // cek saldo grace period cuti jika tdk sama 0 kurangi di grace period dulu
                if($sisa_cuti_thn_lalu > 0){
                    $update_saldo_cuti = $sisa_cuti_thn_lalu - $jumlah_cuti;
                }else{
                    $update_saldo_cuti = $saldo_cuti - $jumlah_cuti;
                }
            }

        }else{
        
            if($hak_cuti!=0){
                //cek jumlah yg diajukan
                $k=1;
                for($l = $leave_start_date; $l <= $leave_end_date; $l++){
                    $jumlah_yg_diajukan = $k;
                    $k++;
                }
                if($jumlah_yg_diajukan > $hak_cuti){
                    $update_saldo_cuti 	= $saldo_cuti - ($jumlah_yg_diajukan - $hak_cuti);
                }else{
                    $update_saldo_cuti = $saldo_cuti;
                }
            }else{
                $update_saldo_cuti = $saldo_cuti;
            }
            
        }	

        // cek saldo grace period cuti jika tdk sama 0 kurangi di grace period dulu
        if($sisa_cuti_thn_lalu > 0){
            $update_employee['sisa_cuti_thn_lalu']  = $update_saldo_cuti;
            $update_employee['saldo_cuti']          = $update_saldo_cuti;
        }else{
            $update_employee['saldo_cuti']          = $update_saldo_cuti;
        }

        $sisa_cuti = $update_saldo_cuti;
        
        //Update saldo cuti karyawan
        $where = array('employee_id' => $empID);
        $this->employee_model->set_action($where, $update_employee, 'tbl_saldo_cuti');

        return $sisa_cuti;
    }

    public function save_attendance() {
        $this->attendance_model->_table_name = "tbl_attendance"; // table name
        $this->attendance_model->_primary_key = "attendance_id"; // $id                    
        $attendance_status = $this->input->post('attendance', TRUE);

        $leave_category_id = $this->input->post('leave_category_id', TRUE);

        $employee_id = $this->input->post('employee_id', TRUE);

        $attendance_id = $this->input->post('attendance_id', TRUE);
        if (!empty($attendance_id)) {
            $key = 0;
            foreach ($employee_id as $empID) {
                $data['date'] = $this->input->post('date', TRUE);
                $data['attendance_status'] = 0;
                $data['employee_id'] = $empID;
                if (!empty($leave_category_id[$key])) {
                    $data['leave_category_id'] = $leave_category_id[$key];
                } else {
                    $data['leave_category_id'] = NULL;
                }
                if (!empty($attendance_status)) {
                    foreach ($attendance_status as $v_status) {
                        if ($empID == $v_status) {
                            $data['attendance_status'] = 1;
                            $data['leave_category_id'] = NULL;
                        }
                    }
                }
                $id = $attendance_id[$key];
                if (!empty($id)) {
                    $this->attendance_model->save($data, $id);
                } else {
                    $this->attendance_model->save($data, $id);
                }

                $key++;
            }
        } else {
            $key = 0;

            foreach ($employee_id as $empID) {
                $data['date'] = $this->input->post('date', TRUE);
                $data['attendance_status'] = 0;
                $data['employee_id'] = $empID;
                if (!empty($leave_category_id[$key])) {
                    $data['leave_category_id'] = $leave_category_id[$key];
                } else {
                    $data['leave_category_id'] = NULL;
                }
                if (!empty($attendance_status)) {
                    foreach ($attendance_status as $v_status) {
                        if ($empID == $v_status) {
                            $data['attendance_status'] = 1;
                            $data['leave_category_id'] = NULL;
                        }
                    }
                }
                $this->attendance_model->save($data);
                $key++;
            }
        }
        $fdata['department_id'] = $this->input->post('department_id', TRUE);
        $fdata['date'] = $this->input->post('date');
        $fdata['flag'] = 1;
        $this->session->set_userdata($fdata);
        // messages for user        
        $type = "success";
        $message = "Attendance Information Successfully Saved!";
        set_message($type, $message);
        redirect('admin/attendance/manage_attendance'); //redirect page
    }


    public function attendance_report() {
        $data['title'] = "Attendance Report";
        $this->attendance_model->_table_name = "tbl_department"; //table name
        $this->attendance_model->_order_by = "department_id";
        $data['all_department'] = $this->attendance_model->get();

        $this->attendance_model->_table_name = "company"; //table name
        $this->attendance_model->_order_by = "id";
        $data['all_company'] = $this->attendance_model->get();

        $data['subview'] = $this->load->view('admin/attendance/attendance_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

   

    //page report overtime
    public function overtime_report() {
        $data['title'] = "Overtime Report";
        $this->attendance_model->_table_name = "tbl_department"; //table name
        $this->attendance_model->_order_by = "department_id";
        $data['all_department'] = $this->attendance_model->get();
        $data['subview'] = $this->load->view('admin/attendance/overtime_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }


    public function get_overtime_report() {
        //DEBUG
        //$this->output->enable_profiler(TRUE);      
        $department_id = $this->input->post('department_id', TRUE);
        
        $start_date     = $this->input->post('start_date', TRUE);
        $start_day      = date('d', strtotime($start_date));
        $start_month    = date('n', strtotime($start_date));
        $start_year     = date('Y', strtotime($start_date));
        $start_num      = cal_days_in_month(CAL_GREGORIAN, $start_month, $start_year);
        
        $end_date     = $this->input->post('end_date', TRUE);
        $end_day      = date('d', strtotime($end_date));
        $end_month    = date('n', strtotime($end_date));
        $end_year     = date('Y', strtotime($end_date));
        $end_num      = cal_days_in_month(CAL_GREGORIAN, $end_month, $end_year);
        

        $data['employee'] = $this->attendance_model->get_employee_id_by_dept_id($department_id);        
		
		$holidays = $this->global_model->get_holidays(); //tbl working Days Holiday
		$public_holiday = $this->global_model->get_public_holidays_2($start_date, $end_date);
		        
        //tbl a_calendar Days Holiday        
        if (!empty($public_holiday)) {
            foreach ($public_holiday as $p_holiday) {
                
                $current    = strtotime( $start_date );
                $last       = strtotime( $end_date );
                $step       = '+1 day';
                $format     = 'Y-m-d';
            
                while( $current <= $last ) {
            
                    $sdate   = date( $format, $current );
                    $current = strtotime( $step, $current );

                    if ($p_holiday->start_date == $sdate && $p_holiday->end_date == $sdate) {
                        $p_hday[] = $sdate;
                    }
                    if ($p_holiday->start_date == $sdate) {
                        for ($j = $p_holiday->start_date; $j <= $p_holiday->end_date; $j++) {
                            $p_hday[] = $j;
                        }
                    }
                }                    

            }
        }
		
					
		
		$current    = strtotime( $start_date );
        $last       = strtotime( $end_date );
        $step       = '+1 day';
        $format     = 'Y-m-d';
    
        while( $current <= $last ) {
    
            $sdate   = date( $format, $current );
            $current = strtotime( $step, $current );
			$day_name = date('l', strtotime($sdate));
			
			//TODO set tgl merah pada report overtime  
			//if (!empty($holidays)) {
				//foreach ($holidays as $v_holiday) {

					if ($day_name == 'Saturday' || $day_name == 'Sunday') {
						//$flag = 'L';
						$data['dateSl'][] = '<span style="padding:2px; 4px" class="label label-danger std_p">'. date('d', strtotime($sdate)) .'</span>';
					} else {
						$data['dateSl'][] = date('d', strtotime($sdate));
					}
				//}
			//}
				
        }




        foreach ($data['employee'] as $sl => $v_employee) {

                //Get flag overtime list for generate O to report
                $overtime_list = $this->attendance_model->get_overtime_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

                //tbl overtime_list       
                if (!empty($overtime_list)) {
                    foreach ($overtime_list as $p_overtime_list) {

                        $current    = strtotime( $start_date );
                        $last       = strtotime( $end_date );
                        $step       = '+1 day';
                        $format     = 'Y-m-d';
                    
                        while( $current <= $last ) {
                    
                            $sdate   = date( $format, $current );
                            $current = strtotime( $step, $current );

                            if ($p_overtime_list->overtime_date == $sdate && $p_overtime_list->overtime_date == $sdate) {
                                $p_overtime_list_date[] = $sdate;
                            }
                            if ($p_overtime_list->overtime_date == $sdate) {
                                for ($j = $p_overtime_list->overtime_date; $j <= $p_overtime_list->overtime_date; $j++) {
                                    $p_overtime_list_date[] = $j;
                                }
                            }
                        }                                                         

                    }
                }

				$key = 1;
				$x = 0;
            
                $current    = strtotime( $start_date );
                $last       = strtotime( $end_date );
                $step       = '+1 day';
                $format     = 'Y-m-d';
            
                while( $current <= $last ) {
            
                    $sdate   = date( $format, $current );
                    $current = strtotime( $step, $current );
                    $day_name = date('l', strtotime($sdate));
                    if (!empty($holidays)) {
                        foreach ($holidays as $v_holiday) {

                            if ($v_holiday->day == $day_name) {
                                $flag = 'L';
                            }
                        }
                    }
                    if (!empty($p_hday)) {
                        foreach ($p_hday as $v_hday) {
                            if ($v_hday == $sdate) {
                                $flag = 'L';
                            }
                        }
						
                    }
					
                 
                    //TODO set report overtime list from tabel overtime list    
                    if (!empty($p_overtime_list_date) && $p_overtime_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_overtime_list_date as $v_overtime_list_date) {                           
                            if ($v_overtime_list_date == $sdate) {                               
                                    $flag = 'O';                              
                            }
                        }
                    }

                    if (!empty($flag)) {
                        $data['overtime'][$sl][] = $this->attendance_model->overtime_report_by_empid_with_time($v_employee->employee_id, $sdate, $flag);
                    } else {
                        $data['overtime'][$sl][] = $this->attendance_model->overtime_report_by_empid_with_time($v_employee->employee_id, $sdate);
                    }

                    $key++;
                    $flag = '';

                    
                }

            }        
        $data['title'] = "Overtime Report";
        $this->attendance_model->_table_name = "tbl_department"; //table name
        $this->attendance_model->_order_by = "department_id";
        $data['all_department'] = $this->attendance_model->get();
        $data['department_id'] = $this->input->post('department_id', TRUE);
        $data['date'] = $this->input->post('date', TRUE);
        $where = array('department_id' => $department_id);
        $data['dept_name'] = $this->attendance_model->check_by($where, 'tbl_department');

        $data['month']      = date('F-Y', strtotime($start_year . '-' . $start_month));
        $data['start_date'] = date('d-F-Y', strtotime($start_date));
        $data['end_date']   = date('d-F-Y', strtotime($end_date));

        $data['subview'] = $this->load->view('admin/attendance/overtime_report', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
        
    }
	
	
	public function create_excel_overtime($department_id, $start_date, $end_date) {

        $start_day      = date('d', strtotime($start_date));
        $start_month    = date('n', strtotime($start_date));
        $start_year     = date('Y', strtotime($start_date));
        $start_num      = cal_days_in_month(CAL_GREGORIAN, $start_month, $start_year);
       
        $end_day      = date('d', strtotime($end_date));
        $end_month    = date('n', strtotime($end_date));
        $end_year     = date('Y', strtotime($end_date));
        $end_num      = cal_days_in_month(CAL_GREGORIAN, $end_month, $end_year);

        $employee = $this->attendance_model->get_employee_id_by_dept_id($department_id);       
        $where = array('department_id' => $department_id);
        $dept_name = $this->attendance_model->check_by($where, 'tbl_department');

        $current    = strtotime( $start_date );
        $last       = strtotime( $end_date );
        $step       = '+1 day';
        $format     = 'Y-m-d';
    
        while( $current <= $last ) {
    
            $sdate   = date( $format, $current );
            $current = strtotime( $step, $current );

            $dateSl[] = date('d', strtotime($sdate));
        }

        // print_r($dateSl);
        // die;    
        
        foreach ($employee as $sl => $v_employee) {
            		
            $key = 1;
            $x = 0;

            $current    = strtotime( $start_date );
            $last       = strtotime( $end_date );
            $step       = '+1 day';
            $format     = 'Y-m-d';
        
            while( $current <= $last ) {
        
            $sdate   = date( $format, $current );
            $current = strtotime( $step, $current );
	
                if (!empty($flag)) {
                    $attendance[$sl][] = $this->attendance_model->overtime_report_by_empid_with_time($v_employee->employee_id, $sdate, $flag);
                } else {
                    $attendance[$sl][] = $this->attendance_model->overtime_report_by_empid_with_time($v_employee->employee_id, $sdate);
					//$attendance[$sl][] = $this->attendance_model->overtime_report_by_empid_with_time($v_employee->employee_id, $sdate);
                }
                $key++;
                $flag = '';
            }
        }
        //load PHPExcel library
        $this->load->library('Excel');
        ob_start();
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $styleArray = array(
            'font' => array(
                'size' => 13,
                'name' => 'Verdana'
        ));
        $fontArray = array(
            'font' => array(
                'bold' => true,
                'size' => 11,
                'name' => 'Verdana'
        ));
        $dateArray = array(
            'font' => array(
                'bold' => true,
        ));
        $bgcolor = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'E7E7E7'),
        ));
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B' . '1', 'Periode:')
                ->setCellValue('D' . '1', date('d M Y', strtotime($start_date)).' s_d '.date('d M Y', strtotime($end_date)));
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B1:C1');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D1:H1');
        $objPHPExcel->getActiveSheet()->getStyle('B1:C1')->applyFromArray($fontArray);

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('J' . '1', 'Department:')
                ->setCellValue('N' . '1', $dept_name->department_name);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J1:M1');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('N1:V1');
        $objPHPExcel->getActiveSheet()->getStyle('J1:L1')->applyFromArray($fontArray);

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Comprehensive HRMS")
                ->setLastModifiedBy("Comprehensive HRMS")
                ->setTitle("Office  XLSX Test Document")
                ->setSubject("Office XLSX Test Document")
                ->setDescription("Test document for Office XLSX, generated by PHP classes.")
                ->setKeywords("office openxml php")
                ->setCategory("Excel Sheet");


        // Add some data                
        $cl = 'B';
        $bg = 'A';
        foreach ($dateSl as $edate) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($cl . '3', $edate);
            $objPHPExcel->getActiveSheet()->getColumnDimension($cl)->setWidth(3);
            $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->applyFromArray($dateArray);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '4')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '1')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '2')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '4')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '1')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '2')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $cl++;
        }
		$objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($cl . '3', 'Total Overtime Hours');
		$objPHPExcel->getActiveSheet()->getColumnDimension($cl)->setWidth(3);
            $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->applyFromArray($dateArray);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '4')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '1')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '2')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '4')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '1')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '2')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $row = 5;
        $c = 0;
        foreach ($attendance as $name => $v_Emp) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A3', 'Name')
                    ->setCellValue('A' . $row, $employee[$name]->first_name . ' ' . $employee[$name]->last_name);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A1')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
            $col = 1;
            $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle($row, $col)->getFont()->setSize(10);
            $total_lembur_jam = 0; $total_lembur_menit = 0;
			foreach ($v_Emp as $v_result) {
                //if (!empty($v_result)) {
                    
                    foreach ($v_result as $Emp_atndnce) {
													  
						 $awal  = strtotime($Emp_atndnce->overtime_start_time);
						 $akhir = strtotime($Emp_atndnce->overtime_end_time);
						 $diff  = $akhir - $awal;

						 $jam   = floor($diff / (60 * 60));
						 $menit = $diff - $jam * (60 * 60);
						 //echo 'Waktu tinggal: ' . $jam .  ' jam, ' . floor( $menit / 60 ) . ' menit';
						 $total_lembur_jam += $jam;
						 $total_lembur_menit += $menit;
						
                        
                        if(!empty($Emp_atndnce->overtime_start_time)){
							$overtime_start_time 	= date('h:i A', strtotime($Emp_atndnce->overtime_start_time));      	
							$overtime_end_time 		= date('h:i A',strtotime($Emp_atndnce->overtime_end_time));                        
						}else{
							$overtime_start_time 	= '';
							$overtime_end_time 		= '';
						}
                        $overtime = $overtime_start_time.' - '.$overtime_end_time;
						
						//if($overtime_start_time != ''){
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValueByColumnAndRow($col, $row, $overtime);
                        //}    
                            if ($Emp_atndnce->StatusAbsen == 'L') {
                                $objPHPExcel->setActiveSheetIndex(0)
                                        ->setCellValueByColumnAndRow($col, $row, 'L');
                            }

                           
                    }        
                    $col++;
					
                //}
               
            }
			$objPHPExcel->setActiveSheetIndex(0)
                                        ->setCellValueByColumnAndRow($col, $row, $total_lembur_jam .' jam, '. floor($total_lembur_menit / 60) .' menit ');
			
			//
			
			//
			
            $row++;
            $c++;
        }
        // Rename worksheet (worksheet, not filename)
        $objPHPExcel->getActiveSheet()->setTitle('Employee Overtime');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        //clean the output buffer
        ob_end_clean();
        
        $filename = date('d M Y', strtotime($start_date)).' s_d '.date('d M Y', strtotime($end_date)) . '  ' . 'Employee Overtime.xls'; //save our workbook as this file name
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

	
	public function create_pdf_overtime($department_id, $start_date, $end_date) {
	
        $data['employee'] = $this->attendance_model->get_employee_id_by_dept_id($department_id);

        //$start_date     = $this->input->post('start_date', TRUE);
        $start_day      = date('d', strtotime($start_date));
        $start_month    = date('n', strtotime($start_date));
        $start_year     = date('Y', strtotime($start_date));
        $start_num      = cal_days_in_month(CAL_GREGORIAN, $start_month, $start_year);
       
        //$end_date     = $this->input->post('end_date', TRUE);
        $end_day      = date('d', strtotime($end_date));
        $end_month    = date('n', strtotime($end_date));
        $end_year     = date('Y', strtotime($end_date));
        $end_num      = cal_days_in_month(CAL_GREGORIAN, $end_month, $end_year);

       
        $current    = strtotime( $start_date );
        $last       = strtotime( $end_date );
        $step       = '+1 day';
        $format     = 'Y-m-d';
    
        while( $current <= $last ) {
    
            $sdate   = date( $format, $current );
            $current = strtotime( $step, $current );

            $data['dateSl'][] = date('d', strtotime($sdate));
        }

        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday
        
        $public_holiday = $this->global_model->get_public_holidays($start_date, $end_date);
        
        if (!empty($public_holiday)) {
            //tbl a_calendar Days Holiday        
            foreach ($public_holiday as $p_holiday) {
               
                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {
                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );
            
                        if ($p_holiday->start_date == $sdate && $p_holiday->end_date == $sdate) {
                            $p_hday[] = $sdate;
                        }
                        if ($p_holiday->start_date == $sdate) {
                            for ($j = $p_holiday->start_date; $j <= $p_holiday->end_date; $j++) {
                                $p_hday[] = $j;
                            }
                        }
                    }

            }
        }
        foreach ($data['employee'] as $sl => $v_employee) {

                $key = 1;
                $x = 0;
           
                $current    = strtotime( $start_date );
                $last       = strtotime( $end_date );
                $step       = '+1 day';
                $format     = 'Y-m-d';
            
                while( $current <= $last ) {
            
                    $sdate   = date( $format, $current );
                    $current = strtotime( $step, $current );
        
                    if (!empty($flag)) {
                        $data['overtime'][$sl][] = $this->attendance_model->overtime_report_by_empid_with_time($v_employee->employee_id, $sdate, $flag);
                    } else {
                        $data['overtime'][$sl][] = $this->attendance_model->overtime_report_by_empid_with_time($v_employee->employee_id, $sdate);
                    }
                    $key++;
                    $flag = '';
                }  
            
        }
        $where = array('department_id' => $department_id);
        $data['dept_name'] = $this->attendance_model->check_by($where, 'tbl_department');
        $data['start_date'] = date('d M Y', strtotime($start_date));
        $data['end_date'] = date('d M Y', strtotime($end_date));
        $this->load->helper('dompdf');
        $view_file = $this->load->view('admin/attendance/overtime_report_pdf', $data, true);
        $file_name = pdf_create($view_file, date('d-F-Y', strtotime($start_date)).' s/d '.date('d-F-Y', strtotime($end_date)));
        echo $file_name;
    }

    public function getDepartment(){
        $id = $this->input->post('id');
        $getDepartment = $this->attendance_model->getDepartmentByCompanyId($id);
        $list = "<option></option>";
        foreach($getDepartment as $row){
            $list .= "<option value='".$row['department_id']."'>".$row['department_name']."";
        }
        echo json_encode($list);
    }

    public function get_report() {
        //DEBUG
        // $this->output->enable_profiler(TRUE);
        if (isset($_POST['Search1'])) {
            $department_id = $this->input->post('department_id', TRUE);
            $date = $this->input->post('date', TRUE);
            $month = date('n', strtotime($date));
            $year = date('Y', strtotime($date));
            $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);

            $data['employee'] = $this->attendance_model->get_employee_id_by_dept_id($department_id);
            $day = date('d', strtotime($date));
            for ($i = 1; $i <= $num; $i++) {
                $data['dateSl'][] = $i;
            }
            $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday

            if ($month >= 1 && $month <= 9) {
                $yymm = $year . '-' . '0' . $month;
            } else {
                $yymm = $year . '-' . $month;
            }

           $public_holiday = $this->global_model->get_public_holidays($yymm);


            //tbl a_calendar Days Holiday        
            if (!empty($public_holiday)) {
                foreach ($public_holiday as $p_holiday) {
                    for ($k = 1; $k <= $num; $k++) {

                        if ($k >= 1 && $k <= 9) {
                            $sdate = $yymm . '-' . '0' . $k;
                        } else {
                            $sdate = $yymm . '-' . $k;
                        }

                        if ($p_holiday->start_date == $sdate && $p_holiday->end_date == $sdate) {
                            $p_hday[] = $sdate;
                        }
                        if ($p_holiday->start_date == $sdate) {
                            for ($j = $p_holiday->start_date; $j <= $p_holiday->end_date; $j++) {
                                $p_hday[] = $j;
                            }
                        }
                    }
                }
            }
            foreach ($data['employee'] as $sl => $v_employee) {
                $key = 1;
                $x = 0;
                for ($i = 1; $i <= $num; $i++) {

                    if ($i >= 1 && $i <= 9) {

                        $sdate = $yymm . '-' . '0' . $i;
                    } else {
                        $sdate = $yymm . '-' . $i;
                    }
                    $day_name = date('l', strtotime("+$x days", strtotime($year . '-' . $month . '-' . $key)));



                    if (!empty($holidays)) {
                        foreach ($holidays as $v_holiday) {

                            if ($v_holiday->day == $day_name) {
                                $flag = 'L';
                            }
                        }
                    }
                    if (!empty($p_hday)) {
                        foreach ($p_hday as $v_hday) {
                            if ($v_hday == $sdate) {
                                $flag = 'L';
                            }
                        }
                    }
                    if (!empty($flag)) {
                        $data['attendance'][$sl][] = $this->attendance_model->attendance_report_by_empid($v_employee->employee_id, $sdate, $flag);
                    } else {
                        $data['attendance'][$sl][] = $this->attendance_model->attendance_report_by_empid($v_employee->employee_id, $sdate);
                    }

                    $key++;
                    $flag = '';
                }
            }        
            $data['title'] = "Attendance Report";
            $this->attendance_model->_table_name    = "tbl_department"; //table name
            $this->attendance_model->_order_by      = "department_id";
            $data['all_department'] = $this->attendance_model->get();
            $data['department_id']  = $this->input->post('department_id', TRUE);
            $data['date']           = $this->input->post('date', TRUE);
            $where                  = array('department_id' => $department_id);
            $data['dept_name']      = $this->attendance_model->check_by($where, 'tbl_department');

            $data['month'] = date('F-Y', strtotime($yymm));
            $data['subview'] = $this->load->view('admin/attendance/attendance_report', $data, TRUE);
            $this->load->view('admin/_layout_main', $data);
			
		//Report attendance v2
        }elseif(isset($_POST['Search2'])){
            //$this->output->enable_profiler(TRUE);
            $company_id = $this->input->post('company_id', TRUE);
            $department_id = $this->input->post('department_id', TRUE);
            $start_date     = $this->input->post('start_date', TRUE);
            $start_day      = date('d', strtotime($start_date));
            $start_month    = date('n', strtotime($start_date));
            $start_year     = date('Y', strtotime($start_date));
            $start_num      = cal_days_in_month(CAL_GREGORIAN, $start_month, $start_year);
           
            $end_date     = $this->input->post('end_date', TRUE);
            $end_day      = date('d', strtotime($end_date));
            $end_month    = date('n', strtotime($end_date));
            $end_year     = date('Y', strtotime($end_date));
            $end_num      = cal_days_in_month(CAL_GREGORIAN, $end_month, $end_year);
          
            $data['employee'] = $this->attendance_model->get_employee_id_by_dept_id($department_id);
            
            $current    = strtotime( $start_date );
            $last       = strtotime( $end_date );
            $step       = '+1 day';
            $format     = 'Y-m-d';
        
            while( $current <= $last ) {
        
                $sdate   = date( $format, $current );
                $current = strtotime( $step, $current );

                $data['dateSl'][] = date('d', strtotime($sdate));
            }

          
            $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday
            $public_holiday = $this->global_model->get_public_holidays_2($start_date, $end_date);
            
            //tbl a_calendar Days Holiday        
            if (!empty($public_holiday)) {
                foreach ($public_holiday as $p_holiday) {                  
                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );

                        if ($p_holiday->start_date == $sdate && $p_holiday->end_date == $sdate) {
                            $p_hday[] = $sdate;
                        }
                        if ($p_holiday->start_date == $sdate) {
                            for ($j = $p_holiday->start_date; $j <= $p_holiday->end_date; $j++) {
                                $p_hday[] = $j;
                            }
                        }
                    }

                }
            }


            foreach ($data['employee'] as $sl => $v_employee) {

                    //Get no show data list for generate L to report
                    $noshow_list = $this->attendance_model->get_noshow_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);       
                    if (!empty($noshow_list)) {
                        foreach ($noshow_list as $p_noshow_list) {
                            $current    = strtotime( $start_date );
                            $last       = strtotime( $end_date );
                            $step       = '+1 day';
                            $format     = 'Y-m-d';                        
                            while( $current <= $last ) {                        
                                $sdate   = date( $format, $current );
                                $current = strtotime( $step, $current );

                                if ($p_noshow_list->leave_start_date == $sdate && $p_noshow_list->leave_end_date == $sdate) {
                                    $p_noshow_list_date[] = $sdate;
                                }
                                if ($p_noshow_list->leave_start_date == $sdate) {
                                    for ($j = $p_noshow_list->leave_start_date; $j <= $p_noshow_list->leave_end_date; $j++) {
                                        $p_noshow_list_date[] = $j;
                                    }
                                }
                            }
                        }
                    }

                    //Get flag application list for generate L to report
                    $application_list = $this->attendance_model->get_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);
                    //tbl application_list       
                    if (!empty($application_list)) {
                        foreach ($application_list as $p_application_list) {
                            $current    = strtotime( $start_date );
                            $last       = strtotime( $end_date );
                            $step       = '+1 day';
                            $format     = 'Y-m-d';                        
                            while( $current <= $last ) {                        
                                $sdate   = date( $format, $current );
                                $current = strtotime( $step, $current );

                                if ($p_application_list->leave_start_date == $sdate && $p_application_list->leave_end_date == $sdate) {
                                    $p_application_list_date[] = $sdate;
                                }
                                if ($p_application_list->leave_start_date == $sdate) {
                                    for ($j = $p_application_list->leave_start_date; $j <= $p_application_list->leave_end_date; $j++) {
                                        $p_application_list_date[] = $j;
                                    }
                                }
                            }
                        }
                    }


                    //Get flag permit list for generate P to report
                    $permit_list = $this->attendance_model->get_permit_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);
                    //tbl permit_list       
                    if (!empty($permit_list)) {
                        foreach ($permit_list as $p_permit_list) {

                            $current    = strtotime( $start_date );
                            $last       = strtotime( $end_date );
                            $step       = '+1 day';
                            $format     = 'Y-m-d';
                        
                            while( $current <= $last ) {
                        
                                $sdate   = date( $format, $current );
                                $current = strtotime( $step, $current );

                                if ($p_permit_list->permit_date == $sdate && $p_permit_list->permit_date == $sdate) {
                                    $p_permit_list_date[] = $sdate;
                                }
                                if ($p_permit_list->permit_date == $sdate) {
                                    for ($j = $p_permit_list->permit_date; $j <= $p_permit_list->permit_date; $j++) {
                                        $p_permit_list_date[] = $j;
                                    }
                                }
                            }                                                         

                        }
                    }


                    //Get flag overtime list for generate O to report
                    $overtime_list = $this->attendance_model->get_overtime_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);
                    //tbl overtime_list       
                    if (!empty($overtime_list)) {
                        foreach ($overtime_list as $p_overtime_list) {

                            $current    = strtotime( $start_date );
                            $last       = strtotime( $end_date );
                            $step       = '+1 day';
                            $format     = 'Y-m-d';
                        
                            while( $current <= $last ) {
                        
                                $sdate   = date( $format, $current );
                                $current = strtotime( $step, $current );

                                if ($p_overtime_list->overtime_date == $sdate && $p_overtime_list->overtime_date == $sdate) {
                                    $p_overtime_list_date[] = $sdate;
                                }
                                if ($p_overtime_list->overtime_date == $sdate) {
                                    for ($j = $p_overtime_list->overtime_date; $j <= $p_overtime_list->overtime_date; $j++) {
                                        $p_overtime_list_date[] = $j;
                                    }
                                }
                            }                                                         

                        }
                    }

                    //Get flag sakit utk generate S to report
                    $sick_list = $this->attendance_model->get_sick_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);
                    //tbl sick_application_list       
                    if (!empty($sick_list)) {
                        foreach ($sick_list as $p_sick_list) {

                            $current    = strtotime( $start_date );
                            $last       = strtotime( $end_date );
                            $step       = '+1 day';
                            $format     = 'Y-m-d';
                        
                            while( $current <= $last ) {
                        
                                $sdate   = date( $format, $current );
                                $current = strtotime( $step, $current );

                                if ($p_sick_list->sick_start_date == $sdate && $p_sick_list->sick_end_date == $sdate) {
                                    $p_sick_list_date[] = $sdate;
                                }
                                if ($p_sick_list->sick_start_date == $sdate) {
                                    for ($j = $p_sick_list->sick_start_date; $j <= $p_sick_list->sick_end_date; $j++) {
                                        $p_sick_list_date[] = $j;
                                    }
                                }
                            }                                                         

                        }
                    }

                    //Get flag perdin utk generate D to report
                    $perdin_list = $this->attendance_model->get_perdin_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);
                    //tbl sick_application_list       
                    if (!empty($perdin_list)) {
                        foreach ($perdin_list as $p_perdin_list) {

                            $current    = strtotime( $start_date );
                            $last       = strtotime( $end_date );
                            $step       = '+1 day';
                            $format     = 'Y-m-d';
                        
                            while( $current <= $last ) {
                        
                                $sdate   = date( $format, $current );
                                $current = strtotime( $step, $current );

                                if ($p_perdin_list->perdin_start_date == $sdate && $p_perdin_list->perdin_end_date == $sdate) {
                                    $p_perdin_list_date[] = $sdate;
                                }
                                if ($p_perdin_list->perdin_start_date == $sdate) {
                                    for ($j = $p_perdin_list->perdin_start_date; $j <= $p_perdin_list->perdin_end_date; $j++) {
                                        $p_perdin_list_date[] = $j;
                                    }
                                }
                            }                                                         

                        }
                    }

                $key = 1;
                $x = 0;
               
                $current    = strtotime( $start_date );
                $last       = strtotime( $end_date );
                $step       = '+1 day';
                $format     = 'Y-m-d';
            
                while( $current <= $last ) {
                    
                    $sdate   = date( $format, $current );
                    $current = strtotime( $step, $current );
                    // $day_name = date('l', strtotime("+$x days", strtotime($start_year . '-' . $start_month . '-' .intval($day) )));
                    $day_name = date('l', strtotime($sdate));

                    //TODO set report noshow list from tabel noshow list    
                    if (!empty($p_noshow_list_date) && $p_noshow_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_noshow_list_date as $v_noshow_list_date) {                           
                            if ($v_noshow_list_date == $sdate) {
                                // No Show
                                $flag = 'N';
                            }
                        }
                    }

                    //TODO set report application list from tabel application list    
                    if (!empty($p_application_list_date) && $p_application_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_application_list_date as $v_application_list_date) {                           
                            if($v_application_list_date == $sdate){
                                $flag = 'C';
                            }
                        }
                    }

                    //TODO set report permit list from tabel permit list    
                    if (!empty($p_permit_list_date) && $p_permit_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_permit_list_date as $v_permit_list_date) {                           
                            if ($v_permit_list_date == $sdate) {                               
                                if($p_permit_list->permit_category_id == 3){
                                    // WFH
                                    $flag = 'W';
                                } else{
                                    $flag = 'I';                              
                                }                           
                            }
                        }
                    }

                    //TODO set report overtime list from tabel overtime list    
                    if (!empty($p_overtime_list_date) && $p_overtime_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_overtime_list_date as $v_overtime_list_date) {                           
                            if ($v_overtime_list_date == $sdate) {                               
                                    $flag = 'O';                              
                            }
                        }
                    }

                    //TODO set report sick list from tabel sick application list    
                    if (!empty($p_sick_list_date) && $p_sick_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_sick_list_date as $v_sick_list_date) {                           
                            if ($v_sick_list_date == $sdate) {                               
                                    $flag = 'S';                              
                            }
                        }
                    }

                    //TODO set report perdin list from tabel perdin list    
                    if (!empty($p_perdin_list_date) && $p_perdin_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_perdin_list_date as $v_perdin_list_date) {                           
                            if ($v_perdin_list_date == $sdate) {                               
                                    $flag = 'D';                              
                            }
                        }
                    }

                    if (!empty($holidays)) {
                        foreach ($holidays as $v_holiday) {
                            if ($v_holiday->day == $day_name) {
                                $flag = 'L';
                            }
                        }
                    }

                    if (!empty($p_hday)) {
                        foreach ($p_hday as $v_hday) {
                            if ($v_hday == $sdate) {
                                $flag = 'L';
                            }
                        }
                    }

                    if (!empty($flag)) {
                        $data['attendance_v2'][$sl][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                    } else {
                        $data['attendance_v2'][$sl][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                    }

                    $key++;
                    $flag = '';

                }  

            }  
                 
            $data['title']                          = "Attendance Report";
            $this->attendance_model->_table_name    = "tbl_department"; //table name
            $this->attendance_model->_order_by      = "department_id";
            $data['all_department']                 = $this->attendance_model->get();

            $this->attendance_model->_table_name    = "company"; //table name
            $this->attendance_model->_order_by      = "id";
            $data['all_company']                    = $this->attendance_model->get();
            
            $data['company_id']                     = $this->input->post('company_id', TRUE);
            $data['department_id']                  = $this->input->post('department_id', TRUE);
            $data['date']                           = $this->input->post('date', TRUE);
            $where                                  = array('department_id' => $department_id);
            $data['dept_name']                      = $this->attendance_model->check_by($where, 'tbl_department');

            $data['month']      = date('F-Y', strtotime($start_year . '-' . $start_month));
            $data['start_date'] = date('d-F-Y', strtotime($start_date));
            $data['end_date']   = date('d-F-Y', strtotime($end_date));

            $data['subview'] = $this->load->view('admin/attendance/attendance_report', $data, TRUE);
            $this->load->view('admin/_layout_main', $data);
        }
    }


    public function attendance_report_by_emp() {
        $data['title'] = "Attendance Report By Employee";
        $this->attendance_model->_table_name = "tbl_employee"; //table name
        $this->attendance_model->_order_by = "employee_id";
        $data['all_employee'] = $this->attendance_model->get_by(array('status' => 1,), FALSE);
        $data['subview'] = $this->load->view('admin/attendance/attendance_report_by_emp', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }



    public function get_att_report_by_emp() {
        //DEBUG
        //$this->output->enable_profiler(TRUE);
        if (isset($_POST['Search1'])) {
            
			
		//Report attendance v2
        }elseif(isset($_POST['Search2'])){
            //DEBUG
            //$this->output->enable_profiler(TRUE);
            $employee_id = $this->input->post('employee_id', TRUE);
            
            $start_date     = $this->input->post('start_date', TRUE);
            $start_day      = date('d', strtotime($start_date));
            $start_month    = date('n', strtotime($start_date));
            $start_year     = date('Y', strtotime($start_date));
            $start_num      = cal_days_in_month(CAL_GREGORIAN, $start_month, $start_year);
           
            $end_date     = $this->input->post('end_date', TRUE);
            $end_day      = date('d', strtotime($end_date));
            $end_month    = date('n', strtotime($end_date));
            $end_year     = date('Y', strtotime($end_date));
            $end_num      = cal_days_in_month(CAL_GREGORIAN, $end_month, $end_year);
            

            // $data['employee'] = $this->attendance_model->get_employee_id_by_dept_id($department_id);

            $data['employee']    = $this->attendance_model->get_employee($employee_id);
            

            //$day = date('d', strtotime($date));
            //$total_day = $start_num + $end_day;
            // for ($i = $start_day; $i <= $start_num; $i++) {
            //     $data['dateSl'][] = $i;
            // }
           
            // for ($d = 1; $d <= $end_day; $d++) {
            //     $data['dateSl'][] = $d;
            // }

            $current    = strtotime( $start_date );
            $last       = strtotime( $end_date );
            $step       = '+1 day';
            $format     = 'Y-m-d';
        
            while( $current <= $last ) {
        
                $sdate   = date( $format, $current );
                $current = strtotime( $step, $current );
                //echo date('m', $sdate)
                if(date('m', strtotime($sdate))=='01'){
                    $data['dateSl']['Jan'][] = date('d', strtotime($sdate));
                }elseif(date('m', strtotime($sdate))=='02'){
                    $data['dateSl']['Feb'][] = date('d', strtotime($sdate));
                }elseif(date('m', strtotime($sdate))=='03'){
                    $data['dateSl']['Mar'][] = date('d', strtotime($sdate));
                }elseif(date('m', strtotime($sdate))=='04'){
                    $data['dateSl']['Apr'][] = date('d', strtotime($sdate));
                }elseif(date('m', strtotime($sdate))=='05'){
                    $data['dateSl']['May'][] = date('d', strtotime($sdate));
                }elseif(date('m', strtotime($sdate))=='06'){
                    $data['dateSl']['Jun'][] = date('d', strtotime($sdate));
                }elseif(date('m', strtotime($sdate))=='07'){
                    $data['dateSl']['Jul'][] = date('d', strtotime($sdate));
                }elseif(date('m', strtotime($sdate))=='08'){
                    $data['dateSl']['Aug'][] = date('d', strtotime($sdate));
                }elseif(date('m', strtotime($sdate))=='09'){
                    $data['dateSl']['Sep'][] = date('d', strtotime($sdate));
                }elseif(date('m', strtotime($sdate))=='10'){
                    $data['dateSl']['Oct'][] = date('d', strtotime($sdate));
                }elseif(date('m', strtotime($sdate))=='11'){
                    $data['dateSl']['Nov'][] = date('d', strtotime($sdate));
                }elseif(date('m', strtotime($sdate))=='12'){
                    $data['dateSl']['Dec'][] = date('d', strtotime($sdate));
                }
            }
            //  echo '<pre>';
            //  print_r($data['dateSl']);
            //  echo '</pre>';
             //die;

            $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday
            $public_holiday = $this->global_model->get_public_holidays_2($start_date, $end_date);
            
            //tbl a_calendar Days Holiday        
            if (!empty($public_holiday)) {
                foreach ($public_holiday as $p_holiday) {

                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {
                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );

                        if ($p_holiday->start_date == $sdate && $p_holiday->end_date == $sdate) {
                            $p_hday[] = $sdate;
                        }
                        if ($p_holiday->start_date == $sdate) {
                            for ($j = $p_holiday->start_date; $j <= $p_holiday->end_date; $j++) {
                                $p_hday[] = $j;
                            }
                        }
                    }
                }
            }


            foreach ($data['employee'] as $sl => $v_employee) {

                    //Get flag application list for generate L to report
                    $application_list = $this->attendance_model->get_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

                    //tbl application_list       
                    if (!empty($application_list)) {
                        foreach ($application_list as $p_application_list) {

                            $current    = strtotime( $start_date );
                            $last       = strtotime( $end_date );
                            $step       = '+1 day';
                            $format     = 'Y-m-d';
                        
                            while( $current <= $last ) {
                        
                                $sdate   = date( $format, $current );
                                $current = strtotime( $step, $current );

                                if ($p_application_list->leave_start_date == $sdate && $p_application_list->leave_end_date == $sdate) {
                                    $p_application_list_date[] = $sdate;
                                }
                                if ($p_application_list->leave_start_date == $sdate) {
                                    for ($j = $p_application_list->leave_start_date; $j <= $p_application_list->leave_end_date; $j++) {
                                        $p_application_list_date[] = $j;
                                    }
                                }
                            }
                        }
                    }


                    //Get flag permit list for generate P to report
                    $permit_list = $this->attendance_model->get_permit_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

                    //tbl permit_list       
                    if (!empty($permit_list)) {
                        foreach ($permit_list as $p_permit_list) {

                            $current    = strtotime( $start_date );
                            $last       = strtotime( $end_date );
                            $step       = '+1 day';
                            $format     = 'Y-m-d';
                        
                            while( $current <= $last ) {
                        
                                $sdate   = date( $format, $current );
                                $current = strtotime( $step, $current );

                                if ($p_permit_list->permit_date == $sdate && $p_permit_list->permit_date == $sdate) {
                                    $p_permit_list_date[] = $sdate;
                                }
                                if ($p_permit_list->permit_date == $sdate) {
                                    for ($j = $p_permit_list->permit_date; $j <= $p_permit_list->permit_date; $j++) {
                                        $p_permit_list_date[] = $j;
                                    }
                                }
                            }                                                         

                        }
                    }


                    //Get flag overtime list for generate O to report
                    $overtime_list = $this->attendance_model->get_overtime_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

                    //tbl overtime_list       
                    if (!empty($overtime_list)) {
                        foreach ($overtime_list as $p_overtime_list) {

                            $current    = strtotime( $start_date );
                            $last       = strtotime( $end_date );
                            $step       = '+1 day';
                            $format     = 'Y-m-d';
                        
                            while( $current <= $last ) {
                        
                                $sdate   = date( $format, $current );
                                $current = strtotime( $step, $current );

                                if ($p_overtime_list->overtime_date == $sdate && $p_overtime_list->overtime_date == $sdate) {
                                    $p_overtime_list_date[] = $sdate;
                                }
                                if ($p_overtime_list->overtime_date == $sdate) {
                                    for ($j = $p_overtime_list->overtime_date; $j <= $p_overtime_list->overtime_date; $j++) {
                                        $p_overtime_list_date[] = $j;
                                    }
                                }
                            }                                                         

                        }
                    }

                    //Get flag sakit utk generate S to report
                    $sick_list = $this->attendance_model->get_sick_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

                    //tbl sick_application_list       
                    if (!empty($sick_list)) {
                        foreach ($sick_list as $p_sick_list) {

                            $current    = strtotime( $start_date );
                            $last       = strtotime( $end_date );
                            $step       = '+1 day';
                            $format     = 'Y-m-d';
                        
                            while( $current <= $last ) {
                        
                                $sdate   = date( $format, $current );
                                $current = strtotime( $step, $current );

                                if ($p_sick_list->sick_start_date == $sdate && $p_sick_list->sick_end_date == $sdate) {
                                    $p_sick_list_date[] = $sdate;
                                }
                                if ($p_sick_list->sick_start_date == $sdate) {
                                    for ($j = $p_sick_list->sick_start_date; $j <= $p_sick_list->sick_end_date; $j++) {
                                        $p_sick_list_date[] = $j;
                                    }
                                }
                            }                                                         

                        }
                    }


           
                $key = 1;
                $x = 0;
               

                $current    = strtotime( $start_date );
                $last       = strtotime( $end_date );
                $step       = '+1 day';
                $format     = 'Y-m-d';
            
                while( $current <= $last ) {
            
                    $sdate   = date( $format, $current );
                    $current = strtotime( $step, $current );
                   
                    $day_name = date('l', strtotime($sdate));
                    
                    //TODO set report application list from tabel application list    
                    if (!empty($p_application_list_date) && $p_application_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_application_list_date as $v_application_list_date) {                           
                            if ($v_application_list_date == $sdate) {                               
                                    $flag = 'C';                              
                            }
                        }
                    }

                        //TODO set report permit list from tabel permit list    
                        if (!empty($p_permit_list_date) && $p_permit_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_permit_list_date as $v_permit_list_date) {                           
                            if ($v_permit_list_date == $sdate) {                               
                                    $flag = 'I';                              
                            }
                        }
                    }

                    //TODO set report overtime list from tabel overtime list    
                    if (!empty($p_overtime_list_date) && $p_overtime_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_overtime_list_date as $v_overtime_list_date) {                           
                            if ($v_overtime_list_date == $sdate) {                               
                                    $flag = 'O';                              
                            }
                        }
                    }


                     //TODO set report sick list from tabel sick application list    
                     if (!empty($p_sick_list_date) && $p_sick_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_sick_list_date as $v_sick_list_date) {                           
                            if ($v_sick_list_date == $sdate) {                               
                                    $flag = 'S';                              
                            }
                        }
                    }

                    if (!empty($holidays)) {
                        foreach ($holidays as $v_holiday) {

                            if ($v_holiday->day == $day_name) {
                                $flag = 'L';
                            }
                        }
                    }
                    if (!empty($p_hday)) {
                        foreach ($p_hday as $v_hday) {
                            if ($v_hday == $sdate) {
                                $flag = 'L';
                            }
                        }
                    }
                  

                    if (!empty($flag)) {
                        if(date('m',strtotime($sdate))=='01'){
                            $data['attendance_v2'][$sl]['Jan'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='02'){
                            $data['attendance_v2'][$sl]['Feb'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='03'){
                            $data['attendance_v2'][$sl]['Mar'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='04'){
                            $data['attendance_v2'][$sl]['Apr'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='05'){
                            $data['attendance_v2'][$sl]['May'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='06'){
                            $data['attendance_v2'][$sl]['Jun'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='07'){
                            $data['attendance_v2'][$sl]['Jul'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='08'){
                            $data['attendance_v2'][$sl]['Aug'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='09'){
                            $data['attendance_v2'][$sl]['Sep'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='10'){
                            $data['attendance_v2'][$sl]['Oct'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='11'){
                            $data['attendance_v2'][$sl]['Nov'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='12'){
                            $data['attendance_v2'][$sl]['Dec'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }                       
                    } else {

                        if(date('m',strtotime($sdate))=='01'){
                            $data['attendance_v2'][$sl]['Jan'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='02'){
                            $data['attendance_v2'][$sl]['Feb'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='03'){
                            $data['attendance_v2'][$sl]['Mar'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='04'){
                            $data['attendance_v2'][$sl]['Apr'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='05'){
                            $data['attendance_v2'][$sl]['May'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='06'){
                            $data['attendance_v2'][$sl]['Jun'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='07'){
                            $data['attendance_v2'][$sl]['Jul'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='08'){
                            $data['attendance_v2'][$sl]['Aug'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='09'){
                            $data['attendance_v2'][$sl]['Sep'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='10'){
                            $data['attendance_v2'][$sl]['Oct'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='11'){
                            $data['attendance_v2'][$sl]['Nov'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='12'){
                            $data['attendance_v2'][$sl]['Dec'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }                    
                    }

                    $key++;
                    $flag = '';

                    
                }      
            }

            $data['title']                          = "Attendance Report";
            $this->attendance_model->_table_name    = "tbl_employee"; //table name
            $this->attendance_model->_order_by      = "employee_id";
            $data['all_employee']                   = $this->attendance_model->get_by(array('status' => 1,), FALSE);
            $data['employee_id']                    = $this->input->post('employee_id', TRUE);
            $data['date']                           = $this->input->post('date', TRUE);
            $data['month']      = date('F-Y', strtotime($start_year . '-' . $start_month));
            $data['start_date'] = date('d-F-Y', strtotime($start_date));
            $data['end_date']   = date('d-F-Y', strtotime($end_date));

            $data['subview'] = $this->load->view('admin/attendance/attendance_report_by_emp', $data, TRUE);
            $this->load->view('admin/_layout_main', $data);
        }
    }


    public function create_pdf_att_report_by_emp($employee_id, $start_date, $end_date) {
        //DEBUG
        //$this->output->enable_profiler(TRUE);
        $data['employee']    = $this->attendance_model->get_employee($employee_id);
       
        $current    = strtotime( $start_date );
        $last       = strtotime( $end_date );
        $step       = '+1 day';
        $format     = 'Y-m-d';
    
        while( $current <= $last ) {
    
            $sdate   = date( $format, $current );
            $current = strtotime( $step, $current );
            //echo date('m', $sdate)
            if(date('m', strtotime($sdate))=='01'){
                $data['dateSl']['Jan'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='02'){
                $data['dateSl']['Feb'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='03'){
                $data['dateSl']['Mar'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='04'){
                $data['dateSl']['Apr'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='05'){
                $data['dateSl']['May'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='06'){
                $data['dateSl']['Jun'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='07'){
                $data['dateSl']['Jul'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='08'){
                $data['dateSl']['Aug'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='09'){
                $data['dateSl']['Sep'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='10'){
                $data['dateSl']['Oct'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='11'){
                $data['dateSl']['Nov'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='12'){
                $data['dateSl']['Dec'][] = date('d', strtotime($sdate));
            }
        }

            $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday
            $public_holiday = $this->global_model->get_public_holidays_2($start_date, $end_date);
            
            //tbl a_calendar Days Holiday        
            if (!empty($public_holiday)) {
                foreach ($public_holiday as $p_holiday) {

                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {
                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );

                        if ($p_holiday->start_date == $sdate && $p_holiday->end_date == $sdate) {
                            $p_hday[] = $sdate;
                        }
                        if ($p_holiday->start_date == $sdate) {
                            for ($j = $p_holiday->start_date; $j <= $p_holiday->end_date; $j++) {
                                $p_hday[] = $j;
                            }
                        }
                    }
                }
            }

        foreach ($data['employee'] as $sl => $v_employee) {

            $full_name = $v_employee->first_name.' '.$v_employee->last_name;

             //Get flag application list for generate L to report
             $application_list = $this->attendance_model->get_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

             //tbl application_list       
             if (!empty($application_list)) {
                 foreach ($application_list as $p_application_list) {

                     $current    = strtotime( $start_date );
                     $last       = strtotime( $end_date );
                     $step       = '+1 day';
                     $format     = 'Y-m-d';
                 
                     while( $current <= $last ) {
                 
                         $sdate   = date( $format, $current );
                         $current = strtotime( $step, $current );

                         if ($p_application_list->leave_start_date == $sdate && $p_application_list->leave_end_date == $sdate) {
                             $p_application_list_date[] = $sdate;
                         }
                         if ($p_application_list->leave_start_date == $sdate) {
                             for ($j = $p_application_list->leave_start_date; $j <= $p_application_list->leave_end_date; $j++) {
                                 $p_application_list_date[] = $j;
                             }
                         }
                     }
                 }
             }


           //Get flag permit list for generate P to report
           $permit_list = $this->attendance_model->get_permit_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

           //tbl permit_list       
           if (!empty($permit_list)) {
               foreach ($permit_list as $p_permit_list) {

                   $current    = strtotime( $start_date );
                   $last       = strtotime( $end_date );
                   $step       = '+1 day';
                   $format     = 'Y-m-d';
               
                   while( $current <= $last ) {
               
                       $sdate   = date( $format, $current );
                       $current = strtotime( $step, $current );

                       if ($p_permit_list->permit_date == $sdate && $p_permit_list->permit_date == $sdate) {
                           $p_permit_list_date[] = $sdate;
                       }
                       if ($p_permit_list->permit_date == $sdate) {
                           for ($j = $p_permit_list->permit_date; $j <= $p_permit_list->permit_date; $j++) {
                               $p_permit_list_date[] = $j;
                           }
                       }
                   }                                                         

               }
           }


           //Get flag overtime list for generate O to report
           $overtime_list = $this->attendance_model->get_overtime_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

           //tbl overtime_list       
           if (!empty($overtime_list)) {
               foreach ($overtime_list as $p_overtime_list) {

                   $current    = strtotime( $start_date );
                   $last       = strtotime( $end_date );
                   $step       = '+1 day';
                   $format     = 'Y-m-d';
               
                   while( $current <= $last ) {
               
                       $sdate   = date( $format, $current );
                       $current = strtotime( $step, $current );

                       if ($p_overtime_list->overtime_date == $sdate && $p_overtime_list->overtime_date == $sdate) {
                           $p_overtime_list_date[] = $sdate;
                       }
                       if ($p_overtime_list->overtime_date == $sdate) {
                           for ($j = $p_overtime_list->overtime_date; $j <= $p_overtime_list->overtime_date; $j++) {
                               $p_overtime_list_date[] = $j;
                           }
                       }
                   }                                                         

               }
           }

           //Get flag sakit utk generate S to report
           $sick_list = $this->attendance_model->get_sick_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

           //tbl sick_application_list       
           if (!empty($sick_list)) {
               foreach ($sick_list as $p_sick_list) {

                   $current    = strtotime( $start_date );
                   $last       = strtotime( $end_date );
                   $step       = '+1 day';
                   $format     = 'Y-m-d';
               
                   while( $current <= $last ) {
               
                       $sdate   = date( $format, $current );
                       $current = strtotime( $step, $current );

                       if ($p_sick_list->sick_start_date == $sdate && $p_sick_list->sick_end_date == $sdate) {
                           $p_sick_list_date[] = $sdate;
                       }
                       if ($p_sick_list->sick_start_date == $sdate) {
                           for ($j = $p_sick_list->sick_start_date; $j <= $p_sick_list->sick_end_date; $j++) {
                               $p_sick_list_date[] = $j;
                           }
                       }
                   }                                                         

               }
           }
  
            $key = 1;
            $x = 0;
           
            $current    = strtotime( $start_date );
            $last       = strtotime( $end_date );
            $step       = '+1 day';
            $format     = 'Y-m-d';
        
            while( $current <= $last ) {
        
                $sdate   = date( $format, $current );
                $current = strtotime( $step, $current );
               
                $day_name = date('l', strtotime($sdate));
               

                //TODO set report application list from tabel application list    
                if (!empty($p_application_list_date) && $p_application_list->employee_id == $v_employee->employee_id) {
                    foreach ($p_application_list_date as $v_application_list_date) {                           
                        if ($v_application_list_date == $sdate) {                               
                                $flag = 'C';                              
                        }
                    }
                }

                    //TODO set report permit list from tabel permit list    
                    if (!empty($p_permit_list_date) && $p_permit_list->employee_id == $v_employee->employee_id) {
                    foreach ($p_permit_list_date as $v_permit_list_date) {                           
                        if ($v_permit_list_date == $sdate) {                               
                                $flag = 'I';                              
                        }
                    }
                }

                //TODO set report overtime list from tabel overtime list    
                if (!empty($p_overtime_list_date) && $p_overtime_list->employee_id == $v_employee->employee_id) {
                    foreach ($p_overtime_list_date as $v_overtime_list_date) {                           
                        if ($v_overtime_list_date == $sdate) {                               
                                $flag = 'O';                              
                        }
                    }
                }

                //TODO set report sick list from tabel sick application list    
                if (!empty($p_sick_list_date) && $p_sick_list->employee_id == $v_employee->employee_id) {
                    foreach ($p_sick_list_date as $v_sick_list_date) {                           
                        if ($v_sick_list_date == $sdate) {                               
                                $flag = 'S';                              
                        }
                    }
                }
                
                if (!empty($holidays)) {
                    foreach ($holidays as $v_holiday) {

                        if ($v_holiday->day == $day_name) {
                            $flag = 'L';
                        }
                    }
                }
                if (!empty($p_hday)) {
                    foreach ($p_hday as $v_hday) {
                        if ($v_hday == $sdate) {
                            $flag = 'L';
                        }
                    }
                }
                    
                    if (!empty($flag)) {
                        if(date('m',strtotime($sdate))=='01'){
                            $data['attendance_v2'][$sl]['Jan'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='02'){
                            $data['attendance_v2'][$sl]['Feb'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='03'){
                            $data['attendance_v2'][$sl]['Mar'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='04'){
                            $data['attendance_v2'][$sl]['Apr'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='05'){
                            $data['attendance_v2'][$sl]['May'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='06'){
                            $data['attendance_v2'][$sl]['Jun'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='07'){
                            $data['attendance_v2'][$sl]['Jul'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='08'){
                            $data['attendance_v2'][$sl]['Aug'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='09'){
                            $data['attendance_v2'][$sl]['Sep'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='10'){
                            $data['attendance_v2'][$sl]['Oct'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='11'){
                            $data['attendance_v2'][$sl]['Nov'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='12'){
                            $data['attendance_v2'][$sl]['Dec'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }                       
                    } else {

                        if(date('m',strtotime($sdate))=='01'){
                            $data['attendance_v2'][$sl]['Jan'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='02'){
                            $data['attendance_v2'][$sl]['Feb'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='03'){
                            $data['attendance_v2'][$sl]['Mar'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='04'){
                            $data['attendance_v2'][$sl]['Apr'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='05'){
                            $data['attendance_v2'][$sl]['May'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='06'){
                            $data['attendance_v2'][$sl]['Jun'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='07'){
                            $data['attendance_v2'][$sl]['Jul'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='08'){
                            $data['attendance_v2'][$sl]['Aug'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='09'){
                            $data['attendance_v2'][$sl]['Sep'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='10'){
                            $data['attendance_v2'][$sl]['Oct'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='11'){
                            $data['attendance_v2'][$sl]['Nov'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='12'){
                            $data['attendance_v2'][$sl]['Dec'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }                    
                    }
                    $key++;
                    $flag = '';
                }  
            
        }

        $data['full_name']  = $full_name;
        $data['start_date'] = date('d-F-Y', strtotime($start_date));
        $data['end_date']   = date('d-F-Y', strtotime($end_date));
        $this->load->helper('dompdf');
        $view_file = $this->load->view('admin/attendance/Emp_report_pdf_att_by_emp', $data, true);
        $file_name = pdf_create($view_file, date('d-F-Y', strtotime($start_date)).' s/d '.date('d-F-Y', strtotime($end_date)), "a4", "landscape");
        echo $file_name;

    }


    public function create_excel_att_report_by_emp($employee_id, $start_date, $end_date) {
        
        $data['employee']    = $this->attendance_model->get_employee($employee_id);
       
        $current    = strtotime( $start_date );
        $last       = strtotime( $end_date );
        $step       = '+1 day';
        $format     = 'Y-m-d';
    
        while( $current <= $last ) {
    
            $sdate   = date( $format, $current );
            $current = strtotime( $step, $current );
            //echo date('m', $sdate)
            if(date('m', strtotime($sdate))=='01'){
                $data['dateSl']['Jan'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='02'){
                $data['dateSl']['Feb'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='03'){
                $data['dateSl']['Mar'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='04'){
                $data['dateSl']['Apr'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='05'){
                $data['dateSl']['May'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='06'){
                $data['dateSl']['Jun'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='07'){
                $data['dateSl']['Jul'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='08'){
                $data['dateSl']['Aug'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='09'){
                $data['dateSl']['Sep'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='10'){
                $data['dateSl']['Oct'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='11'){
                $data['dateSl']['Nov'][] = date('d', strtotime($sdate));
            }elseif(date('m', strtotime($sdate))=='12'){
                $data['dateSl']['Dec'][] = date('d', strtotime($sdate));
            }
        }

        // print_r($data['dateSl']);
        // die;

            $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday
            $public_holiday = $this->global_model->get_public_holidays_2($start_date, $end_date);
            
            //tbl a_calendar Days Holiday        
            if (!empty($public_holiday)) {
                foreach ($public_holiday as $p_holiday) {

                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {
                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );

                        if ($p_holiday->start_date == $sdate && $p_holiday->end_date == $sdate) {
                            $p_hday[] = $sdate;
                        }
                        if ($p_holiday->start_date == $sdate) {
                            for ($j = $p_holiday->start_date; $j <= $p_holiday->end_date; $j++) {
                                $p_hday[] = $j;
                            }
                        }
                    }
                }
            }
        
        foreach ($data['employee'] as $sl => $v_employee) {
		
			$full_name = $v_employee->first_name.' '.$v_employee->last_name;

             //Get flag application list for generate L to report
             $application_list = $this->attendance_model->get_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

             //tbl application_list       
             if (!empty($application_list)) {
                 foreach ($application_list as $p_application_list) {

                     $current    = strtotime( $start_date );
                     $last       = strtotime( $end_date );
                     $step       = '+1 day';
                     $format     = 'Y-m-d';
                 
                     while( $current <= $last ) {
                 
                         $sdate   = date( $format, $current );
                         $current = strtotime( $step, $current );

                         if ($p_application_list->leave_start_date == $sdate && $p_application_list->leave_end_date == $sdate) {
                             $p_application_list_date[] = $sdate;
                         }
                         if ($p_application_list->leave_start_date == $sdate) {
                             for ($j = $p_application_list->leave_start_date; $j <= $p_application_list->leave_end_date; $j++) {
                                 $p_application_list_date[] = $j;
                             }
                         }
                     }
                 }
             }


           //Get flag permit list for generate P to report
           $permit_list = $this->attendance_model->get_permit_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

           //tbl permit_list       
           if (!empty($permit_list)) {
               foreach ($permit_list as $p_permit_list) {

                   $current    = strtotime( $start_date );
                   $last       = strtotime( $end_date );
                   $step       = '+1 day';
                   $format     = 'Y-m-d';
               
                   while( $current <= $last ) {
               
                       $sdate   = date( $format, $current );
                       $current = strtotime( $step, $current );

                       if ($p_permit_list->permit_date == $sdate && $p_permit_list->permit_date == $sdate) {
                           $p_permit_list_date[] = $sdate;
                       }
                       if ($p_permit_list->permit_date == $sdate) {
                           for ($j = $p_permit_list->permit_date; $j <= $p_permit_list->permit_date; $j++) {
                               $p_permit_list_date[] = $j;
                           }
                       }
                   }                                                         

               }
           }


           //Get flag overtime list for generate O to report
           $overtime_list = $this->attendance_model->get_overtime_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

           //tbl overtime_list       
           if (!empty($overtime_list)) {
               foreach ($overtime_list as $p_overtime_list) {

                   $current    = strtotime( $start_date );
                   $last       = strtotime( $end_date );
                   $step       = '+1 day';
                   $format     = 'Y-m-d';
               
                   while( $current <= $last ) {
               
                       $sdate   = date( $format, $current );
                       $current = strtotime( $step, $current );

                       if ($p_overtime_list->overtime_date == $sdate && $p_overtime_list->overtime_date == $sdate) {
                           $p_overtime_list_date[] = $sdate;
                       }
                       if ($p_overtime_list->overtime_date == $sdate) {
                           for ($j = $p_overtime_list->overtime_date; $j <= $p_overtime_list->overtime_date; $j++) {
                               $p_overtime_list_date[] = $j;
                           }
                       }
                   }                                                         

               }
           }
  
            $key = 1;
            $x = 0;

            $current    = strtotime( $start_date );
            $last       = strtotime( $end_date );
            $step       = '+1 day';
            $format     = 'Y-m-d';
        
            while( $current <= $last ) {
        
                $sdate   = date( $format, $current );
                $current = strtotime( $step, $current );
               
                $day_name = date('l', strtotime($sdate));
                if (!empty($holidays)) {
                    foreach ($holidays as $v_holiday) {

                        if ($v_holiday->day == $day_name) {
                            $flag = 'L';
                        }
                    }
                }
                if (!empty($p_hday)) {
                    foreach ($p_hday as $v_hday) {
                        if ($v_hday == $sdate) {
                            $flag = 'L';
                        }
                    }
                }

                //TODO set report application list from tabel application list    
                if (!empty($p_application_list_date) && $p_application_list->employee_id == $v_employee->employee_id) {
                    foreach ($p_application_list_date as $v_application_list_date) {                           
                        if ($v_application_list_date == $sdate) {                               
                                $flag = 'C';                              
                        }
                    }
                }

                    //TODO set report permit list from tabel permit list    
                    if (!empty($p_permit_list_date) && $p_permit_list->employee_id == $v_employee->employee_id) {
                    foreach ($p_permit_list_date as $v_permit_list_date) {                           
                        if ($v_permit_list_date == $sdate) {                               
                                $flag = 'I';                              
                        }
                    }
                }

                //TODO set report overtime list from tabel overtime list    
                if (!empty($p_overtime_list_date) && $p_overtime_list->employee_id == $v_employee->employee_id) {
                    foreach ($p_overtime_list_date as $v_overtime_list_date) {                           
                        if ($v_overtime_list_date == $sdate) {                               
                                $flag = 'O';                              
                        }
                    }
                }
                    
                    if (!empty($flag)) {
                        if(date('m',strtotime($sdate))=='01'){
                            //echo 'jan';
                            $data['attendance_v2'][$sl]['Jan'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='02'){
                            $data['attendance_v2'][$sl]['Feb'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='03'){
                            $data['attendance_v2'][$sl]['Mar'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='04'){
                            $data['attendance_v2'][$sl]['Apr'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='05'){
                            $data['attendance_v2'][$sl]['May'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='06'){
                            $data['attendance_v2'][$sl]['Jun'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='07'){
                            $data['attendance_v2'][$sl]['Jul'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='08'){
                            $data['attendance_v2'][$sl]['Aug'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='09'){
                            $data['attendance_v2'][$sl]['Sep'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='10'){
                            $data['attendance_v2'][$sl]['Oct'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='11'){
                            $data['attendance_v2'][$sl]['Nov'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }elseif(date('m',strtotime($sdate))=='12'){
                            $data['attendance_v2'][$sl]['Dec'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                        }                       
                    } else {

                        if(date('m',strtotime($sdate))=='01'){
                            $data['attendance_v2'][$sl]['Jan'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='02'){
                            $data['attendance_v2'][$sl]['Feb'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='03'){
                            $data['attendance_v2'][$sl]['Mar'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='04'){
                            $data['attendance_v2'][$sl]['Apr'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='05'){
                            $data['attendance_v2'][$sl]['May'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='06'){
                            $data['attendance_v2'][$sl]['Jun'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='07'){
                            $data['attendance_v2'][$sl]['Jul'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='08'){
                            $data['attendance_v2'][$sl]['Aug'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='09'){
                            $data['attendance_v2'][$sl]['Sep'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='10'){
                            $data['attendance_v2'][$sl]['Oct'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='11'){
                            $data['attendance_v2'][$sl]['Nov'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }elseif(date('m',strtotime($sdate))=='12'){
                            $data['attendance_v2'][$sl]['Dec'][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                        }                    
                    }
                    $key++;
                    $flag = '';
                }
        }

        // foreach ($attendance_v2 as $v_employee){

        // }

        // print_r($data['attendance_v2'][$sl]);
        // die;


        //load PHPExcel library
        $this->load->library('Excel');
        ob_start();
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $styleArray = array(
            'font' => array(
                'size' => 13,
                'name' => 'Verdana'
        ));
        $fontArray = array(
            'font' => array(
                'bold' => true,
                'size' => 11,
                'name' => 'Verdana'
        ));
        $dateArray = array(
            'font' => array(
                'bold' => true,
        ));
        $bgcolor = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'E7E7E7'),
        ));

        $fontcolor = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '#fc0303'),
        ));

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B' . '1', 'Periode:')
                ->setCellValue('D' . '1', date('d M Y', strtotime($start_date)).' s_d '.date('d M Y', strtotime($end_date)));
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B1:C1');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D1:H1');
        $objPHPExcel->getActiveSheet()->getStyle('B1:C1')->applyFromArray($fontArray);

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('J' . '1', 'Nama:')
                ->setCellValue('N' . '1', $full_name);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J1:M1');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('N1:V1');
        $objPHPExcel->getActiveSheet()->getStyle('J1:L1')->applyFromArray($fontArray);

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Comprehensive HRMS")
                ->setLastModifiedBy("Comprehensive HRMS")
                ->setTitle("Office  XLSX Test Document")
                ->setSubject("Office XLSX Test Document")
                ->setDescription("Test document for Office XLSX, generated by PHP classes.")
                ->setKeywords("office openxml php")
                ->setCategory("Excel Sheet");


        // Add some data                
        $cl = 'B';
        $bg = 'A';
        foreach ($data['dateSl'] as $edate) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($cl . '3', $edate);
            $objPHPExcel->getActiveSheet()->getColumnDimension($cl)->setWidth(3);
            $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->applyFromArray($dateArray);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '4')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '1')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '2')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '4')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '1')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '2')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $cl++;
        }

        $row = 5;
        $c = 0;
        foreach ($data['attendance_v2'] as $v_Emp) {

                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A3', 'Date')
                ->setCellValue('A5', 'Jan')
                ->setCellValue('A6', 'Feb')
                ->setCellValue('A7', 'Mar')
                ->setCellValue('A8', 'Apr')
                ->setCellValue('A9', 'May')
                ->setCellValue('A10', 'Jun')
                ->setCellValue('A11', 'Jul')
                ->setCellValue('A12', 'Aug')
                ->setCellValue('A13', 'Sep')
                ->setCellValue('A14', 'Oct')
                ->setCellValue('A15', 'Nov')
                ->setCellValue('A16', 'Dec');
                $objPHPExcel->getActiveSheet()->getColumnDimension('A1')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
                $col = 1;
                $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle($row, $col)->getFont()->setSize(10);
                 
           
                foreach ($v_Emp['Jan'] as $v_result){
                    if (!empty($v_result)) {
                    
                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, $row, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row, 'O');
                                }
                            }

                        $col++;
                    }
                }  

                // Feb
                foreach ($v_Emp['Feb'] as $v_result){
                    if (!empty($v_result)) {

                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, 6, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, 6, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, 6, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, 6, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, 6, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, 6, 'O');
                                }
                            }

                        $col++;
                    }
                } 
                // 

                 // Mar
                 foreach ($v_Emp['Mar'] as $v_result){
                    if (!empty($v_result)) {

                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, $row+2, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+2, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+2, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+2, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+2, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+2, 'O');
                                }
                            }

                        $col++;
                    }
                } 
                // 

                 // Apr
                 foreach ($v_Emp['Apr'] as $v_result){
                    if (!empty($v_result)) {

                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, $row+3, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+3, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+3, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+3, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+3, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+3, 'O');
                                }
                            }

                        $col++;
                    }
                } 
                // 

                 // May
                 foreach ($v_Emp['May'] as $v_result){
                    if (!empty($v_result)) {

                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, $row+4, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+4, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+4, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+4, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+4, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+4, 'O');
                                }
                            }

                        $col++;
                    }

                    $row++;
                } 
                // 

                 // Jun
                 foreach ($v_Emp['Jun'] as $v_result){
                    if (!empty($v_result)) {

                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, $row+5, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+5, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+5, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+5, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+5, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+5, 'O');
                                }
                            }

                        $col++;
                    }
                } 
                // 

                 // Jul
                 foreach ($v_Emp['Jul'] as $v_result){
                    if (!empty($v_result)) {

                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, $row+6, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+6, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+6, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+6, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+6, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+6, 'O');
                                }
                            }

                        $col++;
                    }
                } 
                // 

                 // Aug
                 foreach ($v_Emp['Aug'] as $v_result){
                    if (!empty($v_result)) {

                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, $row+7, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+7, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+7, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+7, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+7, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+7, 'O');
                                }
                            }

                        $col++;
                    }
                } 
                // 

                 // Sep
                 foreach ($v_Emp['Sep'] as $v_result){
                    if (!empty($v_result)) {

                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, $row, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+8, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+8, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+8, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+8, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+8, 'O');
                                }
                            }

                        $col++;
                    }
                } 
                // 

                 // Oct
                 foreach ($v_Emp['Oct'] as $v_result){
                    if (!empty($v_result)) {

                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, $row+9, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+9, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+9, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+9, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+9, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+9, 'O');
                                }
                            }

                        $col++;
                    }
                } 
                // 

                 // Nov
                 foreach ($v_Emp['Nov'] as $v_result){
                    if (!empty($v_result)) {

                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, $row+10, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+10, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+10, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+10, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+10, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+10, 'O');
                                }
                            }

                        $col++;
                    }
                } 
                // 

                 // Dec
                 foreach ($v_Emp['Dec'] as $v_result){
                    if (!empty($v_result)) {

                        $cekin  = '';
                        $cekout = '';
                        $absen  = '';

                        foreach ($v_result as $emp_attendance){

                            if($emp_attendance->StatusAbsen == 'C/In'){

                                $cekin_tmp = date('H:i',strtotime($emp_attendance->Time));
                                //set jam telat
                                if($cekin_tmp > '10:00:00'){
                                    $cekin = date("H:i",strtotime($emp_attendance->Time));
                                }else{
                                    $cekin = date('H:i',strtotime($emp_attendance->Time));    
                                }
                            }
                            if($emp_attendance->StatusAbsen == 'C/Out'){
                                $cekout = date('H:i',strtotime($emp_attendance->Time));
                            }

                    

                                $absen = $cekin.' - '.$cekout;

                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValueByColumnAndRow($col, $row+11, $absen);
                                
                                if ($emp_attendance->StatusAbsen == 'L') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+11, 'L');
                                }

                                if ($emp_attendance->StatusAbsen == '') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+11, 'A');
                                }
                                
                                if ($emp_attendance->StatusAbsen == 'C') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+11, 'C');
                                }

                                if ($emp_attendance->StatusAbsen == 'I') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+11, 'I');
                                }

                                if ($emp_attendance->StatusAbsen == 'O') {
                                    $objPHPExcel->setActiveSheetIndex(0)
                                            ->setCellValueByColumnAndRow($col, $row+11, 'O');
                                }
                            }

                        $col++;
                    }
                } 
              
            $c++;
        }

        // Rename worksheet (worksheet, not filename)
        $objPHPExcel->getActiveSheet()->setTitle('Attendance By Employee');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        //clean the output buffer
        ob_end_clean();
        //this is the header given from PHPExcel examples. but the output seems somewhat corrupted in some cases.
        //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //so, we use this header instead.
        // $file_name = pdf_create($view_file, date('d-F-Y', strtotime($start_date)).' s/d '.date('d-F-Y', strtotime($end_date)));
        $filename = date('d M Y', strtotime($start_date)).' s_d '.date('d M Y', strtotime($end_date)) . '  ' . 'Attendance By Employee.xls'; //save our workbook as this file name
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }


    public function create_pdf_old($department_id, $date) {
        $month = date('n', strtotime($date));
        $year = date('Y', strtotime($date));
        $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $data['employee'] = $this->attendance_model->get_employee_id_by_dept_id($department_id);

        $day = date('d', strtotime($date));
        for ($i = 1; $i <= $num; $i++) {
            $data['dateSl'][] = $i;
        }
        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday
        if ($month >= 1 && $month <= 9) {
            $yymm = $year . '-' . '0' . $month;
        } else {
            $yymm = $year . '-' . $month;
        }
        $public_holiday = $this->global_model->get_public_holidays($yymm);
        if (!empty($public_holiday)) {
            //tbl a_calendar Days Holiday        
            foreach ($public_holiday as $p_holiday) {
                for ($k = 1; $k <= $num; $k++) {

                    if ($k >= 1 && $k <= 9) {
                        $sdate = $yymm . '-' . '0' . $k;
                    } else {
                        $sdate = $yymm . '-' . $k;
                    }
                    if ($p_holiday->start_date == $sdate && $p_holiday->end_date == $sdate) {
                        $p_hday[] = $sdate;
                    }
                    if ($p_holiday->start_date == $sdate) {
                        for ($j = $p_holiday->start_date; $j <= $p_holiday->end_date; $j++) {
                            $p_hday[] = $j;
                        }
                    }
                }
            }
        }
        foreach ($data['employee'] as $sl => $v_employee) {
            $key = 1;
            $x = 0;
            for ($i = 1; $i <= $num; $i++) {

                if ($i >= 1 && $i <= 9) {

                    $sdate = $yymm . '-' . '0' . $i;
                } else {
                    $sdate = $yymm . '-' . $i;
                }
                $day_name = date('l', strtotime("+$x days", strtotime($year . '-' . $month . '-' . $key)));
                if (!empty($holidays)) {
                    foreach ($holidays as $v_holiday) {

                        if ($v_holiday->day == $day_name) {
                            $flag = 'L';
                        }
                    }
                }
                if (!empty($p_hday)) {
                    foreach ($p_hday as $v_hday) {
                        if ($v_hday == $sdate) {
                            $flag = 'L';
                        }
                    }
                }
                if (!empty($flag)) {
                    $data['attendance'][$sl][] = $this->attendance_model->attendance_report_by_empid($v_employee->employee_id, $sdate, $flag);
                } else {
                    $data['attendance'][$sl][] = $this->attendance_model->attendance_report_by_empid($v_employee->employee_id, $sdate);
                }
                $key++;
                $flag = '';
            }
        }
        $where = array('department_id' => $department_id);
        $data['dept_name'] = $this->attendance_model->check_by($where, 'tbl_department');
        $data['date'] = date('F-Y', strtotime($yymm));
        $this->load->helper('dompdf');
        $view_file = $this->load->view('admin/attendance/Emp_report_pdf', $data, true);
        $file_name = pdf_create($view_file, date('F-Y', strtotime($yymm)));
        echo $file_name;
    }
	
	

    public function create_pdf($department_id, $start_date, $end_date) {
        // $month = date('n', strtotime($date));
        // $year = date('Y', strtotime($date));
        // $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $data['employee'] = $this->attendance_model->get_employee_id_by_dept_id($department_id);

        //$start_date     = $this->input->post('start_date', TRUE);
        $start_day      = date('d', strtotime($start_date));
        $start_month    = date('n', strtotime($start_date));
        $start_year     = date('Y', strtotime($start_date));
        $start_num      = cal_days_in_month(CAL_GREGORIAN, $start_month, $start_year);
       
        //$end_date     = $this->input->post('end_date', TRUE);
        $end_day      = date('d', strtotime($end_date));
        $end_month    = date('n', strtotime($end_date));
        $end_year     = date('Y', strtotime($end_date));
        $end_num      = cal_days_in_month(CAL_GREGORIAN, $end_month, $end_year);

       
        $current    = strtotime( $start_date );
        $last       = strtotime( $end_date );
        $step       = '+1 day';
        $format     = 'Y-m-d';
    
        while( $current <= $last ) {
    
            $sdate   = date( $format, $current );
            $current = strtotime( $step, $current );

            $data['dateSl'][] = date('d', strtotime($sdate));
        }

        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday
        
        // if ($month >= 1 && $month <= 9) {
        //     $yymm = $year . '-' . '0' . $month;
        // } else {
        //     $yymm = $year . '-' . $month;
        // }

        $public_holiday = $this->global_model->get_public_holidays($start_date, $end_date);
        
        if (!empty($public_holiday)) {
            //tbl a_calendar Days Holiday        
            foreach ($public_holiday as $p_holiday) {
                // for ($k = 1; $k <= $num; $k++) {

                    // if ($k >= 1 && $k <= 9) {
                    //     $sdate = $yymm . '-' . '0' . $k;
                    // } else {
                    //     $sdate = $yymm . '-' . $k;
                    // }
                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {
                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );
            
                        if ($p_holiday->start_date == $sdate && $p_holiday->end_date == $sdate) {
                            $p_hday[] = $sdate;
                        }
                        if ($p_holiday->start_date == $sdate) {
                            for ($j = $p_holiday->start_date; $j <= $p_holiday->end_date; $j++) {
                                $p_hday[] = $j;
                            }
                        }
                    }

                // }
            }
        }
        foreach ($data['employee'] as $sl => $v_employee) {

            //Get no show data list for generate L to report
            $noshow_list = $this->attendance_model->get_noshow_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);       
            if (!empty($noshow_list)) {
                foreach ($noshow_list as $p_noshow_list) {
                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';                        
                    while( $current <= $last ) {                        
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );

                        if ($p_noshow_list->leave_start_date == $sdate && $p_noshow_list->leave_end_date == $sdate) {
                            $p_noshow_list_date[] = $sdate;
                        }
                        if ($p_noshow_list->leave_start_date == $sdate) {
                            for ($j = $p_noshow_list->leave_start_date; $j <= $p_noshow_list->leave_end_date; $j++) {
                                $p_noshow_list_date[] = $j;
                            }
                        }
                    }
                }
            }


            //Get flag application list for generate L to report
            $application_list = $this->attendance_model->get_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);
            //tbl application_list       
            if (!empty($application_list)) {
                foreach ($application_list as $p_application_list) {
                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';

                    while( $current <= $last ) {
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );
            
                        if ($p_application_list->leave_start_date == $sdate && $p_application_list->leave_end_date == $sdate) {
                            $p_application_list_date[] = $sdate;
                        }
                        if ($p_application_list->leave_start_date == $sdate) {
                            for ($j = $p_application_list->leave_start_date; $j <= $p_application_list->leave_end_date; $j++) {
                                $p_application_list_date[] = $j;
                            }
                        }
                    }   
                    
                }
            }


            //Get flag permit list for generate P to report
            $permit_list = $this->attendance_model->get_permit_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

            //tbl permit_list       
            if (!empty($permit_list)) {
                foreach ($permit_list as $p_permit_list) {

                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {
                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );

                        if ($p_permit_list->permit_date == $sdate && $p_permit_list->permit_date == $sdate) {
                            $p_permit_list_date[] = $sdate;
                        }
                        if ($p_permit_list->permit_date == $sdate) {
                            for ($j = $p_permit_list->permit_date; $j <= $p_permit_list->permit_date; $j++) {
                                $p_permit_list_date[] = $j;
                            }
                        }
                    }                                                         

                }
            }


            //Get flag overtime list for generate O to report
            $overtime_list = $this->attendance_model->get_overtime_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

            //tbl overtime_list       
            if (!empty($overtime_list)) {
                foreach ($overtime_list as $p_overtime_list) {

                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {
                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );

                        if ($p_overtime_list->overtime_date == $sdate && $p_overtime_list->overtime_date == $sdate) {
                            $p_overtime_list_date[] = $sdate;
                        }
                        if ($p_overtime_list->overtime_date == $sdate) {
                            for ($j = $p_overtime_list->overtime_date; $j <= $p_overtime_list->overtime_date; $j++) {
                                $p_overtime_list_date[] = $j;
                            }
                        }
                    }                                                         

                }
            }


             //Get flag sakit utk generate S to report
             $sick_list = $this->attendance_model->get_sick_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

             //tbl sick_application_list       
             if (!empty($sick_list)) {
                 foreach ($sick_list as $p_sick_list) {

                     $current    = strtotime( $start_date );
                     $last       = strtotime( $end_date );
                     $step       = '+1 day';
                     $format     = 'Y-m-d';
                 
                     while( $current <= $last ) {
                 
                         $sdate   = date( $format, $current );
                         $current = strtotime( $step, $current );

                         if ($p_sick_list->sick_start_date == $sdate && $p_sick_list->sick_end_date == $sdate) {
                             $p_sick_list_date[] = $sdate;
                         }
                         if ($p_sick_list->sick_start_date == $sdate) {
                             for ($j = $p_sick_list->sick_start_date; $j <= $p_sick_list->sick_end_date; $j++) {
                                 $p_sick_list_date[] = $j;
                             }
                         }
                     }                                                         

                 }
             }

              //Get flag perdin utk generate D to report
              $perdin_list = $this->attendance_model->get_perdin_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

              //tbl perdin_list       
              if (!empty($perdin_list)) {
                  foreach ($perdin_list as $p_perdin_list) {

                      $current    = strtotime( $start_date );
                      $last       = strtotime( $end_date );
                      $step       = '+1 day';
                      $format     = 'Y-m-d';
                  
                      while( $current <= $last ) {
                  
                          $sdate   = date( $format, $current );
                          $current = strtotime( $step, $current );

                          if ($p_perdin_list->perdin_start_date == $sdate && $p_perdin_list->perdin_end_date == $sdate) {
                              $p_perdin_list_date[] = $sdate;
                          }
                          if ($p_perdin_list->perdin_start_date == $sdate) {
                              for ($j = $p_perdin_list->perdin_start_date; $j <= $p_perdin_list->perdin_end_date; $j++) {
                                  $p_perdin_list_date[] = $j;
                              }
                          }
                      }                                                         

                  }
              }


                $key = 1;
                $x = 0;
           
                $current    = strtotime( $start_date );
                $last       = strtotime( $end_date );
                $step       = '+1 day';
                $format     = 'Y-m-d';
            
                while( $current <= $last ) {
            
                    $sdate   = date( $format, $current );
                    $current = strtotime( $step, $current );
        
                     //TODO set report noshow list from tabel noshow list    
                     if (!empty($p_noshow_list_date) && $p_noshow_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_noshow_list_date as $v_noshow_list_date) {                           
                            if ($v_noshow_list_date == $sdate) {                               
                                    $flag = 'N';                              
                            }
                        }
                    }

                    //TODO set report application list from tabel application list    
                    if (!empty($p_application_list_date) && $p_application_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_application_list_date as $v_application_list_date) {                           
                            if ($v_application_list_date == $sdate) {                               
                                    $flag = 'C';                              
                            }
                        }
                    }

                     //TODO set report permit list from tabel permit list    
                     if (!empty($p_permit_list_date) && $p_permit_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_permit_list_date as $v_permit_list_date) {                           
                            if ($v_permit_list_date == $sdate) {  
                                // print_r($v_permit_list_date);
                                   if($p_permit_list->permit_category_id == 3){
                                    $flag = 'W';
                                   } else{
                                    $flag = 'I';                              
                                   }
                                   
                            }
                        }
                    }

                    //TODO set report overtime list from tabel overtime list    
                    if (!empty($p_overtime_list_date) && $p_overtime_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_overtime_list_date as $v_overtime_list_date) {                           
                            if ($v_overtime_list_date == $sdate) {                               
                                    $flag = 'O';                              
                            }
                        }
                    }

                    //TODO set report sick list from tabel overtime list    
                    if (!empty($p_sick_list_date) && $p_sick_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_sick_list_date as $v_sick_list_date) {                           
                            if ($v_sick_list_date == $sdate) {                               
                                    $flag = 'S';                              
                            }
                        }
                    }

                     //TODO set report perdin list from tabel perdin list    
                     if (!empty($p_perdin_list_date) && $p_perdin_list->employee_id == $v_employee->employee_id) {
                        foreach ($p_perdin_list_date as $v_perdin_list_date) {                           
                            if ($v_perdin_list_date == $sdate) {                               
                                    $flag = 'D';                              
                            }
                        }
                    }

                    //TODO set flag L (libur) 
                    $day_name = date('l', strtotime($sdate));
                    if (!empty($holidays)) {
                        foreach ($holidays as $v_holiday) {

                            if ($v_holiday->day == $day_name) {
                                $flag = 'L';
                            }
                        }
                    }
                    if (!empty($p_hday)) {
                        foreach ($p_hday as $v_hday) {
                            if ($v_hday == $sdate) {
                                $flag = 'L';
                            }
                        }
                    }
                    
                    if (!empty($flag)) {
                        $data['attendance_v2'][$sl][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                    } else {
                        $data['attendance_v2'][$sl][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                    }
                    $key++;
                    $flag = '';
                }  
            
        }
        $where = array('department_id' => $department_id);
        if($department_id!=0){
            $data['dept_name'] = $this->attendance_model->check_by($where, 'tbl_department');
        }else{
            $data['dept_name'] = array('department_id' => 0, 'department_name' => 'ALL');
        }
        //$data['date'] = date('F-Y', strtotime($yymm));
        $data['start_date'] = date('d M Y', strtotime($start_date));
        $data['end_date'] = date('d M Y', strtotime($end_date));
        $this->load->helper('dompdf');
        $view_file = $this->load->view('admin/attendance/Emp_report_pdf_v2', $data, true);
        $file_name = pdf_create($view_file, date('d-F-Y', strtotime($start_date)).' s/d '.date('d-F-Y', strtotime($end_date)));
        echo $file_name;
    }


    public function create_excel($department_id, $start_date, $end_date) {

        $start_day      = date('d', strtotime($start_date));
        $start_month    = date('n', strtotime($start_date));
        $start_year     = date('Y', strtotime($start_date));
        $start_num      = cal_days_in_month(CAL_GREGORIAN, $start_month, $start_year);
       
        $end_day      = date('d', strtotime($end_date));
        $end_month    = date('n', strtotime($end_date));
        $end_year     = date('Y', strtotime($end_date));
        $end_num      = cal_days_in_month(CAL_GREGORIAN, $end_month, $end_year);

        $employee = $this->attendance_model->get_employee_id_by_dept_id($department_id);       
        $where = array('department_id' => $department_id);
        $dept_name = $this->attendance_model->check_by($where, 'tbl_department');

        // $data['employee'] = $this->attendance_model->get_employee_id_by_dept_id($department_id); 
        
        // for ($i = 1; $i <= $num; $i++) {
        //     $dateSl[] = $i;
        // }
        
        // for ($i = 1; $i <= $num; $i++) {
        //     $data['dateSl'][] = $i;
        // }

        $current    = strtotime( $start_date );
        $last       = strtotime( $end_date );
        $step       = '+1 day';
        $format     = 'Y-m-d';
    
        while( $current <= $last ) {
    
            $sdate   = date( $format, $current );
            $current = strtotime( $step, $current );

            $dateSl[] = date('d', strtotime($sdate));
        }

        // print_r($dateSl);
        // die;

      

        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday
        
        $public_holiday = $this->global_model->get_public_holidays($start_date, $end_date);
        if (!empty($public_holiday)) {
            //tbl a_calendar Days Holiday        
            foreach ($public_holiday as $p_holiday) {
                
                $current    = strtotime( $start_date );
                $last       = strtotime( $end_date );
                $step       = '+1 day';
                $format     = 'Y-m-d';
            
                while( $current <= $last ) {
            
                    $sdate   = date( $format, $current );
                    $current = strtotime( $step, $current );
        
                    if ($p_holiday->start_date == $sdate && $p_holiday->end_date == $sdate) {
                        $p_hday[] = $sdate;
                    }
                    if ($p_holiday->start_date == $sdate) {
                        for ($j = $p_holiday->start_date; $j <= $p_holiday->end_date; $j++) {
                            $p_hday[] = $j;
                        }
                    }
                }
            }
        }
        
        foreach ($employee as $sl => $v_employee) {
		
			//Get flag noshow list for generate L to report
            $noshow_list = $this->attendance_model->get_noshow_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);      
            if (!empty($noshow_list)) {
                foreach ($noshow_list as $p_noshow_list) {
                    
                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {
                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );
            
                        if ($p_noshow_list->leave_start_date == $sdate && $p_noshow_list->leave_end_date == $sdate) {
                            $p_noshow_list_date[] = $sdate;
                        }
                        if ($p_noshow_list->leave_start_date == $sdate) {
                            for ($j = $p_noshow_list->leave_start_date; $j <= $p_noshow_list->leave_end_date; $j++) {
                                $p_noshow_list_date[] = $j;
                            }
                        }
                    }   
                    
                }
            }

			//Get flag application list for generate L to report
            $application_list = $this->attendance_model->get_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);      
            if (!empty($application_list)) {
                foreach ($application_list as $p_application_list) {
                    
                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {
                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );
            
                        if ($p_application_list->leave_start_date == $sdate && $p_application_list->leave_end_date == $sdate) {
                            $p_application_list_date[] = $sdate;
                        }
                        if ($p_application_list->leave_start_date == $sdate) {
                            for ($j = $p_application_list->leave_start_date; $j <= $p_application_list->leave_end_date; $j++) {
                                $p_application_list_date[] = $j;
                            }
                        }
                    }   
                    
                }
            }


            //Get flag permit list for generate P to report
            $permit_list = $this->attendance_model->get_permit_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

            //tbl permit_list       
            if (!empty($permit_list)) {
                foreach ($permit_list as $p_permit_list) {

                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {
                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );

                        if ($p_permit_list->permit_date == $sdate && $p_permit_list->permit_date == $sdate) {
                            $p_permit_list_date[] = $sdate;
                        }
                        if ($p_permit_list->permit_date == $sdate) {
                            for ($j = $p_permit_list->permit_date; $j <= $p_permit_list->permit_date; $j++) {
                                $p_permit_list_date[] = $j;
                            }
                        }
                    }                                                         

                }
            }


            //Get flag overtime list for generate O to report
            $overtime_list = $this->attendance_model->get_overtime_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

            //tbl overtime_list       
            if (!empty($overtime_list)) {
                foreach ($overtime_list as $p_overtime_list) {

                    $current    = strtotime( $start_date );
                    $last       = strtotime( $end_date );
                    $step       = '+1 day';
                    $format     = 'Y-m-d';
                
                    while( $current <= $last ) {
                
                        $sdate   = date( $format, $current );
                        $current = strtotime( $step, $current );

                        if ($p_overtime_list->overtime_date == $sdate && $p_overtime_list->overtime_date == $sdate) {
                            $p_overtime_list_date[] = $sdate;
                        }
                        if ($p_overtime_list->overtime_date == $sdate) {
                            for ($j = $p_overtime_list->overtime_date; $j <= $p_overtime_list->overtime_date; $j++) {
                                $p_overtime_list_date[] = $j;
                            }
                        }
                    }                                                         

                }
            }


             //Get flag sakit utk generate S to report
             $sick_list = $this->attendance_model->get_sick_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

             //tbl sick_application_list       
             if (!empty($sick_list)) {
                 foreach ($sick_list as $p_sick_list) {

                     $current    = strtotime( $start_date );
                     $last       = strtotime( $end_date );
                     $step       = '+1 day';
                     $format     = 'Y-m-d';
                 
                     while( $current <= $last ) {
                 
                         $sdate   = date( $format, $current );
                         $current = strtotime( $step, $current );

                         if ($p_sick_list->sick_start_date == $sdate && $p_sick_list->sick_end_date == $sdate) {
                             $p_sick_list_date[] = $sdate;
                         }
                         if ($p_sick_list->sick_start_date == $sdate) {
                             for ($j = $p_sick_list->sick_start_date; $j <= $p_sick_list->sick_end_date; $j++) {
                                 $p_sick_list_date[] = $j;
                             }
                         }
                     }                                                         

                 }
             }


             //Get flag perdin utk generate D to report
             $perdin_list = $this->attendance_model->get_perdin_application_list_by_employee_id($start_date, $end_date, $v_employee->employee_id);

             //tbl perdin_list       
             if (!empty($perdin_list)) {
                 foreach ($perdin_list as $p_perdin_list) {

                     $current    = strtotime( $start_date );
                     $last       = strtotime( $end_date );
                     $step       = '+1 day';
                     $format     = 'Y-m-d';
                 
                     while( $current <= $last ) {
                 
                         $sdate   = date( $format, $current );
                         $current = strtotime( $step, $current );

                         if ($p_perdin_list->perdin_start_date == $sdate && $p_perdin_list->perdin_end_date == $sdate) {
                             $p_perdin_list_date[] = $sdate;
                         }
                         if ($p_perdin_list->perdin_start_date == $sdate) {
                             for ($j = $p_perdin_list->perdin_start_date; $j <= $p_perdin_list->perdin_end_date; $j++) {
                                 $p_perdin_list_date[] = $j;
                             }
                         }
                     }                                                         

                 }
             }
            
					
            $key = 1;
            $x = 0;

            $current    = strtotime( $start_date );
            $last       = strtotime( $end_date );
            $step       = '+1 day';
            $format     = 'Y-m-d';
        
            while( $current <= $last ) {
        
            $sdate   = date( $format, $current );
            $current = strtotime( $step, $current );

            
            //TODO set report application list from tabel application list    
            if (!empty($p_noshow_list_date) && $p_noshow_list->employee_id == $v_employee->employee_id) {
                foreach ($p_noshow_list_date as $v_noshow_list_date) {                           
                    if ($v_noshow_list_date == $sdate) {                               
                            $flag = 'N';                              
                    }
                }
            }

            //TODO set report application list from tabel application list    
            if (!empty($p_application_list_date) && $p_application_list->employee_id == $v_employee->employee_id) {
                foreach ($p_application_list_date as $v_application_list_date) {                           
                    if ($v_application_list_date == $sdate) {                               
                            $flag = 'C';                              
                    }
                }
            }

                //TODO set report permit list from tabel permit list    
                if (!empty($p_permit_list_date) && $p_permit_list->employee_id == $v_employee->employee_id) {
                foreach ($p_permit_list_date as $v_permit_list_date) {                           
                    if ($v_permit_list_date == $sdate) {                               
                            $flag = 'I';                              
                    }
                }
            }

            //TODO set report overtime list from tabel overtime list    
            if (!empty($p_overtime_list_date) && $p_overtime_list->employee_id == $v_employee->employee_id) {
                foreach ($p_overtime_list_date as $v_overtime_list_date) {                           
                    if ($v_overtime_list_date == $sdate) {                               
                            $flag = 'O';                              
                    }
                }
            }

             //TODO set report sick list from tabel overtime list    
             if (!empty($p_sick_list_date) && $p_sick_list->employee_id == $v_employee->employee_id) {
                foreach ($p_sick_list_date as $v_sick_list_date) {                           
                    if ($v_sick_list_date == $sdate) {                               
                            $flag = 'S';                              
                    }
                }
            }

            //TODO set report perdin list from tabel perdin list    
            if (!empty($p_perdin_list_date) && $p_perdin_list->employee_id == $v_employee->employee_id) {
                foreach ($p_perdin_list_date as $v_perdin_list_date) {                           
                    if ($v_perdin_list_date == $sdate) {                               
                            $flag = 'D';                              
                    }
                }
            }

            $day_name = date('l', strtotime($sdate));
            if (!empty($holidays)) {
                foreach ($holidays as $v_holiday) {

                    if ($v_holiday->day == $day_name) {
                        $flag = 'L';
                    }
                }
            }
            if (!empty($p_hday)) {
                foreach ($p_hday as $v_hday) {
                    if ($v_hday == $sdate) {
                        $flag = 'L';
                    }
                }
            }
				
                if (!empty($flag)) {
                    $attendance[$sl][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate, $flag);
                } else {
                    $attendance[$sl][] = $this->attendance_model->attendance_report_by_empid_with_time($v_employee->finger_id, $sdate);
                }
                $key++;
                $flag = '';
            }
        }
        //load PHPExcel library
        $this->load->library('Excel');
        ob_start();
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $styleArray = array(
            'font' => array(
                'size' => 13,
                'name' => 'Verdana'
        ));
        $fontArray = array(
            'font' => array(
                'bold' => true,
                'size' => 11,
                'name' => 'Verdana'
        ));
        $dateArray = array(
            'font' => array(
                'bold' => true,
        ));
        $bgcolor = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'E7E7E7'),
        ));

        $fontcolor = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '#fc0303'),
        ));

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B' . '1', 'Periode:')
                ->setCellValue('D' . '1', date('d M Y', strtotime($start_date)).' s_d '.date('d M Y', strtotime($end_date)));
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B1:C1');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D1:H1');
        $objPHPExcel->getActiveSheet()->getStyle('B1:C1')->applyFromArray($fontArray);

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('J' . '1', 'Department:')
                ->setCellValue('N' . '1', $dept_name->department_name);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J1:M1');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('N1:V1');
        $objPHPExcel->getActiveSheet()->getStyle('J1:L1')->applyFromArray($fontArray);

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Comprehensive HRMS")
                ->setLastModifiedBy("Comprehensive HRMS")
                ->setTitle("Office  XLSX Test Document")
                ->setSubject("Office XLSX Test Document")
                ->setDescription("Test document for Office XLSX, generated by PHP classes.")
                ->setKeywords("office openxml php")
                ->setCategory("Excel Sheet");


        // Add some data                
        $cl = 'B';
        $bg = 'A';
        foreach ($dateSl as $edate) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($cl . '3', $edate);
            $objPHPExcel->getActiveSheet()->getColumnDimension($cl)->setWidth(3);
            $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->applyFromArray($dateArray);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '4')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '1')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '2')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '4')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '1')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '2')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $cl++;
        }

        $row = 5;
        $c = 0;
        foreach ($attendance as $name => $v_Emp) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A3', 'Name')
                    ->setCellValue('A' . $row, $employee[$name]->first_name . ' ' . $employee[$name]->last_name);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A1')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
            $col = 1;
            $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle($row, $col)->getFont()->setSize(10);
            foreach ($v_Emp as $v_result) {
                if (!empty($v_result)) {
                    $cekin = '';
                    $cekout = '';
                    foreach ($v_result as $Emp_atndnce) {
                        
                        if($Emp_atndnce->StatusAbsen == 'C/In'){
                            $cekin_tmp = date('H:i',strtotime($Emp_atndnce->Time));                            
                            if($cekin_tmp > '09:00:00'){
                                //TODO set fontcolor to red PHPExcel
                                $cekin = date("H:i",strtotime($Emp_atndnce->Time));
                            }else{
                                $cekin = date('H:i',strtotime($Emp_atndnce->Time));    
                            }
                        }
                                        
                        if($Emp_atndnce->StatusAbsen == 'C/Out'){
                            $cekout = date('H:i',strtotime($Emp_atndnce->Time));
                        }

                        $absen = $cekin.' - '.$cekout;
                       
                        $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValueByColumnAndRow($col, $row, $absen);
                            
                            if ($Emp_atndnce->StatusAbsen == 'L') {
                                $objPHPExcel->setActiveSheetIndex(0)
                                        ->setCellValueByColumnAndRow($col, $row, 'L');
                            }

                            if ($Emp_atndnce->StatusAbsen == '') {
                                $objPHPExcel->setActiveSheetIndex(0)
                                        ->setCellValueByColumnAndRow($col, $row, 'A');
                            }
							
							if ($Emp_atndnce->StatusAbsen == 'C') {
                                $objPHPExcel->setActiveSheetIndex(0)
                                        ->setCellValueByColumnAndRow($col, $row, 'C');
                            }
                            if ($Emp_atndnce->StatusAbsen == 'N') {
                                $objPHPExcel->setActiveSheetIndex(0)
                                        ->setCellValueByColumnAndRow($col, $row, 'N');
                            }

                            if ($Emp_atndnce->StatusAbsen == 'I') {
                                $objPHPExcel->setActiveSheetIndex(0)
                                        ->setCellValueByColumnAndRow($col, $row, 'I');
                            }

                            if ($Emp_atndnce->StatusAbsen == 'O') {
                                $objPHPExcel->setActiveSheetIndex(0)
                                        ->setCellValueByColumnAndRow($col, $row, 'O');
                            }

                            if ($Emp_atndnce->StatusAbsen == 'S') {
                                $objPHPExcel->setActiveSheetIndex(0)
                                        ->setCellValueByColumnAndRow($col, $row, 'S');
                            }

                            if ($Emp_atndnce->StatusAbsen == 'D') {
                                $objPHPExcel->setActiveSheetIndex(0)
                                        ->setCellValueByColumnAndRow($col, $row, 'D');
                            }
                    }        
                    $col++;
                }
               
            }
            $row++;
            $c++;
        }
        // Rename worksheet (worksheet, not filename)
        $objPHPExcel->getActiveSheet()->setTitle('Employee Attendance');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        //clean the output buffer
        ob_end_clean();
        //this is the header given from PHPExcel examples. but the output seems somewhat corrupted in some cases.
        //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //so, we use this header instead.
        // $file_name = pdf_create($view_file, date('d-F-Y', strtotime($start_date)).' s/d '.date('d-F-Y', strtotime($end_date)));
        $filename = date('d M Y', strtotime($start_date)).' s_d '.date('d M Y', strtotime($end_date)) . '  ' . 'Employee Attendance.xls'; //save our workbook as this file name
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function create_excel_old($department_id, $date) {
        $month = date('n', strtotime($date));
        $year = date('Y', strtotime($date));
        $num = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $employee = $this->attendance_model->get_employee_id_by_dept_id($department_id);
        $where = array('department_id' => $department_id);
        $dept_name = $this->attendance_model->check_by($where, 'tbl_department');

        $day = date('d', strtotime($date));
        for ($i = 1; $i <= $num; $i++) {
            $dateSl[] = $i;
        }
        $holidays = $this->global_model->get_holidays(); //tbl working Days Holiday
        if ($month >= 1 && $month <= 9) {
            $yymm = $year . '-' . '0' . $month;
        } else {
            $yymm = $year . '-' . $month;
        }
        $public_holiday = $this->global_model->get_public_holidays($yymm);
        if (!empty($public_holiday)) {
            //tbl a_calendar Days Holiday        
            foreach ($public_holiday as $p_holiday) {
                for ($k = 1; $k <= $num; $k++) {

                    if ($k >= 1 && $k <= 9) {
                        $sdate = $yymm . '-' . '0' . $k;
                    } else {
                        $sdate = $yymm . '-' . $k;
                    }
                    if ($p_holiday->start_date == $sdate && $p_holiday->end_date == $sdate) {
                        $p_hday[] = $sdate;
                    }
                    if ($p_holiday->start_date == $sdate) {
                        for ($j = $p_holiday->start_date; $j <= $p_holiday->end_date; $j++) {
                            $p_hday[] = $j;
                        }
                    }
                }
            }
        }
        foreach ($employee as $sl => $v_employee) {
            $key = 1;
            $x = 0;
            for ($i = 1; $i <= $num; $i++) {

                if ($i >= 1 && $i <= 9) {

                    $sdate = $yymm . '-' . '0' . $i;
                } else {
                    $sdate = $yymm . '-' . $i;
                }
                $day_name = date('l', strtotime("+$x days", strtotime($year . '-' . $month . '-' . $key)));
                if (!empty($holidays)) {
                    foreach ($holidays as $v_holiday) {

                        if ($v_holiday->day == $day_name) {
                            $flag = 'L';
                        }
                    }
                }
                if (!empty($p_hday)) {
                    foreach ($p_hday as $v_hday) {
                        if ($v_hday == $sdate) {
                            $flag = 'L';
                        }
                    }
                }
                if (!empty($flag)) {
                    $attendance[$sl][] = $this->attendance_model->attendance_report_by_empid($v_employee->employee_id, $sdate, $flag);
                } else {
                    $attendance[$sl][] = $this->attendance_model->attendance_report_by_empid($v_employee->employee_id, $sdate);
                }
                $key++;
                $flag = '';
            }
        }
        //load PHPExcel library
        $this->load->library('Excel');
        ob_start();
        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $styleArray = array(
            'font' => array(
                'size' => 13,
                'name' => 'Verdana'
        ));
        $fontArray = array(
            'font' => array(
                'bold' => true,
                'size' => 11,
                'name' => 'Verdana'
        ));
        $dateArray = array(
            'font' => array(
                'bold' => true,
        ));
        $bgcolor = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'E7E7E7'),
        ));
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B' . '1', 'Date:')
                ->setCellValue('D' . '1', date('F-Y', strtotime($yymm)));
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B1:C1');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D1:H1');
        $objPHPExcel->getActiveSheet()->getStyle('B1:C1')->applyFromArray($fontArray);

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('J' . '1', 'Department:')
                ->setCellValue('N' . '1', $dept_name->department_name);
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J1:M1');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('N1:V1');
        $objPHPExcel->getActiveSheet()->getStyle('J1:L1')->applyFromArray($fontArray);

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Comprehensive School Management")
                ->setLastModifiedBy("Comprehensive School Management")
                ->setTitle("Office  XLSX Test Document")
                ->setSubject("Office XLSX Test Document")
                ->setDescription("Test document for Office XLSX, generated by PHP classes.")
                ->setKeywords("office openxml php")
                ->setCategory("Excel Sheet");


        // Add some data                
        $cl = 'B';
        $bg = 'A';
        foreach ($dateSl as $date) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue($cl . '3', $date);
            $objPHPExcel->getActiveSheet()->getColumnDimension($cl)->setWidth(3);
            $objPHPExcel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->getFont()->setSize(9);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->applyFromArray($dateArray);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '4')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '1')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '2')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '4')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '1')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($bg . '2')->applyFromArray($bgcolor);
            $objPHPExcel->getActiveSheet()->getStyle($cl . '3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $cl++;
        }

        $row = 5;
        $c = 0;
        foreach ($attendance as $name => $v_Emp) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A3', 'Name')
                    ->setCellValue('A' . $row, $employee[$name]->first_name . ' ' . $employee[$name]->last_name);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A1')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(20);
            $col = 1;
            foreach ($v_Emp as $v_result) {
                if (!empty($v_result)) {
                    foreach ($v_result as $Emp_atndnce) {
                        $objPHPExcel->getActiveSheet()->getStyle($row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $objPHPExcel->getActiveSheet()->getStyle($row, $col)->getFont()->setSize(10);
                        if ($Emp_atndnce->attendance_status == '0') {
                            $objPHPExcel->setActiveSheetIndex(0)
                                    ->setCellValueByColumnAndRow($col, $row, 'A');
                        }
                        if ($Emp_atndnce->attendance_status == '1') {
                            $objPHPExcel->setActiveSheetIndex(0)
                                    ->setCellValueByColumnAndRow($col, $row, 'I');
                        }
                        if ($Emp_atndnce->attendance_status == 'L') {
                            $objPHPExcel->setActiveSheetIndex(0)
                                    ->setCellValueByColumnAndRow($col, $row, 'L');
                        }
                        $col++;
                    }
                }
            }
            $row++;
            $c++;
        }
        // Rename worksheet (worksheet, not filename)
        $objPHPExcel->getActiveSheet()->setTitle('Student Attendance');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        //clean the output buffer
        ob_end_clean();
        //this is the header given from PHPExcel examples. but the output seems somewhat corrupted in some cases.
        //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //so, we use this header instead.
        $filename = date('M-Y', strtotime($yymm)) . '  ' . 'Employee Attendance.xls'; //save our workbook as this file name
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    

}
