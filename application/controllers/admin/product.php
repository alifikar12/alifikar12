<?php
/**
 * Master Data Products
 *
 * @author doniebes
 */
class Product extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('product_model');
    }

    public function index($id=NULL) {
        //DEBUG
        // $this->output->enable_profiler(TRUE);
        $data['title'] = "Product List";

        $this->product_model->_table_name  = "tbl_product"; //table name
        $this->product_model->_order_by    = "created_at DESC";

        // retrive data from db by id
        if (!empty($id)) {
            $data['product_info_by_id'] = $this->product_model->get_by(array('id' => $id,), TRUE);

            if (empty($data['product_info_by_id'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/product');
            }
        }
        // retrive all data from db
        $data['product_info'] = $this->product_model->get();
        $data['role'] = $this->session->userdata('role');

        $data['subview'] = $this->load->view('admin/product/product', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_product() {
        $this->product_model->_table_name  = "tbl_product"; //table name        
        $this->product_model->_primary_key = "id";    //id
        
        $id = $this->input->post('id');
        
        // input data
        $data = $this->product_model->array_from_post(array('product_id', 'product_name', 'price', 'active')); //input post
        
        if(!empty($id)){
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['updated_by'] = $this->session->userdata('full_name');
            $this->product_model->save($data, $id);    
            // messages for user
            $type = "success";
            $message = "Data Successfully Saved!";

        }else{
            // check data product
            $check_product = $this->product_model->check_update('tbl_product', $where = array('product_id' => $data['product_id']));

            if (!empty($check_product)) {
                $type = "error";
                $message = "Product Already Exist!";
            }else{
                $data['created_at'] = date('Y-m-d H:i:s');
                $data['created_by'] = $this->session->userdata('full_name');
                $this->product_model->save($data);    
                // messages for user
                $type = "success";
                $message = "Data Successfully Saved!";
            }
        }
        set_message($type, $message);  
        redirect('admin/product'); //redirect page
    }
    
    public function delete_product($id) {    
        $this->product_model->_table_name   = "tbl_product"; //table name        
        $this->product_model->_primary_key  = "id";    //id
        $this->product_model->delete($id);
        $type       = "success";
        $message    = "Data Successfully Delete!";
        set_message($type, $message);    
        redirect('admin/product'); //redirect page
    }   


}
