<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of department
 *
 * @author NaYeM
 */
class Level_jabatan extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('level_jabatan_model');
    }

    public function add_level_jabatan($id = NULL) {
        $this->level_jabatan_model->_table_name = "tbl_level_jabatan"; //table name
        $this->level_jabatan_model->_order_by = "level_id";
        $data['title'] = "Add Level Jabatan";

        if ($id) { // retrive data from db by id
            // get all department by id
            $data['department_info'] = $this->level_jabatan_model->get_by(array('level_id' => $id), TRUE);
            // get all designation by department id
            $this->level_jabatan_model->_table_name = "tbl_level_jabatan"; //table name
            $data['designation_info'] = $this->level_jabatan_model->get_by(array('level_id' => $id), FALSE);

            if (empty($data['department_info'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/level_jabatan/add_level_jabatan');
            }
        }

        //page load
        $data['subview'] = $this->load->view('admin/level_jabatan/add_level_jabatan', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    

    public function save_level_jabatan($id = NULL) {
        $this->level_jabatan_model->_table_name  = "tbl_level_jabatan"; //table name        
        $this->level_jabatan_model->_primary_key = "level_id";    //id
        // input data
        $data = $this->level_jabatan_model->array_from_post(array('level_name')); //input post

        // dublicacy check 
        if (!empty($id)) {
            $level_id = array('level_id !=' => $id);
        } else {
            $level_id = null;
        }
        // check check_leave_category by where        
        // if not empty show alert message else save data
        $check_level_jabatan = $this->level_jabatan_model->check_update('tbl_level_jabatan', $where = array('level_name' => $data['level_name']), $level_id);

        if (!empty($check_level_jabatan)) {
            $type = "error";
            $message = "Level Jabatan Already Exist!";
            set_message($type, $message);
        } else {
            $this->level_jabatan_model->save($data, $id);
            // messages for user
            $type = "success";
            $message = "Level Jabatan Successfully Saved!";
            set_message($type, $message);
        }

        redirect('admin/level_jabatan/level_jabatan_list'); //redirect page
    }

   

    public function delete_level_jabatan($id, $level_name) {
        // check into tbl_employee
        $level_name = str_replace('%20', ' ', $level_name);
        $where = array('level' => $level_name);
        // print_r($where);
        // die;
        // check existing level jabatan into tbl_employee
        $check_existing_ctgry = $this->level_jabatan_model->check_by($where, 'tbl_employee');
        
        if (!empty($check_existing_ctgry)) { // if not empty do not delete this else delete
            // messages for user
            $type = "error";
            $message = "Level Jabatan Already Used!";
            set_message($type, $message);
        } else {
            $this->level_jabatan_model->_table_name = "tbl_level_jabatan"; //table name        
            $this->level_jabatan_model->_primary_key = "level_id";    //id
            $this->level_jabatan_model->delete($id);
            $type       = "success";
            $message    = "Level Jabatan Successfully Delete!";
            set_message($type, $message);
        }
        redirect('admin/level_jabatan/level_jabatan_list'); //redirect page
    }

    public function check_designations($department_id, $v_designations) { // check_designations by id and designation
        $where = array('department_id' => $department_id, 'designations' => $v_designations);
        return $this->level_jabatan_model->check_by($where, 'tbl_designations');
    }

    public function level_jabatan_list($id=NULL) {
        $data['title'] = "Set Level Jabatan";

        $this->level_jabatan_model->_table_name  = "tbl_level_jabatan"; //table name
        $this->level_jabatan_model->_order_by    = "level_id";

        // retrive data from db by id
        if (!empty($id)) {
            $data['level_jabatan'] = $this->level_jabatan_model->get_by(array('level_id' => $id,), TRUE);

            if (empty($data['level_jabatan'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/level_jabatan/level_jabatan_list');
            }
        }
        // retrive all data from db
        $data['all_level_jabatan_info'] = $this->level_jabatan_model->get();

        $data['subview'] = $this->load->view('admin/level_jabatan/level_jabatan_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

}
