<?php
/**
 *
 * @author doniebes
 */
class Company extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('company_model');
    }

    public function index($id=NULL) {
        $data['title'] = "Company";

        $this->company_model->_table_name  = "company"; //table name
        $this->company_model->_order_by    = "id";

        // retrive data from db by id
        if (!empty($id)) {
            $data['company'] = $this->company_model->get_by(array('id' => $id,), TRUE);

            if (empty($data['company'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/company');
            }
        }
        // retrive all data from db
        $data['company_info'] = $this->company_model->get();

        $data['subview'] = $this->load->view('admin/company/company_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save() {
        $this->company_model->_table_name  = "company"; //table name        
        $this->company_model->_primary_key = "id";    //id
   
        $data['id']   = $this->input->post('id');
        $data['name'] = $this->input->post('name');
       
        $id = $data['id'];
        if($_FILES['img']['name'] != ""){
            $upload = $this->company_model->uploadImg();
            if($upload['result'] == 'success'){
                if(!empty($id)){
                    $this->company_model->updateCompany($upload['file']['file_name'], $id);                                      
                }else{
                    $this->company_model->addCompany($upload['file']['file_name']);        
                }
                $type       = "success";
                $message    = "Data Successfully Add!";
                set_message($type, $message);                
                redirect(base_url() . 'admin/company');

            }else{
                $type       = "error";
                $message    = "Gagal mengubah data, pastikan icon berukuran maksimal 2mb dan berformat png, jpg, jpeg. Silakan ulangi lagi.";
                set_message($type, $message);
                redirect(base_url() . 'admin/company/');
            }
        }
        
    }

    public function delete_company($id) {
        // check into tbl_employee
        $where = array('company_id' => $id);
       
        $check_existing_company = $this->company_model->check_by($where, 'tbl_employee');
        if (!empty($check_existing_company)) { // if not empty do not delete this else delete
            // messages for user
            $type       = "error";
            $message    = "Company Already Used!";
            set_message($type, $message);
        } else {
            $this->company_model->_table_name  = "company"; //table name        
            $this->company_model->_primary_key = "id";    //id
            $this->company_model->delete($id);
            $type       = "success";
            $message    = "Data Successfully Delete!";
            set_message($type, $message);
        }
        redirect('admin/company'); //redirect page
    }

}