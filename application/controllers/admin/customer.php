<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of department
 *
 * @author NaYeM
 */
class Customer extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('level_jabatan_model');
    }

    public function add_level_jabatan($id = NULL) {
        $this->level_jabatan_model->_table_name = "tbl_level_jabatan"; //table name
        $this->level_jabatan_model->_order_by = "level_id";
        $data['title'] = "Add Level Jabatan";

        if ($id) { // retrive data from db by id
            // get all department by id
            $data['department_info'] = $this->level_jabatan_model->get_by(array('level_id' => $id), TRUE);
            // get all designation by department id
            $this->level_jabatan_model->_table_name = "tbl_level_jabatan"; //table name
            $data['designation_info'] = $this->level_jabatan_model->get_by(array('level_id' => $id), FALSE);

            if (empty($data['department_info'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/level_jabatan/add_level_jabatan');
            }
        }

        //page load
        $data['subview'] = $this->load->view('admin/level_jabatan/add_level_jabatan', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }
   

    public function customer_list($id=NULL) {
        //DEBUG
        // $this->output->enable_profiler(TRUE);

        $data['title'] = "Customer List";

        $this->level_jabatan_model->_table_name  = "tb_cust"; //table name
        $this->level_jabatan_model->_order_by    = "id_cust";

        // retrive data from db by id
        if (!empty($id)) {
            $data['level_jabatan'] = $this->level_jabatan_model->get_by(array('id_cust' => $id,), TRUE);

            if (empty($data['level_jabatan'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/customer/customer_list');
            }
        }
        // retrive all data from db
        $data['all_level_jabatan_info'] = $this->level_jabatan_model->get();

        $data['role'] = $this->session->userdata('role');

        $data['subview'] = $this->load->view('admin/customer/customer_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }


    public function save_customer($id = NULL) {
        $this->level_jabatan_model->_table_name  = "tb_cust"; //table name        
        $this->level_jabatan_model->_primary_key = "id_cust";    //id
        // input data
        $data = $this->level_jabatan_model->array_from_post(array('name', 'email', 'no_telp')); //input post

        // dublicacy check 
        if (!empty($id)) {
            $id_cust = array('id_cust !=' => $id);
        } else {
            $id_cust = null;
        }
        // check check_leave_category by where        
        // if not empty show alert message else save data
        $check_by_no_telp = $this->level_jabatan_model->check_update('tb_cust', $where = array('no_telp' => $data['no_telp']), $id_cust);

        if (!empty($check_by_no_telp)) {
            $type = "error";
            $message = "No Telp Already Exist!";
            set_message($type, $message);
        } else {
            $this->level_jabatan_model->save($data, $id);
            // messages for user
            $type = "success";
            $message = "Data Successfully Saved!";
            set_message($type, $message);
        }

        redirect('admin/customer/customer_list'); //redirect page
    }


    public function delete_customer($id) {    
        $this->level_jabatan_model->_table_name = "tb_cust"; //table name        
        $this->level_jabatan_model->_primary_key = "id_cust";    //id
        $this->level_jabatan_model->delete($id);
        $type       = "success";
        $message    = "Data Successfully Delete!";
        set_message($type, $message);
    
        redirect('admin/customer/customer_list'); //redirect page
    }

}
