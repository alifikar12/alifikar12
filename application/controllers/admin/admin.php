<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        // $this->load->library('encryption');
    }

    public function admin_list() {
        //DEBUG
        // $this->output->enable_profiler(TRUE);
        $data['menu']       = array("user_role" => 1, "c_user_role" => 1);
        $data['title']      = "List User";
        $data['users']      = $this->user_model->get_users();
        $data['subview']    = $this->load->view('admin/admin/admin_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function admin_add($id = NULL) {
        
        $data['operation'] = is_null($id) ? 'Tambah' : 'Sunting';

        if ($_POST == TRUE) {

            if ($this->input->post('user_id')) {
                $params['user_id'] = $id;
            } else {
                $params['email']        = $this->input->post('email');
                $params['password']     = $this->hash($this->input->post('password'));
                $params['role']         = $this->input->post('role');
                $params['first_name']   = $this->input->post('first_name');
                $params['user_name']    = $this->input->post('user_name');
            }

            $status = $this->user_model->add($params);
           
            // if (!empty($_FILES['user_image']['name'])) {
                // $paramsupdate['user_image'] = $this->do_upload($name = 'user_image', $fileName= $params['first_name']);
            // } 

            // $paramsupdate['user_id'] = $status;
            // $this->user_model->add($paramsupdate);
            
            if($status != FALSE){
                // $this->session->set_flashdata('success', $data['operation'] . ' Pengguna Berhasil');    
                set_message("success", "Insert Data Successfully!");
            }else{
                set_message("error", "Insert Data Failed!");
                // $this->session->set_flashdata('failed', $data['operation'] . ' Pengguna Gagal');    
            }
            
            redirect('admin/admin/admin_list');

        } else {

            if ($this->input->post('user_id')) {
                redirect('manage/users/edit/' . $this->input->post('user_id'));
            }
 
            // Edit mode
            if (!is_null($id)) {
                $object = $this->user_model->get(array('id' => $id));
                if ($object == NULL) {
                    redirect('admin/admin/admin_list');
                } else {
                    $data['user'] = $object;
                }
            }

            $data['roles'] = $this->user_model->get_role();
            $data['title'] = $data['operation'] . ' Pengguna';
            $data['subview'] = $this->load->view('admin/admin/admin_add', $data, TRUE);
            $this->load->view('admin/_layout_main', $data);
        }
    }

    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }


    public function create_user($id = null) {
        $data['title'] = "Create User";
        if (!empty($id)) {
            $data['employee_id'] = $id;
        } else {
            $data['employee_id'] = null;
        }

        $data['employee_login_details'] = $this->user_model->all_emplyee($id);
        $data['subview'] = $this->load->view('admin/user/create_user', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function buildChild($parent, $menu) {

        if (isset($menu['parents'][$parent])) {

            foreach ($menu['parents'][$parent] as $ItemID) {

                if (!isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label] = $ItemID->menu_id;
                }
                if (isset($menu['parents'][$ItemID->menu_id])) {
                    $result[$ItemID->label][$ItemID->menu_id] = self::buildChild($ItemID->menu_id, $menu);
                }
            }
        }
        return $result;
    }

    

    public function save_user_employee() {
        // $data = $this->user_model->array_from_post(array('user_name', 'name', 'email'));
        $employee_id = $this->input->post('employee_id');
        $password = $this->input->post('password');
        $password == '****************' || $data['password'] = $this->encryption->hash($this->input->post('password'));

        $this->user_model->_table_name = "tbl_employee_login"; // table name
        $this->user_model->_primary_key = "employee_login_id"; // $id

        if (!empty($employee_id)) {
            $id = $this->user_model->save($data, $employee_id);
        } else {
            $id = $this->user_model->save($data);
        }

        
        if (!empty($employee_id)) {
            $type = "success";
            $message = "User Login Information Update Successfully!";
            set_message($type, $message);
            redirect('admin/user/user_list'); //redirect page
        } else {
            $type = "success";
            $message = "New User Create Successfully!";
            set_message($type, $message);
            redirect('admin/user/user_list'); //redirect page
        }
    }

    public function save_user() {
        $data = $this->user_model->array_from_post(array('user_name', 'name', 'email', 'flag'));
        $employee_id = $this->input->post('employee_id');
        $password = $this->input->post('password');
        $password == '****************' || $data['password'] = $this->encryption->hash($this->input->post('password'));

        //delete existing userroll by login id
        if (!empty($employee_id)) {
            $this->user_model->_table_name = "tbl_user_role"; //table name
            $this->user_model->_order_by = "user_id";
            $this->user_model->_primary_key = "user_role_id";

            $roll = $this->user_model->get_by(array('user_id' => $employee_id), FALSE);

            foreach ($roll as $v_roll) {
                $this->user_model->delete($v_roll->user_role_id);
            }
        }

        $this->user_model->_table_name = "tbl_user"; // table name
        $this->user_model->_primary_key = "user_id"; // $id

        if (!empty($employee_id)) {
            $id = $this->user_model->save($data, $employee_id);
        } else {
            $id = $this->user_model->save($data);
        }

        $this->user_model->_table_name = "tbl_user_role"; // table name
        $this->user_model->_primary_key = "user_role_id"; // $id
        $menu = $this->user_model->array_from_post(array('menu'));
        if (!empty($menu['menu'])) {
            foreach ($menu as $v_menu) {
                foreach ($v_menu as $value) {
                    $mdata['menu_id'] = $value;
                    $mdata['user_id'] = $id;
                    $this->user_model->save($mdata);
                }
            }
        }
        if (!empty($employee_id)) {
            $type = "success";
            $message = "User Login Information Update Successfully!";
            set_message($type, $message);
            redirect('admin/user/user_list'); //redirect page
        } else {
            $type = "success";
            $message = "New User Create Successfully!";
            set_message($type, $message);
            redirect('admin/user/user_list'); //redirect page
        }
    }

    // Delete to database
    public function admin_delete() {        
        if ($_POST) {     
            $id = $this->user_model->delete_user($this->input->post('id'));
            set_message('success', 'Hapus data berhasil');
            redirect('admin/admin/admin_list');
        } 
    }

    function admin_rpw($id = NULL) {
        if ($_POST) {
            $id = $this->input->post('user_id');
            $params['password'] = $this->hash($this->input->post('user_password'));
            $status = $this->user_model->change_password($id, $params);
            set_message('success', 'Reset Password Berhasil');
            redirect('admin/admin/admin_list');    
        } else {

            if ($this->user_model->get(array('user_id' => $id)) == NULL) {
                redirect('admin/admin/admin_list');
            }
            $data['user']       = $this->user_model->get(array('user_id' => $id));
            $data['title']      = 'Reset Password';
            $data['subview']    = $this->load->view('admin/admin/change_pass', $data, TRUE);
            $this->load->view('admin/_layout_main', $data);

        }
    }
    

    public function delete_user($id = null) {
        if (!empty($id)) {
            $id = $this->encryption->decrypt($id);
            $user_id = $this->session->userdata('employee_id');

            //checking login user trying delete his own account
            if ($id == $user_id) {
                //same user can not delete his own account
                // redirect with error msg
                $type = "error";
                $message = "Sorry You can not delete your own account!";
                set_message($type, $message);
                redirect('admin/user/user_list'); //redirect page
            } else {
                //delete procedure run
                // Check user in db or not
                $this->user_model->_table_name = "tbl_user"; //table name
                $this->user_model->_order_by = "user_id";
                $result = $this->user_model->get_by(array('user_id' => $id), true);

                if (count($result)) {
                    //delete user roll id
                    $this->db->where('user_id', $id);
                    $this->db->delete('tbl_user_role');
                    //delete user by id
                    $this->db->where('user_id =', $id);
                    $this->db->delete('tbl_user');
                    //redirect successful msg
                    $type = "success";
                    $message = "User Delete Successfully!";
                    set_message($type, $message);
                    redirect('admin/user/user_list'); //redirect page
                } else {
                    //redirect error msg
                    $type = "error";
                    $message = "Sorry this user not find in database!";
                    set_message($type, $message);
                    redirect('admin/user/user_list'); //redirect page
                }
            }
        }
    }

}
