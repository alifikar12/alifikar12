<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Application_List extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('application_model');
        $this->load->model('employee_model');
        $this->load->model('email_model');
        // $this->load->model('email_model');
    }

    public function index()
    {
        //DEBUG
        // $this->output->enable_profiler(TRUE);	
        $data['title'] = "Application List";
        $employment_id = $this->session->userdata('employment_id');
        if ($this->session->userdata('role') == 'HRGAManager') {
            $data['all_application_info'] = $this->application_model->get_all_emp_leave_info();
        } else {
            $data['all_application_info'] = $this->application_model->get_emp_leave_info_2($employment_id);
        }

        $data['subview'] = $this->load->view('admin/application/application_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function application_list_pdf()
    {
        $data['title'] = "Application List";
        $id = $this->input->post('id');
        $data['cuti_info'] = $this->application_model->get_applist();
        $this->load->helper('dompdf');
        $view_file = $this->load->view('admin/application/application_list_pdf', $data, true);
        // echo json_encode($view_file); 
        pdf_create($view_file, 'ApplicationList');
    }

    public function permit_list()
    {
        $data['title'] = "Permit List";
        $data['all_permit_info'] = $this->application_model->get_emp_permit_info();

        $data['subview'] = $this->load->view('admin/application/permit_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function perdin_list()
    {
        $data['title'] = "Perdin List";
        $data['all_perdin_info'] = $this->application_model->get_emp_perdin_info();

        $data['subview'] = $this->load->view('admin/application/perdin_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function overtime_list()
    {
        $data['title'] = "Overtime List";
        $data['all_overtime_info'] = $this->application_model->get_emp_overtime_info();

        $data['subview'] = $this->load->view('admin/application/overtime_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }


    public function view_overtime_details($id)
    {
        $data['title']              = "Overtime List";
        $data['overtime_info']      = $this->application_model->get_emp_overtime_info($id);
        // set view status by id
        $where = array('overtime_id' => $id);
        $updata['view_status']      = '1';
        $this->application_model->set_action($where, $updata, 'tbl_overtime_list');

        $data['subview'] = $this->load->view('admin/application/overtime_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function view_redeem_point_details($id)
    {
        $data['title']              = "Redeem Point";
        $data['redeem_info']        = $this->application_model->get_emp_redeem_info($id);
        $data['product_list_info']  = $this->application_model->get_emp_redeem_info_rows($id);

        $employee_id                = $this->application_model->get_emp_redeem_info($id)->employee_id;
        $data['saldo_point']        = $this->application_model->get_saldo_point($employee_id);

        // set view status by id
        $where = array('doc_number' => $id);
        $updata['view_status']      = '1';
        $this->application_model->set_action($where, $updata, 'tbl_transaksi_point');

        $data['subview'] = $this->load->view('admin/application/redeem_point_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }


    public function redeem_point_list()
    {
        //DEBUG
        //$this->output->enable_profiler(TRUE);
        $data['title'] = "Redeem Point List";
        $data['all_redeem_info'] = $this->application_model->get_emp_redeem_info();

        $data['subview'] = $this->load->view('admin/application/redeem_point_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function proses_redeem($id)
    {
        //DEBUG
        //$this->output->enable_profiler(TRUE);	
        if (isset($_POST['approve_btn'])) {
            $set_transaksi_point['status']             = 'fully approved';
            $set_transaksi_point['approve1']        = $this->session->userdata('full_name');
            $set_transaksi_point['date_approve1']   = date('Y-m-d');

            //send email notif to approval
            $email_to                 = $this->input->post('email_to');
            $doc_number             = $this->input->post('document_number');
            $link                     = $this->input->post('link');
            $receipt_name             = $this->input->post('receipt_name');
            $approval_name             = $this->session->userdata('full_name');
            $submit_date            = $this->input->post('submit_date');
            $jk_req                 = $this->input->post('gender_requester');

            // update saldo employee point jika di approve
            $saldo_point     = $this->input->post('saldo_point');
            $ttl_redeem      = $this->input->post('ttl_redeem');
            $set_saldo_point = array('saldo_point' => $saldo_point - $ttl_redeem);
            $where = array('employment_id' => $this->application_model->get_emp_redeem_info($doc_number)->employment_id);
            $this->application_model->set_action($where, $set_saldo_point, 'tbl_employee_point');
        } else if (isset($_POST['unapprove_btn'])) {
            $set_transaksi_point['status']         = 'cancel';
            $set_transaksi_point['unapprove1']  = $this->session->userdata('full_name');
            $set_transaksi_point['date_unapprove1']   = date('Y-m-d');

            $email_to                 = $this->input->post('email_to');
            $doc_number             = $this->input->post('document_number');
            $link                     = $this->input->post('link');
            $receipt_name             = $this->input->post('receipt_name');
            $approval_name             = $this->session->userdata('full_name');
            $submit_date            = $this->input->post('submit_date');
            $jk_req                 = $this->input->post('gender_requester');
        }

        //update status tbl_transaksi_point
        $where = array('doc_number' => $id);
        $this->application_model->set_action($where, $set_transaksi_point, 'tbl_transaksi_point');

        $status_redeem    = $this->application_model->get_emp_redeem_info($doc_number)->status_redeem;
        // echo $status_redeem;
        // die;

        //get data form tbl_approval_redeem 
        $this->email_model->_table_name = "tbl_approval_redeem"; //table name
        $this->email_model->_order_by   = "approval_redeem_id";
        $data['approval_redeem'] = $this->email_model->get_by(array('approval_redeem_id' => 1,), TRUE);
        // print_r($data['approval_redeem']->acknowledge_email);
        // die;
        $ack_email      = $data['approval_redeem']->acknowledge_email;
        $ack_name       = $data['approval_redeem']->acknowledge_name;
        $ack_gender     = $data['approval_redeem']->acknowledge_gender;
        $ack_email_cc   = $data['approval_redeem']->acknowledge_email_cc;

        // send mail notif to requester
        $this->email_model->email_notif_redeem_to_requester($email_to, $doc_number, $link, $receipt_name, $approval_name, $status_redeem, $submit_date, $jk_req);

        // send notif & attachment to marketing & cc to acknowledge bu astrid
        // $email_marketing = 'ebes.doni@gmail.com';
        $this->email_model->email_notif_to_marketing($ack_email, $ack_email_cc, $doc_number, $link, $ack_name, $approval_name, $status_redeem, $submit_date, $ack_gender, $jk_req);

        $type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('admin/application_list/redeem_point_list'); //redirect page

    }

    public function view_permit_details($id)
    {
        $data['title']              = "Permit List";
        $data['permit_info']        = $this->application_model->get_emp_permit_info($id);
        // set view status by id
        $where = array('permit_id' => $id);
        $updata['view_status']      = '1';
        $this->application_model->set_action($where, $updata, 'tbl_permit_list');

        $data['subview'] = $this->load->view('admin/application/permit_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function permit_list_pdf()
    {
        $data['title'] = "Permit List";
        $data['all_permit_info'] = $this->application_model->get_emp_permit_info();
        $this->load->helper('dompdf');
        // $this->dompdf->set_option('isRemoteEnabled', TRUE);
        $view_file = $this->load->view('admin/application/permit_list_pdf', $data, true);
        pdf_create($view_file, 'Permit List');
    }

    public function view_perdin_details($id)
    {
        $data['title']              = "Perdin List";
        $data['perdin_info']        = $this->application_model->get_emp_perdin_info($id);
        // set view status by id
        $where = array('perdin_id' => $id);
        $updata['view_status']      = '1';
        $this->application_model->set_action($where, $updata, 'tbl_perdin_list');

        $data['subview'] = $this->load->view('admin/application/perdin_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function view_application($id)
    {
        $data['title'] = "Application List";
        $data['application_info'] = $this->application_model->get_emp_leave_info($id);
        // set view status by id
        $where = array('application_list_id' => $id);
        $updata['view_status'] = '1';
        $this->application_model->set_action($where, $updata, 'tbl_application_list');
        //$this->employee_model->set_action($where, $updata, 'tbl_employee');

        $data['subview'] = $this->load->view('admin/application/application_details', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function set_action($id)
    {
        $data['application_status'] = $this->input->post('application_status', TRUE);
        if ($data['application_status'] == 2) {
            $atdnc_data = $this->application_model->array_from_post(array('employee_id', 'leave_category_id'));
            $leave_start_date = $this->input->post('leave_start_date', TRUE);
            $leave_end_date = $this->input->post('leave_end_date', TRUE);
            if ($leave_start_date == $leave_end_date) {
                $this->admin_model->_table_name = 'tbl_attendance';
                $this->admin_model->_order_by = 'attendance_id';
                $check_leave_date = $this->admin_model->get_by(array('employee_id' => $atdnc_data['employee_id'], 'date' => $leave_end_date), FALSE);
                if (empty($check_leave_date)) {
                    $atdnc_data['date'] = $leave_start_date;
                    $atdnc_data['attendance_status'] = '3';
                    $this->admin_model->_table_name = 'tbl_attendance';
                    $this->admin_model->_primary_key = "attendance_id";
                    $this->admin_model->save($atdnc_data);
                }
            } else {
                for ($l = $leave_start_date; $l <= $leave_end_date; $l++) {
                    $this->admin_model->_table_name = 'tbl_attendance';
                    $this->admin_model->_order_by = 'attendance_id';
                    $check_leave_date = $this->admin_model->get_by(array('employee_id' => $atdnc_data['employee_id'], 'date' => $l), FALSE);
                    if (empty($check_leave_date)) {
                        $atdnc_data['date'] = $l;
                        $atdnc_data['attendance_status'] = '3';
                        $this->admin_model->_table_name = 'tbl_attendance';
                        $this->admin_model->_primary_key = "attendance_id";
                        $this->admin_model->save($atdnc_data);
                    }
                }
            }
        }
        $where = array('application_list_id' => $id);
        $this->application_model->set_action($where, $data, 'tbl_application_list');
        $type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('admin/application_list'); //redirect page
    }

    public function proses_leave($id)
    {
        //$this->output->enable_profiler(TRUE);
        if (isset($_POST['approve_btn'])) {
            $update_application_list['leave_status']     = 'fully approved';
            $update_application_list['approve3']         = $this->session->userdata('full_name');
            $update_application_list['date_approve3']     = date('Y-m-d');

            //TODO cek saldo cuti
            $employee_id         = $this->input->post('employee_id', TRUE);
            $leave_start_date     = $this->input->post('leave_start_date', TRUE);
            $leave_end_date     = $this->input->post('leave_end_date', TRUE);
            $leave_type         = $this->input->post('leave_type', TRUE);
            $leave_category_id     = $this->input->post('leave_category_id', TRUE);

            // retrieve hak cuti by leave category id
            $this->application_model->_table_name     = "tbl_leave_category"; //table name
            $this->application_model->_order_by     = "leave_category_id";
            $tbl_leave_category         = $this->application_model->cek_hak_cuti_from_leave_category($leave_category_id);

            $hak_cuti = $tbl_leave_category[0]->hak_cuti;

            //ambil data sisa cuti dari tabel;			
            $employee_data         = $this->application_model->cek_saldo_cuti($employee_id);
            $saldo_cuti = $employee_data[0]->saldo_cuti;
            $sisa_cuti_thn_lalu = $this->application_model->cek_saldo_cuti($employee_id)[0]->sisa_cuti_thn_lalu;

            if ($leave_category_id == 3) {
                if ($leave_start_date == $leave_end_date) {
                    $jumlah_cuti = 1;
                    //kurangi dari grace period saldo cuti dulu jika tdk sama dengan 0 
                    if ($sisa_cuti_thn_lalu != 0) {
                        $update_sisa_cuti = $sisa_cuti_thn_lalu - $jumlah_cuti;
                        $update_saldo_cuti = $saldo_cuti - $jumlah_cuti;
                    } else {
                        $update_saldo_cuti = $saldo_cuti - $jumlah_cuti;
                    }
                } else {
                    // weekend sabtu/minggu tdk di hitung
                    $awal_cuti = strtotime($leave_start_date);
                    $akhir_cuti = strtotime($leave_end_date);

                    $haricuti = array();
                    $sabtuminggu = array();

                    for ($i = $awal_cuti; $i <= $akhir_cuti; $i += (60 * 60 * 24)) {
                        if (date('w', $i) !== '0' && date('w', $i) !== '6') {
                            $haricuti[] = $i;
                        } else {
                            $sabtuminggu[] = $i;
                        }
                    }

                    $jumlah_cuti = count($haricuti);
                    $jumlah_sabtuminggu = count($sabtuminggu);
                    $abtotal = $jumlah_cuti + $jumlah_sabtuminggu;

                    // cek saldo grace period cuti jika tdk sama 0 kurangi di grace period dulu
                    if ($sisa_cuti_thn_lalu != 0) {
                        $update_sisa_cuti = $sisa_cuti_thn_lalu - $jumlah_cuti;
                        $update_saldo_cuti = $saldo_cuti - $jumlah_cuti;
                    } else {
                        $update_saldo_cuti = $saldo_cuti - $jumlah_cuti;
                    }
                }
            }

            //cuti setengah hari
            if ($leave_category_id = 14) {
                if ($leave_start_date == $leave_end_date) {
                    $jumlah_cuti = 0.5;
                    //kurangi dari grace period saldo cuti dulu jika tdk sama dengan 0 
                    if ($sisa_cuti_thn_lalu != 0) {
                        $update_sisa_cuti = $sisa_cuti_thn_lalu - $jumlah_cuti;
                        $update_saldo_cuti = $saldo_cuti - $jumlah_cuti;
                    } else {
                        $update_saldo_cuti = $saldo_cuti - $jumlah_cuti;
                    }
                } else {
                    // weekend sabtu/minggu tdk di hitung
                    $awal_cuti = strtotime($leave_start_date);
                    $akhir_cuti = strtotime($leave_end_date);

                    $haricuti = array();
                    $sabtuminggu = array();

                    for ($i = $awal_cuti; $i <= $akhir_cuti; $i += (30 * 30 * 12)) {
                        if (date('w', $i) !== '0' && date('w', $i) !== '3') {
                            $haricuti[] = $i;
                        } else {
                            $sabtuminggu[] = $i;
                        }
                    }

                    //         $jumlah_cuti = count($haricuti);
                    //         $jumlah_sabtuminggu = count($sabtuminggu);
                    //         $abtotal = $jumlah_cuti + $jumlah_sabtuminggu;

                    //         // cek saldo grace period cuti jika tdk sama 0 kurangi di grace period dulu
                    //         if ($sisa_cuti_thn_lalu != 0) {
                    //             $update_sisa_cuti = $sisa_cuti_thn_lalu - $jumlah_cuti;
                    //             $update_saldo_cuti = $saldo_cuti - $jumlah_cuti;
                    //         } else {
                    //             $update_saldo_cuti = $saldo_cuti - $jumlah_cuti;
                    //         }
                    //     }
                }
            } else {

                if ($hak_cuti != 0) {
                    //cek jumlah yg diajukan
                    $k = 1;
                    for ($l = $leave_start_date; $l <= $leave_end_date; $l++) {
                        $jumlah_yg_diajukan = $k;
                        $k++;
                    }
                    if ($jumlah_yg_diajukan > $hak_cuti) {
                        $update_saldo_cuti     = $saldo_cuti - ($jumlah_yg_diajukan - $hak_cuti);
                    } else {
                        $update_saldo_cuti = $saldo_cuti;
                    }
                } else {
                    $update_saldo_cuti = $saldo_cuti;
                }
            }
            //else {

            //     if ($hak_cuti != 0) {
            //         //cek jumlah yg diajukan
            //         $k = 0.5;
            //         for ($l = $leave_start_date; $l <= $leave_end_date; $l++) {
            //             $jumlah_yg_diajukan = $k;
            //             $k++;
            //         }
            //         if ($jumlah_yg_diajukan > $hak_cuti) {
            //             $update_saldo_cuti     = $saldo_cuti - ($jumlah_yg_diajukan - $hak_cuti);
            //         } else {
            //             $update_saldo_cuti = $saldo_cuti;
            //         }
            //     } else {
            //         $update_saldo_cuti = $saldo_cuti;
            //     }
            // } //////////


            // cek saldo grace period cuti jika tdk sama 0 kurangi di grace period dulu
            if ($sisa_cuti_thn_lalu != 0) {
                $update_employee['sisa_cuti_thn_lalu']  = $update_sisa_cuti;
                $update_employee['saldo_cuti']          = $update_saldo_cuti;
            } else {
                $update_employee['saldo_cuti']          = $update_saldo_cuti;
            }

            // save data saldo cuti to application list
            $update_application_list['sisa_cuti']     = $update_saldo_cuti;

            //Update saldo cuti karyawan
            $where = array('employee_id' => $employee_id);
            $this->employee_model->set_action($where, $update_employee, 'tbl_saldo_cuti');

            //send email notif
            $email_to             = $this->input->post('email_to');
            $doc_number         = $this->input->post('document_number');
            $link                 = $this->input->post('link');
            $receipt_name         = $this->input->post('receipt_name');
            $sender_name         = $this->input->post('sender_name');
            $leave_start_date    = $this->input->post('leave_start_date');
            $leave_end_date        = $this->input->post('leave_end_date');
            $email_hybris        = $this->input->post('email_hybris');
            $hybris_name        = $this->input->post('hybris_name');
            $jk_hybris            = $this->input->post('jk_hybris');
            $jk_sender            = $this->input->post('jk_sender');
            $jk_receipt            = $this->input->post('jk_receipt');
        } else if (isset($_POST['unapprove_btn'])) {
            $update_application_list['leave_status']     = 'cancel';
            $update_application_list['unapprove3']         = $this->session->userdata('full_name');
            $update_application_list['date_unapprove3'] = date('Y-m-d');

            $email_to             = $this->input->post('email_to');
            $doc_number         = $this->input->post('document_number');
            $link                 = $this->input->post('link');
            $receipt_name         = $this->input->post('receipt_name');
            $sender_name         = $this->input->post('sender_name');
            $leave_start_date    = $this->input->post('leave_start_date');
            $leave_end_date        = $this->input->post('leave_end_date');
            $jk_hybris            = $this->input->post('jk_hybris');
            $jk_sender            = $this->input->post('jk_sender');
            $jk_receipt            = $this->input->post('jk_receipt');
        } else if (isset($_POST['cancel_btn'])) {
            $update_application_list['leave_status']         = 'cancel';
            $update_application_list['unapprove3']          = $this->session->userdata('full_name');
            $update_application_list['date_unapprove3']     = date('Y-m-d');

            //TODO cek saldo cuti
            $employee_id         = $this->input->post('employee_id', TRUE);
            $leave_start_date     = $this->input->post('leave_start_date', TRUE);
            $leave_end_date     = $this->input->post('leave_end_date', TRUE);
            $leave_type         = $this->input->post('leave_type', TRUE);
            $leave_category_id     = $this->input->post('leave_category_id', TRUE);

            //ambil data sisa cuti dari tabel;			
            $saldo_cuti         = $this->application_model->cek_saldo_cuti($employee_id)[0]->saldo_cuti;
            $sisa_cuti_thn_lalu = $this->application_model->cek_saldo_cuti($employee_id)[0]->sisa_cuti_thn_lalu;

            /** leave category id 3 sama dengan annual leave atau cuti tahunan 
             * or leave category id 4 : cuti bersama hari libur resmi		
             */
            if ($leave_category_id == 3 || $leave_category_id == 4) {
                if ($leave_start_date == $leave_end_date) {
                    $jumlah_yg_diajukan = 1;
                    $update_saldo_cuti  = $saldo_cuti + $jumlah_yg_diajukan;
                } else {

                    // weekend sabtu/minggu tdk di hitung
                    $awal_cuti = strtotime($leave_start_date);
                    $akhir_cuti = strtotime($leave_end_date);

                    $haricuti = array();
                    $sabtuminggu = array();

                    for ($i = $awal_cuti; $i <= $akhir_cuti; $i += (60 * 60 * 24)) {
                        if (date('w', $i) !== '0' && date('w', $i) !== '6') {
                            $haricuti[] = $i;
                        } else {
                            $sabtuminggu[] = $i;
                        }
                    }



                    $jumlah_cuti = count($haricuti);
                    $jumlah_sabtuminggu = count($sabtuminggu);
                    $abtotal = $jumlah_cuti + $jumlah_sabtuminggu;

                    // cek saldo grace period cuti jika tdk sama 0 kurangi di grace period dulu
                    if ($sisa_cuti_thn_lalu != 0) {
                        $update_sisa_cuti = $sisa_cuti_thn_lalu + $jumlah_cuti;
                        $update_saldo_cuti = $saldo_cuti + $jumlah_cuti;
                    } else {
                        $update_saldo_cuti = $saldo_cuti + $jumlah_cuti;
                    }
                }
            }

            // /// Leave catagory 14 sama dengan setengah hari

            // if ($leave_category_id = 14) {
            //     if ($leave_start_date == $leave_end_date) {
            //         $jumlah_yg_diajukan = 0.5;
            //         $update_saldo_cuti  = $saldo_cuti + $jumlah_yg_diajukan;
            //     } else {

            //         // weekend sabtu/minggu tdk di hitung
            //         $awal_cuti = strtotime($leave_start_date);
            //         $akhir_cuti = strtotime($leave_end_date);

            //         $haricuti = array();
            //         $sabtuminggu = array();

            //         for ($i = $awal_cuti; $i <= $akhir_cuti; $i += (30 * 30 * 12)) {
            //             if (date('w', $i) !== '0' && date('w', $i) !== '3') {
            //                 $haricuti[] = $i;
            //             } else {
            //                 $sabtuminggu[] = $i;
            //             }
            //         }



            //         $jumlah_cuti = count($haricuti);
            //         $jumlah_sabtuminggu = count($sabtuminggu);
            //         $abtotal = $jumlah_cuti + $jumlah_sabtuminggu;

            //         // cek saldo grace period cuti jika tdk sama 0 kurangi di grace period dulu
            //         if ($sisa_cuti_thn_lalu != 0) {
            //             $update_sisa_cuti = $sisa_cuti_thn_lalu + $jumlah_cuti;
            //             $update_saldo_cuti = $saldo_cuti + $jumlah_cuti;
            //         } else {
            //             $update_saldo_cuti = $saldo_cuti + $jumlah_cuti;
            //         }
            //     }
            // }

            // cek saldo grace period cuti jika tdk sama 0 kurangi di grace period dulu
            if ($sisa_cuti_thn_lalu != 0) {
                $update_employee['sisa_cuti_thn_lalu']  = $update_sisa_cuti;
                $update_employee['saldo_cuti']          = $update_saldo_cuti;
            } else {
                $update_employee['saldo_cuti']          = $update_saldo_cuti;
            }

            //Update saldo cuti karyawan
            $where = array('employee_id' => $employee_id);
            $this->employee_model->set_action($where, $update_employee, 'tbl_saldo_cuti');

            // //params send email notif to approval
            // $email_to 			= $this->input->post('email_to');
            // $doc_number 	    = $this->input->post('document_number');
            // $link 				= $this->input->post('link');
            // $receipt_name 		= $this->input->post('receipt_name');
            // $sender_name 		= $this->input->post('sender_name');	
            // $leave_start_date	= $this->input->post('leave_start_date');
            // $leave_end_date	    = $this->input->post('leave_end_date');	
            // $email_hybris	    = $this->input->post('email_hybris');
            // $hybris_name	    = $this->input->post('hybris_name');	

        }

        //update status application list karyawan
        // $where = array('application_list_id' => $doc_number);
        // $this->application_model->set_action($where, $update_application_list, 'tbl_application_list');
        // $leave_status	= $this->application_model->get_application_info($doc_number)->leave_status;

        // $this->email_model->email_notif_to_requester($email_to, $doc_number, $link, $receipt_name, $sender_name, $leave_status, $leave_start_date, $leave_end_date, $jk_sender, $jk_receipt);
        // $this->email_model->email_notif_to_hybris($email_hybris, $receipt_name, $hybris_name, $leave_start_date, $leave_end_date, $jk_hybris, $jk_receipt);

        /**
         * set using new template
         */
        //set new template email
        $data['subject']           = 'Notifikasi Pengajuan Cuti';
        $data['email_to']             = $this->input->post('email_to');
        $data['doc_number']        = $this->input->post('document_number');
        $data['link']              = $this->input->post('link');
        $data['receipt_name']      = $this->input->post('receipt_name');
        $data['sender_name']       = $this->input->post('sender_name');
        $data['leave_start_date']  = $this->input->post('leave_start_date');
        $data['leave_end_date']    = $this->input->post('leave_end_date');
        $data['jk_hybris']            = $this->input->post('jk_hybris');
        $data['jk_sender']            = $this->input->post('jk_sender');
        $data['jk_receipt']        = $this->input->post('jk_receipt');
        $data['email_hybris']        = $this->input->post('email_hybris');
        $data['hybris_name']        = $this->input->post('hybris_name');

        //update status application list karyawan
        $where = array('application_list_id' => $data['doc_number']);
        $this->application_model->set_action($where, $update_application_list, 'tbl_application_list');
        $data['leave_status']    = $this->application_model->get_application_info($doc_number)->leave_status;

        $template_to_requester   = $this->load->view('email/notif_to_requester', $data, TRUE);
        $notif_to_requester = $this->smtp_server->itpkg($data['email_to'], $data['subject'], $template_to_requester);

        $template_to_hybris = $this->load->view('email/notif_to_hybris', $data, TRUE);
        $notif_to_hybris = $this->smtp_server->itpkg($data['email_hybris'], $data['subject'], $template_to_hybris);
        /**
         * end set uding new template
         */
        $type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('admin/application_list'); //redirect page

    }

    // /**
    //  * view template email
    //  */
    //     //cek template 
    //     public function template_notif(){
    //         $data=NULL;
    //         $data['subview'] = $this->load->view('email/notif_to_requester', TRUE);
    //         $this->load->view('email/notif_to_requester', $data, TRUE);
    //     }
    // /**
    //  * 
    //  */

    public function proses_permit($id)
    {
        if (isset($_POST['approve_btn'])) {
            $update_permit_list['permit_status']     = 'fully approved';
            $update_permit_list['acknowledge']        = $this->session->userdata('full_name');
            $update_permit_list['date_acknowledge'] = date('Y-m-d');


            //send email notif to approval
            $email_to             = $this->input->post('email_to');
            $document_number     = $this->input->post('document_number');
            $link                 = $this->input->post('link');
            $receipt_name         = $this->input->post('receipt_name');
            $sender_name         = $this->input->post('sender_name');
            $permit_start_time    = $this->input->post('permit_start_time');
            $permit_end_time    = $this->input->post('permit_end_time');
            $dl_name            = $this->input->post('dl_name');
            $gender_requester   = $this->input->post('gender_requester');


            /*
            $email_hybris	    = $this->input->post('email_hybris');
            $hybris_name	    = $this->input->post('hybris_name');	
            
            $this->email_model->email_notif_to_hybris($email_hybris, $receipt_name, $hybris_name, $leave_start_date, $leave_end_date);
            */
        } else if (isset($_POST['unapprove_btn'])) {
            $update_permit_list['permit_status']     = 'cancel';
            /*
            $update_permit_list['unapprove2'] 		= $this->session->userdata('full_name');
			$update_permit_list['date_unapprove2'] = date('Y-m-d');
            */

            $email_to             = $this->input->post('email_to');
            $document_number     = $this->input->post('document_number');
            $link                 = $this->input->post('link');
            $receipt_name         = $this->input->post('receipt_name');
            $sender_name         = $this->input->post('sender_name');
            $permit_start_time    = $this->input->post('permit_start_time');
            $permit_end_time    = $this->input->post('permit_end_time');
        }
        //update status permit list karyawan
        $where = array('permit_id' => $id);
        $this->application_model->set_action($where, $update_permit_list, 'tbl_permit_list');

        $permit_info    = $this->application_model->get_permit_info($document_number);
        $permit_status  = $permit_info->permit_status;

        $this->email_model->email_notif_permit_to_requester($email_to, $document_number, $link, $receipt_name, $sender_name, $permit_status, $permit_start_time, $permit_end_time, $dl_name, $gender_requester);

        $type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('admin/application_list/permit_list'); //redirect page

    }

    public function proses_perdin($id)
    {
        if (isset($_POST['approve_btn'])) {
            $update_perdin_list['perdin_status']     = 'fully approved';
            $update_perdin_list['acknowledge']        = $this->session->userdata('full_name');
            $update_perdin_list['date_acknowledge'] = date('Y-m-d');


            //send email notif to approval
            $email_to             = $this->input->post('email_to');
            $doc_number         = $this->input->post('document_number');
            $link                 = $this->input->post('link');
            $receipt_name         = $this->input->post('receipt_name');
            $sender_name         = $this->input->post('sender_name');
            $perdin_start_date    = $this->input->post('perdin_start_date');
            $perdin_end_date    = $this->input->post('perdin_end_date');
            $dl_name            = $this->input->post('dl_name');
            $gender_requester   = $this->input->post('gender_requester');


            /*
            $email_hybris	    = $this->input->post('email_hybris');
            $hybris_name	    = $this->input->post('hybris_name');	
            
            $this->email_model->email_notif_to_hybris($email_hybris, $receipt_name, $hybris_name, $leave_start_date, $leave_end_date);
            */
        } else if (isset($_POST['unapprove_btn'])) {
            $update_perdin_list['perdin_status']     = 'cancel';
            /*
            $update_perdin_list['unapprove2'] 		= $this->session->userdata('full_name');
			$update_perdin_list['date_unapprove2'] = date('Y-m-d');
            */

            $email_to             = $this->input->post('email_to');
            $doc_number         = $this->input->post('document_number');
            $link                 = $this->input->post('link');
            $receipt_name         = $this->input->post('receipt_name');
            $sender_name         = $this->input->post('sender_name');
            $perdin_start_date    = $this->input->post('perdin_start_date');
            $perdin_end_date    = $this->input->post('perdin_end_date');
        }
        //update status perdin list karyawan
        $where = array('perdin_id' => $id);
        $this->application_model->set_action($where, $update_perdin_list, 'tbl_perdin_list');

        $perdin_info    = $this->application_model->get_perdin_info($doc_number);
        $perdin_status  = $perdin_info->perdin_status;

        $this->email_model->email_notif_perdin_to_requester($email_to, $doc_number, $link, $receipt_name, $sender_name, $perdin_status, $perdin_start_date, $perdin_end_date, $dl_name, $gender_requester);

        $type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('admin/application_list/perdin_list'); //redirect page

    }


    public function proses_overtime($id)
    {
        if (isset($_POST['approve_btn'])) {
            $update_overtime_list['overtime_status']     = 'fully approved';
            $update_overtime_list['acknowledge']        = $this->session->userdata('full_name');
            $update_overtime_list['date_acknowledge']   = date('Y-m-d');


            //send email notif to approval
            $email_to                 = $this->input->post('email_to');
            $document_number         = $this->input->post('document_number');
            $link                     = $this->input->post('link');
            $receipt_name             = $this->input->post('receipt_name');
            $sender_name             = $this->input->post('sender_name');
            $overtime_start_time    = $this->input->post('overtime_start_time');
            $overtime_end_time      = $this->input->post('overtime_end_time');
            $dl_name                = $this->input->post('dl_name');
            $gender_requester       = $this->input->post('gender_requester');
            $overtime_date          = $this->input->post('overtime_date');
        } else if (isset($_POST['unapprove_btn'])) {
            $update_overtime_list['overtime_status']     = 'cancel';

            $email_to                 = $this->input->post('email_to');
            $document_number         = $this->input->post('document_number');
            $link                     = $this->input->post('link');
            $receipt_name             = $this->input->post('receipt_name');
            $sender_name             = $this->input->post('sender_name');
            $overtime_start_time    = $this->input->post('overtime_start_time');
            $overtime_end_time      = $this->input->post('overtime_end_time');
            $overtime_date          = $this->input->post('overtime_date');
        }

        //update status overtime list karyawan
        $where = array('overtime_id' => $id);
        $this->application_model->set_action($where, $update_overtime_list, 'tbl_overtime_list');

        $overtime_info    = $this->application_model->get_overtime_info($document_number);
        $overtime_status  = $overtime_info->overtime_status;

        $this->email_model->email_notif_overtime_to_requester($email_to, $document_number, $link, $receipt_name, $sender_name, $overtime_status, $overtime_start_time, $overtime_end_time, $dl_name, $gender_requester, $overtime_date);

        $type = "success";
        $message = "Application Status Successfully Changed!";
        set_message($type, $message);
        redirect('admin/application_list/overtime_list'); //redirect page

    }
}
