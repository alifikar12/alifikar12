<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author DSB
 */
class Employee_Store extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('employee_store_model');
        $this->load->model('employee_model');
    }

    public function employee_list_store($id = NULL) {
		//DEBUG
        $this->output->enable_profiler(TRUE);
        $data['title'] = "Employee List";
        $data['all_employee_store_info'] = $this->employee_store_model->all_emplyee_store_info();
        $data['subview'] = $this->load->view('admin/employee_store/employee_list_store', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }


    public function add_employee_store($id = NULL) {
        // $this->output->enable_profiler(TRUE);
        $data['title'] = "Add Employee Store";

        if (!empty($id)) {// retrive data from db by id            
            $data['employee_store_info'] = $this->employee_store_model->all_emplyee_store_info($id);

            if (empty($data['employee_store_info'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/employee_store/add_employee_store');
            }
        }

        // retrive all data from department table
        $this->employee_store_model->_table_name = "tbl_department"; //table name
        $this->employee_store_model->_order_by = "department_id";
        $all_dept_info = $this->employee_store_model->get();
        // get all department info and designation info
        foreach ($all_dept_info as $v_dept_info) {
            $data['all_department_info'][$v_dept_info->department_name] = $this->employee_model->get_add_department_by_id($v_dept_info->department_id);
        }
        // retrive country
        $this->employee_store_model->_table_name = "countries"; //table name
        $this->employee_store_model->_order_by = "countryName";
        $data['all_country'] = $this->employee_store_model->get();

        // retrive department
        $this->employee_store_model->_table_name = "tbl_department"; //table name
        $this->employee_store_model->_order_by = "department_id";
        $data['all_department'] = $this->employee_store_model->get();

        // retrive level
        $this->employee_store_model->_table_name  = "tbl_level_jabatan"; //table name
        $this->employee_store_model->_order_by    = "level_id";
        $data['all_level'] = $this->employee_store_model->get();

        // retrive penempatan
        $this->employee_store_model->_table_name  = "tbl_penempatan"; //table name
        $this->employee_store_model->_order_by    = "lokasi_id";
        $data['all_lokasi'] = $this->employee_store_model->get();

        // $data['all_level'] = $this->employee_store_model->get_level_employee();

        // retrive employee
        //$this->employee_store_model->_table_name = "tbl_employee_store"; //table name
        //$this->employee_store_model->_order_by = "employee_id";
        // $data['all_employee_store'] = $this->employee_store_model->all_approval_info();

        //get approval employee_store
        // $data['approval_employee_store'] = $this->employee_store_model->get_approval_employee();

        //get HR & GA List
        // $data['hrga_list'] = $this->employee_store_model->get_hrga_list();

        //get HR & GA List
        // $data['bod_list'] = $this->employee_store_model->get_bod_list();       

        //page load
        $data['subview'] = $this->load->view('admin/employee_store/add_employee_store', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }


    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }


    public function delete_employee_store($id, $bank_id, $doc_id, $ess_id, $matrix_approval_id) {
        // ************* Delete into Employee Table 
        $this->employee_store_model->_table_name = "tbl_store_employee"; // table name
        $this->employee_store_model->_primary_key = "employee_id"; // $id
        $this->employee_store_model->delete($id);
        // delete into tbl bank 
        $this->employee_store_model->_table_name = "tbl_store_employee_bank"; // table name
        $this->employee_store_model->_primary_key = "employee_bank_id"; // $id
        $this->employee_store_model->delete($bank_id);

        // delete into tbl employee document
        $this->employee_store_model->_table_name = "tbl_store_employee_document"; // table name
        $this->employee_store_model->_primary_key = "document_id"; // $id
        $this->employee_store_model->delete($doc_id);
		
		// delete into tbl ess setup
        $this->employee_store_model->_table_name = "tbl_store_employee_ess_setup"; // table name
        $this->employee_store_model->_primary_key = "ess_id"; // $id
        $this->employee_store_model->delete($ess_id);
		
		// delete into tbl ess setup
        $this->employee_store_model->_table_name = "tbl_store_employee_matrix_approval"; // table name
        $this->employee_store_model->_primary_key = "matrix_approval_id"; // $id
        $this->employee_store_model->delete($matrix_approval_id);

        // delete into tbl employee login
        $this->employee_store_model->_table_name = "tbl_store_employee_login"; // table name
        $this->employee_store_model->_order_by = "employee_id"; // table name        
        $this->employee_store_model->_primary_key = "employee_login_id"; // $id
        $login_id = $this->employee_store_model->get_by(array('employee_id'=> $id), TRUE);
        
        $this->employee_store_model->delete($login_id->employee_login_id);

        // messages for user
        $type = "success";
        $message = "Employee Information Successfully Delete!";
        set_message($type, $message);
        redirect('admin/employee_store/employee_list_store'); //redirect page
    }

    public function save_employee_store($id = NULL) {
        // **** Employee Personal Details,Contact Details and Official Status Save And Update Start *** 
        //input post
        $data = $this->employee_store_model->array_from_post(array('first_name', 'last_name', 'date_of_birth', 'gender', 'maratial_status', 'religion', 'mother_name', 'nationality',
            'personal_id', 'present_address', 'city', 'country_id', 'mobile', 'phone', 'email', 'employment_id', 'department_id', 'designations_id', 'joining_date', 'employment_status',
            'end_of_contract_date', 'status_pajak', 'npwp', 'level', 'resign_date', 'resign_status', 'finger_id', 'penempatan'));
        
		
		/*
		$where_ess_setup 					= array('employee_id' => $id);
		//update tabel ess setup
		$this->employee_store_model->set_action($where_ess_setup, $ess_setup, 'ess_setup'); // update tbl ess setup
		*/
		
		
		//image upload
        if (!empty($_FILES['photo']['name'])) {
            $old_path = $this->input->post('old_path');
            if ($old_path) {
                unlink($old_path);
            }

            $val = $this->employee_store_model->uploadImage('photo');
            $val == TRUE || redirect('admin/employee_store/add_employee');
            $data['photo'] = $val['path'];
            $data['photo_a_path'] = $val['fullPath'];
        }

        // ************* Save into Employee Table 
        $this->employee_store_model->_table_name = "tbl_store_employee"; // table name
        $this->employee_store_model->_primary_key = "employee_id"; // $id
        if (!empty($id)) {
            $employee_id = $id;
            $data['status'] = $this->input->post('status', TRUE);
            $this->employee_store_model->save($data, $id);
        } else {
            $data['status'] = 1;
            $employee_id = $this->employee_store_model->save($data);
        }
        // save into tbl employee login 
        $this->employee_store_model->_table_name = "tbl_store_employee_login"; // table name
        $this->employee_store_model->_primary_key = "employee_login_id"; // $id
        // check employee login details exsist or not 
        // if existing do not save 
        // else save the login details
        $check_existing_data 	= $this->employee_store_model->check_by(array('employee_id' => $employee_id), 'tbl_store_employee_login');
        $ldata['employee_id'] 	= $employee_id;
        $ldata['user_name'] 	= $data['employment_id'];
        $ldata['password'] 		= $this->hash('qwerty123');
        $ldata['activate'] 		= $data['status'];

        if (!empty($check_existing_data)) {
            $this->employee_store_model->save($ldata, $check_existing_data->employee_login_id);
        } else {
            $this->employee_store_model->save($ldata);
        }
		
		$essdata = $this->employee_store_model->array_from_post(array('overtime_application', 'permit_application', 'leave_application'));
		
		// save into tbl ess setup 
        $this->employee_store_model->_table_name 	= "tbl_store_employee_ess_setup"; // table name
        $this->employee_store_model->_primary_key = "ess_id"; // $id
		
		$essdata['employee_id']				= $employee_id;	
		
		$ess_id = $this->input->post('ess_id', TRUE);
        if (!empty($ess_id)) {
            $this->employee_store_model->save($essdata, $ess_id);
        } else {
            $this->employee_store_model->save($essdata);
        }
		
 
        // ** Employee Matrix Approval Save & Update data  **
        $matrix_data = $this->employee_store_model->array_from_post(array('direct_leader_name', 'direct_leader_email', 'email_hr_pic', 'nama_pic_hr', 'email_hybris', 'hybris_name', 'direct_leader_id'));
        $matrix_data['employee_id']         = $employee_id;
        $matrix_data['employment_id']       = $data['employment_id'];
        $matrix_data['first_name']          = $data['first_name'];
        $matrix_data['last_name']           = $data['last_name'];
        $matrix_data['direct_leader_id']    = $matrix_data['direct_leader_id'];

        $this->employee_store_model->_table_name = "tbl_store_employee_matrix_approval"; // table name
        $this->employee_store_model->_primary_key = "matrix_approval_id"; // $id

        $matrix_approval_id = $this->input->post('matrix_approval_id', TRUE);
        if (!empty($matrix_approval_id)) {
            $this->employee_store_model->save($matrix_data, $matrix_approval_id);
        } else {
            $this->employee_store_model->save($matrix_data);
        }
		
        // 
        // **** Employee Personal Details,Contact Details and Official Status Save And Update End *** 
        // ** Employee Bank Information Save and Update Start  **
        $bank_data = $this->employee_store_model->array_from_post(array('bank_name', 'branch_name', 'account_name', 'account_number'));
        $bank_data['employee_id'] = $employee_id;
        $this->employee_store_model->_table_name = "tbl_store_employee_bank"; // table name
        $this->employee_store_model->_primary_key = "employee_bank_id"; // $id

        $employee_bank_id = $this->input->post('employee_bank_id', TRUE);
        if (!empty($employee_bank_id)) {
            $this->employee_store_model->save($bank_data, $employee_bank_id);
        } else {
            $this->employee_store_model->save($bank_data);
        }
        // * Employee Bank Information Save and Update End   *
        // ** Employee Document Information Save and Update Start  **
        // Resume File upload
        if (!empty($_FILES['resume']['name'])) {
            $old_path = $this->input->post('resume_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_store_model->uploadFile('resume');
            $val == TRUE || redirect('admin/employee_store/add_employee_store');
            $document_data['resume_filename'] = $val['fileName'];
            $document_data['resume'] = $val['path'];
            $document_data['resume_path'] = $val['fullPath'];
        }

        // offer_letter File upload
        if (!empty($_FILES['offer_letter']['name'])) {
            $old_path = $this->input->post('offer_letter_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_store_model->uploadFile('offer_letter');
            $val == TRUE || redirect('admin/employee_store/add_employee_store');
            $document_data['offer_letter_filename'] = $val['fileName'];
            $document_data['offer_letter'] = $val['path'];
            $document_data['offer_letter_path'] = $val['fullPath'];
        }
        // joining_letter File upload
        if (!empty($_FILES['joining_letter']['name'])) {
            $old_path = $this->input->post('joining_letter_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_store_model->uploadFile('joining_letter');
            $val == TRUE || redirect('admin/employee_store/add_employee_store');
            $document_data['joining_letter_filename'] = $val['fileName'];
            $document_data['joining_letter'] = $val['path'];
            $document_data['joining_letter_path'] = $val['fullPath'];
        }

        // contract_paper File upload
        if (!empty($_FILES['contract_paper']['name'])) {
            $old_path = $this->input->post('contract_paper_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_store_model->uploadFile('contract_paper');
            $val == TRUE || redirect('admin/employee_store/add_employee_store');
            $document_data['contract_paper_filename'] = $val['fileName'];
            $document_data['contract_paper'] = $val['path'];
            $document_data['contract_paper_path'] = $val['fullPath'];
        }
        // id_proff File upload
        if (!empty($_FILES['id_proff']['name'])) {
            $old_path = $this->input->post('id_proff_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_store_model->uploadFile('id_proff');
            $val == TRUE || redirect('admin/employee_store/add_employee_store');
            $document_data['id_proff_filename'] = $val['fileName'];
            $document_data['id_proff'] = $val['path'];
            $document_data['id_proff_path'] = $val['fullPath'];
        }
        // id_proff File upload
        if (!empty($_FILES['other_document']['name'])) {
            $old_path = $this->input->post('other_document_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_store_model->uploadFile('other_document');
            $val == TRUE || redirect('admin/employee_store/add_employee_store');
            $document_data['other_document_filename'] = $val['fileName'];
            $document_data['other_document'] = $val['path'];
            $document_data['other_document_path'] = $val['fullPath'];
        } else {
            
        }

        $document_data['employee_id'] = $employee_id;

        $this->employee_store_model->_table_name = "tbl_store_employee_document"; // table name
        $this->employee_store_model->_primary_key = "document_id"; // $id
        $document_id = $this->input->post('document_id', TRUE);
        if (!empty($document_id)) {
            $this->employee_store_model->save($document_data, $document_id);
        } else {
            $this->employee_store_model->save($document_data);
        }
        // ***Employee Document Information Save and Update End   ***
        // messages for user
        $type = "success";
        $message = "Employee Information Successfully Saved!";
        set_message($type, $message);
        redirect('admin/employee_store/employee_list_store'); //redirect page
    }

}