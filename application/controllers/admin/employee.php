<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of employee
 *
 * @author DSB
 */
class Employee extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('employee_model');
        $this->load->model('application_model');
    }

    public function add_employee($id = NULL) {
        // $this->output->enable_profiler(TRUE);
        $data['title'] = "Add Employee";

        if (!empty($id)) {// retrive data from db by id            
            $data['employee_info'] = $this->employee_model->all_emplyee_info($id);

            $id_approval1 = $this->employee_model->all_emplyee_info($id)->direct_leader_id;
            $id_approval2 = $this->employee_model->all_emplyee_info($id)->direct_leader_id_2;
            //get approval employee

            if($this->employee_model->get_approval1_employee($id_approval1)){
                $data['approval1_employee']  = $this->employee_model->get_approval1_employee($id_approval1);
            }else{
                $data['approval1_employee']  = $this->employee_model->get_approval_from_tbl_user($id_approval1);
            }

            if($this->employee_model->get_approval2_employee($id_approval2)){
                $data['approval2_employee']  = $this->employee_model->get_approval2_employee($id_approval2);
            }else{
                $data['approval2_employee']  = $this->employee_model->get_approval_from_tbl_user($id_approval2);
            }
            
            // $data['tbl_user']           = $this->employee_model->get_tbl_user();

            // for($i=0; $i<count($data['tbl_user']); $i++){
            //     array_push($data['approval1_employee'], $data['tbl_user'][$i]);
            //     array_push($data['approval2_employee'], $data['tbl_user'][$i]);
            // }

            if (empty($data['employee_info'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/employee/add_employee');
            }
        }
            
        

        // retrive all data from department table
        $this->employee_model->_table_name = "tbl_department"; //table name
        $this->employee_model->_order_by = "department_id";
        $all_dept_info = $this->employee_model->get();
        // get all department info and designation info
        foreach ($all_dept_info as $v_dept_info) {
            $data['all_department_info'][$v_dept_info->department_name] = $this->employee_model->get_add_department_by_id($v_dept_info->department_id);
        }
        // retrive country
        $this->employee_model->_table_name = "countries"; //table name
        $this->employee_model->_order_by = "countryName";
        $data['all_country'] = $this->employee_model->get();

        // retrive department
        $this->employee_model->_table_name = "tbl_department"; //table name
        $this->employee_model->_order_by = "department_id";
        $data['all_department'] = $this->employee_model->get();

        // retrive level
        $this->employee_model->_table_name  = "tbl_level_jabatan"; //table name
        $this->employee_model->_order_by    = "level_id";
        $data['all_level'] = $this->employee_model->get();

        // retrive penempatan
        $this->employee_model->_table_name  = "tbl_penempatan"; //table name
        $this->employee_model->_order_by    = "lokasi_id";
        $data['all_lokasi'] = $this->employee_model->get();

        // $data['all_level'] = $this->employee_model->get_level_employee();

        // retrive employee
        //$this->employee_model->_table_name = "tbl_employee"; //table name
        //$this->employee_model->_order_by = "employee_id";
        $data['all_employee'] = $this->employee_model->all_approval_info();
        $data['approval_employee']  = $this->employee_model->get_approval_employee();
        $data['tbl_user']           = $this->employee_model->get_tbl_user();

        for($i=0; $i<count($data['tbl_user']); $i++){
            array_push($data['approval_employee'], $data['tbl_user'][$i]);
        }

        // echo '<pre>';
        // print_r($data['approval_employee']);
        // echo '</pre>';
        // die;

        //get HR & GA List
        $data['hrga_list'] = $this->employee_model->get_hrga_list();

        //get HR & GA List
        $data['bod_list'] = $this->employee_model->get_bod_list();  
        
        for($i=0; $i<count($data['tbl_user']); $i++){
            array_push($data['bod_list'], $data['tbl_user'][$i]);
        }

        // retrive company
        $this->employee_model->_table_name = "company"; //table name
        $this->employee_model->_order_by   = "id";
        $data['all_company'] = $this->employee_model->get();

        //page load
        $data['subview'] = $this->load->view('admin/employee/add_employee', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_employee($id = NULL) {
        // $this->output->enable_profiler(TRUE);
        // **** Employee Personal Details,Contact Details and Official Status Save And Update Start *** 
        //input post
        $data = $this->employee_model->array_from_post(array('first_name', 'last_name', 'date_of_birth', 'gender', 'maratial_status', 'religion', 'mother_name', 'nationality',
            'personal_id', 'present_address', 'city', 'country_id', 'mobile', 'phone', 'email', 'employment_id', 'department_id', 'designations_id', 'joining_date', 'employment_status',
            'end_of_contract_date', 'status_pajak', 'npwp', 'level', 'resign_date', 'resign_status', 'finger_id', 'penempatan', 'email_2', 'company_id'));
		/*
		$where_ess_setup 					= array('employee_id' => $id);
		//update tabel ess setup
		$this->employee_model->set_action($where_ess_setup, $ess_setup, 'ess_setup'); // update tbl ess setup
		*/
		
		//image upload
        if (!empty($_FILES['photo']['name'])) {
            $old_path = $this->input->post('old_path');
            if ($old_path) {
                unlink($old_path);
            }

            $val = $this->employee_model->uploadImage('photo');
            $val == TRUE || redirect('admin/employee/add_employee');
            $data['photo'] = $val['path'];
            $data['photo_a_path'] = $val['fullPath'];
        }

        // ************* Save into Employee Table 
        $this->employee_model->_table_name = "tbl_employee"; // table name
        $this->employee_model->_primary_key = "employee_id"; // $id
        if (!empty($id)) {
            $employee_id = $id;
            $data['status'] = $this->input->post('status', TRUE);
            $this->employee_model->save($data, $id);
        } else {
            $data['status'] = 1;
            $employee_id = $this->employee_model->save($data);
        }
        // save into tbl employee login 
        $this->employee_model->_table_name = "tbl_employee_login"; // table name
        $this->employee_model->_primary_key = "employee_login_id"; // $id
        // check employee login details exsist or not 
        // if existing do not save 
        // else save the login details
        $check_existing_data 	= $this->employee_model->check_by(array('employee_id' => $employee_id), 'tbl_employee_login');
        $ldata['employee_id'] 	= $employee_id;
        $ldata['user_name'] 	= $data['employment_id'];
        $ldata['password'] 		= $this->hash('qwerty123');
        $ldata['activate'] 		= $data['status'];

        if (!empty($check_existing_data)) {
            $this->employee_model->save($ldata, $check_existing_data->employee_login_id);
        } else {
            $this->employee_model->save($ldata);
        }
		
		$essdata = $this->employee_model->array_from_post(array('overtime_application', 'permit_application', 'leave_application', 'redeem_point', 'perdin_application'));
		
		// save into tbl ess setup 
        $this->employee_model->_table_name 	= "tbl_employee_ess_setup"; // table name
        $this->employee_model->_primary_key = "ess_id"; // $id
		
		$essdata['employee_id']				= $employee_id;	
		
		// $ess_id = $this->input->post('ess_id', TRUE);
        $check_existing_ess_data_by_emp_id	    = $this->employee_model->check_by(array('employee_id' => $employee_id), 'tbl_employee_ess_setup');
        if (!empty($check_existing_ess_data_by_emp_id)) {
            // $this->employee_model->save($essdata, $employee_id);
            //update tbl_employee_ess_setup
            $where = array('employee_id' => $employee_id);
            $this->employee_model->set_action($where, $essdata, 'tbl_employee_ess_setup');
        } else {
            $this->employee_model->save($essdata);
        }
		
 
        // ** Employee Matrix Approval Save & Update data  **
        $matrix_data = $this->employee_model->array_from_post(array('direct_leader_id', 
                                                                    'direct_leader_id_2',
                                                                    'direct_leader_gender',
                                                                    'direct_leader_gender_2',
                                                                    'direct_leader_name', 
                                                                    'direct_leader_email', 
                                                                    'direct_leader_name_2',
                                                                    'direct_leader_email_2',
                                                                    'nama_pic_hr', 
                                                                    'email_hr_pic', 
                                                                    'hybris_name',
                                                                    'email_hybris'
                                                                ));
        $matrix_data['employee_id']         = $employee_id;
        $matrix_data['employment_id']       = $data['employment_id'];
        $matrix_data['first_name']          = $data['first_name'];
        $matrix_data['last_name']           = $data['last_name'];
        // print_r($matrix_data);
        // die;        

        $this->employee_model->_table_name  = "tbl_employee_matrix_approval"; // table name
        $this->employee_model->_primary_key = "matrix_approval_id"; // $id

        $check_existing_matrix_approval	    = $this->employee_model->check_by(array('employee_id' => $employee_id), 'tbl_employee_matrix_approval');
        if (!empty($check_existing_matrix_approval)) {
            // $this->employee_model->save($matrix_data, $employee_id);
             //update tbl_employee_matrix_approval
             $where = array('employee_id' => $employee_id);
             $this->employee_model->set_action($where, $matrix_data, 'tbl_employee_matrix_approval');
        } else {
            $this->employee_model->save($matrix_data);
        }


        // ** Employee Point Save & Update data  **
        $point_data                        = $this->employee_model->array_from_post(array('saldo_point'));
        $point_data['employee_id']         = $employee_id;
        $point_data['employment_id']       = $data['employment_id'];
        $point_data['expired']             = date('Y-m-d');
        $point_data['date_reload']         = date('Y-m-d');
        // $point_data['sisa_point_bln_lalu'] = 0;
        // $point_data['saldo_point']         = $this->input->post('saldo_point');
        $point_data['last_updated']        = date('Y-m-d H:i:s');
        $point_data['updated_by']          = $this->session->userdata('full_name');

        $this->employee_model->_table_name = "tbl_employee_point"; // table name
        $this->employee_model->_primary_key = "employee_point_id"; // $id
        // print_r($point_data);
        // die;

        $check_existing_data_point 	       = $this->employee_model->check_by(array('employee_id' => $employee_id), 'tbl_employee_point');
        if ($check_existing_data_point) {
            // $this->employee_model->save($point_data, $check_existing_data_point->employee_id);
            //update tbl_employee_point
            $where = array('employee_id' => $employee_id);
            $this->employee_model->set_action($where, $point_data, 'tbl_employee_point');
        } else {
            $this->employee_model->save($point_data);
        }


        // ** Employee Cuti Save & Update data  ** 
        $cuti_data                        = $this->employee_model->array_from_post(array('saldo_cuti'));
        $cuti_data['employee_id']         = $employee_id;
        $cuti_data['employment_id']       = $data['employment_id'];
        $cuti_data['expired']             = date('Y-m-d');
        $cuti_data['date_reload']         = date('Y-m-d');
        // $cuti_data['sisa_cuti_thn_lalu']  = 0;
        $cuti_data['saldo_cuti']          = $this->input->post('saldo_cuti');
        $cuti_data['last_updated']        = date('Y-m-d H:i:s');
        $cuti_data['updated_by']          = $this->session->userdata('full_name');

        $this->employee_model->_table_name = "tbl_saldo_cuti"; // table name
        $this->employee_model->_primary_key = "saldo_cuti_id"; // $id
      
        $check_existing_data_cuti 	       = $this->employee_model->check_by(array('employee_id' => $employee_id), 'tbl_saldo_cuti');
        // print_r($cuti_data);
        // die;
       
        if (!empty($check_existing_data_cuti)) {
            // $this->employee_model->save($cuti_data, $employee_id);
            //update tbl_saldo_cuti
            $where = array('employee_id' => $employee_id);
            $this->employee_model->set_action($where, $cuti_data, 'tbl_saldo_cuti');

        } else {
            $this->employee_model->save($cuti_data);
        }
		
        // 
        // **** Employee Personal Details,Contact Details and Official Status Save And Update End *** 
        // ** Employee Bank Information Save and Update Start  **
        $bank_data = $this->employee_model->array_from_post(array('bank_name', 'branch_name', 'account_name', 'account_number'));
        $bank_data['employee_id'] = $employee_id;
        $this->employee_model->_table_name = "tbl_employee_bank"; // table name
        $this->employee_model->_primary_key = "employee_bank_id"; // $id

        $employee_bank_id = $this->input->post('employee_bank_id', TRUE);
        if (!empty($employee_bank_id)) {
            $this->employee_model->save($bank_data, $employee_bank_id);
        } else {
            $this->employee_model->save($bank_data);
        }
        // * Employee Bank Information Save and Update End   *
        // ** Employee Document Information Save and Update Start  **
        // Resume File upload
        if (!empty($_FILES['resume']['name'])) {
            $old_path = $this->input->post('resume_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_model->uploadFile('resume');
            $val == TRUE || redirect('admin/employee/add_employee');
            $document_data['resume_filename'] = $val['fileName'];
            $document_data['resume'] = $val['path'];
            $document_data['resume_path'] = $val['fullPath'];
        }

        // offer_letter File upload
        if (!empty($_FILES['offer_letter']['name'])) {
            $old_path = $this->input->post('offer_letter_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_model->uploadFile('offer_letter');
            $val == TRUE || redirect('admin/employee/add_employee');
            $document_data['offer_letter_filename'] = $val['fileName'];
            $document_data['offer_letter'] = $val['path'];
            $document_data['offer_letter_path'] = $val['fullPath'];
        }
        // joining_letter File upload
        if (!empty($_FILES['joining_letter']['name'])) {
            $old_path = $this->input->post('joining_letter_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_model->uploadFile('joining_letter');
            $val == TRUE || redirect('admin/employee/add_employee');
            $document_data['joining_letter_filename'] = $val['fileName'];
            $document_data['joining_letter'] = $val['path'];
            $document_data['joining_letter_path'] = $val['fullPath'];
        }

        // contract_paper File upload
        if (!empty($_FILES['contract_paper']['name'])) {
            $old_path = $this->input->post('contract_paper_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_model->uploadFile('contract_paper');
            $val == TRUE || redirect('admin/employee/add_employee');
            $document_data['contract_paper_filename'] = $val['fileName'];
            $document_data['contract_paper'] = $val['path'];
            $document_data['contract_paper_path'] = $val['fullPath'];
        }
        // id_proff File upload
        if (!empty($_FILES['id_proff']['name'])) {
            $old_path = $this->input->post('id_proff_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_model->uploadFile('id_proff');
            $val == TRUE || redirect('admin/employee/add_employee');
            $document_data['id_proff_filename'] = $val['fileName'];
            $document_data['id_proff'] = $val['path'];
            $document_data['id_proff_path'] = $val['fullPath'];
        }
        // id_proff File upload
        if (!empty($_FILES['other_document']['name'])) {
            $old_path = $this->input->post('other_document_path');
            if ($old_path) {
                unlink($old_path);
            }
            $val = $this->employee_model->uploadFile('other_document');
            $val == TRUE || redirect('admin/employee/add_employee');
            $document_data['other_document_filename'] = $val['fileName'];
            $document_data['other_document'] = $val['path'];
            $document_data['other_document_path'] = $val['fullPath'];
        } else {
            
        }

        $document_data['employee_id'] = $employee_id;

        $this->employee_model->_table_name = "tbl_employee_document"; // table name
        $this->employee_model->_primary_key = "document_id"; // $id
        $document_id = $this->input->post('document_id', TRUE);
        if (!empty($document_id)) {
            $this->employee_model->save($document_data, $document_id);
        } else {
            $this->employee_model->save($document_data);
        }
        // ***Employee Document Information Save and Update End   ***
        // messages for user
        $type = "success";
        $message = "Employee Information Successfully Saved!";
        set_message($type, $message);
        redirect('admin/employee/employee_list'); //redirect page
    }

    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }

    public function delete_employee($id, $bank_id, $doc_id, $ess_id, $matrix_approval_id=NULL) {
        // ************* Delete into Employee Table 
        $this->employee_model->_table_name = "tbl_employee"; // table name
        $this->employee_model->_primary_key = "employee_id"; // $id
        $this->employee_model->delete($id);
        // delete into tbl bank 
        $this->employee_model->_table_name = "tbl_employee_bank"; // table name
        $this->employee_model->_primary_key = "employee_bank_id"; // $id
        $this->employee_model->delete($bank_id);

        // delete into tbl employee document
        $this->employee_model->_table_name = "tbl_employee_document"; // table name
        $this->employee_model->_primary_key = "document_id"; // $id
        $this->employee_model->delete($doc_id);
		
		// delete into tbl ess setup
        $this->employee_model->_table_name = "tbl_employee_ess_setup"; // table name
        $this->employee_model->_primary_key = "ess_id"; // $id
        $this->employee_model->delete($ess_id);
		
		// delete into tbl ess setup
        $this->employee_model->_table_name = "tbl_employee_matrix_approval"; // table name
        $this->employee_model->_primary_key = "matrix_approval_id"; // $id
        $this->employee_model->delete($id);

        // delete into tbl employee login
        $this->employee_model->_table_name = "tbl_employee_login"; // table name
        $this->employee_model->_order_by = "employee_id"; // table name        
        $this->employee_model->_primary_key = "employee_login_id"; // $id
        $login_id = $this->employee_model->get_by(array('employee_id'=> $id), TRUE);
        
        $this->employee_model->delete($login_id->employee_login_id);

        // delete into tbl employee point
        $this->employee_model->_table_name = "tbl_employee_point"; // table name   
        $this->employee_model->_primary_key = "id"; // $id
        $point_id = $this->employee_model->get_by(array('employee_id'=> $id), TRUE);
        
        $this->employee_model->delete($point_id->employee_id);

        // delete into tbl saldo cuti
        $this->employee_model->_table_name = "tbl_saldo_cuti"; // table name   
        $this->employee_model->_primary_key = "id"; // $id
        $cuti_id = $this->employee_model->get_by(array('employee_id'=> $id), TRUE);
        
        $this->employee_model->delete($cuti_id->employee_id);

        // messages for user
        $type = "success";
        $message = "Employee Information Successfully Delete!";
        set_message($type, $message);
        redirect('admin/employee/employee_list'); //redirect page
    }

    public function employee_list($id = NULL) {
		//DEBUG
        // $this->output->enable_profiler(TRUE);
        $data['title'] = "Employee List";
        $data['all_employee_info'] = $this->employee_model->all_emplyee_info();
        $data['subview'] = $this->load->view('admin/employee/employee_list', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function employee_saldo_cuti($id = NULL) {
		//DEBUG
        // $this->output->enable_profiler(TRUE);
        $data['title'] = "Employee Saldo Cuti";
        $data['all_employee_info'] = $this->employee_model->all_emplyee_info();
        $data['subview'] = $this->load->view('admin/employee/employee_saldo_cuti', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function employee_list_pdf() {
        $data['title'] = "Employee List";
        $data['all_employee_info'] = $this->employee_model->all_emplyee_info();
        $this->load->helper('dompdf');
        $view_file = $this->load->view('admin/employee/employee_list_pdf', $data, true);
        pdf_create($view_file, 'Employee List');
    }

    public function view_employee($id = NULL) {
        $data['title'] = "View Employee";
        if (!empty($id)) {// retrive data from db by id
            $data['employee_info'] = $this->employee_model->all_emplyee_info($id);
            if (empty($data['employee_info'])) {
                $type = "error";
                $message = "No Record Found";
                set_message($type, $message);
                redirect('admin/employee/employee_list');
            }
        }
        $data['subview'] = $this->load->view('admin/employee/view_employee', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function employee_award($id = NULL, $designations_id = NULL) {
        $data['title'] = "Employee List";
        // retrive all data from department table
        $this->employee_model->_table_name = "tbl_department"; //table name
        $this->employee_model->_order_by = "department_id";
        $all_dept_info = $this->employee_model->get();
        // get all department info and designation info
        foreach ($all_dept_info as $v_dept_info) {
            $data['all_department_info'][$v_dept_info->department_name] = $this->employee_model->get_add_department_by_id($v_dept_info->department_id);
        }
        /// edit and update get employee award info
        if (!empty($id)) {
            $data['award_info'] = $this->employee_model->get_employee_award_by_id($id);

            // get all employee info by designation id
            $this->employee_model->_table_name = 'tbl_employee';
            $this->employee_model->_order_by = 'designations_id';
            $data['employee_info'] = $this->employee_model->get_by(array('designations_id' => $designations_id), FALSE);
        }
        // get all_employee_award_info
        $data['all_employee_award_info'] = $this->employee_model->get_employee_award_by_id();

        $data['subview'] = $this->load->view('admin/employee/employee_award', $data, TRUE);
        $this->load->view('admin/_layout_main', $data);
    }

    public function save_employee_award($id = NULL) {
        $data = $this->employee_model->array_from_post(array('award_name', 'employee_id', 'gift_item', 'award_amount', 'award_date'));

        $this->employee_model->_table_name = "tbl_employee_award"; // table name
        $this->employee_model->_primary_key = "employee_award_id"; // $id
        $this->employee_model->save($data, $id);
        // messages for user
        $type = "success";
        $message = "Employee Award Information Successfully Saved!";
        set_message($type, $message);
        redirect('admin/employee/employee_award'); //redirect page
    }

    public function delete_employee_award($id = NULL) {

        $this->employee_model->_table_name = "tbl_employee_award"; // table name
        $this->employee_model->_primary_key = "employee_award_id"; // $id
        $this->employee_model->delete($id); // delete 
        // messages for user
        $type = "success";
        $message = "Employee Award Information Successfully Delete !";
        set_message($type, $message);
        redirect('admin/employee/employee_award'); //redirect page
    }

    public function make_pdf($employee_id) {
        $data['title'] = "Employee List";
        $data['employee_info'] = $this->employee_model->all_emplyee_info($employee_id);
        $this->load->helper('dompdf');
        $view_file = $this->load->view('admin/employee/employee_view_pdf', $data, true);
        pdf_create($view_file, $data['employee_info']->first_name);
    }

    public function make_pdf_redeem_point($doc_number) {
        $data['title'] = "Redeem Point";

        $data['redeem_info']        = $this->application_model->get_emp_redeem_info($doc_number);
        $data['product_list_info']  = $this->application_model->get_emp_redeem_info_rows($doc_number);       
        $employee_id                = $this->application_model->get_emp_redeem_info($doc_number)->employee_id;
        $data['saldo_point']        = $this->application_model->get_saldo_point($employee_id);

        $this->load->helper('dompdf');
        $view_file = $this->load->view('admin/employee/employee_redeem_point_pdf', $data, true);
        pdf_create($view_file, $data['redeem_info']->doc_number);
    }

}
