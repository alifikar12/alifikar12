/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.1.41 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `tbl_menu` (
	`��` int (11),
	`@�` varchar (300),
	`��` varchar (300),
	`��` varchar (300),
	`�` varchar (300),
	``�` int (11),
	`��` int (11),
	`` int (11),
	`H�` text ,
	`��` text ,
	`��` text ,
	` �` varchar (300)
); 
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('1','Dashboard','Administrator','admin/dashboard','fa fa-dashboard','1','0','0','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('2','Settings','Administrator','#','fa fa-cogs','2','0','2','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('3','General Settings','Administrator','admin/settings/general_settings','fa fa-dashboard','3','2','1','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('4','Set Working Days','Administrator','admin/settings/set_working_days','fa fa-calendar','4','2','2','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('5','Holiday List','Administrator','admin/settings/holiday_list','entypo-list','5','2','3','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('6','Leave Category','Administrator','admin/settings/leave_category','fa fa-dedent','6','2','4','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('7','Notification Settings','Administrator','admin/settings/notification_settings','fa fa-bell-o','7','2','5','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('8','Department','Administrator','#','entypo-list-add','8','0','3','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('9','Add Department','Administrator','admin/department/add_department','entypo-list-add','9','8','1','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('10','Department List','Administrator','admin/department/department_list','entypo-list','10','8','2','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('11','Employee','Administrator','#','fa fa-user','11','0','4','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('12','Add Employee','Administrator','admin/employee/add_employee','entypo-user-add','12','11','1','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('13','Employee List','Administrator','admin/employee/employee_list','entypo-users','13','11','2','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('14','Employee Award','Administrator','admin/employee/employee_award','fa fa-trophy','14','11','3','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('15','Attendance','Administrator','#','fa fa-file-text','15','0','5','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('16','Manage Attendance','Administrator','admin/attendance/manage_attendance','fa fa-file-text-o','16','15','1','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('17','Attendance Report','Administrator','admin/attendance/attendance_report','fa fa-file-text','17','15','2','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('18','Application List','Administrator','admin/application_list','fa fa-rocket','18','0','6','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('19','Payroll Management','Administrator','#','fa fa-usd','19','0','7','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('20','Manage Salary Details','Administrator','admin/payroll/manage_salary_details','fa fa-usd','20','19','1','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('21','Employee Salary List','Administrator','admin/payroll/employee_salary_list','entypo-users','21','19','2','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('22','Make Payment','Administrator','admin/payroll/make_payment','fa fa-tasks','22','19','3','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('23','Generate Payslip','Administrator','admin/payroll/generate_payslip','fa fa-list-ul','23','19','4','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('24','Expense Management','Administrator','#','fa fa-money','24','0','8','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('26','Add Expense','Administrator','admin/expense/add_expense','fa fa-delicious','26','24','2','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('27','Expense Report','Administrator','admin/expense/expense_report','fa fa-file-o','27','24','3','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('28','Notice Board','Administrator','#','fa fa-list-alt','28','0','9','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('29','Add Notice','Administrator','admin/notice/add_notice','entypo-docs','29','28','1','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('30','Manage Notice','Administrator','admin/notice/manage_notice','entypo-doc','30','28','2','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('31','Mailbox','Administrator','#','fa fa-credit-card','31','0','3','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('32','Inbox','Administrator','admin/mailbox/inbox','fa fa-inbox','32','31','1','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('33','Sent','Administrator','admin/mailbox/sent','fa fa-paper-plane','33','31','2','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('34','Personal Event','Administrator','admin/settings/view_event','entypo-newspaper','34','2','6','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('35','Language Settings','Administrator','admin/settings/language_settings','fa fa-language','35','2','7','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('36','Database Backup','Administrator','admin/settings/database_backup','fa fa-database','36','0','20','','','','');
insert into `tbl_menu` (`menu_id`, `English`, `role`, `link`, `icon`, `parent_id`, `parent`, `sort`, `Spanish`, `French`, `Arabic`, `label`) values('37','Manage Menu','Administrator','admin/navigation','fa fa-list','37','0','21','','','','');
