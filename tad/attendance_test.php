<?php
	require 'lib/TADFactory.php';
    require 'lib/TAD.php';
	require 'lib/TADResponse.php';
	require 'lib/Providers/TADSoap.php';
	require 'lib/Providers/TADZKLib.php';
	require 'lib/Exceptions/ConnectionError.php';
	require 'lib/Exceptions/FilterArgumentError.php';
	require 'lib/Exceptions/UnrecognizedArgument.php';
	require 'lib/Exceptions/UnrecognizedCommand.php';

	use TADPHP\TADFactory;
	use TADPHP\TAD;

	$options = [
		'ip' => '192.168.250.5',   // '169.254.0.1' by default (totally useless!!!).
		'internal_id' => 1,    // 1 by default.
		'com_key' => 0,        // 0 by default.
		'description' => 'N/A', // 'N/A' by default.
		'soap_port' => 80,     // 80 by default,
		'udp_port' => 4370,      // 4370 by default.
		'encoding' => 'utf-8'    // iso8859-1 by default.
	  ];
	  
	  $tad_factory = new TADFactory($options);
	  $tad = $tad_factory->get_instance();  

	  $dt = $tad->get_date();
	  $att_logs = $tad->get_att_log();
	  $all_user_info = $tad->get_all_user_info();

	  $filtered_att_logs = $att_logs->filter_by_date(
		['start' => '2019-02-01','end' => '2019-03-04']
	);

	  $json_all_user_info = $all_user_info->to_array();
	  $json_att_logs = $filtered_att_logs->to_array();

	//   echo '<pre>';
	//   var_dump($json_att_logs);
	//   echo '</pre>';
	//   die;

	  
	  	$total_logs = count($json_att_logs['Row']);
	    //echo $total_r;
		$data_att = array();
		$awal_cekin 	= strtotime('06:30:00');
		$terakhir_cekin = strtotime('11:30:00');
		$awal_cekout 	= strtotime('14:30:00');
		$terakhir_cekout = strtotime('23:30:00');

		//TODO Insert data finger attendance logs
		for($i=0; $i<$total_logs; $i++){
				
			$date_i = strtotime($json_att_logs['Row'][$i]['DateTime']);		
			$pin_i = $json_att_logs['Row'][$i]['PIN'];

			$date = '';
			$pin = '';
			$cekin = '';
			$cekout = '';
			//$cek_exist_key = array(); 
			for($j=0; $j<count($json_att_logs['Row']); $j++){

				$jam_tap = strtotime(date('H:i:s', strtotime($json_att_logs['Row'][$j]['DateTime'])));
				$date_j = date('Y-m-d', strtotime($json_att_logs['Row'][$j]['DateTime']));
				$pin_j = $json_att_logs['Row'][$j]['PIN'];
				
				if($date_i==strtotime($json_att_logs['Row'][$j]['DateTime']) && $pin_i==$pin_j){
					
						if($jam_tap > $awal_cekin && $jam_tap < $terakhir_cekin){	
							$cekin = date('H:i:s', strtotime($json_att_logs['Row'][$j]['DateTime']));	
						}

						if($jam_tap > $awal_cekout && $jam_tap < $terakhir_cekout){
							$cekout = date('H:i:s', strtotime($json_att_logs['Row'][$j]['DateTime']));
						}	
					
				}		
					
			}

			//if (!in_array(date('Y-m-d', $date_i), $data_att[$i]) && !in_array($pin_i, $data_att[$i])) {
				$data_att[$i]["Date"] = date('Y-m-d',$date_i);
				$data_att[$i]["PIN"] = $pin_i;
				$data_att[$i]["CekIn"] = $cekin;
				$data_att[$i]["CekOut"] = $cekout;
			//}	
				
		}

		

	// echo '<pre>';	
	// var_dump($data_att);
	// echo '</pre>';
	// die;

	$data_result = array();
	$total_data_att = count($data_att);
	for($x=0; $x<$total_data_att; $x++){
		$date_x = $data_att[$x]['Date'];
		$pin_x = $data_att[$x]['PIN'];
		$cekin_x = $data_att[$x]['CekIn'];
		for($y=0; $y<$total_data_att; $y++){
			if($date_x==$data_att[$y]['Date'] && $pin_x==$data_att[$y]['PIN']){
				if($jam_tap > $awal_cekout && $jam_tap < $terakhir_cekout){
					$cekoutx = $data_att[$y]['CekOut'];
				}	
			}
		}	
		//if (!in_array($pin_x, $data_result)) {
			$data_result[$x]["Date"] = $data_att[$x]['Date'];
			$data_result[$x]["PIN"] = $data_att[$x]['PIN'];
			$data_result[$x]["CekIn"] = $data_att[$x]['CekIn'];
			$data_result[$x]["CekOut"] = $cekoutx;
		//}
	}

	// function unique_key($array,$keyname){

	// 	$new_array = array();
	// 	foreach($array as $key=>$value){
	   
	// 	  if(!isset($new_array[$value[$keyname]])){
	// 		$new_array[$value[$keyname]] = $value;
	// 	  }
	   
	// 	}
	// 	$new_array = array_values($new_array);
	// 	return $new_array;
	//    }
	   
	// $student_unique_arr = unique_key($data_result,'PIN');

	// echo '<pre>';	
	// var_dump($data_result);
	// echo '</pre>';
	// die;

	// $con = new mysqli("localhost","root","@dminpkg33","pmki_hris");
 
	// // Check connection
	// if ($con->connect_errno) {
	// 	echo 'Connect failed: ' . $con->connect_error;
	// 	exit();
	// }

	$total_row = count($data_result);
	echo $total_row;  
	die;
	//TODO Insert data finger attendance logs
	  for($i=0; $i<$total_r; $i++){
		  
			  $PIN = $data_result[$i]['PIN'];
			  $Date = $data_result[$i]['Date'];
			  $CekIn = $data_result[$i]['CekIn'];
			  $CekOut = $data_result[$i]['CekOut'];
			  
			  $query = "INSERT INTO `pmki_hris`.`tbl_attendance2`
							(`PIN`,
							`Date`,
							`CekIn`,
							`CekOut`)
								VALUES ('$PIN',
										'$Date',
										'$CekIn',
										'$CekOut');";

		  mysqli_query($con, $query);
	  }

	
	  

		// $records = array(
		// 	"0" => array("Parvez", "Alam", "123"),
		// 	"1" => array("Affan", "Alam", "344"),
		// 	"2" => array("Ajay", "Sharma", "22")
		// );

		// echo '<pre>';
		// var_dump($records);
		// echo '</pre>';
		// $total_r = count($json_all_user_info['Row']);
		// // echo $total_r;

		// //TODO Insert data finger user info	
		// for($i=0; $i<$total_r; $i++){
			
		// 		$pin = $json_all_user_info['Row'][$i]['PIN'];
		// 		$name = $json_all_user_info['Row'][$i]['Name'];
		// 		$password = $json_all_user_info['Row'][$i]['Password'];
		// 		$group = $json_all_user_info['Row'][$i]['Group'];
		// 		$privilage = $json_all_user_info['Row'][$i]['Privilage'];
		// 		$card = $json_all_user_info['Row'][$i]['Card'];
		// 		$pin2 = $json_all_user_info['Row'][$i]['PIN2'];
		// 		$tz1 = $json_all_user_info['Row'][$i]['TZ1'];
		// 		$tz2 = $json_all_user_info['Row'][$i]['TZ2'];
		// 		$tz3 = $json_all_user_info['Row'][$i]['TZ3'];

		// 		$query = "INSERT INTO `pmki_hris`.`finger_user_info`
	    //         (`PIN`, `Name`, `Password`, `Group`, `Privilege`, `Card`, `PIN2`, `TZ1`, `TZ2`, `TZ3`)
		// 		VALUES ('$pin', '$name', '$password', '$group', '$privilage', '$card', '$pin2', '$tz1', '$tz2', '$tz3')";

		// 	mysqli_query($con, $query);

		// }

		// die;


	  //die;	
	  //echo 'Name :' .$json_all_user_info['Row'][0]['Name'];

	
		//else{
			//echo 'success connect';
			//die;
		//}
		//$records = $json_all_user_info['Row'];

		// if(is_array($records)){
		// 	foreach ($records as $row){
		// 		// var_dump($row);
		// 		// die;
		// 		//$pin = mysql_real_escape_string($json_all_user_info[$row]['PIN']);
		// 		//$name = mysql_real_escape_string($json_all_user_info[$row]['Name']);
		// 		//$password = mysql_real_escape_string($json_all_user_info[$row]['Password']);
		// 		//$group = mysql_real_escape_string($json_all_user_info[$row]['Group']);
		// 		//$privilage = mysql_real_escape_string($json_all_user_info[$row]['Privilege']);
		// 		//$card = mysql_real_escape_string($json_all_user_info[$row]['Card']);
		// 		//$pin2 = mysql_real_escape_string($json_all_user_info[$row]['PIN2']);
		// 		//$tz1 = mysql_real_escape_string($json_all_user_info[$row]['TZ1']);
		// 		//$tz2 = mysql_real_escape_string($json_all_user_info[$row]['TZ2']);
		// 		//$tz3 = mysql_real_escape_string($json_all_user_info[$row]['TZ3']);
						
		// 		//echo $records[$row][0];
		// 		//$name = $records[$row][0];

		// 		//$query = "INSERT INTO `pmki_hris`.`finger_user_info`
	    //         //(`PIN`, `Name`, `Password`, `Group`, `Privilege`, `Card`, `PIN2`, `TZ1`, `TZ2`, `TZ3`)
		// 		//VALUES ('$pin', '$name', '$password', '$group', '$privilage', '$card', '$pin2', '$tz1', '$tz2', '$tz3')";

		// 	//mysqli_query($con, $query);

		// 	}
		// 	//echo $name;
			
		// }

	  

	  ?>


