##QUERY IMPORT CSV DEPARTMENT TO MYSQL TABLE
LOAD DATA LOCAL INFILE 'C:\\xampp\\htdocs\\PMKI-HRIS\\template data\\department.csv'
INTO TABLE tbl_department     
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(department_id,department_name);

##QUERY IMPORT CSV DESIGNATIONS TO MYSQL TABLE
LOAD DATA LOCAL INFILE 'C:\\xampp\\htdocs\\PMKI-HRIS\\template data\\designations.csv'
INTO TABLE tbl_designations     
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(designations_id, department_id, designations);

##QUERY IMPORT CSV EMPLOYEE TO MYSQL TABLE
LOAD DATA LOCAL INFILE 'C:\\xampp\\htdocs\\PMKI-HRIS\\template data\\employee.csv'
INTO TABLE tbl_employee     
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(employee_id, employment_id, first_name, last_name, date_of_birth, gender, maratial_status, father_name, nationality, passport_number, photo, photo_a_path, present_address, city, country_id, mobile, phone, email, email_2,
        designations_id, joining_date, STATUS, direct_leader_id, direct_leader_name, direct_leader_name_2, finger_id, saldo_cuti, resign_date, resign_status, job_position, personal_id, personal_email, religion, status_pajak, mother_name, LEVEL);


##DELETE FROM tbl_employee

SELECT employee_id FROM tbl_employee

##SET AUTOINCREMENT tbl_employee_document TO 1
ALTER TABLE tbl_employee_document AUTO_INCREMENT = 1
##INSERT employee_id FROM tbl_employee to tbl_employee_document
INSERT INTO tbl_employee_document (employee_id)
SELECT employee_id FROM tbl_employee;

##SET AUTOINCREMENT tbl_employee_bank TO 1
ALTER TABLE tbl_employee_bank AUTO_INCREMENT = 1;
##INSERT employee_id FROM tbl_employee to tbl_employee_document
INSERT INTO tbl_employee_bank (employee_id)
SELECT employee_id FROM tbl_employee;

##SET AUTOINCREMENT employee_login TO 1
ALTER TABLE tbl_employee_login AUTO_INCREMENT = 1;
##INSERT employee_id FROM tbl_employee to tbl_employee_document
INSERT INTO tbl_employee_login (employee_id, user_name, `password`, activate)
SELECT employee_id, employment_id, '3f6264119f9edfe2faa6e04be142c6c62dffd51cdb1710fceed0dea0b3cb03aa7655a0cac3fa99c994743cfd9b2621e61752c6c2da91053e6799e5f708c883db', `status` FROM tbl_employee;

##insert  into `tbl_application_list`(`application_list_id`,`employee_id`,`employment_id`,`finger_id`,`leave_category_id`,`reason`,`leave_start_date`,`leave_end_date`,`application_status`,`view_status`,`notify_me`,`application_date`,`approve1`,`approve2`,`date_approve1`,`date_approve2`,`unapprove1`,`unapprove2`,`date_unapprove1`,`date_unapprove2`,`leave_status`) values (1,2,'333','133',4,'test','2019-03-14','2019-03-15',0,1,1,'2019-03-09 19:37:39','Doni Saiful Bahri','','2019-03-03','0000-00-00','','Nova','0000-00-00','2019-03-03','cancel'),(2,2,'333','133',4,'test','2019-03-11','2019-03-11',0,1,1,'2019-03-09 17:04:02','David Wijaya','Nova','2019-03-03','2019-03-03','','','0000-00-00','0000-00-00','fully approved'),(3,2,'333','133',4,'test','2019-03-11','2019-03-12',0,1,1,'2019-03-09 20:02:14','David Wijaya','hrga','2019-03-03','2019-03-09','','','0000-00-00','0000-00-00','fully approved'),(4,4,'222','223',4,'sdfsdf','2019-03-12','2019-03-12',1,1,1,'2019-03-09 12:27:11','Jefferey Alexander','Nova','2019-03-03','2019-03-03','','','0000-00-00','0000-00-00','fully approved'),(5,2,'333','133',4,'','2019-03-12','2019-03-12',1,1,1,'2019-03-09 20:10:21','David Wijaya','Nova','2019-03-03','2019-03-03','','','0000-00-00','0000-00-00','fully approved'),(6,2,'333','133',4,'','2019-03-18','2019-03-18',1,1,1,'2019-03-09 20:03:31','DAVID  WIJAYA','Nova','2019-03-09','0000-00-00','','hrga','0000-00-00','2019-03-09','cancel'),(7,2,'333','133',4,'sfsd','2019-03-11','2019-03-11',1,1,1,'2019-03-09 20:09:11','DAVID  WIJAYA','hrga','2019-03-09','2019-03-09','','','0000-00-00','0000-00-00','fully approved');
##UPDATE tbl_employee SET email_hr_pic = 'admin.hrga@pmki.co.id'

